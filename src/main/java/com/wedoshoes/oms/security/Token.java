package com.wedoshoes.oms.security;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Token {
	
	private final Long userId;
	private final String userAgent;
	private final String authToken;
	private final Integer userType;
	
	
	@SuppressWarnings("unused")
	private Token() {
		this(null, null, null, null);
	}

	public Token(final Long userId, final String userAgent, final String authToken, 
			final Integer userType) {
		this.userId = userId;
		this.userAgent = userAgent;
		this.authToken = authToken;
		this.userType = userType;
	}

	@JsonProperty("user_id")
	public Long getUserId() {
		return userId;
	}

	@JsonProperty("user_agent")
	public String getUserAgent() {
		return userAgent;
	}

	@JsonProperty("auth_token")
	public String getAuthToken() {
		return authToken;
	}

	@JsonProperty("user_type")
	public Integer getUserType() {
		return userType;
	}	
}