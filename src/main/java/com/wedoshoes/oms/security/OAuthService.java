package com.wedoshoes.oms.security;


/**
 * Class that implement this interface need to implement activity based authorization.
 * @author Navrattan Yadav
 *
 */
public interface OAuthService {

	/**
	 * This function authenticate and authorize user token for given action.
	 * @param action
	 * @param token
	 * @return
	 */
	Token authorize(Long userId,String action, Token token);
	
	/**
	 * This function authenticate token.
	 * @param token
	 * @return
	 */
	Token authenticate(Long userId,Token token);
	
	Boolean authenticate(String serverId, String secret);
	
}
