package com.wedoshoes.oms.security.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.wedoshoes.oms.http.CoreServerHttpRequester;
import com.wedoshoes.oms.security.OAuthService;
import com.wedoshoes.oms.security.Token;

/**
 * 
 * @author Navrattan Yadav
 *
 */
@Service
public class OAuthServiceImpl implements OAuthService {

	private static final Logger LOGGER = LoggerFactory.getLogger(OAuthServiceImpl.class);
	private final Map<String, String> SERVERS_CREDIENTIAL = new HashMap<>();
	private final CoreServerHttpRequester requester;

	@Autowired
	public OAuthServiceImpl(final CoreServerHttpRequester requester,
			@Value("#{servers}") final Map<String, String> servers) {
		SERVERS_CREDIENTIAL.putAll(servers);
		this.requester = requester;
	}

	@Override
	public Token authorize(final Long userId, final String action, final Token token) {
		return requester.authorize(userId, action, token);
	}

	@Override
	public Token authenticate(final Long userId, final Token token) {
		return requester.authenticate(userId, token);
	}

	@Override
	public Boolean authenticate(final String serverId, final String secret) {
		if(SERVERS_CREDIENTIAL.containsKey(serverId) && 
				SERVERS_CREDIENTIAL.get(serverId).equals(secret)) {
			return Boolean.TRUE;
		}
		LOGGER.info("UnAuthorized. ServerId = {}, Secret = {}", serverId, secret);
		return Boolean.FALSE;
	}	
}
