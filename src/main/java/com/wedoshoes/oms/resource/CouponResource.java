package com.wedoshoes.oms.resource;

import static com.wedoshoes.oms.provider.filter.HeaderParam.AUTHORIZATION;
import static com.wedoshoes.oms.provider.filter.HeaderParam.USER_ID;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.wedoshoes.oms.model.Coupon;
import com.wedoshoes.oms.model.CouponSource;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
/**
 * 
 * @author Lawakush Chaudhary
 *
 */
@Path("/api/v1/coupons")
@Api(value = "Coupon", description = "Operations about Coupon",produces = "application/json")
public interface CouponResource {

	/**
	 * *******************************************************************
	 * **************************** Coupon API ****************************
	 * *******************************************************************
	 */

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation("Create")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID, required = true, dataType = "string", paramType = "header") 
	  })
	Response create(@ApiParam Coupon coupon);

	@PUT
	@Path("/{coupon_id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation("Update")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID, required = true, dataType = "string", paramType = "header") 
	  })
	Response update(@PathParam("coupon_id") Integer couponId,@ApiParam Coupon coupon);

	@GET
	@Path("/{coupon_id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("Get Coupon By Id")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID, required = true, dataType = "string", paramType = "header") 
	  })
	Response getById(@PathParam("coupon_id") Integer couponId,
			@QueryParam("summary") @DefaultValue("true") Boolean summary);

	@DELETE
	@Path("/{coupon_id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("Delete")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID, required = true, dataType = "string", paramType = "header") 
	  })
	Response delete(@PathParam("coupon_id") Integer couponId);

	@POST
	@Path("/validate")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("Validate")
	Response validate(@ApiParam Coupon coupon);

	@POST
	@Path("/apply")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("Apply")
	Response apply(@ApiParam Coupon coupon);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("Get Coupon Detail By Coupon Code")
	Response getByCode(@QueryParam("coupon_code") String couponCode,
			@QueryParam("summary") @DefaultValue("true") Boolean summary);

	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("List Coupon By User Type")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID, required = true, dataType = "string", paramType = "header") 
	  })
	Response list(@QueryParam("user_type") Byte userTypeId,
			@QueryParam("category") Integer categoryId,
			@QueryParam("service") Integer serviceId,
			@QueryParam("product") Integer productId,
			@QueryParam("offset") @DefaultValue("0") Integer offset,
			@QueryParam("limit") @DefaultValue("20") Integer limit);

	
//	@DELETE
//	@Path("/{coupon_id}/remove")
//	@Produces(MediaType.APPLICATION_JSON)
//	Response removeAppliedCoupon(@QueryParam("order_id") Long orderId, 
//			@QueryParam("item_id") Long itemId,
//			@PathParam("coupon_id") Integer discountid);
//	
	/**
	 * *******************************************************************
	 * **************************** Coupon Source API ********************
	 * *******************************************************************
	 */

	@POST
	@Path("/sources")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation("Create Coupon Source")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID, required = true, dataType = "string", paramType = "header") 
	  })
	Response addSource(@ApiParam CouponSource couponSource);

	@PUT
	@Path("/sources/{source_id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation("Update Coupon Source")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID, required = true, dataType = "string", paramType = "header") 
	  })
	Response updateSource(@PathParam("source_id") Integer sourceId,
			@ApiParam CouponSource couponSource);

	@GET
	@Path("/sources/{source_id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("get Coupon Source")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID, required = true, dataType = "string", paramType = "header") 
	  })
	Response getSource(@PathParam("source_id") Integer sourceId,
			@QueryParam("summary") @DefaultValue("true") Boolean summary);

	@DELETE
	@Path("/sources/{source_id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("Delete Coupon Source")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID, required = true, dataType = "string", paramType = "header") 
	  })
	Response deleteSource(@PathParam("source_id") Integer sourceId);

	@GET
	@Path("/sources")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("List Coupon Source By summary")
	Response list(@QueryParam("summary") @DefaultValue("true") Boolean summary);
}
