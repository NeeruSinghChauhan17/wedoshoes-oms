package com.wedoshoes.oms.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.wedoshoes.oms.model.order.Cart;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.Order;
import com.wedoshoes.oms.model.order.WedoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Path("/api/v1/carts")
@Api(value = "Cart", description = "Operations about Cart",produces = "application/json")
public interface CartResource {
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("Create Cart")
	Response create(@ApiParam Cart cart);
	
	@POST
	@Path("/{cart_id}/items")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("Add Item")
	Response addItems(@PathParam("cart_id") Long cartId,@ApiParam Cart cart);
	
	@POST
	@Path("/{cart_id}/checkout")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("Checkout Cart")
	Response checkoutCart(@PathParam("cart_id") Long cartId,@ApiParam Order order);
	
	@POST
	@Path("/{cart_id}/items/{item_id}/services")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("Add Services To Item")
	Response addItemServices(@PathParam("cart_id") Long cartId,
			@PathParam("item_id") Long itemId,@ApiParam List<WedoService> services);
	
	@DELETE
	@Path("/{cart_id}/items/{item_id}/services/{service_id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("Delete Item Services")
	Response deleteItemService(@PathParam("cart_id") Long cartId,
			@PathParam("item_id") Long itemId, @PathParam("service_id") Long serviceId);
	
	@DELETE
	@Path("/{cart_id}/items/{item_id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("Delete Item")
	Response deleteItem(@PathParam("cart_id") Long cartId,
			@PathParam("item_id") Long itemId);
	
	@PUT
	@Path("/{cart_id}/items/{item_id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("Change Item")
	Response updateItem(Item item, @PathParam("cart_id") Long cartId,
			@PathParam("item_id") Long itemId);
	
	@DELETE
	@Path("/{cart_id}/clear")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("Clear")
	Response clear(@PathParam("cart_id") Long cartId);
	
	@GET
	@Path("/{cart_id}/items")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("List Items By cart Id")
	Response listItem(@PathParam("cart_id") Long cartId);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("List Carts By User Id")
	Response listCarts(@QueryParam("user_id") Long userId,
			@QueryParam("offset") @DefaultValue("0") Integer offset,
			@QueryParam("limit") @DefaultValue("20") Integer limit);

	@GET
	@Path("/{cart_id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("Get")
	Response getCart(@PathParam("cart_id") Long cartId,
			@QueryParam("summary") @DefaultValue("true") Boolean summary);

}
