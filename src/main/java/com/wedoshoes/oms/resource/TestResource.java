package com.wedoshoes.oms.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.wedoshoes.oms.model.order.Order;

@Path("/api/v1")
public interface TestResource {

	@GET
	@Path("/wedo-services/{service_id}")
	@Produces(MediaType.APPLICATION_JSON)
	Response getServiceName(@PathParam("service_id") Integer serviceId);
	
	@GET
	@Path("/products/{product_id}")
	@Produces(MediaType.APPLICATION_JSON)
	Response getProductName(@PathParam("product_id") Integer productId);
	
	@GET
	@Path("/time-slots/{time_slot_id}")
	@Produces(MediaType.APPLICATION_JSON)
	Response getTimeSlotName(@PathParam("time_slot_id") Byte timeSlotId);

	@POST
	@Path("workshop/{workshop_id}/order/{order_id}/state/{state_id}/send/mail")
	@Produces(MediaType.APPLICATION_JSON)
	Response sendOrderMail(Order order, @PathParam("workshop_id") Byte workshopId, 
			@PathParam("order_id") Long orderId, @PathParam("state_id") Byte timeSlotId);
	
	@GET
	@Path("/template/{template_id}")
	@Produces(MediaType.APPLICATION_JSON)
	Response getTemplate(@PathParam("template_id") Byte templateId);
	
	@POST
	@Path("/workshop/{workshopId}/create_invoice")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	Response createInvoice(@PathParam("workshopId") Byte workshopId, Order order);
}
