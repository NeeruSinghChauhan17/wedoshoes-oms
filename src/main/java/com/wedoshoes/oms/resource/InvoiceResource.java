package com.wedoshoes.oms.resource;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Path("/api/v1/users/{user_id}/workshops/{workshop_id}/invoices")
@Api(value = "Invoice", description = "Operations about Invoice",produces = "application/json")
public interface InvoiceResource {

	/**
	 * *******************************************************************
	 * ********************** Invoice Related Methods ********************
	 * *******************************************************************
	 */
	
	@GET
	@Path("orders/{order_id}/create_invoice")
	@Produces("application/pdf")
	@ApiOperation("Send Invoice")
	Response sendInvoice(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@QueryParam("item_id") Long itemId);
	
	@GET
	@Path("orders/{order_id}/create_challan")
	@Produces("application/pdf")
	@ApiOperation("Send Challan")
	Response sendChallan(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@QueryParam("item_id") Long itemId);
	
	@GET
	@Path("orders/{order_id}/invoice")
	@Produces("application/pdf")
	@ApiOperation("Get Invoice")
	Response getInvoice(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@QueryParam("item_id") Long itemId);
	
	@GET
	@Path("orders/{order_id}/challan")
	@Produces("application/pdf")
	@ApiOperation("Get Challan")
	Response getChallan(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@QueryParam("item_id") Long itemId);
	

	@GET
	@Path("orders")
	@Produces("application/pdf")
	@ApiOperation("Get Invoices")
	Response getInvoices(
			@PathParam("workshop_id") Byte workshopId,
			@QueryParam("order_id") Long orderId,
			@QueryParam("item_id") Long itemId,
			@QueryParam("date") Long date,
			@DefaultValue("1") @QueryParam("duration") Integer duration );

}
