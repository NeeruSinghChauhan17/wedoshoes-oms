package com.wedoshoes.oms.resource;

import static com.wedoshoes.oms.provider.filter.HeaderParam.AUTHORIZATION;
import static com.wedoshoes.oms.provider.filter.HeaderParam.USER_ID;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.wedoshoes.oms.model.shipment.Shipment;
import com.wedoshoes.oms.model.state.StateTransition;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Path("/api/v1/users/{user_id}/workshops/{workshop_id}/shipments")
@Api(value = "Shipment", description = "Operations about Admin",produces = "application/json")
public interface ShipmentResource {	
	
	@POST
	@Path("/orders/{order_id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation("Assign Order for Pickup/Delivery")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID, required = true, dataType = "string", paramType = "header") 
	  })
	Response addShipmentOrder(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId, @ApiParam StateTransition transition);

	@POST
	@Path("/orders/{order_id}/items/{item_id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation("	Assign Item for Pickup/Delivery")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID, required = true, dataType = "string", paramType = "header") 
	  })
	Response addShipmentItem(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,
			@ApiParam StateTransition transition);
	
	@PUT
	@Path("/{shipment_id}/state")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation("Change Item State")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID, required = true, dataType = "string", paramType = "header") 
	  })
	Response changeShipmentItemState(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("shipment_id") Long shipmentId,@ApiParam StateTransition transition);
	
	
	@PUT
	@Path("/{shipment_id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation("Change Shipment User")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID, required = true, dataType = "string", paramType = "header") 
	  })
	Response update(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("shipment_id") Long shipmentId,@ApiParam Shipment shipment);
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation("Change Shipments User")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID, required = true, dataType = "string", paramType = "header") 
	  })
	Response updateShipment(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@ApiParam List<Shipment> shipment);
	
	@GET
	@Path("/{shipment_id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation("Get Shipment By Id")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID, required = true, dataType = "string", paramType = "header") 
	  })
	Response get(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("shipment_id") Long shipmentId);
	
	@GET
	@Path("/date/{shipment_date}/orders/{order_id}/items/{item_id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation("Get Shipment By Date And OrderID and Item Id")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID, required = true, dataType = "string", paramType = "header") 
	  })
	Response find(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("shipment_date") Long shipmentDate,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId);
	
	
	@GET
	@Path("/orders/date/{shipment_date}/shipment-users/{shipment_user_id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation("Get Shipment Orders By Date And ShipmentUserId")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID, required = true, dataType = "string", paramType = "header") 
	  })
	Response getOrders(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("shipment_date") Long shipmentDate,
			@PathParam("shipment_user_id") Long shipemtUserId);
	
	@GET
	@Path("/summary/{shipment_date}")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation("Get Shipment Summary")
	Response getShipmentSummary(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("shipment_date") Long shipmentDate);
	
	@GET
	@Path("/routes/{shipment_date}")
	@Consumes(MediaType.APPLICATION_JSON)
	Response getShipmentRoutes(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("shipment_date") Long shipmentDate,
			@QueryParam("latitude") @DefaultValue("0.0") Double latitude,
			@QueryParam("longitude") @DefaultValue("0.0") Double longitude,
			@QueryParam("current_time_slot")  Byte currentTimeSlot);
	
}
