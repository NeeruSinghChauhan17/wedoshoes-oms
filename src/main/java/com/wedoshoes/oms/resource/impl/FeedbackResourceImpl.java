package com.wedoshoes.oms.resource.impl;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wedoshoes.oms.resource.FeedbackResource;
import com.wedoshoes.oms.service.FeedbackService;
/**
 * 
 * @author Lawakush Chaudhary
 *
 */
@Component
public class FeedbackResourceImpl implements FeedbackResource{

	private final FeedbackService feedbackService;

	@Autowired
	public FeedbackResourceImpl(final FeedbackService feedbackService) {
	  this.feedbackService=feedbackService;
   }
	
	@Override
	public Response getFeedbackCategory(final Integer ratingPoint) {
		return Response.ok().entity(feedbackService.getFeedbackCategory(ratingPoint)).build(); 
	}

	@Administrative(action = "get_feedback_categories")
    @Override
    public Response getFeedbackCategoryByRatingPoint(final Integer ratingPoint) {
        return Response
                .ok()
                .entity(feedbackService
                        .getFeedbackCategoryByRatingPoint(ratingPoint)).build();
    }
}
