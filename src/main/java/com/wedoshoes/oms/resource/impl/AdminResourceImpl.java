package com.wedoshoes.oms.resource.impl;

import java.awt.image.BufferedImage;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.wedoshoes.oms.http.impl.model.core.CommFactory;
import com.wedoshoes.oms.model.PaymentTransaction;
import com.wedoshoes.oms.model.order.Image;
import com.wedoshoes.oms.model.order.ImageType;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.ItemDefect;
import com.wedoshoes.oms.model.order.Order;
import com.wedoshoes.oms.model.order.Payment;
import com.wedoshoes.oms.model.order.WedoService;
import com.wedoshoes.oms.model.shipment.Shipment;
import com.wedoshoes.oms.model.shipment.ShipmentOrder;
import com.wedoshoes.oms.model.state.StateTransition;
import com.wedoshoes.oms.resource.AdminResource;
import com.wedoshoes.oms.service.OrderService;
import com.wedoshoes.oms.service.ShipmentService;

@Component
public class AdminResourceImpl implements AdminResource {

	private final OrderService orderService;
	private final ShipmentService shipmentService;
	@Autowired
	public AdminResourceImpl(final OrderService orderService, final ShipmentService shipmentService) {
		this.orderService = orderService;
		this.shipmentService=shipmentService;
	}

	@Administrative(action = "create_order")
	@Override
	public Response create(final Byte workshopId,final Order order, final Boolean sendNotification,
			final Integer commSett, final Boolean saveSettings) {
		return Response.ok(orderService.createOrder(order,CommFactory.createCommSettings(commSett), saveSettings)).build();
	}

	@Administrative(action = "update_order")
	@Override
	public Response update(final Byte workshopId,final Long orderId, final Order order) {
		orderService.updateOrder(workshopId, Order.createWithId(orderId, order));
		return Response.noContent().build();
	}

	@Administrative(action = "get_order_by_order_id")
	@Override
	public Response get(final Byte workshopId,final Long orderId, final Boolean summary) {
		final Optional<Order> optional = orderService.getOrder(workshopId, orderId, summary);
		if(optional.isPresent()) {
			return Response.ok(optional.get()).build();
		}
		return Response.status(Status.NOT_FOUND).build();
	}
	
	@Override
	public Response search(final Byte workshopId,final Long userId, final Long createrUserId,
			final Long referredByUserId, final String customerName, final Long orderId,
			final Long itemId, final String cc, final Long phone, final Byte state, final Long createDate,
			final Long pickUpDate, final Long deliveryDate, final Long fromDate, final Long toDate,final String filterBy,final Integer offset, final Integer limit) {
		final Collection<Order> orders;
		if(!StringUtils.isEmpty(filterBy)){
			orders = orderService.search(workshopId,userId,customerName, cc, phone, state, 
					fromDate, toDate,filterBy, offset, limit);
		}else{
		    orders = orderService.search(workshopId,userId, createrUserId, 
				     referredByUserId, customerName, orderId, itemId,cc, phone, state, 
				     createDate, pickUpDate, deliveryDate, fromDate, toDate, offset, limit);
		}		
		if(orders.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(orders).build();
	}


	@Administrative(action = "add_order_item")
	@Override
	public Response addOrderItem(final Byte workshopId,final Long orderId, final Item item) {
		return Response.ok(orderService.addItem(workshopId, orderId, item)).build();
	}

	@Administrative(action = "update_order_item")
	@Override
	public Response updateItem(final Byte workshopId,final Long orderId, 
			final Long itemId, final Item item) {
		orderService.updateItem(workshopId, orderId, itemId, item);
		return Response.noContent().build();
	}
	
	@Administrative(action = "get_order_item")
	@Override
	public Response getItem(final Byte workshopId,final Long orderId, final Long itemId,
			final Boolean summary) {
		final Optional<Item> opItem = orderService.getItem(workshopId, orderId, itemId, summary);
		if(opItem.isPresent()) {
			return Response.ok(opItem.get()).build();
		}
		return Response.status(Status.NOT_FOUND).build();
	}

	@Administrative(action = "list_order_item")
	@Override
	public Response listOrderItems(final Byte workshopId,final Long userId, final Long orderId,
			final Byte state,
			final Long deliveryDate, final Integer offset, final Integer limit) {
		final Collection<Item> items =  orderService.listOrderItems(workshopId, orderId, state, 
				deliveryDate, offset, limit);
		if(items.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(items).build();
	}

	@Administrative(action = "add_item_service")
	@Override
	public Response addItemServices(final Byte workshopId,final Long orderId, final Long itemId,
			final List<WedoService> services) {
		orderService.addItemServices(workshopId, orderId, itemId, services);
		return Response.noContent().build();
	}

	@Administrative(action = "update_item_service")
	@Override
	public Response updateItemService(final Byte workshopId,final Long orderId, final Long itemId,
			final Long serviceId, final WedoService service) {
		orderService.updateItemService(workshopId, orderId, itemId, serviceId, service);
		return Response.noContent().build();
	}

	@Administrative(action = "delete_item_service")
	@Override
	public Response deleteItemService(final Byte workshopId,final Long orderId, final Long itemId,
			final Long serviceId) {
		orderService.removeItemService(workshopId, orderId, itemId, serviceId);
		return Response.noContent().build();
	}

	@Administrative(action = "delete_item_services")
	@Override
	public Response deleteItemServices(final Byte workshopId,final Long orderId, final Long itemId) {
		orderService.removeItemServices(workshopId, orderId, itemId);
		return Response.noContent().build();
	}

	@Administrative(action = "get_item_service")
	@Override
	public Response getItemService(final Byte workshopId,final Long orderId, final Long itemId,
			final Long serviceId, final Boolean summary) {
		final Optional<WedoService> opService = orderService.
				getItemService(workshopId, orderId, itemId, serviceId, summary);
		if(opService.isPresent()) {
			return Response.ok(opService.get()).build();
		}
		return Response.status(Status.NOT_FOUND).build();
	}

	@Administrative(action = "list_item_services")
	@Override
	public Response listItemServices(final Byte workshopId,final Long orderId, final Long itemId,
			final Boolean summary) {
		final Collection<WedoService> services = orderService.
				listItemServices(workshopId, orderId, itemId, summary);
		if(services.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(services).build();
	}

	@Administrative(action = "add_item_images")
	@Override
	public Response addItemImages(final Byte workshopId,final Long orderId, final Long itemId,
			final List<Image> images) {
		orderService.addItemImages(workshopId, orderId, itemId, images);
		return Response.noContent().build();
	}

	@Administrative(action = "update_item_image")
	@Override
	public Response updateItemImage(final Byte workshopId,final Long orderId, final Long itemId,
			final Long imageId, final Image image) {
		orderService.updateItemImage(workshopId, orderId, itemId, imageId, image);
		return Response.noContent().build();
	}

	@Administrative(action = "delete_item_image")
	@Override
	public Response deleteItemImage(final Byte workshopId,final Long orderId, final Long itemId,
			final Long imageId) {
		orderService.removeItemImage(workshopId, orderId, itemId, imageId);
		return Response.noContent().build();
	}

	@Administrative(action = "delete_item_images")
	@Override
	public Response deleteItemImages(final Byte workshopId,final Long orderId, final Long itemId) {
		orderService.removeItemImagesByItem(workshopId, orderId, itemId);
		return Response.noContent().build();
	}

	@Administrative(action = "get_item_image")
	@Override
	public Response getItemImage(final Byte workshopId,final Long orderId, final Long itemId,
			final Long imageId, final Boolean summary) {
		final Optional<Image> opImage = orderService.getItemImage(workshopId, orderId, itemId, imageId, summary);
		if(opImage.isPresent()) {
			return Response.ok(opImage.get()).build();
		}
		return Response.status(Status.NOT_FOUND).build();
	}

	@Administrative(action = "list_item_images")
	@Override
	public Response listItemImages(final Byte workshopId,final Long orderId, final Long itemId,
			final ImageType imageType, final Boolean summary) {
		final Collection<Image> images;
		if(imageType == null ) {
			images = orderService.listItemImages(workshopId, orderId, itemId, summary);
		} else {
			images = orderService.listItemImages(workshopId, orderId, itemId, imageType, summary);
		}
		if(images.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(images).build();
	}

	@Administrative(action = "make_order_advance_payment")
	@Override
	public Response makeOrderPayment(final Long userId,final Byte workshopId,
			final Long orderId, final Payment payment) {
		orderService.addAdvancedPayment(workshopId, userId, orderId, payment);
		return Response.noContent().build();
	}

	@Administrative(action = "make_order_item_payment")
	@Override
	public Response makeOrderItemPayment(final Long userId,final Byte workshopId, final Long orderId,
			final Long itemId, final Payment payment) {
		orderService.addItemPayment(workshopId, userId, orderId, itemId, payment);
		return Response.noContent().build();
	}

	@Administrative(action = "change_order_items_state")
	@Override
	public Response updateOrderState(final Byte workshopId, final Long orderId,
			final StateTransition transition) {
		orderService.updateOrderState(workshopId, orderId, transition);
		return Response.noContent().build();
	}

	@Administrative(action = "change_item_state")
	@Override
	public Response updateItemState(final Byte workshopId,
			final Long orderId, final Long itemId,
			final StateTransition transition) {
		orderService.updateItemState(workshopId, orderId, itemId, transition);
		return Response.noContent().build();
	}

	@Administrative(action = "get_item_transitions")                          
	@Override
	public Response getItemStateTransitions(final Byte workshopId,final Long orderId,
			final Long itemId,final Integer stateType) {
		final List<StateTransition> transitions = orderService
				.listExternalStateTransition(workshopId, itemId,stateType);
		if(transitions.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(transitions).build();
	}

	@Override
	public Response searchItems(final Byte workshopId,final Byte state, 
			final Long deliveryDate, final Long date, final Integer offset,
			final Integer limit) {
		final Collection<Item> items =  orderService.searchItems(workshopId, state, 
				deliveryDate, date, offset, limit);
		if(items.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(items).build();
	}

	
	@Administrative(action = "generate barcode with info")
	@Override
	public Response generateBarcodeWithInfo(final Byte workshopId, final Long orderId,
			final Long itemId) {
		final Optional<BufferedImage> imageOp = orderService.generateBarcodeWithInfo(workshopId,
				orderId, itemId);
		if(imageOp.isPresent()){
			return Response.ok(imageOp.get(), "image/png").build();
		}
		return Response.status(Status.NOT_FOUND).build();
	}

	//@Administrative(action = "generate barcode")
	@Override
	public Response generateBarcode(final Byte workshopId,final Long orderId, final Long itemId) {
		final Optional<BufferedImage> imageOp  = orderService.generateBarcode(workshopId,
				orderId,itemId);
		if(imageOp.isPresent()){
			return Response.ok(imageOp.get(), "image/png").build();
		}
		return Response.status(Status.NOT_FOUND).build();
	}

	@Override
	@Administrative(action = "add_item_defects")
	public Response addItemDefects(final Byte workshopId, final Long orderId,
			final Long itemId, final List<Byte> defects) {
		orderService.addItemDefects(workshopId, orderId, itemId, defects);
		return Response.noContent().build();
	}

	@Override
	@Administrative(action = "list_item_defects")
	public Response listItemDefects(final Byte workshopId, final Long orderId,
			final Long itemId) {
		final Collection<ItemDefect> defects = orderService.
				listItemDefects(workshopId, orderId, itemId);
		if(defects.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(defects).build();
	}

	@Override
	public Response updateOrdersLocality(final Byte workshopId, final Long localityId, final String code) {
		orderService.updateOrdersLocality(workshopId, localityId, code); 
		return Response.noContent().build();
	}
	
//	@Administrative(action = "get_shipment_orders_by_date_and_user_id_by_admin")
    @Override
    public Response getShipmentOrders(final Byte workshopId,
            final Long shipmentDate, final Long shipemtUserId) {
        final Collection<ShipmentOrder> orders = shipmentService
                .findOrderByDateAndUser(workshopId, shipmentDate, shipemtUserId);
        if (orders.isEmpty()) {
            return Response.noContent().build();
        }
        return Response.ok(orders).build();
    }
    
    @Administrative(action = "list_payment_transactions")
    @Override
    public Response listTransactions(final Byte workshopId,final Long orderId,final Long itemId,final Long userId,
            final Long startDate, final Long endDate, final Integer offset,
            final Integer limit) {
        final Collection<PaymentTransaction> orders = orderService
                .listTransactions(workshopId,orderId,itemId,userId,startDate, endDate, offset, limit);
        if (orders.isEmpty()) {
            return Response.noContent().build();
        }
        return Response.ok(orders).build();
    }
    
    @Override
	public Response getShipmentOrderAssignee(final Byte workshopId, final Long orderId, final Long itemId) {
    	 final Collection<Shipment> orders = shipmentService
                 .getShipmentOrderAssignee(workshopId, orderId, itemId);
         if (orders.isEmpty()) {
             return Response.noContent().build();
         }
         return Response.ok(orders).build();
	}

	@Override
	public Response getPaymentModeById(final Byte workshopId, final Integer paymentModeId) {
		final Map<String, String> paymentMode=orderService.findPaymentModeById(paymentModeId);
		if(!paymentMode.isEmpty()){
			return Response.ok(paymentMode).build();
		}
		return Response.noContent().build();
	}

	@Override
	public Response listPaymentModes(final Byte workshopId) {
		final List<Map<String, String>> paymentMode=orderService.listPaymentModes(workshopId);
		if(!paymentMode.isEmpty()){
			return Response.ok(paymentMode).build();
		}
		return Response.noContent().build();
	}

	@Administrative(action = "change_orders_items_state")
	@Override
	public Response updateOrderItemState(final  Byte workshopId, final List<Order> order) {
		orderService.updateOrderItemState(workshopId, order);
		return Response.noContent().build();
	}

	//@Administrative(action = "get_tax_reports")
	@Override
	public Response getTaxReports(final Byte workshopId, final Byte state, final Long fromDate, final Long toDate) {
		Collection<Order> report= orderService.getTaxReports(workshopId, state, fromDate, toDate);
		if(report.isEmpty()){	
			return Response.noContent().build();
		}
		return Response.ok(report).build();
	}
}
