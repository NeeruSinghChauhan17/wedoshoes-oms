package com.wedoshoes.oms.resource.impl;

import java.util.List;
import java.util.Optional;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wedoshoes.oms.model.order.Cart;
import com.wedoshoes.oms.model.order.CartState;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.Order;
import com.wedoshoes.oms.model.order.WedoService;
import com.wedoshoes.oms.resource.CartResource;
import com.wedoshoes.oms.service.CartService;

@Component
public class CartResourceImpl implements CartResource {

	private final CartService cartService;
	private final Byte DEFAULT_WORKSHOP_ID=1;
	@Autowired
	public CartResourceImpl(final  CartService cartService) {
		this.cartService = cartService;
	}
	
	@Override
	public Response create(final Cart cart) {
		final Optional<Cart> opCart = cartService.getCartByUserId(cart.getUserId());
		if(!opCart.isPresent()){
			return Response.ok(Cart.createWithId(cartService.createCart(cart))).build();
		}
		return Response.status(Status.CONFLICT).build();
	}

	@Override
	public Response addItems(final Long cartId, final Cart cart) {
		final Optional<Cart> cartOp = cartService.getCartById(cartId);
		if(cartOp.isPresent() && cartOp.get().getState()==CartState.ACTIVE){
			cartService.addCartItems(cartId, cart.getItems());
			return Response.noContent().build();
		}
		return Response.status(Status.FORBIDDEN).build();
	}

	@Override
	public Response addItemServices(final Long cartId, final Long itemId,
			final List<WedoService> services) {
		final Optional<Cart> cartOp = cartService.getCartById(cartId);
		if(cartOp.isPresent() && cartOp.get().getState()==CartState.ACTIVE){
			cartService.addCartItemServices(cartId, itemId, services);
			return Response.noContent().build();
		}
		return Response.status(Status.FORBIDDEN).build();
	}

	@Override
	public Response deleteItemService(final Long cartId, final Long itemId, final Long serviceId) {
		final Optional<Cart> cartOp = cartService.getCartById(cartId);
		if(cartOp.isPresent() && cartOp.get().getState()==CartState.ACTIVE){
			cartService.deleteItemServiceByServiceId(cartId, itemId, serviceId);
			return Response.noContent().build();
		}
		return Response.status(Status.FORBIDDEN).build();
	}

	@Override
	public Response deleteItem(final Long cartId, final Long itemId) {
		final Optional<Cart> cartOp = cartService.getCartById(cartId);
		if(cartOp.isPresent() && cartOp.get().getState()==CartState.ACTIVE){
			cartService.deleteItem(cartId, itemId);
			return Response.noContent().build();
		}
		return Response.status(Status.FORBIDDEN).build();
	}

	@Override
	public Response clear(final Long cartId) {
		final Optional<Cart> cartOp = cartService.getCartById(cartId);
		if(cartOp.isPresent() && cartOp.get().getState()==CartState.ACTIVE){
			cartService.clearCart(cartId);
			return Response.noContent().build();
		}
		return Response.status(Status.FORBIDDEN).build();
	}

	@Override
	public Response checkoutCart(final Long cartId, final Order order) {
		final Optional<Cart> cartOp = cartService.getCartById(cartId);
		if(cartOp.isPresent() && cartOp.get().getState()==CartState.ACTIVE){
			return Response.ok(cartService.checkoutCart(cartId, order)).build();
		}
		return Response.status(Status.FORBIDDEN).build();
	}

	@Override
	public Response listItem(final Long cartId) {
		final List<Item> items = cartService.listCartItemByCartId(cartId);
		if(items.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(items).build();
	}

	@Override
	public Response listCarts(final Long userId, final Integer offset,
			final Integer limit) {
		if(userId!=null){
			final Optional<Cart> opCart = cartService.getCartByUserId(userId);
			if(opCart.isPresent()){
				return Response.ok(opCart.get()).build();
			}
			return Response.noContent().build();
		}
		
		final List<Cart> carts = cartService.listCarts(offset, limit);
		if(!carts.isEmpty()){
			return Response.ok(carts).build();
		}
		return Response.noContent().build();
	}

	@Override
	public Response getCart(final Long cartId, final Boolean summary) {
		Optional<Cart> cartOptional= cartService.getCartDetail(cartId, summary);
		if(cartOptional.isPresent()){
			return Response.ok().entity(cartOptional.get()).build();
		}
		return Response.status(Status.NOT_FOUND).build(); 
	}

	@Override
	public Response updateItem(final Item item, final Long cartId, final Long itemId) {
		cartService.updateItem(DEFAULT_WORKSHOP_ID, cartId, itemId, item); 
		return Response.noContent().build();
	}
}
