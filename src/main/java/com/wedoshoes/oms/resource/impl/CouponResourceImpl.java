package com.wedoshoes.oms.resource.impl;

import java.util.Collection;
import java.util.Optional;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wedoshoes.oms.model.Coupon;
import com.wedoshoes.oms.model.CouponSource;
import com.wedoshoes.oms.resource.CouponResource;
import com.wedoshoes.oms.service.CouponService;
/**
 * 
 * @author Lawakush Chaudhary
 *
 */
@Component
public class CouponResourceImpl implements CouponResource {

	private final CouponService couponService;

	@Autowired
	public CouponResourceImpl(final CouponService couponService) {
		this.couponService = couponService;
	}

	@Override
	@Administrative(action = "add_coupon")
	public Response create(final Coupon coupon) {
		return Response.ok(couponService.save(coupon)).build();
	}

	@Override
	@Administrative(action = "update_coupon")
	public Response update(final Integer couponId, final Coupon coupon) {
		couponService.update(Coupon.createWithId(couponId, coupon));
		return Response.noContent().build();
	}

	@Override
	@Administrative(action = "delete_coupon")
	public Response delete(final Integer couponId) {
		couponService.delete(couponId);
		return Response.noContent().build();
	}

	@Override
	@Administrative(action = "list_coupon")
	public Response list(final Byte userTypeId, final Integer categoryId,
			final Integer serviceId, final Integer productId,
			final Integer offset, final Integer limit) {
		final Collection<Coupon> couponList = couponService.list(userTypeId,
				categoryId, serviceId, productId, offset, limit);
		if (!couponList.isEmpty()) {
			return Response.ok().entity(couponList).build();
		}
		return Response.status(Status.NO_CONTENT).build();
	}

	@Override
	public Response validate(final Coupon coupon) {
		final Optional<Coupon> couponResp = couponService.validate(coupon);
		if (couponResp.isPresent()) {
			if (couponResp.get().getUserTypes()!=null 
					|| couponResp.get().getProducts()!=null 
					|| couponResp.get().getCategories()!=null 
					|| couponResp.get().getServices()!=null 
					|| couponResp.get().getMaxUse()!=null) 
			{
				return Response.status(Status.FORBIDDEN).entity(couponResp.get())
						.build();
			}
			return Response.ok().entity(couponResp.get()).build();
		}
		return Response.status(Status.NOT_FOUND).build();
	}

	@Override
	public Response apply(final Coupon coupon) {
		final Optional<Coupon> couponResp = couponService.apply(coupon);
		if (couponResp.isPresent()) {
			return Response.ok().entity(couponResp.get()).build();
		}
		return Response.status(Status.NOT_FOUND).build();
	}

	@Override
	public Response getByCode(final String couponCode, final Boolean summary) {
		final Optional<Coupon> optional = couponService.getCouponByCode(
				couponCode, summary);
		if (optional.isPresent()) {
			return Response.ok(optional.get()).build();
		}
		return Response.status(Status.NOT_FOUND).build();
	}

	@Override
	@Administrative(action = "get_coupon")
	public Response getById(final Integer couponId, final Boolean summary) {
		final Optional<Coupon> optional = couponService.getCouponById(couponId,
				summary);
		if (optional.isPresent()) {
			return Response.ok(optional.get()).build();
		}
		return Response.status(Status.NOT_FOUND).build();
	}

	@Override
	@Administrative(action = "add_coupon_source")
	public Response addSource(final CouponSource couponSource) {
		return Response.ok(couponService.save(couponSource)).build();
	}

	@Override
	@Administrative(action = "update_coupon_source")
	public Response updateSource(final Integer sourceId,
			final CouponSource couponSource) {
		couponService.update(CouponSource.createWithId(sourceId, couponSource));
		return Response.noContent().build();
	}

	@Override
	@Administrative(action = "get_coupon_source")
	public Response getSource(final Integer sourceId, final Boolean summary) {
		final Optional<CouponSource> optional = couponService
				.getCouponSourceById(sourceId, summary);
		if (optional.isPresent()) {
			return Response.ok(optional.get()).build();
		}
		return Response.status(Status.NOT_FOUND).build();
	}

	@Override
	@Administrative(action = "delete_coupon_source")
	public Response deleteSource(final Integer sourceId) {
		couponService.deleteCouponSource(sourceId);
		return Response.noContent().build();
	}

	@Override
	public Response list(final Boolean summary) {
		final Collection<CouponSource> couponSources = couponService
				.list();
		if (!couponSources.isEmpty()){
			return Response.ok(couponSources).build();
		}
		return Response.status(Status.NO_CONTENT).build();
	}

//	@Override
//	public Response removeAppliedCoupon(final Long orderId, final Long itemId, final Integer discountid) {
//		couponService.removeAppliedCoupon(orderId, itemId, discountid); 
//		return Response.noContent().build();
//	}

		
	
}


