package com.wedoshoes.oms.resource.impl;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.ws.rs.NameBinding;

/**
 * Indicate user with ADMIN previllages allow to perform this  
 * @author Navrattan Yadav
 *
 */
@NameBinding
@Retention(RetentionPolicy.RUNTIME)
public @interface Administrative {
	String action();
}
