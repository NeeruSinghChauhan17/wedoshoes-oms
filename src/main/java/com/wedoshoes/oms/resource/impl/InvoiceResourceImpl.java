package com.wedoshoes.oms.resource.impl;

import java.io.File;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wedoshoes.oms.resource.InvoiceResource;
import com.wedoshoes.oms.service.InvoiceService;

@Component
public class InvoiceResourceImpl implements InvoiceResource{

	private final InvoiceService invoiceService;
	
	@Autowired
	 public InvoiceResourceImpl(final InvoiceService invoiceService) {
		 this.invoiceService=invoiceService;
	}
	
	@Administrative(action = "send_invoice")
	@Override
	public Response sendInvoice(final Long userId, final Byte workshopId, final Long orderId,
			final Long itemId) {
			invoiceService.sendInvoices(userId, workshopId, orderId, itemId);
			return Response.noContent().build();
	}

	@Administrative(action = "send_challan")
	@Override
	public Response sendChallan(final Long userId, final Byte workshopId, final Long orderId,
			final Long itemId) {
			invoiceService.sendChallan(userId, workshopId, orderId, itemId);
			return Response.noContent().build();
			
	}

	@Administrative(action = "get_invoices")
	@Override
	public Response getInvoices( final Byte workshopId, final Long orderId,
			final Long itemId, final Long date, final Integer duration) {
			File file=invoiceService.getInvoices(workshopId, orderId, itemId, date, duration);
			return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM)
				      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
				      .build();
			
	}
	
	@Administrative(action = "get_invoice")
	@Override
	public Response getInvoice(final Long userId, final Byte workshopId, final Long orderId,final Long itemId) {
		File file=invoiceService.createInvoices(workshopId, orderId, itemId);
		return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM)
			      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
			      .build();
	}
	
	@Administrative(action = "get_challan")
	@Override
	public Response getChallan(final Long userId, final Byte workshopId, final Long orderId, final Long itemId) {
		File file=invoiceService.createChallan(workshopId, orderId, itemId);
		return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM)
			      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
			      .build();
	}


}
