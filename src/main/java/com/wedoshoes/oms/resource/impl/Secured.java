package com.wedoshoes.oms.resource.impl;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.ws.rs.NameBinding;

/**
 * Indicate user authentication and authorization required.
 * @author Navrattan Yadav
 *
 */
@NameBinding
@Retention(RetentionPolicy.RUNTIME)
public @interface Secured {

	String action() default "";
}
