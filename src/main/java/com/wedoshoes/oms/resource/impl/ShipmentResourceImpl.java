package com.wedoshoes.oms.resource.impl;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wedoshoes.oms.model.shipment.Shipment;
import com.wedoshoes.oms.model.shipment.ShipmentPoint;
import com.wedoshoes.oms.model.shipment.ShipmentOrder;
import com.wedoshoes.oms.model.shipment.ShipmentSummary;
import com.wedoshoes.oms.model.state.StateTransition;
import com.wedoshoes.oms.resource.ShipmentResource;
import com.wedoshoes.oms.service.ShipmentService;

/**
 * 
 * @author Navrattan Yadav
 *
 */
@Component
public class ShipmentResourceImpl implements ShipmentResource {

	private final ShipmentService shipmentService;
	
	@Autowired
	public ShipmentResourceImpl(final ShipmentService shipmentService) {
		this.shipmentService = shipmentService;
	}

	@Administrative(action = "add_shipment_order")
	@Override
	public Response addShipmentOrder(final Long userId, final Byte workshopId,
			final Long orderId, final StateTransition transition) {
		shipmentService.addShipmentOrder(workshopId, userId,orderId, transition);
		return Response.noContent().build();
	}

	@Administrative(action = "add_shipment_item")
	@Override
	public Response addShipmentItem(final Long userId, final Byte workshopId, final Long orderId,
			final Long itemId, final StateTransition transition) {
		return Response.ok(shipmentService.addShipmentItem(workshopId, 
				userId,orderId, itemId, transition)).build();
	}

	@Administrative(action = "change_shipment_item_state")
	@Override
	public Response changeShipmentItemState(final Long userId, final Byte workshopId,
			final Long shipmentId, final StateTransition transition) {
		shipmentService.changeShipmentItemState(workshopId, userId, shipmentId, transition);
		return Response.noContent().build();
	}

	@Administrative(action = "change_shipment_user")
	@Override
	public Response update(final Long userId, final Byte workshopId, final Long shipmentId,
			final Shipment shipment) {
		shipmentService.update(workshopId, shipmentId, shipment);
		return Response.noContent().build();
	}

	@Administrative(action = "get_shipment_id")
	@Override
	public Response get(final Long userId, final Byte workshopId, final Long shipmentId) {
		final Optional<Shipment> optinal = shipmentService.find(workshopId, shipmentId);
		if(optinal.isPresent()) {
			return Response.ok(optinal.get()).build();
		}
		
		return Response.status(Status.NOT_FOUND).build();
	}

	@Administrative(action = "find_shipment_by_date_order_id_item_id")
	@Override
	public Response find(final Long userId, final Byte workshopId, final Long shipmentDate,
			final Long orderId, final Long itemId) {
		final Optional<Shipment> optinal = shipmentService
				.findShipmentByDate(workshopId, shipmentDate, orderId, itemId);
		if(optinal.isPresent()) {
			return Response.ok(optinal.get()).build();
		}
		
		return Response.status(Status.NOT_FOUND).build();
	}

	@Administrative(action = "get_shipment_orders_by_date_and_shipment_user_id")
	@Override
	public Response getOrders(final Long userId, final Byte workshopId, final Long shipmentDate,
			final Long shipemtUserId) {
		final Collection<ShipmentOrder> orders = shipmentService.findOrderByDateAndUser(workshopId, shipmentDate, shipemtUserId);
		if(orders.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(orders).build();
	}

	@Override
	public Response getShipmentSummary(final Long userId, final Byte workshopId, final Long shipmentDate) {
		final Optional<ShipmentSummary> summaryOp = shipmentService.getShipmentSummary(workshopId, userId, shipmentDate);
		if(summaryOp.isPresent() && !summaryOp.get().isEmpty()){
			return Response.ok(summaryOp.get()).build();
		}
		return Response.noContent().build();
	}

	@Administrative(action = "get_shipment_routes_by_date_and_shipment_user_id")
	@Override
	public Response getShipmentRoutes(final Long userId,final  Byte workshopId,
			final Long shipmentDate, final Double latitude,final  Double longitude,final Byte currentTimeSlot) {
		final Collection<ShipmentPoint> shortestLocations=shipmentService.getShipmentRoutes(workshopId,userId,
				shipmentDate,latitude,longitude,currentTimeSlot);
		if(shortestLocations.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(shortestLocations).build();
	}

	// FOR  CHANGING MULTIPLE SHIPMENTS POS USER
	@Administrative(action = "change_shipments_user")
	@Override
	public Response updateShipment(final Long userId,final  Byte workshopId,
			final List<Shipment> shipment) {
		shipmentService.updateShipments(workshopId,  shipment);
		return Response.noContent().build();
		}

}
