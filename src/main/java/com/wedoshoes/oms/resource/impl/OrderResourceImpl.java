package com.wedoshoes.oms.resource.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.wedoshoes.oms.model.LastDeliveredItem;
import com.wedoshoes.oms.model.order.Discount;
import com.wedoshoes.oms.model.order.Feedback;
import com.wedoshoes.oms.model.order.Image;
import com.wedoshoes.oms.model.order.ImageType;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.Order;
import com.wedoshoes.oms.model.order.Payment;
import com.wedoshoes.oms.model.order.WedoService;
import com.wedoshoes.oms.model.payment.PaymentAdjustment;
import com.wedoshoes.oms.model.state.StateTransition;
import com.wedoshoes.oms.resource.OrderResource;
import com.wedoshoes.oms.service.OrderService;


@Controller
public class OrderResourceImpl implements OrderResource {

	private final OrderService orderService;
	
	@Autowired
	public OrderResourceImpl(final OrderService orderService) {
		this.orderService = orderService;
	}

	@Secured
	@Override
	public Response create(final Long userId, final Order order, final Boolean sendNotification) {
		return Response.ok(orderService.createOrder(Order.createWithUserId(userId, order),null, null)).build();
	}

	@Secured
	@Override
	public Response update(final Long userId, final Byte workshopId, final Long orderId, final Order order) {
		orderService.updateOrder(workshopId,Order.createWithId(orderId, order));
		return Response.noContent().build();
	}

	@Secured
	@Override
	public Response get(final Long userId, final Byte workshopId, final Long orderId, final Boolean summary) {
		final Optional<Order> optional = orderService.getOrder(workshopId,orderId, summary);
		if(optional.isPresent()) {
			return Response.ok(optional.get()).build();
		}
		return Response.status(Status.NOT_FOUND).build();
	}

	@Secured
	@Override
	public Response list(final Long userId, final Integer offset, final Integer limit) {
		final Collection<Order> orders = orderService.listByUser(userId, offset, limit);
		if(orders.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(orders).build();
	}
	
	
	@Override
 //   @Administrative(action = "is_order_exist")
    public Response getUserOrderCounts(final Long userId) {
           Integer count= orderService.getUserOrderCounts(userId);
           if(count!=BigDecimal.ZERO.intValue()){
        	   return Response.ok().entity(count).build();        	  
           }
           return Response.status(Status.NOT_FOUND).build();
    }

	@Secured
	@Override
	public Response addOrderItem(final Long userId, final Byte workshopId, final Long orderId, final Item item) {
		return Response.ok(orderService.addItem(workshopId, orderId, item)).build();
	}

	@Secured
	@Override
	public Response updateItem(final Long userId, final Byte workshopId, final Long orderId, 
			final Long itemId, final Item item) {
		orderService.updateItem(workshopId, orderId, itemId, item);
		return Response.noContent().build();
	}
	
	@Secured
	@Override
	public Response getItem(final Long userId, final Byte workshopId, final Long orderId, final Long itemId,
			final Boolean summary) {
		final Optional<Item> opItem = orderService.getItem(workshopId, orderId, itemId, summary);
		if(opItem.isPresent()) {
			return Response.ok(opItem.get()).build();
		}
		return Response.status(Status.NOT_FOUND).build();
	}

	@Secured
	@Override
	public Response listOrderItems(final Long userId, final Byte workshopId, 
			final Long orderId, final Byte state,
			final Long deliveryDate, final Integer offset, final Integer limit) {
		final Collection<Item> items =  orderService.listOrderItems(workshopId, orderId, state, 
				deliveryDate, offset, limit);
		if(items.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(items).build();
	}

	@Secured
	@Override
	public Response addItemServices(final Long userId, final Byte workshopId, 
			final Long orderId, final Long itemId,
			final List<WedoService> services) {
		orderService.addItemServices(workshopId, orderId, itemId, services);
		return Response.noContent().build();
	}

	@Secured
	@Override
	public Response updateItemService(final Long userId, final Byte workshopId, 
			final Long orderId, final Long itemId,
			final Long serviceId, final WedoService service) {
		orderService.updateItemService(workshopId, orderId, itemId, serviceId, service);
		return Response.noContent().build();
	}

	@Secured
	@Override
	public Response deleteItemService(final Long userId, final Byte workshopId, 
			final Long orderId, final Long itemId,
			final Long serviceId) {
		orderService.removeItemService(workshopId, orderId, itemId, serviceId);
		return Response.noContent().build();
	}

	@Secured
	@Override
	public Response deleteItemServices(final Long userId, final Byte workshopId, 
			final Long orderId, final Long itemId) {
		orderService.removeItemServices(workshopId, orderId, itemId);
		return Response.noContent().build();
	}

	@Secured
	@Override
	public Response getItemService(final Long userId, final Byte workshopId, 
			final Long orderId, final Long itemId,
			final Long serviceId, final Boolean summary) {
		final Optional<WedoService> opService = orderService.
				getItemService(workshopId, orderId, itemId, serviceId, summary);
		if(opService.isPresent()) {
			return Response.ok(opService.get()).build();
		}
		return Response.status(Status.NOT_FOUND).build();
	}

	@Secured
	@Override
	public Response listItemServices(final Long userId, final Byte workshopId, 
			final Long orderId, final Long itemId,
			final Boolean summary) {
		final Collection<WedoService> services = orderService.
				listItemServices(workshopId, orderId, itemId, summary);
		if(services.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(services).build();
	}

	@Secured
	@Override
	public Response addItemImages(final Long userId, final Byte workshopId, 
			final Long orderId, final Long itemId,
			final List<Image> images) {
		orderService.addItemImages(workshopId, orderId, itemId, images);
		return Response.noContent().build();
	}

	@Secured
	@Override
	public Response updateItemImage(final Long userId, final Byte workshopId, 
			final Long orderId, final Long itemId,
			final Long imageId, final Image image) {
		orderService.updateItemImage(workshopId, orderId, itemId, imageId, image);
		return Response.noContent().build();
	}

	@Secured
	@Override
	public Response deleteItemImage(final Long userId, final Byte workshopId, 
			final Long orderId, final Long itemId,
			final Long imageId) {
		orderService.removeItemImage(workshopId, orderId, itemId, imageId);
		return Response.noContent().build();
	}

	@Secured
	@Override
	public Response deleteItemImages(final Long userId, final Byte workshopId, 
			final Long orderId, final Long itemId) {
		orderService.removeItemImagesByItem(workshopId, orderId, itemId);
		return Response.noContent().build();
	}

	@Secured
	@Override
	public Response getItemImage(final Long userId, final Byte workshopId, 
			final Long orderId, final Long itemId,
			final Long imageId, final Boolean summary) {
		final Optional<Image> opImage = orderService.getItemImage(workshopId, orderId, itemId, imageId, summary);
		if(opImage.isPresent()) {
			return Response.ok(opImage.get()).build();
		}
		return Response.status(Status.NOT_FOUND).build();
	}

	@Secured
	@Override
	public Response listItemImages(final Long userId, final Byte workshopId, 
			final Long orderId, final Long itemId,
			final ImageType imageType, final Boolean summary) {
		final Collection<Image> images;
		if(imageType == null ) {
			images = orderService.listItemImages(workshopId, orderId, itemId, summary);
		} else {
			images = orderService.listItemImages(workshopId, orderId, itemId, imageType, summary);
		}
		if(images.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(images).build();
	}

	@Secured
	@Override
	public Response makeOrderPayment(final Long userId, final Byte workshopId, 
			final Long orderId, final Payment payment) {
		orderService.addAdvancedPayment(workshopId, userId, orderId, payment);
		return Response.noContent().build();
	}

	@Secured
	@Override
	public Response makeOrderItemPayment(final Long userId, final Byte workshopId, final Long orderId,
			final Long itemId, final Payment payment) {
		orderService.addItemPayment(workshopId, userId, orderId, itemId, payment);
		return Response.noContent().build();
	}

	@Secured
	@Override
	public Response listOrderPayments(final Long userId, final Byte workshopId, final Long orderId) {
		final Collection<Payment> payments = orderService.listOrderPayments(workshopId, userId, orderId);
		if(!payments.isEmpty()){
			return Response.ok(payments).build();
		}
		return Response.noContent().build();
	}
	
	@Secured
	@Override
	public Response updateOrderState(final Long userId, final Byte workshopId, final Long orderId,
			final StateTransition transition) {
		orderService.updateOrderState(workshopId, orderId, 
				StateTransition.createWithUserId(userId, transition));
		return Response.noContent().build();
	}

	@Secured
	@Override
	public Response updateItemState(final Long userId, final Byte workshopId, 
			final Long orderId, final Long itemId,
			final StateTransition transition) {
		orderService.updateItemState(workshopId, orderId, itemId, 
				StateTransition.createWithUserId(userId, transition));
		return Response.noContent().build();
	}

	@Secured     
	@Override
	public Response getItemStateTransitions(final Long userId, final Byte workshopId, final Long orderId,
			final Long itemId,final Integer stateType) {                       //added-state
		final List<StateTransition> transitions = orderService
				.listExternalStateTransition(workshopId, itemId,stateType);
		if(transitions.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(transitions).build();
	}

	@Secured
	@Override
	public Response listItemDicounts(final Long userId, final Byte workshopId,
			final Long orderId, final Long itemId) {
		final Collection<Discount> discounts = orderService.listDiscountsOnItem(workshopId, itemId);
		if(discounts.isEmpty()) {
			return Response.noContent().build();
		}
		return Response.ok(discounts).build();
	}

	@Secured
	@Override
	public Response getCost(final Long userId, final Byte workshopId, final Long orderId) {
		final Optional<Order> optional = orderService.getOrderCostingDetail(workshopId, orderId);
		if(optional.isPresent()) {
			return Response.ok(optional.get()).build();
		}
		return Response.status(Status.NOT_FOUND).build();
	}

	@Override
	public Response addFeedback(final Long userId, final Byte workshopId, final Long orderId, final Long itemId, final Feedback feedback) {
		return Response.ok().entity(orderService.addFeedback(userId, workshopId, orderId, itemId, feedback)).build();
	}

	@Override
	public Response getFeedback(final Long userId, final Byte workshopId, final Long orderId, final Long itemId) {
		Optional<Feedback> feedbackResp= orderService.getFeedback(userId, workshopId, orderId, itemId);
		if (feedbackResp.isPresent()) {
		return Response.ok().entity(feedbackResp.get()).build();
		}
		return Response.noContent().build();
	}

	@Override
	public Response getLastDeliveredItem(final Long userId, final Byte workshopId) {
		Optional<LastDeliveredItem>	lastDeliveredItemOpt=orderService.getLastDeliveredItem(userId, workshopId);
		if(lastDeliveredItemOpt.isPresent()){
			return Response.ok().entity(lastDeliveredItemOpt.get()).build();
		}
		return Response.noContent().build();
	}

	@Override
	public Response applyCouponOnItem(final Long userId, final Byte workshopId, final Long orderId, final Long itemId, Integer couponId) {
		orderService.applyCouponOnItem(userId, workshopId, orderId, itemId, couponId); 
		return Response.noContent().build();
	}

	@Override
	public Response removeAppliedCoupon(final Byte workshopId, final Long orderId, final Long itemId, final Integer discountid) {
		orderService.removeAppliedCoupon(workshopId, orderId, itemId, discountid); 
		return Response.noContent().build();
	}

	/*
	 * Only for PaymentAdjustment -In this only adjustment will be present not payment with adjustment
	 */
	@Secured
	@Override
	public Response makeOrderItemPaymentAdjustment(final Long userId,final Byte workshopId,final  Long orderId, final Long itemId,
			final PaymentAdjustment paymentAdjustment) {
		orderService.addItemPaymentAdjustment(workshopId, userId, orderId, itemId, paymentAdjustment);
		return Response.noContent().build();
	}

	
}