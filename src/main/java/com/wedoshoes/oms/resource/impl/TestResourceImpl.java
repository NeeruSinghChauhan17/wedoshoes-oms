package com.wedoshoes.oms.resource.impl;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

import com.wedoshoes.oms.http.CMSServerHttpRequester;
import com.wedoshoes.oms.manager.NotificationManager;
import com.wedoshoes.oms.model.order.Order;
import com.wedoshoes.oms.resource.TestResource;
import com.wedoshoes.oms.service.InvoiceService;
import com.wedoshoes.oms.service.OrderService;

@Component
public class TestResourceImpl implements TestResource {

	private final CMSServerHttpRequester cmsRequester;

	private final NotificationManager notificationManager;

	@SuppressWarnings("unused")
	private final OrderService orderService;

	@SuppressWarnings("unused")
	private final InvoiceService invoiceService;

	public TestResourceImpl(final CMSServerHttpRequester cmsRequester,
			final NotificationManager notificationManager,
			final OrderService orderService, final InvoiceService invoiceService) {
		this.cmsRequester = cmsRequester;
		this.notificationManager = notificationManager;
		this.orderService = orderService;
		this.invoiceService = invoiceService;
	}

	@Override
	public Response getServiceName(final Integer serviceId) {
		return Response.ok(cmsRequester.getServiceName(serviceId)).build();
	}

	@Override
	public Response getProductName(final Integer productId) {
		return Response.ok(cmsRequester.getProductName(productId)).build();
	}

	@Override
	public Response getTimeSlotName(final Byte timeSlotId) {
		return Response.ok(cmsRequester.getTimeSlotName(timeSlotId)).build();
	}

	@Override
	public Response sendOrderMail(final Order order, final Byte workshopId,
			final Long orderId, final Byte state) {
		//notificationManager.notifyOrderStateChanged(order, state, null);
		return Response.ok().build();
	}

	@Override
	public Response getTemplate(final Byte templateId) {
		return Response.ok(notificationManager.printTemplate()).build();
	}

	@Override
	public Response createInvoice(final Byte workshopId, final Order order) {
	//	File file=invoiceService.testIncvoices(workshopId, order);
	//	new File("pdftest.pdf");
		return Response.ok(null, MediaType.APPLICATION_OCTET_STREAM)
				.type(MediaType.APPLICATION_OCTET_STREAM).header("Content-Disposition", "attachment; filename=" + null) //optional
			      .build();
	}

}
