package com.wedoshoes.oms.resource;

import static com.wedoshoes.oms.provider.filter.HeaderParam.AUTHORIZATION;
import io.dropwizard.jersey.PATCH;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.wedoshoes.oms.model.order.Feedback;
import com.wedoshoes.oms.model.order.Image;
import com.wedoshoes.oms.model.order.ImageType;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.Order;
import com.wedoshoes.oms.model.order.Payment;
import com.wedoshoes.oms.model.order.WedoService;
import com.wedoshoes.oms.model.payment.PaymentAdjustment;
import com.wedoshoes.oms.model.state.StateTransition;

@Path("/api/v1/users/{user_id}/workshops/{workshop_id}/orders")
@Api(value = "Order", description = "Operations about Order",produces = "application/json")
public interface OrderResource {

	/**
	 * *******************************************************************
	 * **************************** Order API ****************************
	 * *******************************************************************
	 */
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("Create Order")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response create(@PathParam("user_id") Long userId, @ApiParam Order order,
			@QueryParam("send_notification") @DefaultValue("true") Boolean sendNotification);
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}")
	@ApiOperation("Update")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response update(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,@ApiParam Order order);
	
	@PATCH
	@Path("/{order_id}/state")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation("Change Order State")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response updateOrderState(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,@ApiParam StateTransition transition);
	
	@GET
	@Path("/{order_id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("Get")
	Response get(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId, 
			@QueryParam("summary") @DefaultValue("true") Boolean summary);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("List")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response list(@PathParam("user_id") Long userId,
			@QueryParam("offset") @DefaultValue("0") Integer offset,
			@QueryParam("limit") @DefaultValue("20") Integer limit);
	
	
	@GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/counts")
	@ApiOperation("Get Orders count")
    Response getUserOrderCounts(@PathParam("user_id") Long userId);
	
	/**
	 * *******************************************************************
	 * **************************** Order Item API ***********************
	 * *******************************************************************
	 */
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items")
	@ApiOperation("Add Item In Order")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response addOrderItem(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,@ApiParam Item item);
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}")
	@ApiOperation("Update Item")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response updateItem(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,@ApiParam Item item);
	
	@PATCH
	@Path("/{order_id}/items/{item_id}/state")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation("Update Item State")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response updateItemState(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,@ApiParam StateTransition transition);
	
	@GET
	@Path("/{order_id}/items/{item_id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("Get Item By Item Id")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response getItem(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,
			@QueryParam("summary") @DefaultValue("true") Boolean summary);
	
	@GET
	@Path("/{order_id}/items/{item_id}/state-transitions")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("Get item State Transitions")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response getItemStateTransitions(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,
			@QueryParam("state_type") Integer stateType);        //added stateType-either   0-external, 1-internal or   null-all 
	
	@GET
	@Path("/{order_id}/items")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("List Order Items")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response listOrderItems(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@QueryParam("state") Byte state,
			@QueryParam("delivery_date") Long deliveryDate,
			@QueryParam("offset") @DefaultValue("0") Integer offset,
			@QueryParam("limit") @DefaultValue("20") Integer limit);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}/payments")
	@ApiOperation("Pay Order Item Payment")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response makeOrderItemPayment(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,@ApiParam Payment payment);
	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}/payments/adjustment")
	@ApiOperation("Pay Order Item Payment Adjustment")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response makeOrderItemPaymentAdjustment(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,@ApiParam PaymentAdjustment paymentAdjustment);

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}/coupons/{coupon_id}/apply")
	@ApiOperation("Apply Coupon on Item")
	Response applyCouponOnItem(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId, 
			@PathParam("coupon_id") Integer couponId);

	@DELETE
	@Path("/coupons/{coupon_id}/remove")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("Remove Applied Coupon")
	Response removeAppliedCoupon(@QueryParam("workshop_id") Byte workshopId, 
			@QueryParam("order_id") Long orderId,
			@QueryParam("item_id") Long itemId,
			@PathParam("coupon_id") Integer discountid);

	
	/**
	 * ****************************************************Z***************
	 * ********************** Order Item Services API ********************
	 * *******************************************************************
	 */
	
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}/services")
	@ApiOperation("Add Services On Item")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response addItemServices(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,@ApiParam List<WedoService> services);
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}/services/{service_id}")
	@ApiOperation("Change Item Service")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response updateItemService(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,
			@PathParam("service_id") Long serviceId,@ApiParam WedoService service);
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}/services/{service_id}")
	@ApiOperation("Delete Item Service By Service Id")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response deleteItemService(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,
			@PathParam("service_id") Long serviceId);
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}/services")
	@ApiOperation("Delete Item Services")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response deleteItemServices(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId);
	

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}/services/{service_id}")
	@ApiOperation("Get Item Service By Service Id")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response getItemService(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,
			@PathParam("service_id") Long serviceId,
			@QueryParam("summary") @DefaultValue("true") Boolean summary);
	
	@GET
	@Path("/{order_id}/items/{item_id}/services")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("List Item Services")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response listItemServices(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,
			@QueryParam("summary") @DefaultValue("true") Boolean summary);
	
	
	/**
	 * *******************************************************************
	 * ********************** Order Item Images API *******************
	 * *******************************************************************
	 */
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}/images")
	@ApiOperation("Add Item Images")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response addItemImages(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,@ApiParam List<Image> images);
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}/images/{image_id}")
	@ApiOperation("Change Item Image By Image Id")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response updateItemImage(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,
			@PathParam("image_id") Long imageId,@ApiParam Image image);
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}/images/{image_id}")
	@ApiOperation("Delete Item Image By Image Id")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response deleteItemImage(@PathParam("user_id") Long userId, 
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,
			@PathParam("image_id") Long imageId);
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}/images")
	@ApiOperation("Delete Item Images")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response deleteItemImages(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId);
	

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}/images/{image_id}")
	@ApiOperation("Get Item Image By Image Id")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response getItemImage(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,
			@PathParam("image_id") Long imageId,
			@QueryParam("summary") @DefaultValue("true") Boolean summary);
	
	@GET
	@Path("/{order_id}/items/{item_id}/images")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("List Item Images")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response listItemImages(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,
			@QueryParam("image_type") ImageType imageType,
			@QueryParam("summary") @DefaultValue("true") Boolean summary);

	/**
	 * *******************************************************************
	 * ********************** Order Payment *******************
	 * *******************************************************************
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/payments")
	@ApiOperation("Add Order Payment")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response makeOrderPayment(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@ApiParam Payment payment);
	
	@GET
	@Path("/{order_id}/cost")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("Get Order Cost")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response getCost(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId);
	
	@GET
	@Path("/{order_id}/payments")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("List Order Payment")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response listOrderPayments(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId);	
	
	/**
	 * *******************************************************************
	 * ********************** Order Item Discounts *******************
	 * *******************************************************************
	 */
	
	@GET
	@Path("/{order_id}/items/{item_id}/discounts")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("List Item Discount")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header")
	  })
	Response listItemDicounts(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId);

	
	/**
	 * *******************************************************************
	 * ********************** Orders Feedback Related Methods ************
	 * *******************************************************************
	 */
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}/feedback")
	@ApiOperation("Add Feedback")
	Response addFeedback(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,
			@ApiParam Feedback feedback);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/feedback")
	@ApiOperation("Get Feedback")
	Response getFeedback(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@QueryParam("order_id") Long orderId,
			@QueryParam("item_id") Long itemId);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/last_delivered_item")
	@ApiOperation("Get Last Delivered Item")
	Response getLastDeliveredItem(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId);
		
}
