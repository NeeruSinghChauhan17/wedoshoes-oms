package com.wedoshoes.oms.resource;

import static com.wedoshoes.oms.provider.filter.HeaderParam.AUTHORIZATION;
import static com.wedoshoes.oms.provider.filter.HeaderParam.USER_ID;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.wedoshoes.oms.model.order.Image;
import com.wedoshoes.oms.model.order.ImageType;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.Order;
import com.wedoshoes.oms.model.order.Payment;
import com.wedoshoes.oms.model.order.WedoService;
import com.wedoshoes.oms.model.state.StateTransition;

import io.dropwizard.jersey.PATCH;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Path("/admin/api/v1/workshops/{workshop_id}/orders")
@Api(value = "Admin", description = "Operations about Admin",produces = "application/json")
public interface AdminResource {
	

	/**
	 * *******************************************************************
	 * **************************** Order API ****************************
	 * *******************************************************************
	 */
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("Create Order")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID, required = true, dataType = "string", paramType = "header") 
	  })
	Response create(@PathParam("workshop_id") Byte workshopId,
			@ApiParam Order order, @QueryParam("send_notification") @DefaultValue("true") Boolean sendNotification,
			@HeaderParam("Comm-Settings") @DefaultValue("0") Integer commSett,
			@HeaderParam("Save-Settings") Boolean saveSettings);
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}")
	@ApiOperation("Change Order")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID, required = true, dataType = "string", paramType = "header") 
	  })
	Response update(@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId, @ApiParam Order order);
	
	@PATCH
	@Path("/{order_id}/state")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation("Change Order State")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION , required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID,  required = true, dataType = "string", paramType = "header") 
	  })
	Response updateOrderState(@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,@ApiParam StateTransition transition);
	
	@PATCH
	@Path("/state")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation("Change Order Item State")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION , required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID,  required = true, dataType = "string", paramType = "header") 
	  })
	Response updateOrderItemState(@PathParam("workshop_id") Byte workshopId,
			 @ApiParam List<Order> order);
	
	@GET
	@Path("/{order_id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("Get")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION , required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID,  required = true, dataType = "string", paramType = "header") 
	  })
	Response get(
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@QueryParam("summary") @DefaultValue("true") Boolean summary);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("Search Orders")
	Response search(@PathParam("workshop_id") Byte workshopId,
			@QueryParam("user_id") Long userId,
			@QueryParam("creater_user_id") Long createrUserId,
			@QueryParam("referred_by_user_id") Long referredByUserId,
			@QueryParam("customer_name") String customerName,
			@QueryParam("order_id") Long orderId,
			@QueryParam("item_id") Long itemId,
			@QueryParam("cc") String cc,
			@QueryParam("phone") Long phone,
			@QueryParam("state") Byte state,
			@QueryParam("create_date") Long createDate,
			@QueryParam("pick_up_date") Long pickUpDate,
			@QueryParam("delivery_date") Long deliveryDate,
			@QueryParam("from_date") Long fromDate,
			@QueryParam("to_date") Long toDate,
			@QueryParam("filter_by") String filterBy,
			@QueryParam("offset") @DefaultValue("0") Integer offset,
			@QueryParam("limit") @DefaultValue("20") Integer limit);
	
	
	/**
	 * *******************************************************************
	 * **************************** Order Item API ***********************
	 * *******************************************************************
	 */
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items")
	@ApiOperation("Add Items In Orders")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION , required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID,  required = true, dataType = "string", paramType = "header") 
	  })
	Response addOrderItem(@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,@ApiParam Item item);
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}")    //url item
	@ApiOperation("Update Item")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION , required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID,  required = true, dataType = "string", paramType = "header") 
	  })
	Response updateItem(@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId, Item item);
	
	@PATCH
	@Path("/{order_id}/items/{item_id}/state")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation("Update Item State")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION , required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID,  required = true, dataType = "string", paramType = "header") 
	  })
	Response updateItemState(@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,@ApiParam StateTransition transition);
	
	@GET
	@Path("/{order_id}/items/{item_id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("Get Item")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION , required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID,  required = true, dataType = "string", paramType = "header") 
	  })
	Response getItem(@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,
			@QueryParam("summary") @DefaultValue("true") Boolean summary);
	
	@GET
	@Path("/{order_id}/items/{item_id}/state-transitions")          //internal-external state
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("List Item State-Transitions")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION , required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID,  required = true, dataType = "string", paramType = "header") 
	  })
	Response getItemStateTransitions(@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,@QueryParam("state_type") Integer stateType);
	
	@GET
	@Path("/{order_id}/items")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("List Items By Orders ")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION , required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID,  required = true, dataType = "string", paramType = "header") 
	  })
	Response listOrderItems(@PathParam("workshop_id") Byte workshopId,
			@PathParam("user_id") Long userId,
			@PathParam("order_id") Long orderId,
			@QueryParam("state") Byte state,
			@QueryParam("delivery_date") Long deliveryDate,
			@QueryParam("offset") @DefaultValue("0") Integer offset,
			@QueryParam("limit") @DefaultValue("20") Integer limit);
	
	@GET
	@Path("/items/search")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("Search Item By Item State")
	Response searchItems(@PathParam("workshop_id") Byte workshopId,
			@QueryParam("state") Byte state,
			@QueryParam("delivery_date") Long deliveryDate,
			@QueryParam("date") Long date,
			@QueryParam("offset") @DefaultValue("0") Integer offset,
			@QueryParam("limit") @DefaultValue("20") Integer limit);
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}/payments")
	@ApiOperation("Add Order Item Payment")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION , required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID,  required = true, dataType = "string", paramType = "header") 
	  })
	Response makeOrderItemPayment(@HeaderParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,@ApiParam Payment payment);
	
	
	@GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/payments")
	@ApiOperation("List Payment Transaction By Order Id And Item Id and Start And End Date")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION , required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID,  required = true, dataType = "string", paramType = "header") 
	  })
    Response listTransactions(@PathParam("workshop_id") Byte workshopId,
    		@QueryParam("order_id") Long orderId,
			@QueryParam("item_id") Long itemId,
			@QueryParam("user_id") Long userId,
            @QueryParam("start_date") Long startDate,
            @QueryParam("end_date") Long endDate,
            @QueryParam("offset") @DefaultValue("0") Integer offset,
            @QueryParam("limit") @DefaultValue("20") Integer limit);
	
	@GET
	@Path("paymentmode/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("List Payment Mode By Id")
	Response getPaymentModeById(@PathParam("workshop_id") Byte workshopId,
			@PathParam("id") Integer  paymentModeId);
	
	@GET
	@Path("paymentmodes")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("List Payment Modes")
	Response listPaymentModes(@PathParam("workshop_id") Byte workshopId);
	
	
	
	/**
	 * *******************************************************************
	 * ********************** Order Item Services API ********************
	 * *******************************************************************
	 */
	
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}/services")
	@ApiOperation("Add Services In Item")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION , required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID,  required = true, dataType = "string", paramType = "header") 
	  })
	Response addItemServices(@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,@ApiParam List<WedoService> services);
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}/services/{service_id}")
	@ApiOperation("Change Item Service")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION , required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID,  required = true, dataType = "string", paramType = "header") 
	  })
	Response updateItemService(@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,
			@PathParam("service_id") Long serviceId, @ApiParam WedoService service);
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}/services/{service_id}")
	@ApiOperation("Delete Item Service BY Service Id")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION , required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID,  required = true, dataType = "string", paramType = "header") 
	  })	
	Response deleteItemService(@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,
			@PathParam("service_id") Long serviceId);
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}/services")
	@ApiOperation("Delete Item Services")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION , required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID,  required = true, dataType = "string", paramType = "header") 
	  })
	Response deleteItemServices(@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId);
	

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}/services/{service_id}")
	@ApiOperation("List Item Service By Service Id")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION , required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID,  required = true, dataType = "string", paramType = "header") 
	  })
	Response getItemService(@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,
			@PathParam("service_id") Long serviceId,
			@QueryParam("summary") @DefaultValue("true") Boolean summary);
	
	@GET
	@Path("/{order_id}/items/{item_id}/services")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("List Item Services")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION , required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID,  required = true, dataType = "string", paramType = "header") 
	  })
	Response listItemServices(@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,
			@QueryParam("summary") @DefaultValue("true") Boolean summary);
	
	
	/**
	 * *******************************************************************
	 * ********************** Order Item Images API *******************
	 * *******************************************************************
	 */
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}/images")
	@ApiOperation("Add Item Image")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION , required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID,  required = true, dataType = "string", paramType = "header") 
	  })
	Response addItemImages(@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId, @ApiParam List<Image> images);
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}/images/{image_id}")
	@ApiOperation("Change Item Image")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION , required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID,  required = true, dataType = "string", paramType = "header") 
	  })
	Response updateItemImage(@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,
			@PathParam("image_id") Long imageId, @ApiParam Image image);
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}/images/{image_id}")
	@ApiOperation("Delete Item Image By Image Id")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION , required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID,  required = true, dataType = "string", paramType = "header") 
	  })
	Response deleteItemImage(@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,
			@PathParam("image_id") Long imageId);
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}/images")
	@ApiOperation("Delete Item Images ")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION , required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID,  required = true, dataType = "string", paramType = "header") 
	  })
	Response deleteItemImages(@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId);
	

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}/images/{image_id}")
	@ApiOperation("List Item Images By Image Id")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION , required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID,  required = true, dataType = "string", paramType = "header") 
	  })
	Response getItemImage(@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,
			@PathParam("image_id") Long imageId,
			@QueryParam("summary") @DefaultValue("true") Boolean summary);
	
	@GET
	@Path("/{order_id}/items/{item_id}/images")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("List Item Image BY Item ")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION , required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID,  required = true, dataType = "string", paramType = "header") 
	  })
	Response listItemImages(@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,
			@QueryParam("image_type") ImageType imageType,
			@QueryParam("summary") @DefaultValue("true") Boolean summary);

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/payments")
	@ApiOperation("Add Order Payment")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION , required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID,  required = true, dataType = "string", paramType = "header") 
	  })
	Response makeOrderPayment(@HeaderParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@ApiParam Payment payment);
	
	/**
	 * *******************************************************************
	 * ********************** Order Item BarCode API ********************
	 * *******************************************************************
	 */
	
	@GET
	@Path("/{order_id}/items/{item_id}/barcode-with-info")
	@ApiOperation("Genrate Item Barcode With Info")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION , required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID,  required = true, dataType = "string", paramType = "header") 
	  })
	Response generateBarcodeWithInfo(@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId);
			
	
	@GET
	@Path("/{order_id}/items/{item_id}/barcode")
	@ApiOperation("Genrate Item Barcode")
	Response generateBarcode(@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId);

	
	/**
	 * *******************************************************************
	 * ********************** Order Item Defects API ********************
	 * *******************************************************************
	 */
	
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{order_id}/items/{item_id}/defects")
	@ApiOperation("Add Defects In Items")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION , required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID,  required = true, dataType = "string", paramType = "header") 
	  })
	Response addItemDefects(@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,@ApiParam List<Byte> defects);
	
	@GET
	@Path("/{order_id}/items/{item_id}/defects")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation("List Item Defects")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION , required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID,  required = true, dataType = "string", paramType = "header") 
	  })
	Response listItemDefects(@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId);

	
	
	/**
	 * *******************************************************************
	 * ********************** Orders Locality Related Methods ************
	 * *******************************************************************
	 */
	

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/locality/{locality_id}/pins/{code}")
	@ApiOperation("Change Order Locality")
	Response updateOrdersLocality(
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("locality_id") Long localityId,
			@PathParam("code") String code);

	
	/**
     * *******************************************************************
     * ********************** Shipments Related Methods *****************
     * *******************************************************************
     */

    @GET
    @Path("/shipments/date/{shipment_date}/shipment-users/{shipment_user_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation("List Shipment Orders By Date And ShipmentUserId")
    Response getShipmentOrders(@PathParam("workshop_id") Byte workshopId,
            @PathParam("shipment_date") Long shipmentDate,
            @PathParam("shipment_user_id") Long shipemtUserId);
    
    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/shipments/assignee")
	@ApiOperation("List Shipment Order Assignee")
    Response getShipmentOrderAssignee(@PathParam("workshop_id") Byte workshopId,
			@QueryParam("order_id") Long orderId,
			@QueryParam("item_id") Long itemId);
    
    /**
	 * *******************************************************************
	 * **************************** Reporting Related Apis ***************
	 * *******************************************************************
	 */
    
    @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/reports")
	@ApiOperation("List taxes report")
    Response getTaxReports(@PathParam("workshop_id") Byte workshopId,
			@QueryParam("state") Byte state,
			@QueryParam("from_date") Long fromDate,
    		@QueryParam("to_date") Long toDate);
    
}
