package com.wedoshoes.oms.resource;

import static com.wedoshoes.oms.provider.filter.HeaderParam.AUTHORIZATION;
import static com.wedoshoes.oms.provider.filter.HeaderParam.USER_ID;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@Path("/api/v1/feedbacks")
@Api(value = "FeedBack", description = "Operations about FeedBack",produces = "application/json")
public interface FeedbackResource {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/categories")
	@ApiOperation("Get Feedback Category")
	Response getFeedbackCategory(
			@QueryParam("rating_point") Integer ratingPoint);

	
	@GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/categories-sequence")
	@ApiOperation("Get Feedback Category By Rating Point ")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = AUTHORIZATION, required = true, dataType = "string", paramType = "header"),
	    @ApiImplicitParam(name = USER_ID, required = true, dataType = "string", paramType = "header") 
	  })
	Response getFeedbackCategoryByRatingPoint(
            @QueryParam("rating_point") Integer ratingPoint);


}
