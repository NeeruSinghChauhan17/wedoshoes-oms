package com.wedoshoes.oms.dao.impl;

import static com.wedoshoes.oms.dao.impl.DefaultUtils.createUpdatingBigDecimalSupplier;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.createUpdatingIntegerSupplier;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.createUpdatingLongSupplier;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.createUpdatingStringSupplier;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.defaultBigDecimalIfNull;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.defaultIfNull;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.defaultIntegerIfNull;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.defaultStringIfNull;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.updateIfNotNull;

import java.time.Instant;
import java.util.Collection;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.wedoshoes.oms.dao.BaseDao;
import com.wedoshoes.oms.dao.CouponDao;
import com.wedoshoes.oms.dao.rowmapper.CouponDetailResultSetExtractor;
import com.wedoshoes.oms.dao.rowmapper.CouponRowMapper;
import com.wedoshoes.oms.dao.rowmapper.CouponSourceDetailResultSetExtractor;
import com.wedoshoes.oms.dao.rowmapper.CouponSourceRowMapper;
import com.wedoshoes.oms.model.Coupon;
import com.wedoshoes.oms.model.CouponSource;
/**
 * 
 * @author Lawakush Chaudhary
 *
 */
@Repository
public class CouponDaoImpl implements CouponDao {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(CouponDaoImpl.class);

	private final BaseDao baseDao;

	private static final RowMapper<Coupon> COUPON_ROW_MAPPER = new CouponRowMapper();
	private static final ResultSetExtractor<Collection<Coupon>> COUPONS_DEATIL_RESULT_SET_EXTRACTOR = new CouponDetailResultSetExtractor();
	private static final RowMapper<CouponSource> COUPON_SOURCE_ROW_MAPPER = new CouponSourceRowMapper();
	private static final ResultSetExtractor<Collection<CouponSource>> COUPONS_SOURCE_DEATIL_RESULT_SET_EXTRACTOR = new CouponSourceDetailResultSetExtractor();

	@Autowired
	public CouponDaoImpl(final BaseDaoFactory baseDaoFactory) {
		this.baseDao = baseDaoFactory.createBaseDao("coupons.xml");
	}

	@Override
	public Integer save(final Coupon coupon) {
		final String query = baseDao.getQueryById("save.coupon");
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		final SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
				.addValue("code", coupon.getCode())
				.addValue("description",
						defaultStringIfNull(coupon.getDescription()))
				.addValue("value", coupon.getValue())
				.addValue("valueType", coupon.getValueType().toString())
				.addValue(
						"startDate",
						defaultIfNull(coupon.getStartDate(), Instant.now()
								.getEpochSecond()))
				.addValue(
						"endDate",
						defaultIfNull(coupon.getEndDate(), Instant.now()
								.getEpochSecond()))
				.addValue("maxUse", defaultIntegerIfNull(coupon.getMaxUse()))
				.addValue("minOrderAmount", coupon.getMinOrderAmount())
				.addValue("sourceId", coupon.getCouponSource().getId())
				.addValue("maxDiscount",
						defaultBigDecimalIfNull(coupon.getMaxDiscount()));
		baseDao.getJdbcTemplate().update(query, sqlParameterSource, keyHolder);
		return keyHolder.getKey().intValue();
	}

	@Override
	public void update(final Coupon coupon) {
		final Optional<Coupon> optional = findById(coupon.getId());
		if (!optional.isPresent()) {
			return;
		}
		final Coupon oldCoupon = optional.get();

		final String query = baseDao.getQueryById("update.coupon.by.id");
		final MapSqlParameterSource source = new MapSqlParameterSource();

		updateIfNotNull(
				"code",
				source,
				createUpdatingStringSupplier(coupon.getCode(),
						oldCoupon.getCode()));

		updateIfNotNull(
				"description",
				source,
				createUpdatingStringSupplier(coupon.getDescription(),
						oldCoupon.getDescription()));

		updateIfNotNull(
				"value",
				source,
				createUpdatingBigDecimalSupplier(coupon.getValue(),
						oldCoupon.getValue()));

		updateIfNotNull(
				"valueType",
				source,
				createUpdatingStringSupplier(coupon.getValueType().toString(),
						oldCoupon.getValueType().toString()));

		updateIfNotNull(
				"startDate",
				source,
				createUpdatingLongSupplier(coupon.getStartDate(),
						oldCoupon.getStartDate()));

		updateIfNotNull(
				"endDate",
				source,
				createUpdatingLongSupplier(coupon.getEndDate(),
						oldCoupon.getEndDate()));

		updateIfNotNull(
				"maxUse",
				source,
				createUpdatingIntegerSupplier(coupon.getMaxUse(),
						oldCoupon.getMaxUse()));

		updateIfNotNull(
				"minOrderAmount",
				source,
				createUpdatingBigDecimalSupplier(coupon.getMinOrderAmount(),
						oldCoupon.getMinOrderAmount()));

		updateIfNotNull(
				"sourceId",
				source,
				createUpdatingIntegerSupplier(coupon.getCouponSource().getId(),
						oldCoupon.getCouponSource().getId()));

		updateIfNotNull(
				"maxDiscount",
				source,
				createUpdatingBigDecimalSupplier(coupon.getMaxDiscount(),
						oldCoupon.getMaxDiscount()));

		source.addValue("id", coupon.getId());
		baseDao.getJdbcTemplate().update(query, source);
	}

	@Override
	public void delete(final Integer id) {
		final String query = baseDao.getQueryById("delete.coupon.by.id");
		final SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
				.addValue("id", id);
		baseDao.getJdbcTemplate().update(query, sqlParameterSource);
	}

	@Override
	public Optional<Coupon> findById(final Integer id) {
		try {
			final String query = baseDao.getQueryById("find.coupon.by.id");
			final MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource()
					.addValue("id", id);
			return Optional.ofNullable(baseDao.getJdbcTemplate()
					.queryForObject(query, sqlParameterSource,
							COUPON_ROW_MAPPER));
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info(" ======= coupon not found for id = {} =======> ",
					id, e);
			return Optional.empty();
		}
	}

	@Override
	public Optional<Coupon> findDetailById(final Integer id) {
		try {
			final String query = baseDao.getQueryById("find.coupon.detail.by.id");
			final MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource()
					.addValue("id", id);
			return baseDao.getJdbcTemplate().query(query, sqlParameterSource,
							COUPONS_DEATIL_RESULT_SET_EXTRACTOR).stream().findFirst();
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info(" ============= coupon detail not found for id = {} ============= ",id, e);
			return Optional.empty();
		}
	}

	@Override
	public void applyOnUserType(final Integer userTypeId, final Integer couponId) {
		final String query = baseDao.getQueryById("apply.coupon.on.user.type");
		final SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
				.addValue("userTypeId", userTypeId).addValue("couponId",
						couponId);
		baseDao.getJdbcTemplate().update(query, sqlParameterSource);
	}

	@Override
	public void applyOnService(final Integer serviceId, final Integer couponId) {
		final String query = baseDao.getQueryById("apply.coupon.on.service");
		final SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
				.addValue("serviceId", serviceId)
				.addValue("couponId", couponId);
		baseDao.getJdbcTemplate().update(query, sqlParameterSource);
	}

	@Override
	public void applyOnCategory(final Integer categoryId, final Integer couponId) {
		final String query = baseDao.getQueryById("apply.coupon.on.category");
		final SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
				.addValue("categoryId", categoryId).addValue("couponId",
						couponId);
		baseDao.getJdbcTemplate().update(query, sqlParameterSource);
	}

	@Override
	public void applyOnProduct(final Integer productId, final Integer couponId) {
		final String query = baseDao.getQueryById("apply.coupon.on.product");
		final SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
				.addValue("productId", productId)
				.addValue("couponId", couponId);
		baseDao.getJdbcTemplate().update(query, sqlParameterSource);
	}
	
	@Override
	public void apply(final Coupon coupon) {
		final String query = baseDao.getQueryById("apply.coupon");
		final MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource()
				.addValue("code", coupon.getCode()).addValue("usedCount", coupon.getMaxUse());
		baseDao.getJdbcTemplate().update(query, sqlParameterSource);
	}

	@Override
	public Optional<Coupon> getCouponByCode(final String code) {
		try {
			final String query = baseDao.getQueryById("find.coupon.by.code");
			final MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource()
					.addValue("code", code);
			return Optional.ofNullable(baseDao.getJdbcTemplate()
					.queryForObject(query, sqlParameterSource,
							COUPON_ROW_MAPPER));
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info(
					" ================ coupon not found for code = {} ============= ",
					code, e);
			return Optional.empty();
		}

	}

	@Override
	public Optional<Coupon> getCouponDetailByCode(final String code) {
		try {
			final String query = baseDao.getQueryById("find.coupon.detail.by.code");
			final MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource()
					.addValue("code", code);
			return baseDao.getJdbcTemplate().query(query, sqlParameterSource,
							COUPONS_DEATIL_RESULT_SET_EXTRACTOR).stream().findFirst();
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info(" ================ coupon detail not found for code = {} ============= ",
					code, e);
			return Optional.empty();
		}
	}

	
	@Override
	public Collection<Coupon> listByUserType(final Byte userTypeId,final Integer offSet,
			final Integer limit) {
		final String query = baseDao.getQueryById("get.coupons.by.user.type.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("userTypeId", userTypeId)
				.addValue("offSet", offSet)
				.addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, COUPON_ROW_MAPPER);
	}

	@Override
	public Collection<Coupon> listByProduct(final Integer productId,final Integer offSet,
			final Integer limit) {
		final String query = baseDao.getQueryById("get.coupons.by.product.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("productId", productId)
				.addValue("offSet", offSet)
				.addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, COUPON_ROW_MAPPER);
	}

	@Override
	public Collection<Coupon> listByCategory(final Integer categoryId,
			final Integer offSet,final Integer limit) {
		final String query = baseDao.getQueryById("get.coupons.by.category.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("categoryId", categoryId)
				.addValue("offSet", offSet)
				.addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, COUPON_ROW_MAPPER);	}

	@Override
	public Collection<Coupon> listDefaultCoupon(final Integer offSet, final Integer limit) {
		final String query = baseDao.getQueryById("get.coupons");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("offSet", offSet)
				.addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, COUPON_ROW_MAPPER);
	}

	
//============================= Coupon Source ===============================	
	
	@Override
	public Integer save(final CouponSource couponSource) {
		final String query = baseDao.getQueryById("save.source");
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		final SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
				.addValue("name", couponSource.getName());
		baseDao.getJdbcTemplate().update(query, sqlParameterSource, keyHolder);
		return keyHolder.getKey().intValue();
	}

	@Override
	public void update(final CouponSource couponSource) {
		final Optional<CouponSource> optional = findSourceById(couponSource
				.getId());
		if (!optional.isPresent()) {
			return;
		}
		final CouponSource oldSource = optional.get();

		final String query = baseDao.getQueryById("update.source.by.id");
		final MapSqlParameterSource source = new MapSqlParameterSource();

		updateIfNotNull(
				"name",
				source,
				createUpdatingStringSupplier(couponSource.getName(),
						oldSource.getName()));

		source.addValue("id", couponSource.getId());
		baseDao.getJdbcTemplate().update(query, source);
	}

	@Override
	public void deleteSource(final Integer sourceId) {
		final String query = baseDao.getQueryById("delete.source.by.id");
		final SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
				.addValue("id", sourceId);
		baseDao.getJdbcTemplate().update(query, sqlParameterSource);
	}

	@Override
	public Optional<CouponSource> findSourceDetailById(final Integer sourceId) {
		try {
			final String query = baseDao
					.getQueryById("find.source.detail.by.id");
			final MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource()
					.addValue("id", sourceId);
			return baseDao
					.getJdbcTemplate()
					.query(query, sqlParameterSource,
							COUPONS_SOURCE_DEATIL_RESULT_SET_EXTRACTOR)
					.stream().findFirst();
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info(
					" ================ source detail not found for id = {} ============= ",
					sourceId, e);
			return Optional.empty();
		}
	}

	@Override
	public Optional<CouponSource> findSourceById(final Integer sourceId) {
		try {
			final String query = baseDao.getQueryById("find.source.by.id");
			final MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource()
					.addValue("id", sourceId);
			return Optional.ofNullable(baseDao.getJdbcTemplate()
					.queryForObject(query, sqlParameterSource,
							COUPON_SOURCE_ROW_MAPPER));
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info(
					" ================ source not found for id = {} ============= ",
					sourceId, e);
			return Optional.empty();
		}
	}

	@Override
	public Collection<CouponSource> list() {
		final String query = baseDao.getQueryById("list.sources");				
		return baseDao.getJdbcTemplate().query(query, COUPONS_SOURCE_DEATIL_RESULT_SET_EXTRACTOR);
	}

}
