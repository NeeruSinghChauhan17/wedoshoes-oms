package com.wedoshoes.oms.dao.impl;

import static com.wedoshoes.oms.dao.impl.DefaultUtils.createUpdatingBooleanSupplier;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.createUpdatingIntegerSupplier;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.defaultBooleanIfNull;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.defaultIfNull;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.defaultIntegerIfNull;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.updateIfNotNull;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.wedoshoes.oms.dao.BaseDao;
import com.wedoshoes.oms.dao.CartDao;
import com.wedoshoes.oms.dao.rowmapper.cart.CartItemResultSetExtractor;
import com.wedoshoes.oms.dao.rowmapper.cart.CartRowMapper;
import com.wedoshoes.oms.dao.rowmapper.order.CartItemRowMapper;
import com.wedoshoes.oms.model.Size;
import com.wedoshoes.oms.model.order.Cart;
import com.wedoshoes.oms.model.order.CartState;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.WedoService;

@Repository
public class CartDaoImpl implements CartDao {
	
	//private static final String[] CART_ID_COULUMN_NAME = {"id"};
	
	private static final String[] CART_ITEM_ID_COULUMN_NAME = {"id"};
	
	private static final ResultSetExtractor<List<Item>> CART_ITEMS_RESULT_SET_EXTRACTOR = 
			new CartItemResultSetExtractor();
	
	private static final RowMapper<Cart> CART_ROW_MAPPER = new CartRowMapper();
	
	private static final RowMapper<Item> CART_ITEM_ROW_MAPPER = new CartItemRowMapper();
	
	private final BaseDao baseDao;
	
	@Autowired
	public CartDaoImpl(final BaseDaoFactory baseDaoFactory) {
		this.baseDao = baseDaoFactory.createBaseDao("carts.xml");
	}
	
	@Override
	public Long saveCart(final Cart cart) {
		final String query = baseDao.getQueryById("save.cart");
		
		final SqlParameterSource paramSource = new MapSqlParameterSource().
				addValue("cartId", cart.getUserId()).
				addValue("userId", cart.getUserId()).
				addValue("createDate", Instant.now().getEpochSecond());
//		final KeyHolder keyHolder = new GeneratedKeyHolder();
		
		baseDao.getJdbcTemplate().update(query, paramSource);
		return cart.getUserId();
	}
	
	@Override
	public Long saveCartItem(final Long cartId, final Item item) {
		final String query = baseDao.getQueryById("save.cart.item");
		
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("cartId", cartId).
				addValue("createDate", Instant.now().getEpochSecond()).
				addValue("parentServiceId", item.getParentServiceId()).
				addValue("categoryId", item.getCategoryId()).
				addValue("productId", item.getProductId()).
				addValue("size", defaultIntegerIfNull(item.getSize())).
				addValue("brandId", item.getBrandId()).
				addValue("isExpressProcessing", defaultBooleanIfNull(item.isExpressProcessing())).
				addValue("packing", defaultBooleanIfNull(item.isPacking())).
				addValue("itemSizeType", 
						defaultIfNull(item.getSizeType(),Size.NO_SIZE).getCode());
		
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		
		baseDao.getJdbcTemplate().update(query, source, keyHolder, CART_ITEM_ID_COULUMN_NAME);
		return keyHolder.getKey().longValue();
	}
	
	@Override
	public void saveCartItemServices(final Long itemId, final List<WedoService> services) {
		services.stream().forEach(service -> saveCartItemService(itemId, service));
	}
	
	private void saveCartItemService(final Long itemId, final WedoService service) {
		final String query = baseDao.getQueryById("save.cart.item.service");
		
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("itemId", itemId).
				addValue("serviceId", service.getServiceId()).
				addValue("quantity", service.getQuantity());
		
		baseDao.getJdbcTemplate().update(query, source);
	}

	@Override
	public void deleteServiceByServiceId(final Long itemId, final Long serviceId) {
		final String query = baseDao.
				getQueryById("delete.cart.item.service.by.item.id.and.service.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("itemId", itemId).
				addValue("serviceId", serviceId);
		baseDao.getJdbcTemplate().update(query, source);
	}

	@Override
	public void deleteServiceByItemId(final Long itemId) {
		final String query = baseDao.getQueryById("delete.cart.item.services.by.item.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("itemId", itemId);
		baseDao.getJdbcTemplate().update(query, source);
	}

	@Override
	public void deleteItemByItemId(final Long itemId) {
		final String query = baseDao.getQueryById("delete.cart.item.by.item.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("itemId", itemId);
		baseDao.getJdbcTemplate().update(query, source);
	}

	@Override
	public void deleteItemsBycartId(final Long cartId) {
		final String query = baseDao.getQueryById("delete.cart.items.by.cart.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("cartId", cartId);
		baseDao.getJdbcTemplate().update(query, source);
	}

	@Override
	public void deleteServiceByCartId(final Long cartId) {
		final String query = baseDao.getQueryById("delete.cart.items.services.by.cart.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("cartId", cartId);
		baseDao.getJdbcTemplate().update(query, source);
	}
	
	@Override
	public void deActivateCartById(final Long cartId) {
		final String query = baseDao.getQueryById("inactive.cart.by.cart.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("cartId", cartId).
				addValue("active", CartState.INACTIVE.getCode());
		baseDao.getJdbcTemplate().update(query, source);
	}

	@Override
	public List<Item> findCartItemsByCartId(final Long cartId) {
		final String query = baseDao.getQueryById("find.cart.detail.by.cart.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("cartId", cartId);
		return baseDao.getJdbcTemplate().query(query, source, CART_ITEMS_RESULT_SET_EXTRACTOR);
	}

	@Override
	public Optional<Cart> findCartByUserId(final Long userId) {
		final String query = baseDao.getQueryById("find.cart.by.user.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("userId", userId)
				.addValue("active", CartState.ACTIVE.getCode());
		
		try{
		return Optional.ofNullable(baseDao.getJdbcTemplate().queryForObject(query,
				source, CART_ROW_MAPPER));
		}catch(Exception e){
			return Optional.empty();
		}
	}
	
	@Override
	public Optional<Cart> findCartById(final Long id) {
		final String query = baseDao.getQueryById("find.cart.by.cart.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("id", id);
		try{
		return Optional.ofNullable(baseDao.getJdbcTemplate().queryForObject(query,
				source, CART_ROW_MAPPER));
		}catch(Exception e){
			return Optional.empty();
		}
	}

	@Override
	public List<Cart> findCarts(final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("list.carts");
		final SqlParameterSource source = new MapSqlParameterSource()
				.addValue("offset", offset)
				.addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, CART_ROW_MAPPER);
	}

	@Override
	public void increaseCartSize(final Long cartId, final Integer size) {
			final String query = baseDao.getQueryById("increase.cart.size");		
			final SqlParameterSource paramSource = new MapSqlParameterSource().
				addValue("cartId", cartId).
				addValue("size", size);
		baseDao.getJdbcTemplate().update(query, paramSource);
	}
	
	@Override
	public void decreaseCartSize(final Long cartId, final Integer size) {
		String query="";
		if(size != 0){			
			 query = baseDao.getQueryById("decrease.cart.size");
		}else{
			 query = baseDao.getQueryById("clear.cart.size");
		}
		final SqlParameterSource paramSource = new MapSqlParameterSource().
				addValue("cartId", cartId).
				addValue("size", size);
		baseDao.getJdbcTemplate().update(query, paramSource);
	}

	@Override
	public void updateItem(final Byte workshopId, final Long cartId, final Item item) {
		final Optional<Item> optional = findItemById(cartId, item.getId());
		if(!optional.isPresent()) {
			return;
		}
		final Item orgItem = optional.get();
		final MapSqlParameterSource sourceMap =  new MapSqlParameterSource();
		updateIfNotNull("parentServiceId", sourceMap, createUpdatingIntegerSupplier(
				item.getParentServiceId(), orgItem.getParentServiceId()));
		updateIfNotNull("categoryId", sourceMap, createUpdatingIntegerSupplier(
				item.getCategoryId(), orgItem.getCategoryId()));
		updateIfNotNull("productId", sourceMap, createUpdatingIntegerSupplier(
				item.getProductId(), orgItem.getProductId()));
		updateIfNotNull("size", sourceMap, createUpdatingIntegerSupplier(
				item.getSize(), orgItem.getSize()));
		updateIfNotNull("brandId", sourceMap, createUpdatingIntegerSupplier(
				item.getBrandId(), orgItem.getBrandId()));
		updateIfNotNull("itemSizeType", sourceMap, createUpdatingIntegerSupplier(
				item.getSizeType() == null ? null :	item.getSizeType().getCode(),
						orgItem.getSizeType() == null ? null : 
							orgItem.getSizeType().getCode()));
		updateIfNotNull("isExpressProcessing", sourceMap, createUpdatingBooleanSupplier(
				item.isExpressProcessing(), orgItem.isExpressProcessing()));
		updateIfNotNull("packing", sourceMap, createUpdatingBooleanSupplier(
				item.isPacking(), orgItem.isPacking()));
		
		sourceMap.addValue("id", item.getId())
				 .addValue("cartId", cartId); 
		final String query = baseDao.getQueryById("update.cart.item.by.id");
		baseDao.getJdbcTemplate().update(query, sourceMap);
		
	}
	
	private Optional<Item> findItemById(final Long cartId, final Long itemId) {
		final String query = baseDao.getQueryById("find.cart.item.by.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("itemId", itemId)
				.addValue("cartId", cartId);
		return baseDao.getJdbcTemplate().query(query, source, CART_ITEM_ROW_MAPPER).stream().findFirst();
	}

}
