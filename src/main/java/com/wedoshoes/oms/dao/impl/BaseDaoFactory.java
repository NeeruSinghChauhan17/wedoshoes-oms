package com.wedoshoes.oms.dao.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Component;

import com.wedoshoes.oms.dao.BaseDao;


/**
 * 
 * @author Navrattan Yadav
 *
 */
@Component
public class BaseDaoFactory {

	private static final Logger LOGGER = LoggerFactory.getLogger(BaseDaoFactory.class);
	private static final String DB_CLASS_PATH = "/db/";
	private final NamedParameterJdbcOperations jdbcTemplate;
	
	@Autowired
	public BaseDaoFactory(@Qualifier("jdbcTemplate") final NamedParameterJdbcOperations jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	public BaseDao createBaseDao(final String fileName) {
		final Properties properties = new Properties();
		BaseDao baseDao = null;
		try {
			properties.loadFromXML(this.getClass().getResourceAsStream(DB_CLASS_PATH + "" + fileName));
			final Map<String, String> daoQueryMap = new HashMap<>();
			properties.forEach((key, value) -> daoQueryMap.put((String)key, (String)value));
			baseDao = new BaseDaoImpl(jdbcTemplate, daoQueryMap);
		} catch ( IOException e) {
			LOGGER.error("Error while loading file {}", e, fileName);
		}
		return baseDao;
	}
}
