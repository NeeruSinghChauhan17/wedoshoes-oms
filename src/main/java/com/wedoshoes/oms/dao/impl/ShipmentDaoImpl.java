package com.wedoshoes.oms.dao.impl;

import static com.wedoshoes.oms.dao.impl.DefaultUtils.createUpdatingByteSupplier;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.createUpdatingLongSupplier;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.updateIfNotNull;

import java.time.Instant;
import java.util.Collection;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.wedoshoes.oms.dao.BaseDao;
import com.wedoshoes.oms.dao.ShipmentDao;
import com.wedoshoes.oms.dao.rowmapper.ShipmentOrderResultSetExtractor;
import com.wedoshoes.oms.dao.rowmapper.ShipmentResultSetExtractor;
import com.wedoshoes.oms.dao.rowmapper.ShipmentRowMapper;
import com.wedoshoes.oms.dao.rowmapper.ShipmentSummaryResultSetExtractor;
import com.wedoshoes.oms.model.shipment.Shipment;
import com.wedoshoes.oms.model.shipment.ShipmentOrder;
import com.wedoshoes.oms.model.shipment.ShipmentSummary;
import com.wedoshoes.oms.model.state.StateTransition;
import com.wedoshoes.oms.util.DateUtil;

@Repository
public class ShipmentDaoImpl implements ShipmentDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(ShipmentDaoImpl.class);
	private static final String[] SHIPMENT_ID_COULUMN_NAME = {"id"};
	
	
	private static final RowMapper<Shipment> SHIPMENT_ROW_MAPPER = new ShipmentRowMapper();
	private static final ResultSetExtractor<ShipmentSummary> SHIPMENT_SUMMARY_RESULT_SET_EXTRACTOR=
			new ShipmentSummaryResultSetExtractor();
	
	private static final ResultSetExtractor<Collection<Shipment>> SHIPMENT_RESULT_SET_EXTRACTOR = 
			new ShipmentResultSetExtractor();
	private static final ResultSetExtractor<Collection<ShipmentOrder>> SHIPMENT_ORDER_RESULT_SET_EXTRACTOR = 
			new ShipmentOrderResultSetExtractor();
	
	
	private final BaseDao baseDao;
	
	@Autowired
	public ShipmentDaoImpl(final BaseDaoFactory baseDaoFactory) {
		this.baseDao = baseDaoFactory.createBaseDao("shipments.xml");
	}
	
	@Override
	public Long save(final Shipment shipment) {
		final String query = baseDao.getQueryById("save");
		final SqlParameterSource paramSource = new MapSqlParameterSource().
				addValue("userId", shipment.getUserId()).
				addValue("orderId", shipment.getOrderId()).
				addValue("itemId", shipment.getItemId()).
				addValue("shipmentDate", shipment.getShipmentDate()).
				addValue("initialState", shipment.getInitialState()).
				addValue("currentState", shipment.getCurrentState()).
				addValue("lastUpdatedDate", Instant.now().getEpochSecond());
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		baseDao.getJdbcTemplate().update(query, paramSource, keyHolder, SHIPMENT_ID_COULUMN_NAME);
		return keyHolder.getKey().longValue();
	}

	@Override
	public void update(final Long id, final Shipment shipment) {
		final Optional<Shipment> optional = find(id);
		if(!optional.isPresent()) {
			return;
		}
		
		final Shipment orgShipment = optional.get();
		final MapSqlParameterSource sourceMap =  new MapSqlParameterSource();
		
		updateIfNotNull("userId", sourceMap, createUpdatingLongSupplier(
				shipment.getUserId(), orgShipment.getUserId()));
		updateIfNotNull("shipmentDate", sourceMap, createUpdatingLongSupplier(
				shipment.getShipmentDate(), orgShipment.getShipmentDate()));
		updateIfNotNull("currentState", sourceMap, createUpdatingByteSupplier(
				shipment.getCurrentState(), orgShipment.getCurrentState()));
		
		sourceMap.addValue("id", id);
		sourceMap.addValue("lastUpdatedDate", Instant.now().getEpochSecond());
		final String query = baseDao.getQueryById("update");
		baseDao.getJdbcTemplate().update(query, sourceMap);
	}

	@Override
	public Optional<Shipment> find(final Long id) {
		try {
			final String query = baseDao.getQueryById("find.by.id");
			final SqlParameterSource source = new MapSqlParameterSource().
					addValue("id", id);
			return Optional.ofNullable(baseDao.getJdbcTemplate()
					.queryForObject(query, source, SHIPMENT_ROW_MAPPER));
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("Shipment not found for id : {} ", id);
			return Optional.empty();
		}
	}
	
	@Override
	public Optional<Shipment> findShipmentByItemId(final Long itemId) {
		try {
			final String query = baseDao.getQueryById("find.by.item.id");
			final SqlParameterSource source = new MapSqlParameterSource().
					addValue("itemId", itemId);
			return Optional.ofNullable(baseDao.getJdbcTemplate()
					.queryForObject(query, source, SHIPMENT_ROW_MAPPER));
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("Shipment not found for itemId : {} ", itemId);
			return Optional.empty();
		}
	}
	
	@Override
	public Collection<Shipment> findShipmentByOrderId(final Long orderId) {
		final String query = baseDao.getQueryById("find.by.order.id.and.distinct.by.user");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("orderId", orderId);
		return baseDao.getJdbcTemplate().query(query, source, SHIPMENT_RESULT_SET_EXTRACTOR);
	}

	@Override
	public Collection<ShipmentOrder> findOrderByDateAndUser(final Long date, final Long userId) {
		final String query = baseDao.getQueryById("find.by.shipment.date.and.user.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("userId", userId).
				addValue("shipmentDate", date);
		return baseDao.getJdbcTemplate().query(query, source, SHIPMENT_ORDER_RESULT_SET_EXTRACTOR);
	}


	@Override
	public Optional<Shipment> findShipmentByDate(final Long date, final Long orderId, final Long itemId) {
		try {
			final String query = baseDao.getQueryById("find.by.order.id.and.item.id.and.shipment.date");
			final SqlParameterSource source = new MapSqlParameterSource().
					addValue("orderId", orderId).
					addValue("itemId", itemId).
					addValue("shipmentDate", date);
			return Optional.ofNullable(baseDao.getJdbcTemplate()
					.queryForObject(query, source, SHIPMENT_ROW_MAPPER));
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("Shipment not found for orderId : {} item Id {} date {} ", orderId, itemId, date);
			return Optional.empty();
		}
	}
	
	@Override
	public void saveCustomersSignature(final Byte workshopId, final Long shipmentId,
			final StateTransition transition){
		final String query = baseDao.getQueryById("save.customers.signature");
		final SqlParameterSource source = new MapSqlParameterSource().
				 addValue("shipmentId", shipmentId)
				.addValue("orderId", transition.getShipment().getOrderId())
				.addValue("itemId", transition.getShipment().getItemId())
				.addValue("userId", transition.getUserId())
				.addValue("stateId", transition.getState())
				.addValue("signatureURL", transition.getShipment().getCustomerSignatureURL());
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		baseDao.getJdbcTemplate().update(query, source, keyHolder);
	}

	@Override
	public void updateShipmentItemState(final Byte state, final Long itemId,final Long shipmentId) {
		
		final MapSqlParameterSource sourceMap =  new MapSqlParameterSource();
			sourceMap.addValue("currentState", state)
			.addValue("itemId", itemId)
			.addValue("shipmentId", shipmentId);
			final String query = baseDao.getQueryById("update.shipment.state.by.item.id.and.shipment.id");
		baseDao.getJdbcTemplate().update(query, sourceMap);
	}

	@Override
	public void updateShipmentOrderItemState(final Byte state, final Long orderId) {
		
		final MapSqlParameterSource sourceMap =  new MapSqlParameterSource();
			sourceMap.addValue("currentState", state);
			sourceMap.addValue("orderId", orderId);
			final String query = baseDao.getQueryById("update.shipment.state.by.order.id");
		baseDao.getJdbcTemplate().update(query, sourceMap);
	}

	@Override
	public Optional<ShipmentSummary> findShipmentSummary(final Long userId, final Long shipmentDate) {
		try {
			final String query = baseDao.getQueryById("find.shipment.summary.by.user.id.and.shipment.date");
			
			LOGGER.info("Shipment Trip user_id={},date={} query ={}",userId,DateUtil.convertToDBDateString(shipmentDate),query);
			
			final SqlParameterSource source = new MapSqlParameterSource().
					addValue("userId", userId).
					addValue("shipmentDate", DateUtil.convertToDBDateString(shipmentDate));
			return Optional.ofNullable(baseDao.getJdbcTemplate()
					.query(query, source, SHIPMENT_SUMMARY_RESULT_SET_EXTRACTOR));
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("Shipment summary not found for user {} at date {} ", userId, 
					DateUtil.convertToDBDateString(shipmentDate));
			return Optional.empty();
		}
	}
	
}