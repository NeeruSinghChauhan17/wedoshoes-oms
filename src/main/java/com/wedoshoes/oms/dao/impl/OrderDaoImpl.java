package com.wedoshoes.oms.dao.impl;

import static com.wedoshoes.oms.dao.impl.DefaultUtils.createUpdatingBigDecimalSupplier;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.createUpdatingBooleanSupplier;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.createUpdatingByteSupplier;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.createUpdatingDoubleSupplier;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.createUpdatingIntegerSupplier;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.createUpdatingLongSupplier;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.createUpdatingShortSupplier;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.createUpdatingStringSupplier;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.createUpdatingSupplier;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.defaultBooleanIfNull;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.defaultByteIfNull;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.defaultIfNull;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.defaultIntegerIfNull;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.defaultLongIfNull;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.defaultStringIfNull;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.updateIfNotNull;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalTime;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.wedoshoes.oms.dao.BaseDao;
import com.wedoshoes.oms.dao.OrderDao;
import com.wedoshoes.oms.dao.rowmapper.InvoiceRowMapper;
import com.wedoshoes.oms.dao.rowmapper.LastDeliveredItemRowMapper;
import com.wedoshoes.oms.dao.rowmapper.PaymentRowMapper;
import com.wedoshoes.oms.dao.rowmapper.PaymentTransactionRowMapper;
import com.wedoshoes.oms.dao.rowmapper.order.DiscountRowMapper;
import com.wedoshoes.oms.dao.rowmapper.order.FeedbackResultSetExtractor;
import com.wedoshoes.oms.dao.rowmapper.order.ImageRowMapper;
import com.wedoshoes.oms.dao.rowmapper.order.ItemResultSetExtractor;
import com.wedoshoes.oms.dao.rowmapper.order.ItemRowMapper;
import com.wedoshoes.oms.dao.rowmapper.order.ItemStateResultSetExtractor;
import com.wedoshoes.oms.dao.rowmapper.order.ItemStateTransitionRowMapper;
import com.wedoshoes.oms.dao.rowmapper.order.OrderCostResultSetExtractor;
import com.wedoshoes.oms.dao.rowmapper.order.OrderRowMapper;
import com.wedoshoes.oms.dao.rowmapper.order.OrdersDetailResultSetExtractor;
import com.wedoshoes.oms.dao.rowmapper.order.WedoServiceRowMapper;
import com.wedoshoes.oms.invoice.Invoice;
import com.wedoshoes.oms.model.Coupon;
import com.wedoshoes.oms.model.LastDeliveredItem;
import com.wedoshoes.oms.model.PaymentTransaction;
import com.wedoshoes.oms.model.Size;
import com.wedoshoes.oms.model.order.Discount;
import com.wedoshoes.oms.model.order.Feedback;
import com.wedoshoes.oms.model.order.FeedbackType;
import com.wedoshoes.oms.model.order.Image;
import com.wedoshoes.oms.model.order.ImageType;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.Order;
import com.wedoshoes.oms.model.order.Payment;
import com.wedoshoes.oms.model.order.WedoService;
import com.wedoshoes.oms.model.state.StateTransition;
import com.wedoshoes.oms.model.state.UsersLocation;
import com.wedoshoes.oms.util.DateUtil;

@Repository
public class OrderDaoImpl implements OrderDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(OrderDaoImpl.class);
	
	private final BaseDao baseDao;
	
	private static final String[] ORDER_ID_COULUMN_NAME = {"id"};
	
	private static final String[] ORDER_ITEM_ID_COULUMN_NAME = {"id"};
	
	private static final String[] ORDER_ITEM_STATE_TRANSITION_ID_COULUMN_NAME = {"id"};
	
	private static final String[] ID_COULUMN_NAME = {"id"};
	private static final RowMapper<PaymentTransaction> PAYMENT_TRANSACTION_ROW_MAPPER = new PaymentTransactionRowMapper();
	private static final RowMapper<Payment> PAYMENT_ROW_MAPPER = new PaymentRowMapper();
	private static final RowMapper<Order> ORDER_ROW_MAPPER = new OrderRowMapper();
	private static final RowMapper<Item> ITEM_ROM_MAPPER = new ItemRowMapper();
	private static final RowMapper<WedoService> SERVICE_ROW_MAPPER = new WedoServiceRowMapper();
	private static final RowMapper<Image> IMAGE_ROW_MAPPER = new ImageRowMapper();
	private static final RowMapper<Discount> DISCOUNT_ROW_MAPPER = new DiscountRowMapper();
	private static final RowMapper<StateTransition> ITEM_STATE_TRANSITION_ROW_MAPPER = 
			new ItemStateTransitionRowMapper(); 
	private static final RowMapper<Invoice> INVOICE_ROW_MAPPER=new InvoiceRowMapper();
	//private static final RowMapper<Feedback> FEEDBACK_ROW_MAPPER=new FeedbackRowMapper();
	private static final ResultSetExtractor<Collection<Feedback>> FEEDBACK_RESULT_SET_EXTRACTOR=new FeedbackResultSetExtractor();
	private static final RowMapper<LastDeliveredItem> LAST_DELIVERED_ITEM_ROW_MAPPER=new LastDeliveredItemRowMapper();
	
	private static final ResultSetExtractor<Collection<Item>> ITEM_RESULT_SET_EXTRACTOR = 
			new ItemResultSetExtractor();
	private static final ResultSetExtractor<Collection<Order>> ORDERS_DETAIL_RESULT_SET_EXTRACTOR = 
			new OrdersDetailResultSetExtractor();
	
	private static final ResultSetExtractor<Optional<Order>> ORDER_COSTING_DETAIL_RESULT_SET_EXTRACTOR = 
			new OrderCostResultSetExtractor();
	
	private static final ResultSetExtractor<List<StateTransition>> ITEM_STATE_TRANSITION_RESULT_SET_EXTRACTOR = 
			new ItemStateResultSetExtractor();
	
	
	
	@Autowired
	public OrderDaoImpl(final BaseDaoFactory baseDaoFactory) {
		this.baseDao =  baseDaoFactory.createBaseDao("orders.xml");
	}
	
	/**
	 * *****************************************************************************
	 * 	                     Order Related Methods
	 * *****************************************************************************
	 */
	
	@Override
	public Long save(final Byte workshopId, final Order order) {
		
		final String query = baseDao.getQueryById("save");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("userId", order.getUserId()).
				addValue("pickUpDate", order.getPickUpDate()).
				addValue("pickUpTimeSlot", order.getPickUpTimeSlotId()).
				addValue("createDate", DateUtil.getEpochDate()).
				addValue("createTime", LocalTime.now().toSecondOfDay()).
				addValue("customerName", defaultStringIfNull(order.getCustomerName())).
				addValue("lastName", defaultStringIfNull(order.getLastName())).
				addValue("cc", order.getCc()).
				addValue("phone", order.getPhone()).
				addValue("alternatePhone", order.getAlternatePhone()).
				addValue("pickUpStreetAddress", defaultStringIfNull(order.getPickUpAddress().getStreetAddress())).
				addValue("pickUpCity", defaultStringIfNull(order.getPickUpAddress().getCity())).
				addValue("pickUpState", defaultStringIfNull(order.getPickUpAddress().getState())).
				addValue("pickUpPin", defaultStringIfNull(order.getPickUpAddress().getPin())).
				addValue("pickUpLandmark", defaultStringIfNull(order.getPickUpAddress().getLandmark())).
				addValue("pickUpLocality", defaultIntegerIfNull(order.getPickUpAddress().getLocality())).
				addValue("pickUpLatitude", order.getPickUpAddress().getLatitude()).
				addValue("pickUpLongitude", order.getPickUpAddress().getLongitude()).
				addValue("deliveryStreetAddress", defaultStringIfNull(order.getDeliveryAddress().getStreetAddress())).
				addValue("deliveryCity", defaultStringIfNull(order.getDeliveryAddress().getCity())).
				addValue("deliveryState", defaultStringIfNull(order.getDeliveryAddress().getState())).
				addValue("deliveryPin", defaultStringIfNull(order.getDeliveryAddress().getPin())).
				addValue("deliveryLandmark", defaultStringIfNull(order.getDeliveryAddress().getLandmark())).
				addValue("deliveryLocality", defaultIntegerIfNull(order.getDeliveryAddress().getLocality())).
				addValue("deliveryLatitude", order.getDeliveryAddress().getLatitude()).
				addValue("deliveryLongitude", order.getDeliveryAddress().getLongitude()).
				addValue("createrUserId", order.getCreaterUserId()).
				addValue("referredByUserId", order.getReferredByUserId()).
				addValue("workshopId", workshopId);
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		baseDao.getJdbcTemplate().update(query, source, keyHolder, ORDER_ID_COULUMN_NAME);
		return keyHolder.getKey().longValue();
	}
	
	
	@Override
	public void update(final Byte workshopId, final Order order) {
		 // Get Current Order 
		final Optional<Order> optional = findById(workshopId, order.getId());
		if(!optional.isPresent())
			return;
		final Order orgOrder = optional.get();
		final MapSqlParameterSource sourceMap =  new MapSqlParameterSource();
		updateIfNotNull("pickUpDate", sourceMap, createUpdatingLongSupplier(
				order.getPickUpDate(), orgOrder.getPickUpDate()));
		updateIfNotNull("pickUpTimeSlot", sourceMap, createUpdatingByteSupplier(
				order.getPickUpTimeSlotId(), orgOrder.getPickUpTimeSlotId()));
		updateIfNotNull("customerName", sourceMap, createUpdatingStringSupplier(
				order.getCustomerName(), orgOrder.getCustomerName()));
		updateIfNotNull("lastName", sourceMap, createUpdatingStringSupplier(
				order.getLastName(), orgOrder.getLastName()));
		updateIfNotNull("cc", sourceMap, createUpdatingStringSupplier(
				order.getCc(), orgOrder.getCc()));
		updateIfNotNull("phone", sourceMap, createUpdatingLongSupplier(
				order.getPhone(), orgOrder.getPhone()));
		updateIfNotNull("alternatePhone", sourceMap, createUpdatingLongSupplier(
				order.getAlternatePhone(), orgOrder.getAlternatePhone()));
		updateIfNotNull("pickUpStreetAddress", sourceMap, createUpdatingStringSupplier(
				order.getPickUpAddress().getStreetAddress(), orgOrder.getPickUpAddress().getStreetAddress()));
		updateIfNotNull("pickUpCity", sourceMap, createUpdatingStringSupplier(
				order.getPickUpAddress().getCity(), orgOrder.getPickUpAddress().getCity()));
		updateIfNotNull("pickUpState", sourceMap, createUpdatingStringSupplier(
				order.getPickUpAddress().getState(), orgOrder.getPickUpAddress().getState()));
		updateIfNotNull("pickUpPin", sourceMap, createUpdatingStringSupplier(
				order.getPickUpAddress().getPin(), orgOrder.getPickUpAddress().getPin()));
		updateIfNotNull("pickUpLandmark", sourceMap, createUpdatingStringSupplier(
				order.getPickUpAddress().getLandmark(), orgOrder.getPickUpAddress().getLandmark()));
		updateIfNotNull("pickUpLocality", sourceMap, createUpdatingIntegerSupplier(
				order.getPickUpAddress().getLocality(), orgOrder.getPickUpAddress().getLocality()));
		updateIfNotNull("pickUpLatitude", sourceMap, createUpdatingDoubleSupplier(
				order.getPickUpAddress().getLatitude(), orgOrder.getPickUpAddress().getLatitude()));
		updateIfNotNull("pickUpLongitude", sourceMap, createUpdatingDoubleSupplier(
				order.getPickUpAddress().getLongitude(), orgOrder.getPickUpAddress().getLongitude()));
		updateIfNotNull("deliveryStreetAddress", sourceMap, createUpdatingStringSupplier(
				order.getDeliveryAddress().getStreetAddress(), orgOrder.getDeliveryAddress().getStreetAddress()));
		updateIfNotNull("deliveryCity", sourceMap, createUpdatingStringSupplier(
				order.getDeliveryAddress().getCity(), orgOrder.getDeliveryAddress().getCity()));
		updateIfNotNull("deliveryState", sourceMap, createUpdatingStringSupplier(
				order.getDeliveryAddress().getState(), orgOrder.getDeliveryAddress().getState()));
		updateIfNotNull("deliveryPin", sourceMap, createUpdatingStringSupplier(
				order.getDeliveryAddress().getPin(), orgOrder.getDeliveryAddress().getPin()));
		updateIfNotNull("deliveryLandmark", sourceMap, createUpdatingStringSupplier(
				order.getDeliveryAddress().getLandmark(), orgOrder.getDeliveryAddress().getLandmark()));
		updateIfNotNull("deliveryLocality", sourceMap, createUpdatingIntegerSupplier(
				order.getDeliveryAddress().getLocality(), orgOrder.getDeliveryAddress().getLocality()));		
		updateIfNotNull("deliveryLatitude", sourceMap, createUpdatingDoubleSupplier(
				order.getDeliveryAddress().getLatitude(), orgOrder.getDeliveryAddress().getLatitude()));
		updateIfNotNull("deliveryLongitude", sourceMap, createUpdatingDoubleSupplier(
				order.getDeliveryAddress().getLongitude(), orgOrder.getDeliveryAddress().getLongitude()));
		
		/*updateIfNotNull("adjustment", sourceMap, createUpdatingSupplier(
				order.getAdjustment(), orgOrder.getAdjustment(), BigDecimal.ZERO));
		updateIfNotNull("adjustmentType", sourceMap, createUpdatingStringSupplier(
				order.getAdjustmentType() == null ? null :	order.getAdjustmentType().toString(),
						orgOrder.getAdjustmentType() == null ? null : 
							orgOrder.getAdjustmentType().toString()));*/
		/*updateIfNotNull("badDebts", sourceMap, createUpdatingSupplier(
				order.getBadDebts(), orgOrder.getBadDebts(), BigDecimal.ZERO));*/
		sourceMap.addValue("id", order.getId());
		final String query = baseDao.getQueryById("update");
		baseDao.getJdbcTemplate().update(query, sourceMap);
	}
	
	@Override
	public Optional<Order> findById(final Byte workshopId, final Long orderId) {
		try {
			final String query = baseDao.getQueryById("find.by.id");
			final SqlParameterSource source = new MapSqlParameterSource().
					addValue("id", orderId);
			return Optional.ofNullable(baseDao.getJdbcTemplate().
					queryForObject(query, source, ORDER_ROW_MAPPER));
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("Order not found for Order-ID : {} ", orderId);
			return Optional.empty();
		}
	}
	
	@Override
	public Optional<Order> findDetailById(final Byte workshopId, final Long orderId) {
		try {
			final String query = baseDao.getQueryById("find.detail.by.id");
			final SqlParameterSource source = new MapSqlParameterSource().
					addValue("id", orderId);
			return baseDao.getJdbcTemplate()
					.query(query, source, ORDERS_DETAIL_RESULT_SET_EXTRACTOR).stream().findFirst();
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("Order not found for orderId : {} ", orderId);
			return Optional.empty();
		}
	}
	
	
	/**
	 * *****************************************************************************
	 * 	                     Order Item Related Methods
	 * *****************************************************************************
	 */

	@Override
	public Long saveItem(final Byte workshopId, final Long orderId, final Item item) {
		
		final String query = baseDao.getQueryById("save.order.item");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("orderId", orderId).
				addValue("state", item.getState()).
				addValue("igst", item.getGST().getIgst()).
				addValue("cgst", item.getGST().getCgst()).
				addValue("sgst", item.getGST().getSgst()).
				addValue("createDate", DateUtil.getEpochDate()).
				addValue("createTime", LocalTime.now().toSecondOfDay()).
				addValue("deliveryDate", item.getEstDeliveryDate()).
				addValue("deliveryTime", defaultIntegerIfNull(item.getEstDeliveryTime())).
				addValue("estDeliveryDate", item.getEstDeliveryDate()).
				addValue("estDeliveryTime", defaultIntegerIfNull(item.getEstDeliveryTime())).
				addValue("convenienceCharge", item.getConvenienceCharge()).
				addValue("expressProcessingCharge", 
						defaultIfNull(item.getExpressProcessingCharge(), BigDecimal.ZERO)).
				addValue("packingCharge", 
						defaultIfNull(item.getPackingCharge(), BigDecimal.ZERO)).
				addValue("parentServiceId", item.getParentServiceId()).
				addValue("categoryId", item.getCategoryId()).
				addValue("productId", item.getProductId()).
				addValue("size", defaultIntegerIfNull(item.getSize())).
				addValue("rating", defaultByteIfNull(item.getRating())).
				addValue("priority", defaultByteIfNull(item.getPriority())).
				addValue("brandId", item.getBrandId()).
				addValue("isExpressProcessing", defaultBooleanIfNull(item.isExpressProcessing())).
				addValue("packing", defaultBooleanIfNull(item.isPacking())).
				addValue("itemSizeType", defaultIfNull(item.getSizeType(),Size.NO_SIZE).getCode()).
				addValue("costCredited", defaultIfNull(item.getCostCreadited(), BigDecimal.ZERO)).
				addValue("badDebts", defaultIfNull(item.getBadDebts(),BigDecimal.ZERO)).
				addValue("adjustment", defaultIfNull(item.getAdjustment(),BigDecimal.ZERO)).
				addValue("adjustmentType", item.getAdjustmentType() == null ? 
						null : item.getAdjustmentType().toString());
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		baseDao.getJdbcTemplate().update(query, source, keyHolder, ORDER_ITEM_ID_COULUMN_NAME);
		return keyHolder.getKey().longValue();
	}
	
	
	@Override
	public void updateItem(final Byte workshopId, final Item item) {
		final Optional<Item> optional = findItemById(workshopId, item.getId());
		if(!optional.isPresent()) {
			return;
		}
		final Item orgItem = optional.get();
		final MapSqlParameterSource sourceMap =  new MapSqlParameterSource();
//		updateIfNotNull("taxPercentage", sourceMap, createUpdatingBigDecimalSupplier(
//				item.getTaxPercentage(), orgItem.getTaxPercentage()));
		updateIfNotNull("deliveryDate", sourceMap, createUpdatingLongSupplier(
				item.getDeliveryDate(), orgItem.getDeliveryDate()));
		updateIfNotNull("deliveryTime", sourceMap, createUpdatingIntegerSupplier(
				item.getDeliveryTime(), orgItem.getDeliveryTime()));
		updateIfNotNull("estDeliveryDate", sourceMap, createUpdatingLongSupplier(
				item.getEstDeliveryDate(), orgItem.getEstDeliveryDate()));
		
		updateIfNotNull("estDeliveryTime", sourceMap, createUpdatingIntegerSupplier(
				item.getEstDeliveryTime(), orgItem.getEstDeliveryTime()));
		
		updateIfNotNull("deliveryTimeslot", sourceMap, createUpdatingByteSupplier(
				item.getDeliveryTimeSlotId(), orgItem.getDeliveryTimeSlotId()));
		
		updateIfNotNull("convenienceCharge", sourceMap, createUpdatingBigDecimalSupplier(
				item.getConvenienceCharge(), orgItem.getConvenienceCharge()));
		updateIfNotNull("expressProcessingCharge", sourceMap, createUpdatingBigDecimalSupplier(
				item.getExpressProcessingCharge(), orgItem.getExpressProcessingCharge()));
		updateIfNotNull("parentServiceId", sourceMap, createUpdatingIntegerSupplier(
				item.getParentServiceId(), orgItem.getParentServiceId()));
		updateIfNotNull("categoryId", sourceMap, createUpdatingIntegerSupplier(
				item.getCategoryId(), orgItem.getCategoryId()));
		updateIfNotNull("productId", sourceMap, createUpdatingIntegerSupplier(
				item.getProductId(), orgItem.getProductId()));
		updateIfNotNull("size", sourceMap, createUpdatingIntegerSupplier(
				item.getSize(), orgItem.getSize()));
		updateIfNotNull("brandId", sourceMap, createUpdatingIntegerSupplier(
				item.getBrandId(), orgItem.getBrandId()));
		updateIfNotNull("itemSizeType", sourceMap, createUpdatingIntegerSupplier(
				item.getSizeType() == null ? null :	item.getSizeType().getCode(),
						orgItem.getSizeType() == null ? null : 
							orgItem.getSizeType().getCode()));
		updateIfNotNull("isExpressProcessing", sourceMap, createUpdatingBooleanSupplier(
				item.isExpressProcessing(), orgItem.isExpressProcessing()));
		updateIfNotNull("packing", sourceMap, createUpdatingBooleanSupplier(
				item.isPacking(), orgItem.isPacking()));
		updateIfNotNull("packingCharge", sourceMap, createUpdatingBigDecimalSupplier(
				item.getPackingCharge(), orgItem.getPackingCharge()));
		updateIfNotNull("rating", sourceMap, createUpdatingByteSupplier(
				item.getRating(), orgItem.getRating()));
		updateIfNotNull("priority", sourceMap, createUpdatingByteSupplier(
				item.getPriority(), orgItem.getPriority()));
		
		sourceMap.addValue("id", item.getId());
		final String query = baseDao.getQueryById("update.order.item.by.id");
		baseDao.getJdbcTemplate().update(query, sourceMap);
		
	}
	
	@Override
	public void updateItemState(final Byte workshopId, final Long itemId, final Byte state) {
		
		final String query = baseDao.getQueryById("update.order.item.state.by.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("id", itemId).
				addValue("state", state);
		baseDao.getJdbcTemplate().update(query, source);
	}

	@Override
	public void updateItemsStateByOrder(final Byte workshopId, final Long orderId, final Byte state) {
		final String query = baseDao.getQueryById("update.order.items.state.by.order.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("orderId", orderId).
				addValue("state", state);
		baseDao.getJdbcTemplate().update(query, source);
	}

	@Override
	public Optional<Item> findItemById(final Byte workshopId, final Long itemId) {
		final String query = baseDao.getQueryById("find.order.item.by.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("itemId", itemId);
		return baseDao.getJdbcTemplate().query(query, source, ITEM_RESULT_SET_EXTRACTOR).stream().findFirst();
	}
	@Override
	public Collection<Item> findItemsByOrderId(final Byte workshopId, final Long orderId) {
		final String query = baseDao.getQueryById("find.order.all.items.by.order.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("orderId", orderId);
		return baseDao.getJdbcTemplate().query(query, source, ITEM_ROM_MAPPER);
	}
	
	@Override
	public List<Long> findItemIdsByOrderId(final Byte workshopId, final Long orderId) {
		final String query = baseDao.getQueryById("find.order.all.items.ids.by.order.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("orderId", orderId);
		return baseDao.getJdbcTemplate().queryForList(query, source, Long.class);
	}
	
	/**
	 * *****************************************************************************************************
	 * 	                     Order Item Service Related Methods
	 * *****************************************************************************************************
	 */
	

	@Override
	public void saveItemServices(final Byte workshopId, final Long itemId, final List<WedoService> services) {
		services.stream().forEach(service -> saveItemService(itemId, service));
	}
	
	private void saveItemService(final Long itemId, final WedoService service) {
		
		final String query = baseDao.getQueryById("save.order.item.service");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("itemId", itemId).
				addValue("serviceId", service.getServiceId()).
				addValue("quantity", service.getQuantity()).
				addValue("cost", service.getCostPerService());
		baseDao.getJdbcTemplate().update(query, source);
	}
	
	@Override
	public void updateItemService(final Byte workshopId, final Long itemId, final Long serviceId,
			final WedoService wedoService) {	
		final Optional<WedoService> optional = findItemService(workshopId,itemId, serviceId);
		if(!optional.isPresent()) {
			return;
		}
		final WedoService orgService = optional.get();
		final MapSqlParameterSource sourceMap =  new MapSqlParameterSource();
		updateIfNotNull("quantity", sourceMap, createUpdatingShortSupplier(
				wedoService.getQuantity(), orgService.getQuantity()));
		sourceMap.addValue("itemId", itemId);
		sourceMap.addValue("serviceId", serviceId);
		final String query = baseDao.getQueryById("update.order.item.service");
		baseDao.getJdbcTemplate().update(query, sourceMap);
	}

	@Override
	public void deleteItemServicesByItem(final Byte workshopId, final Long itemId) {
		final String query = baseDao.getQueryById("delete.order.item.services.by.item.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("itemId", itemId);
		baseDao.getJdbcTemplate().update(query, source);
	}
	
	@Override
	public void deleteItemService(final Byte workshopId, final Long itemId, final Long serviceId) {
		final String query = baseDao.
				getQueryById("delete.order.item.service.by.item.id.and.service.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("itemId", itemId).
				addValue("serviceId", serviceId);
		baseDao.getJdbcTemplate().update(query, source);		
	}

	@Override
	public Optional<WedoService> findItemService(final Byte workshopId, final Long itemId, final Long serviceId) {
		try {
			final String query = baseDao.getQueryById("find.order.item.service.by.item.id.and.service.id");
			final SqlParameterSource source = new MapSqlParameterSource().
					addValue("itemId", itemId).
					addValue("serviceId", serviceId);
			return Optional.ofNullable(baseDao.getJdbcTemplate()
					.queryForObject(query, source, SERVICE_ROW_MAPPER));
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("Service not found for itemId : {} and ServiceId {}", itemId, serviceId);
			return Optional.empty();
		}
	}
	
	@Override
	public Collection<WedoService> findItemServicesByItem(final Byte workshopId, final Long itemId) {
			final String query = baseDao.getQueryById("find.order.item.services.by.item.id");
			final SqlParameterSource source = new MapSqlParameterSource().
					addValue("itemId", itemId);
			return baseDao.getJdbcTemplate().query(query, source, SERVICE_ROW_MAPPER);
	}

	
	
	/**
	 * *****************************************************************************************************
	 * 	                     Order Item Images Related Methods
	 * *****************************************************************************************************
	 */
	
	@Override
	public void saveItemImages(final Byte workshopId, final Long itemId, final List<Image> images) {
		images.stream().forEach(image -> saveItemImage(itemId, image));
	}
	
	private void saveItemImage(final Long itemId, final Image image) {
		final String query = baseDao.getQueryById("save.order.item.image");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("itemId", itemId).
				addValue("hresUrl", defaultStringIfNull(image.getHresUrl())).
				addValue("lresUrl", defaultStringIfNull(image.getLresUrl())).
				addValue("description", defaultStringIfNull(image.getDescription())).
				addValue("type", defaultIfNull(image.getType(),
						ImageType.defaultType()).getCode());
		baseDao.getJdbcTemplate().update(query, source);
	}
	
	@Override
	public Collection<Image> findItemImagesByItem(final Byte workshopId, final Long itemId) {
		final String query = baseDao.getQueryById("find.order.item.images.by.item.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("itemId", itemId);
		return baseDao.getJdbcTemplate().query(query, source, IMAGE_ROW_MAPPER);
	}


	@Override
	public void updateItemImage(final Byte workshopId, final Long itemId, final Image image) {
		final Optional<Image> optional = findItemImage(workshopId, image.getId());
		if(!optional.isPresent()) {
			return;
		}
		final Image orgImage = optional.get();
		final MapSqlParameterSource sourceMap =  new MapSqlParameterSource();
		updateIfNotNull("hresUrl", sourceMap, createUpdatingStringSupplier(
				image.getHresUrl(), orgImage.getHresUrl()));
		updateIfNotNull("lresUrl", sourceMap, createUpdatingStringSupplier(
				image.getLresUrl(), orgImage.getLresUrl()));
		updateIfNotNull("description", sourceMap, createUpdatingStringSupplier(
				image.getDescription(), orgImage.getDescription()));
		sourceMap.addValue("type", createUpdatingSupplier(image.getType(), 
				orgImage.getType(), ImageType.defaultType()).get().getCode());
		sourceMap.addValue("id", image.getId());
		final String query = baseDao.getQueryById("update.order.item.image");
		baseDao.getJdbcTemplate().update(query, sourceMap);
	}

	@Override
	public void deleteItemImage(final Byte workshopId, final Long imageId) {
		final String query = baseDao.getQueryById("delete.order.item.image.by.image.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("id", imageId);
		baseDao.getJdbcTemplate().update(query, source);
	}

	@Override
	public void deleteItemImagesByItem(final Byte workshopId, final Long itemId) {
		final String query = baseDao.getQueryById("delete.order.item.images.by.item.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("itemId", itemId);
		baseDao.getJdbcTemplate().update(query, source);
	}

	@Override
	public Optional<Image> findItemImage(final Byte workshopId, final Long imageId) {		
		try {
			final String query = baseDao.getQueryById("find.order.item.image.by.image.id");
			final SqlParameterSource source = new MapSqlParameterSource().
					addValue("id", imageId);
			return Optional.ofNullable(baseDao.getJdbcTemplate()
					.queryForObject(query, source, IMAGE_ROW_MAPPER));
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("Image not found for ImageId := {} ", imageId);
			return Optional.empty();
		}
	}

	@Override
	public Collection<Image> findItemImagesByItem(final Byte workshopId, final Long itemId,
			final ImageType imageType) {
		final String query = baseDao.getQueryById("find.order.item.images.by.item.id.and.image.type");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("itemId", itemId).
				addValue("imageType", imageType.getCode());
		return baseDao.getJdbcTemplate().query(query, source, IMAGE_ROW_MAPPER);
	}
	
	
	
	
	/**
	 * *****************************************************************************************************
	 * 	                     Order Payment Detail Related Methods
	 * *****************************************************************************************************
	 */

	@Override
	public void addAdvancedPayment(final Byte workshopId, final Long orderId, final BigDecimal amount) {
		final String query = baseDao.getQueryById("save.order.advanced.payment");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("orderId", orderId).
				addValue("amount", amount);
		baseDao.getJdbcTemplate().update(query, source);
	}

	@Override
	public Long addPaymentTransaction(final Byte workshopId, final Long orderId,final Long itemId,final Payment payment) {
		final String query = baseDao.getQueryById("save.order.payment.transaction");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("orderId", orderId).
				addValue("userId", payment.getUserId()).
				addValue("itemId", defaultLongIfNull(itemId)).
				addValue("txnId", defaultStringIfNull(payment.getTxnId())).
				addValue("mode", payment.getMode().getCode()).
				addValue("amount", payment.getAmount()).
				addValue("date", Instant.now().getEpochSecond()).
				addValue("type", payment.getTransactionType().getCode()).
				addValue("description", defaultStringIfNull(payment.getDescription())).
				addValue("status", payment.getStatus());
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		baseDao.getJdbcTemplate().update(query, source, keyHolder, ORDER_ID_COULUMN_NAME);
		return keyHolder.getKey().longValue();
	}

	@Override
	public void addItemPayment(final Byte workshopId, final Long itemId, final BigDecimal amount) {
		final String query = baseDao.getQueryById("save.order.item.payment");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("costCredited", amount).
				addValue("itemId", itemId);
		baseDao.getJdbcTemplate().update(query, source);
	}
	
	/**
	 * *****************************************************************************************************
	 * 	                     Order Item Discounts Related Methods
	 * *****************************************************************************************************
	 */
	
	@Override
	public void saveItemDiscount(final Byte workshopId, final Long itemId, final List<Discount> discounts) {
		discounts.stream().forEach(discnt -> saveItemDiscount(itemId, discnt));
	}
	
	private void saveItemDiscount(final Long itemId, final Discount discount) {
	
		final String query = baseDao.getQueryById("save.order.item.discount");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("id", discount.getId()).
				addValue("itemId", itemId).
				addValue("value", discount.getValue()).
				addValue("valueType", discount.getValueType().toString());
		baseDao.getJdbcTemplate().update(query, source);
	}
	
	@Override
	public Collection<Discount> findItemDiscountByItem(final Byte workshopId, final Long itemId) {
		final String query = baseDao.getQueryById("find.order.item.discounts.by.item.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("itemId", itemId);
		return baseDao.getJdbcTemplate().query(query, source, DISCOUNT_ROW_MAPPER);
	}

	/*@Override
	public Collection<Order> findByUserId(final Byte workshopId, final Long userId, final Integer offset,
			final Integer limit) {
		final String query = baseDao.getQueryById("find.by.user.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("userId", userId).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}*/

	@Override
	public Long saveItemStateTransition(final Byte workshopId, final Long itemId, 
			final StateTransition transition) {
		final String query = baseDao.getQueryById("save.order.item.state.transition");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("itemId", itemId).
				addValue("userId", transition.getUserId()).
				addValue("state", transition.getState()).
				addValue("colPointId", defaultByteIfNull(transition.getColPointId())).
				addValue("workshopId", defaultByteIfNull(transition.getWorkshopId())).
				addValue("createDate", Instant.now().getEpochSecond()).
				addValue("comment", defaultStringIfNull(transition.getComment()));
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		baseDao.getJdbcTemplate().update(query, source, keyHolder, 
				ORDER_ITEM_STATE_TRANSITION_ID_COULUMN_NAME);
		return keyHolder.getKey().longValue();
	}

	@Override
	public void updateItemStateTransition(final Byte workshopId, final Long itemId, final Long transitionId,
			final StateTransition transition) {
		final Optional<StateTransition> optional = findItemStateTransition(workshopId, itemId, 
				transitionId);
		if(!optional.isPresent()) {
			return;
		}
		final StateTransition orgTransition = optional.get();
		final MapSqlParameterSource sourceMap =  new MapSqlParameterSource();
		updateIfNotNull("comment", sourceMap, createUpdatingStringSupplier(
				transition.getComment(), orgTransition.getComment()));
		sourceMap.addValue("transitionId", transitionId);
		sourceMap.addValue("itemId", itemId);
		final String query = baseDao.getQueryById("update.order.item.state.transition");
		baseDao.getJdbcTemplate().update(query, sourceMap);
	}

	@Override
	public Optional<StateTransition> findItemStateTransition(final Byte workshopId, final Long itemId,
			final Long transitionId) {
		try {
			final String query = baseDao.getQueryById("find.order.item.state.transition.by.transition.id");
			final SqlParameterSource source = new MapSqlParameterSource().
					addValue("transitionId", transitionId).
					addValue("itemId", itemId);
			return Optional.ofNullable(baseDao.getJdbcTemplate()
					.queryForObject(query, source, ITEM_STATE_TRANSITION_ROW_MAPPER));
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("Item State Transition  not found for transitionId = {} and itemId= {}",
					transitionId, itemId);
			return Optional.empty();
		}
	}

	@Override
	public Collection<StateTransition> findItemStateTransitions(final Byte workshopId, final Long itemId) {
		final String query = baseDao.getQueryById("find.order.item.state.transitions.by.item.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("itemId", itemId);
		return baseDao.getJdbcTemplate().query(query, source, ITEM_STATE_TRANSITION_ROW_MAPPER);
	}

	@Override
	public List<StateTransition> findItemExternalStateTransitions(final Byte workshopId, final Long itemId) {
		final String query = baseDao.getQueryById("find.order.item.external.state.transitions.by.item.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("itemId", itemId);
		return baseDao.getJdbcTemplate().query(query, source, ITEM_STATE_TRANSITION_RESULT_SET_EXTRACTOR);
	}

	@Override
	public Optional<Order> findByItemId(final Byte workshopId, final Long itemId) {
		final String query = baseDao.getQueryById("find.by.item.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("itemId", itemId);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER)
				.stream().findFirst();
	}

	@Override
	public Collection<Order> findByPhone(final Byte workshopId,final String cc, final Long phone,
			final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.phone");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("cc", cc).
				addValue("phone", phone).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByPhone(final Byte workshopId,final String cc, final Long phone, final Byte itemState,
			final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.phone.and.item.state");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("cc", cc).
				addValue("phone", phone).
				addValue("state", itemState).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByPhoneAndCreateDate(final Byte workshopId,final String cc, final Long phone,
			final Long createDate, final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.phone.and.create.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("cc", cc).
				addValue("phone", phone).
				addValue("createDate", createDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByPhoneAndPickUpDate(final Byte workshopId, final String cc, final Long phone,
			final Long pickUpDate, final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.phone.and.pick.up.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("cc", cc).
				addValue("phone", phone).
				addValue("pickUpDate", pickUpDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByPhoneAndDeliveryDate(final Byte workshopId,final String cc, final Long phone,
			final Long deliveryDate, final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.phone.and.delivery.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("cc", cc).
				addValue("phone", phone).
				addValue("deliveryDate", deliveryDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByCustomerName(final Byte workshopId,final String customerName,
			final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.customer.name");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("customerName", customerName).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByCustomerName(final Byte workshopId,final String customerName,
			final Byte state, final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.customer.name.and.item.state");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("customerName", customerName).
				addValue("state", state).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByCustomerNameAndCreateDate(final Byte workshopId,
			final String customerName, final Long createDate, final Integer offset, 
			final Integer limit) {
		final String query = baseDao.getQueryById("find.by.customer.name.and.create.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("customerName", customerName).
				addValue("createDate", createDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByCustomerNameAndPickUpDate(final Byte workshopId,
			final String customerName, final Long pickUpDate, final Integer offset, 
			final Integer limit) {
		final String query = baseDao.getQueryById("find.by.customer.name.and.pick.up.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("customerName", customerName).
				addValue("pickUpDate", pickUpDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByCustomerNameAndDeliveryDate(final Byte workshopId,
			final String customerName, final Long deliveryDate, final Integer offset,
			final Integer limit) {
		final String query = baseDao.getQueryById("find.by.customer.name.and.delivery.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("customerName", customerName).
				addValue("deliveryDate", deliveryDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByUserId(final Byte workshopId,final Long userId, final Byte state,
			final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.user.id.and.item.state");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("userId", userId).
				addValue("state", state).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByUserIdAndCreateDate(final Byte workshopId,final Long userId,
			final Long createDate, final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.user.id.and.create.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("userId", userId).
				addValue("createDate", createDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByUserIdAndDeliveryDate(final Byte workshopId,final Long userId,
			final Long deliveryDate, final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.user.id.and.delivery.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("userId", userId).
				addValue("deliveryDate", deliveryDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByUserIdAndPickUpDate(final Byte workshopId,final Long userId,
			final Long pickUpDate, final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.user.id.and.pick.up.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("userId", userId).
				addValue("pickUpDate", pickUpDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByCreaterUserId(final Byte workshopId,final Long userId, final Integer offset,
			final Integer limit) {
		final String query = baseDao.getQueryById("find.by.creater.user.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("createrByUserId", userId).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByCreaterUserId(final Byte workshopId,final Long userId, final Byte state,
			final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.creater.user.id.and.item.state");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("createrByUserId", userId).
				addValue("state", state).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByCreaterUserIdAndCreateDate(final Byte workshopId,final Long userId,
			final Long createDate, final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.creater.user.id.and.create.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("createrByUserId", userId).
				addValue("createDate", createDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByCreaterUserIdAndDeliveryDate(final Byte workshopId, final Long userId,
			final Long deliveryDate, final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.creater.user.id.and.delivery.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("createrByUserId", userId).
				addValue("deliveryDate", deliveryDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByCreaterUserIdAndPickUpDate(final Byte workshopId,final Long userId,
			final Long pickUpDate, final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.creater.user.id.and.pick.up.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("createrByUserId", userId).
				addValue("pickUpDate", pickUpDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByReferredByUserId(final Byte workshopId, final Long userId,
			final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.referred.by.user.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("referredByUserId", userId).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByReferredByUserId(final Byte workshopId,final Long userId, final Byte state,
			final Integer offset, Integer limit) {
		final String query = baseDao.getQueryById("find.by.referred.by.user.id.and.item.state");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("referredByUserId", userId).
				addValue("state", state).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByReferredByUserIdAndCreateDate(final Byte workshopId,final Long userId,
			final Long createDate, final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.referred.by.user.id.and.create.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("referredByUserId", userId).
				addValue("createDate", createDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByReferredByUserIdAndDeliveryDate(final Byte workshopId,final Long userId,
			final Long deliveryDate, final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.referred.by.user.id.and.delivery.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("referredByUserId", userId).
				addValue("deliveryDate", deliveryDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByReferredByUserIdAndPickUpDate(final Byte workshopId,final Long userId,
			final Long pickUpDate, final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.referred.by.user.id.and.pick.up.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("referredByUserId", userId).
				addValue("pickUpDate", pickUpDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByCreateDate(final Byte workshopId,final Long createDate, final Integer offset,
			final Integer limit) {
		final String query = baseDao.getQueryById("find.by.create.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("createDate", createDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByDeliveryDate(final Byte workshopId,final Long deliveryDate,
			final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.delivery.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("deliveryDate", deliveryDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByPickUpDate(final Byte workshopId,final Long pickUpDate, final Integer offset,
			final Integer limit) {
		final String query = baseDao.getQueryById("find.by.pick.up.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("pickUpDate", pickUpDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByDeliveryDateAndState(final Byte workshopId,final Long deliveryDate,
			final Byte state, final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.delivery.date.and.item.state");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("deliveryDate", deliveryDate).
				addValue("state", state).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByPickUpDateAndState(final Byte workshopId,final Long pickUpDate,
			final Byte state, final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.pick.up.date.and.item.state");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("pickUpDate", pickUpDate).
				addValue("state", state).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Item> findItemsByOrderId(final Byte workshopId, final Long orderId, final Integer offset,
			final Integer limit) {
		final String query = baseDao.getQueryById("find.order.items.by.order.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("orderId", orderId).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ITEM_ROM_MAPPER);
	}

	@Override
	public Collection<Item> findItemsByOrderIdAndDeliveryDate(final Byte workshopId, final Long orderId,
			final Long deliveryDate, final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.order.items.by.order.id.and.delivery.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("orderId", orderId).
				addValue("deliveryDate", deliveryDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ITEM_ROM_MAPPER);
	}

	@Override
	public Collection<Item> findItemsByOrderIdAndItemState(final Byte workshopId, final Long orderId,
			final Byte state, final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.order.items.by.order.id.and.item.state");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("orderId", orderId).
				addValue("state", state).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ITEM_ROM_MAPPER);
	}

	@Override
	public Collection<Item> findItemsByOrderIdAndItemState(final Byte workshopId, final Long orderId,
			final Byte state) {
		final String query = baseDao.getQueryById("find.all.items.of.order.by.order.id.and.item.state");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("orderId", orderId).
				addValue("state", state);
		return baseDao.getJdbcTemplate().query(query, source, ITEM_ROM_MAPPER);
	}
	
	@Override
	public Collection<Item> findItemsByDeliveryDate(final Byte workshopId,final Long deliveryDate,
			final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.order.items.by.delivery.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("deliveryDate", deliveryDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ITEM_ROM_MAPPER);
	}

	@Override
	public Collection<Item> findItemsByItemState(final Byte workshopId,final Byte state, final Integer offset,
			final Integer limit) {
		final String query = baseDao.getQueryById("find.order.items.by.item.state");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("state", state).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ITEM_ROM_MAPPER);
	}

	@Override
	public Collection<Item> findItemsByItemStateAndDate(final Byte workshopId,final Byte state, final Long date, final Integer offset,
			final Integer limit) {
		final String query = baseDao.getQueryById("find.order.items.by.item.state.and.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("state", state).
				addValue("date", date).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ITEM_ROM_MAPPER);
	}

	@Override
	public Collection<Order> findByItemState(final Byte workshopId,final Byte itemState, final Integer offset,
			final Integer limit) {
		final String query = baseDao.getQueryById("find.by.item.state");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("state", itemState).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByItemStateAndDate(final Byte workshopId,final Byte itemState, final Long fromDate, final Long toDate, final Integer offset,
			final Integer limit) {
		final String query = baseDao.getQueryById("find.by.item.state.and.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("state", itemState).
				addValue("fromDate", fromDate).
				addValue("toDate", toDate).				
				addValue("offSet", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findBy(final Byte workshopId,final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByUserId(final Byte workshopId, final Long userId,
			final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.user.id.and.workshop.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("userId", userId).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Optional<Order> findOrderCostDetail(final Byte workshopId, final Long orderId) {
		try {
			final String query = baseDao.getQueryById("find.order.cost.detail.by.id");
			final SqlParameterSource source = new MapSqlParameterSource().
					addValue("id", orderId);
			return baseDao.getJdbcTemplate()
					.query(query, source, ORDER_COSTING_DETAIL_RESULT_SET_EXTRACTOR);
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("Order not found for orderId : {}", orderId);
			return Optional.empty();
		}
	}
	
	@Override
	public Collection<Payment> findOrderPayments(final Byte workshopId, final Long orderId) {
		final String query = baseDao.getQueryById("get.payment.by.order.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("orderId", orderId);
		return baseDao.getJdbcTemplate().query(query, source,PAYMENT_ROW_MAPPER);
	}

	@Override
	public Optional<StateTransition> findItemStateTransition(final Byte workshopId, final Long itemId,
			final Byte state) {
		try {
			final String query = baseDao.getQueryById("find.order.item.state.transition.by.item.state");
			final SqlParameterSource source = new MapSqlParameterSource().
					addValue("state", state).
					addValue("itemId", itemId);
			return Optional.ofNullable(baseDao.getJdbcTemplate()
					.queryForObject(query, source, ITEM_STATE_TRANSITION_ROW_MAPPER));
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("Item State Transition  not found for transitionId = {} and itemId= {}",
					state, itemId);
			return Optional.empty();
		}
	}

	/**
	 * *****************************************************************************************************
	 * 	                     Invoice Related Methods
	 * *****************************************************************************************************
	 */
	
	@Override
	public void saveInvoice(final Byte workshopId,final Long orderId,final Long itemId) {	
			final String query = baseDao.getQueryById("save.invoice.data");
			final SqlParameterSource source = new MapSqlParameterSource()
			.addValue("id", ""+workshopId+"-"+orderId+"-"+itemId).
					addValue("itemId", itemId).
					addValue("orderId", orderId).
					addValue("workshopId", workshopId) 
					.addValue("createdDate",DateUtil.getEpochDate())
					.addValue("createdTime",LocalTime.now().toSecondOfDay());
			baseDao.getJdbcTemplate().update(query, source);
		}

	@Override
	public Collection<Invoice> getInvoicesByItemId(Long itemId) {
		final String query = baseDao.getQueryById("get.invoice.by.item.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("itemId", itemId);
		return baseDao.getJdbcTemplate().query(query, source,INVOICE_ROW_MAPPER);
	}

	@Override
	public Collection<Invoice> getInvoicesByOrderId(Byte workshopId,
			Long orderId) {
		final String query = baseDao.getQueryById("get.invoice.by.order.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).addValue("orderId", orderId); 
		return baseDao.getJdbcTemplate().query(query, source,INVOICE_ROW_MAPPER);
	}

	@Override
	public Collection<Invoice> getInvoicesByDateAndDuration(Long date,
			Integer duration) {
		final String query = baseDao.getQueryById("get.invoice.by.date.and.duration");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("date", date).addValue("duration", duration); 
		return baseDao.getJdbcTemplate().query(query, source,INVOICE_ROW_MAPPER);
	}

	@Override
	public Collection<Invoice> getCurrentDateInvoices() {
		final String query = baseDao.getQueryById("get.invoice.of.current.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("currentDate", Instant.now().toEpochMilli()); 
		return baseDao.getJdbcTemplate().query(query, source, INVOICE_ROW_MAPPER);
	}

	/**
	 * *****************************************************************************************************
	 * 	                     Order Item Defects Related Methods
	 * *****************************************************************************************************
	 */
	
	@Override
	public void saveItemDefects(final Byte workshopId, final Long itemId, 
			final List<Byte> defects) {
		defects.stream().forEach(defectId -> saveItemDefect(itemId, defectId));
	}

	private void saveItemDefect(final Long itemId, final Byte defectId) {
		final String query = baseDao.getQueryById("save.order.item.defect");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("itemId", itemId).
				addValue("defectId", defectId);
		baseDao.getJdbcTemplate().update(query, source);
	}
	
	@Override
	public Collection<Byte> findItemDefectsByItem(final Byte workshopId, final Long itemId) {
		final String query = baseDao.getQueryById("find.order.item.defects.by.item.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("itemId", itemId);
		return baseDao.getJdbcTemplate().queryForList(query, source, Byte.class);
	}

	@Override
	public Long saveFeedback(final Long userId, final Feedback feedback,
			final FeedbackType type) {
		final String query = baseDao.getQueryById("save.feedback");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("userId", userId).
				addValue("orderId", feedback.getOrderId()).
				addValue("itemId", feedback.getItemId()).
				addValue("name", feedback.getName()).
				addValue("email", feedback.getEmail()).
				addValue("cc", feedback.getCc()).
				addValue("phone", feedback.getPhone()).
				addValue("text", feedback.getText()).
				addValue("feedbackDate", Instant.now().getEpochSecond()).
				addValue("rating", feedback.getRating()).
				addValue("type", type!=null ? type.getCode():null); 
 		final KeyHolder keyHolder = new GeneratedKeyHolder();
		baseDao.getJdbcTemplate().update(query, source, keyHolder, ID_COULUMN_NAME);
		return keyHolder.getKey().longValue();
	}

	@Override
	public Optional<Feedback> getFeedback(final Long userId,final Byte workshopId, final Long orderId, final Long itemId) {
		try {
			final String query = baseDao.getQueryById("find.feedback.by.item.id");
			final SqlParameterSource source = new MapSqlParameterSource().
					addValue("itemId", itemId).
					addValue("orderId", orderId);
			return baseDao.getJdbcTemplate()
					.query(query, source, FEEDBACK_RESULT_SET_EXTRACTOR).stream().findFirst();
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("Feedback  not found for itemId= {}", itemId);
			return Optional.empty();
		}
	}

	@Override
	public Optional<LastDeliveredItem> getLastDeliveredItem(Long userId, Byte workshopId) {
		try {
			final String query = baseDao.getQueryById("find.last.delivered.item.by.user.id");
			final SqlParameterSource source = new MapSqlParameterSource().
					addValue("userId", userId);
			return Optional.ofNullable(baseDao.getJdbcTemplate()
					.queryForObject(query, source, LAST_DELIVERED_ITEM_ROW_MAPPER));
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("Delivered Item Not found for userId= {}",
					userId);
			return Optional.empty();
		}
	}
	
	@Override
	public void saveUsersLocation(final Long itemStateTransactionId, final Long userId, final UsersLocation usersLocation) {
		final String query = baseDao.getQueryById("save.users.location");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("itemStateTransactionId", itemStateTransactionId).
				addValue("userId", userId).
				addValue("latitude", usersLocation.getLatitude()).
				addValue("longitude", usersLocation.getLongitude());
		baseDao.getJdbcTemplate().update(query, source);
	}

	@Override
	public void applyCouponOnItem(final Byte workshopId,final Long orderId,final Long itemId,final Coupon coupon) {
		final String query = baseDao.getQueryById("apply.coupon.on.existing.item");
		final SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
				.addValue("discountId", coupon.getId())
				.addValue("itemId", itemId)
				.addValue("value", coupon.getValue())
				.addValue("valueType", coupon.getValueType().toString());
		baseDao.getJdbcTemplate().update(query, sqlParameterSource);
		
	}

	@Override
	public void removeAppliedCouponOnItem(final Long itemId, final Integer discountId) {
		final String query = baseDao.getQueryById("delete.applied.coupon.on.item.by.id");
		final SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
				.addValue("discountId", discountId)
				.addValue("itemId", itemId);
		baseDao.getJdbcTemplate().update(query, sqlParameterSource);
	}

	@Override
	public void updateOrdersLocality(final Byte workshopId, final Long localityId, final String pin) {
		final String query = baseDao.getQueryById("update.locality.in.all.orders");
		final SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
				.addValue("localityId", localityId)
				.addValue("pin", pin); 
		baseDao.getJdbcTemplate().update(query, sqlParameterSource);
		
	}
	
	@Override
    public Integer findUserOrderCounts(final Long userId) {
        final String query=baseDao.getQueryById("find.users.orders.count.by.user.id");
        final SqlParameterSource source=new MapSqlParameterSource().addValue("userId", userId);
        return baseDao.getJdbcTemplate().queryForObject(query, source, Integer.class);
    }
	
	@Override
    public Collection<PaymentTransaction> findPaymentTransactionsByStartAndEndDate(final Byte workshopId,
            final Long startDate, final Long endDate, final Integer offset,
            final Integer limit) {
        final String query = baseDao
                .getQueryById("find.payment.transactions.by.start.and.end.date");
        final SqlParameterSource source = new MapSqlParameterSource()
                .addValue("startDate", startDate).addValue("endDate", endDate)
                .addValue("offset", offset).addValue("limit", limit);
        return baseDao.getJdbcTemplate().query(query, source,
                PAYMENT_TRANSACTION_ROW_MAPPER);
    }
	
	@Override
	public Collection<PaymentTransaction> findPaymentTransactionsByOrderId(
			final Byte workshopId,final Long orderId, final Integer offset, final Integer limit) {
		 final String query = baseDao
	                .getQueryById("find.payment.transactions.by.order.id");
	        final SqlParameterSource source = new MapSqlParameterSource()
	                .addValue("orderId", orderId)
	                .addValue("offset", offset)
	                .addValue("limit", limit);
	        return baseDao.getJdbcTemplate().query(query, source,
	                PAYMENT_TRANSACTION_ROW_MAPPER);
	}

	@Override
	public Collection<PaymentTransaction> findPaymentTransactionsByItemId(final Byte workshopId,final Long itemId, final Integer offset, final Integer limit) {
		 final String query = baseDao
	                .getQueryById("find.payment.transactions.by.item.id");
	        final SqlParameterSource source = new MapSqlParameterSource()
	                .addValue("itemId", itemId)
	                .addValue("offset", offset)
	                .addValue("limit", limit);
	        return baseDao.getJdbcTemplate().query(query, source,
	                PAYMENT_TRANSACTION_ROW_MAPPER);
	}
	
	@Override
	public Collection<PaymentTransaction> findPaymentTransactionsByOrderIdAndDate(final Byte workshopId,final  Long orderId,final  Long startDate,final Long endDate,
			final Integer offset, final Integer limit) {
		 final String query = baseDao
	                .getQueryById("find.payment.transactions.by.order.id.and.start.and.end.date");
	        final SqlParameterSource source = new MapSqlParameterSource()
	                .addValue("orderId", orderId)
	                .addValue("startDate", startDate)
	                .addValue("endDate", endDate)
	                .addValue("offset", offset)
	                .addValue("limit", limit);
	        return baseDao.getJdbcTemplate().query(query, source,
	                PAYMENT_TRANSACTION_ROW_MAPPER);
	}

	@Override
	public BigDecimal getTotalOrderTxn(final Long orderId, final Long shipmentDate) {
		try {
			final String query = baseDao.getQueryById("find.total.order.txn.by.id.and.date");
			final SqlParameterSource source = new MapSqlParameterSource().
					addValue("orderId", orderId)
					.addValue("date", DateUtil.convertToDBDateString(shipmentDate));
			final BigDecimal b=  baseDao.getJdbcTemplate().queryForObject(query, source, BigDecimal.class);
			if(b!=null){
				return b;
			}
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("Payment Tnx Not found for orderId= {}",orderId);
		}
		return BigDecimal.ZERO;
	}

	@Override
	public BigDecimal getTotalTxnAmountOfDayByUserId(final Long userId, final Long shipmentDate) {
		try {
			final String query = baseDao.getQueryById("find.sum.of.txn.amount.by.user.id.and.txn.date");
			final SqlParameterSource source = new MapSqlParameterSource().
					addValue("userId", userId)
					.addValue("date", DateUtil.convertToDBDateString(shipmentDate));
			final BigDecimal b=  baseDao.getJdbcTemplate().queryForObject(query, source, BigDecimal.class);
			if(b!=null){
				return b;
			}
		} catch (EmptyResultDataAccessException e) {
			LOGGER.info("Payment Tnx Not found for orderId= {}",userId);
		}
		return BigDecimal.ZERO;
	}

	@Override
	public void updateItemRating(final Byte workshopId,final  Long orderId,final  Long itemId,
			final Short rating) {
		final String query = baseDao.getQueryById("update.item.rating.by.order.id.and.item.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("orderId", orderId).
				addValue("itemId", itemId).
				addValue("rating", rating.byteValue());
		baseDao.getJdbcTemplate().update(query, source);
		
	}

	@Override
	public Collection<Order> findByPhoneAndCreateDateAndState(final Byte workshopId,
			final String cc, final Long phone, final Long createDate, final Byte state, final Integer offset,
			final Integer limit) {
		final String query = baseDao.getQueryById("find.by.phone.and.create.date.and.state");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("cc", cc).
				addValue("phone", phone).
				addValue("createDate", createDate).
				addValue("state", state).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByPhoneAndPickUpDateAndState(final Byte workshopId,
			final String cc, final Long phone, final Long pickUpDate, final Byte state, final Integer offset,
			final Integer limit) {
		final String query = baseDao.getQueryById("find.by.phone.and.pick.up.date.and.state");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("cc", cc).
				addValue("phone", phone).
				addValue("pickUpDate",pickUpDate).
				addValue("state", state).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByPhoneAndDeliveryDateAndState(
			final Byte workshopId, final String cc, final Long phone,final  Long deliveryUpDate,
			final Byte state, final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.phone.and.delivery.date.and.state");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("cc", cc).
				addValue("phone", phone).
				addValue("deliveryDate",deliveryUpDate).
				addValue("state", state).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByUserIdAndCreateDateAndState(final Byte workshopId,
			final Long userId, final Long createDate, final Byte state, final Integer offset,
			final Integer limit) {
		final String query = baseDao.getQueryById("find.by.user.id.and.create.date.and.state");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("userId", userId).
				addValue("createDate", createDate).
				addValue("state", state).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}
		

	@Override
	public Collection<Order> findByUserIdAndPickUpDateAndState(final Byte workshopId,
			final Long userId, final Long pickUpDate,final  Byte state, final Integer offset,
			final Integer limit) {
		final String query = baseDao.getQueryById("find.by.user.id.and.pick.up.date.and.state");
			final SqlParameterSource source = new MapSqlParameterSource().
					addValue("workshopId", workshopId).
					addValue("userId", userId).
					addValue("pickUpDate", pickUpDate).
					addValue("state", state).
					addValue("offset", offset).
					addValue("limit", limit);
			return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
		}

	@Override
	public Collection<Order> findByUserIdAndDeliveryDateAndState(
			final Byte workshopId, final  Long userId, final Long deliveryDate, final Byte state,
			final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.user.id.and.delivery.date.and.state");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("userId", userId).
				addValue("deliveryDate", deliveryDate).
				addValue("state", state).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByCustomerNameAndCreateDateAndState(final Byte workshopId, final String customerName, final Long createDate,final  Byte state,
			final Integer offset,final  Integer limit) {
		final String query = baseDao.getQueryById("find.by.customer.name.and.create.date.and.state");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("customerName", customerName).
				addValue("createDate", createDate).
				addValue("state", state).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByCustomerNameAndPickUpDateAndState(final Byte workshopId, final String customerName,final  Long pickUpDate, final Byte state,final 
			Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.customer.name.and.pick.up.date.and.state");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("customerName", customerName).
				addValue("pickUpDate", pickUpDate).
				addValue("state", state).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByCustomerNameAndDeliveryDateAndState(final Byte workshopId,final String customerName,final Long deliveryDate,final 	Byte state, Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.customer.name.and.delivery.date.and.state");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("customerName", customerName).
				addValue("deliveryDate", deliveryDate).
				addValue("state", state).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public void addItemPaymentAdjustment(final Byte workshopId, final Long itemId,
			final BigDecimal amount,final Integer adjustmentType) {
		final String query = baseDao.getQueryById("save.item.payment.adjustment.by.item.id");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("adjustment", amount).
				addValue("adjustmentType", adjustmentType).
				addValue("itemId", itemId);
		baseDao.getJdbcTemplate().update(query, source);
	}

	@Override
	public Collection<Order> findByPhoneAndDateAndStateAndFilterByCreateDate(final Byte workshopId,final String cc,final Long phone,final Byte state,final Long fromDate,
			final Long toDate, final Integer offset,final  Integer limit) {
		final String query = baseDao.getQueryById("find.by.phone.and.date.and.state.and.filter.by.create.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("cc", cc).
				addValue("phone", phone).
				addValue("fromDate", fromDate).
				addValue("toDate", toDate).				
				addValue("state", state).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByPhoneAndDateAndStateAndFilterByPickUpDate(final Byte workshopId,final String cc,final Long phone,final Byte state,final Long fromDate,
			final Long toDate, final Integer offset,final  Integer limit) {
		final String query = baseDao.getQueryById("find.by.phone.and.date.and.state.and.filter.by.pick.up.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("cc", cc).
				addValue("phone", phone).
				addValue("fromDate", fromDate).
				addValue("toDate", toDate).				
				addValue("state", state).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByPhoneAndDateAndStateAndFilterByDeliveryDate(final Byte workshopId,final String cc,final Long phone,final Byte state,final Long fromDate,
			final Long toDate, final Integer offset,final  Integer limit) {
		final String query = baseDao.getQueryById("find.by.phone.and.date.and.state.and.filter.by.delivery.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("cc", cc).
				addValue("phone", phone).
				addValue("fromDate", fromDate).
				addValue("toDate", toDate).				
				addValue("state", state).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByPhoneAndDateAndFilterByCreateDate(final Byte workshopId,final String cc,final Long phone,final Long fromDate,
			final Long toDate, final Integer offset,final  Integer limit) {
		final String query = baseDao.getQueryById("find.by.phone.and.date.and.filter.by.create.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("cc", cc).
				addValue("phone", phone).
				addValue("fromDate", fromDate).
				addValue("toDate", toDate).				
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByPhoneAndDateAndFilterByPickUpDate(final Byte workshopId,final String cc,final Long phone,final Long fromDate,
			final Long toDate, final Integer offset,final  Integer limit) {
		final String query = baseDao.getQueryById("find.by.phone.and.date.and.filter.by.pick.up.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("cc", cc).
				addValue("phone", phone).
				addValue("fromDate", fromDate).
				addValue("toDate", toDate).				
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByPhoneAndDateAndFilterByDeliveryDate(final Byte workshopId,final String cc,final Long phone,final Long fromDate,
			final Long toDate, final Integer offset,final  Integer limit) {
		final String query = baseDao.getQueryById("find.by.phone.and.date.and.filter.by.delivery.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("cc", cc).
				addValue("phone", phone).
				addValue("fromDate", fromDate).
				addValue("toDate", toDate).				
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByUserIdAndDateAndStateAndFilterByCreateDate(final Byte workshopId,final Long userId,final Byte state,final Long fromDate,final Long toDate,final Integer offset,final Integer limit) {
		final String query = baseDao.getQueryById("find.by.user.id.and.date.and.state.and.filter.by.create.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("userId", userId).
				addValue("state", state).
				addValue("fromDate", fromDate).
				addValue("toDate", toDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByUserIdAndDateAndStateAndFilterByPickUpDate(final Byte workshopId,final Long userId,final Byte state,final Long fromDate,final Long toDate,final Integer offset,final Integer limit) {
		final String query = baseDao.getQueryById("find.by.user.id.and.date.and.state.and.filter.by.pick.up.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("userId", userId).
				addValue("state", state).
				addValue("fromDate", fromDate).
				addValue("toDate", toDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByUserIdAndDateAndStateAndFilterByDeliveryDate(final Byte workshopId,final Long userId,final Byte state,final Long fromDate,final Long toDate,final Integer offset,final Integer limit) {
		final String query = baseDao.getQueryById("find.by.user.id.and.date.and.state.and.filter.by.delivery.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("userId", userId).
				addValue("state", state).
				addValue("fromDate", fromDate).
				addValue("toDate", toDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByUserIdAndDateAndFilterByCreateDate(final Byte workshopId,final Long userId,final Long fromDate,final Long toDate,final Integer offset,final Integer limit) {
		final String query = baseDao.getQueryById("find.by.user.id.and.date.and.filter.by.create.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("userId", userId).
				addValue("fromDate", fromDate).
				addValue("toDate", toDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByUserIdAndDateAndFilterByPickUpDate(final Byte workshopId,final Long userId,final Long fromDate,final Long toDate,final Integer offset,final Integer limit) {
		final String query = baseDao.getQueryById("find.by.user.id.and.date.and.filter.by.pick.up.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("userId", userId).
				addValue("fromDate", fromDate).
				addValue("toDate", toDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByUserIdAndDateAndFilterByDeliveryDate(final Byte workshopId,final Long userId,final Long fromDate,final Long toDate,final Integer offset,final Integer limit) {
		final String query = baseDao.getQueryById("find.by.user.id.and.date.and.filter.by.delivery.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("userId", userId).
				addValue("fromDate", fromDate).
				addValue("toDate", toDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByCustomerNameAndDateAndStateAndFilterByCreateDate(final Byte workshopId,final String customerName,final Byte state,final Long fromDate,
			final Long toDate, final Integer offset,final Integer limit) {
		final String query = baseDao.getQueryById("find.by.customer.name.and.date.and.state.and.filter.by.create.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("customerName", customerName).
				addValue("state", state).
				addValue("fromDate", fromDate).
				addValue("toDate", toDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByCustomerNameAndDateAndStateAndFilterByPickUpDate(final Byte workshopId,final String customerName,final Byte state,final Long fromDate,
			final Long toDate, final Integer offset,final Integer limit) {
		final String query = baseDao.getQueryById("find.by.customer.name.and.date.and.state.and.filter.by.pick.up.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("customerName", customerName).
				addValue("state", state).
				addValue("fromDate", fromDate).
				addValue("toDate", toDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByCustomerNameAndDateAndStateAndFilterByDeliveryDate(final Byte workshopId,final String customerName,final Byte state,final Long fromDate,
			final Long toDate, final Integer offset,final Integer limit) {
		final String query = baseDao.getQueryById("find.by.customer.name.and.date.and.state.and.filter.by.delivery.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("customerName", customerName).
				addValue("state", state).
				addValue("fromDate", fromDate).
				addValue("toDate", toDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByCustomerNameAndDateAndFilterByCreateDate(final Byte workshopId,final String customerName,final Long fromDate,
			final Long toDate, final Integer offset,final Integer limit) {
		final String query = baseDao.getQueryById("find.by.customer.name.and.date.and.filter.by.create.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("customerName", customerName).
				addValue("fromDate", fromDate).
				addValue("toDate", toDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByCustomerNameAndDateAndFilterByPickUpDate(final Byte workshopId,final String customerName,final Long fromDate,
			final Long toDate, final Integer offset,final Integer limit) {
		final String query = baseDao.getQueryById("find.by.customer.name.and.date.and.filter.by.pick.up.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("customerName", customerName).
				addValue("fromDate", fromDate).
				addValue("toDate", toDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByCustomerNameAndDateAndFilterByDeliveryDate(final Byte workshopId,final String customerName,final Long fromDate,
			final Long toDate, final Integer offset,final Integer limit) {
		final String query = baseDao.getQueryById("find.by.customer.name.and.date.and.filter.by.delivery.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("customerName", customerName).
				addValue("fromDate", fromDate).
				addValue("toDate", toDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByDateAndFilterByCreateDate(final Byte workshopId,
			final Long fromDate, final Long toDate, final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.date.and.filter.by.create.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("fromDate", fromDate).
				addValue("toDate", toDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByDateAndFilterByPickUpDate(final Byte workshopId,
			final Long fromDate, final Long toDate, final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.date.and.filter.by.pick.up.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("fromDate", fromDate).
				addValue("toDate", toDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<Order> findByDateAndFilterByDeliveryDate(final Byte workshopId,
			final Long fromDate, final Long toDate, final Integer offset, final Integer limit) {
		final String query = baseDao.getQueryById("find.by.date.and.filter.by.delivery.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("fromDate", fromDate).
				addValue("toDate", toDate).
				addValue("offset", offset).
				addValue("limit", limit);
		return baseDao.getJdbcTemplate().query(query, source, ORDER_ROW_MAPPER);
	}

	@Override
	public Collection<PaymentTransaction> findPaymentTransactionsByUserIdAndDate(final Byte workshopId,final Long userId,
			final Long startDate, final Long endDate, final Integer offset, final Integer limit) {
		final String query = baseDao
                .getQueryById("find.payment.transactions.by.user.id.and.start.and.end.date");
        final SqlParameterSource source = new MapSqlParameterSource()
                .addValue("userId", userId)
                .addValue("startDate", startDate)
                .addValue("endDate", endDate)
                .addValue("offset", offset)
                .addValue("limit", limit);
        return baseDao.getJdbcTemplate().query(query, source,
                PAYMENT_TRANSACTION_ROW_MAPPER);
	}

	@Override
	public Collection<PaymentTransaction> findPaymentTransactionsByUserId(final Byte workshopId,final Long userId,
			final Integer offset, final Integer limit) {
		 final String query = baseDao
	                .getQueryById("find.payment.transactions.by.user.id");
	        final SqlParameterSource source = new MapSqlParameterSource()
	                .addValue("userId", userId)
	                .addValue("offset", offset)
	                .addValue("limit", limit);
	        return baseDao.getJdbcTemplate().query(query, source,
	                PAYMENT_TRANSACTION_ROW_MAPPER);
	}

	@Override
	public Collection<Order> getTaxReports(final Byte workshopId, final String nationState, final Long fromDate, final Long toDate) {
		final String query = baseDao.getQueryById("find.tax.report.by.state.and.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("fromDate", fromDate).
				addValue("toDate", toDate).
				addValue("nationState", nationState);
		return baseDao.getJdbcTemplate().query(query, source, ORDERS_DETAIL_RESULT_SET_EXTRACTOR);
	}
	
	@Override
	public Collection<Order> getTaxReportsByDateRange(final Byte workshopId, final Long fromDate, final Long toDate) {
		final String query = baseDao.getQueryById("find.tax.report.by.date");
		final SqlParameterSource source = new MapSqlParameterSource().
				addValue("workshopId", workshopId).
				addValue("fromDate", fromDate).
				addValue("toDate", toDate);
		return baseDao.getJdbcTemplate().query(query, source, ORDERS_DETAIL_RESULT_SET_EXTRACTOR);
	}
}
