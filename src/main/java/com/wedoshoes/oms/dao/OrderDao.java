package com.wedoshoes.oms.dao;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.wedoshoes.oms.invoice.Invoice;
import com.wedoshoes.oms.model.Coupon;
import com.wedoshoes.oms.model.LastDeliveredItem;
import com.wedoshoes.oms.model.PaymentTransaction;
import com.wedoshoes.oms.model.order.Discount;
import com.wedoshoes.oms.model.order.Feedback;
import com.wedoshoes.oms.model.order.FeedbackType;
import com.wedoshoes.oms.model.order.Image;
import com.wedoshoes.oms.model.order.ImageType;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.Order;
import com.wedoshoes.oms.model.order.Payment;
import com.wedoshoes.oms.model.order.WedoService;
import com.wedoshoes.oms.model.state.StateTransition;
import com.wedoshoes.oms.model.state.UsersLocation;

public interface OrderDao {

	/**
	 * ******************************
	 * 	Order Related Methods
	 * ******************************
	 */
	
	Long save(Byte workshopId, Order order);
	void update(Byte workshopId, Order order);
	Optional<Order> findById(Byte workshopId, Long orderId);
	Optional<Order> findDetailById(Byte workshopId, Long orderId);
	Optional<Order> findOrderCostDetail(Byte workshopId, Long orderId);
	Optional<Order> findByItemId(Byte workshopId, Long itemId);
	
	Collection<Order> findByItemState(Byte workshopId, Byte itemState, Integer offset, Integer limit);
	Collection<Order> findByItemStateAndDate(Byte workshopId, Byte itemState, Long fromDate, Long toDate, Integer offset, Integer limit);
	
	Collection<Order> findByPhone(Byte workshopId,String cc, Long phone, Integer offset, Integer limit);
	Collection<Order> findByPhone(Byte workshopId,String cc, Long phone, Byte itemState, Integer offset, Integer limit);
	Collection<Order> findByPhoneAndCreateDate(Byte workshopId,String cc, Long phone, Long createDate, Integer offset, Integer limit);
	Collection<Order> findByPhoneAndPickUpDate(Byte workshopId,String cc, Long phone, Long pickUpDate, Integer offset, Integer limit);
	Collection<Order> findByPhoneAndDeliveryDate(Byte workshopId,String cc, Long phone, Long deliveryDate, Integer offset, Integer limit);
	
	Collection<Order> findByCustomerName(Byte workshopId,String customerName, Integer offset, Integer limit);
	Collection<Order> findByCustomerName(Byte workshopId,String customerName, Byte state,Integer offset, Integer limit);
	Collection<Order> findByCustomerNameAndCreateDate(Byte workshopId,String customerName, Long createDate, 
			Integer offset, Integer limit);
	Collection<Order> findByCustomerNameAndPickUpDate(Byte workshopId,String customerName, Long pickUpDate, 
			Integer offset, Integer limit);
	Collection<Order> findByCustomerNameAndDeliveryDate(Byte workshopId,String customerName, Long deliveryDate, 
			Integer offset, Integer limit);
	
	//Collection<Order> findByUserId(Long userId, Integer offset, Integer limit);
	Collection<Order> findByUserId(Byte workshopId,Long userId, Integer offset, Integer limit);
	Collection<Order> findByUserId(Byte workshopId,Long userId, Byte state, Integer offset, Integer limit);
	Collection<Order> findByUserIdAndCreateDate(Byte workshopId,Long userId, Long createDate, Integer offset, Integer limit);
	Collection<Order> findByUserIdAndDeliveryDate(Byte workshopId,Long userId, Long deliveryDate, 
			Integer offset, Integer limit);
	Collection<Order> findByUserIdAndPickUpDate(Byte workshopId,Long userId, Long pickUpDate, Integer offset, Integer limit);
	
	
	Collection<Order> findByCreaterUserId(Byte workshopId,Long userId, Integer offset, Integer limit);
	Collection<Order> findByCreaterUserId(Byte workshopId,Long userId, Byte state, Integer offset, Integer limit);
	Collection<Order> findByCreaterUserIdAndCreateDate(Byte workshopId,Long userId, Long createDate, 
			Integer offset, Integer limit);
	Collection<Order> findByCreaterUserIdAndDeliveryDate(Byte workshopId,Long userId, Long deliveryDate, 
			Integer offset, Integer limit);
	Collection<Order> findByCreaterUserIdAndPickUpDate(Byte workshopId,Long userId, Long pickUpDate, 
			Integer offset, Integer limit);
	
	
	Collection<Order> findByReferredByUserId(Byte workshopId,Long userId, Integer offset, Integer limit);
	Collection<Order> findByReferredByUserId(Byte workshopId,Long userId, Byte state, Integer offset, Integer limit);
	Collection<Order> findByReferredByUserIdAndCreateDate(Byte workshopId,Long userId, 
			Long createDate, Integer offset, Integer limit);
	Collection<Order> findByReferredByUserIdAndDeliveryDate(Byte workshopId,Long userId, 
			Long deliveryDate, Integer offset, Integer limit);
	Collection<Order> findByReferredByUserIdAndPickUpDate(Byte workshopId,Long userId, 
			Long pickUpDate, Integer offset, Integer limit);
	
	Collection<Order> findByCreateDate(Byte workshopId,Long createDate, Integer offset, Integer limit);
	Collection<Order> findByDeliveryDate(Byte workshopId,Long deliveryDate, Integer offset, Integer limit);
	Collection<Order> findByPickUpDate(Byte workshopId,Long pickUpDate, Integer offset, Integer limit);
	
	Collection<Order> findByDeliveryDateAndState(Byte workshopId,Long deliveryDate, Byte state, Integer offset, Integer limit);
	Collection<Order> findByPickUpDateAndState(Byte workshopId,Long pickUpDate, Byte state, Integer offset, Integer limit);
	
	Collection<Order> findBy(Byte workshopId,Integer offset, Integer limit);
	
	
	void updateOrdersLocality(Byte workshopId, Long localityId, String pin);
	
	Collection<Order> findByPhoneAndCreateDateAndState(Byte workshopId,	String cc, Long phone, Long createDate, Byte state, Integer offset,	Integer limit);
	Collection<Order> findByPhoneAndPickUpDateAndState(Byte workshopId,	String cc, Long phone, Long createDate, Byte state, Integer offset,	Integer limit);
	Collection<Order> findByPhoneAndDeliveryDateAndState(Byte workshopId,	String cc, Long phone, Long pickUpDate, Byte state, Integer offset,	Integer limit);
	Collection<Order> findByUserIdAndCreateDateAndState(Byte workshopId,Long userId, Long createDate, Byte state, Integer offset,Integer limit);
	Collection<Order> findByUserIdAndPickUpDateAndState(Byte workshopId,Long userId, Long pickUpDate, Byte state, Integer offset,Integer limit);
	Collection<Order> findByUserIdAndDeliveryDateAndState(Byte workshopId,Long userId, Long deliveryDate, Byte state, Integer offset,Integer limit);
	Collection<Order> findByCustomerNameAndCreateDateAndState(Byte workshopId,String customerName, Long createDate, Byte state, Integer offset,	Integer limit);
	Collection<Order> findByCustomerNameAndPickUpDateAndState(Byte workshopId,String customerName, Long pickUpDate, Byte state, Integer offset,	Integer limit);
	Collection<Order> findByCustomerNameAndDeliveryDateAndState(Byte workshopId, String customerName, Long deliveryDate,Byte state, Integer offset, Integer limit);
	
	
	Collection<Order> findByPhoneAndDateAndStateAndFilterByCreateDate(Byte workshopId, String cc, Long phone, Byte state, Long fromDate,Long toDate, Integer offset, Integer limit);	
	Collection<Order> findByPhoneAndDateAndStateAndFilterByPickUpDate(Byte workshopId, String cc, Long phone, Byte state, Long fromDate,Long toDate, Integer offset, Integer limit);
	Collection<Order> findByPhoneAndDateAndStateAndFilterByDeliveryDate(Byte workshopId, String cc, Long phone, Byte state, Long fromDate,Long toDate, Integer offset, Integer limit);
	Collection<Order> findByPhoneAndDateAndFilterByCreateDate(Byte workshopId, String cc, Long phone,  Long fromDate,Long toDate, Integer offset, Integer limit);
	Collection<Order> findByPhoneAndDateAndFilterByPickUpDate(Byte workshopId, String cc, Long phone, Long fromDate,Long toDate, Integer offset, Integer limit);
	Collection<Order> findByPhoneAndDateAndFilterByDeliveryDate(Byte workshopId, String cc, Long phone, Long fromDate,	Long toDate, Integer offset, Integer limit);
	Collection<Order> findByUserIdAndDateAndStateAndFilterByCreateDate(Byte workshopId, Long userId, Byte state, Long fromDate,Long toDate, Integer offset, Integer limit);
	Collection<Order> findByUserIdAndDateAndStateAndFilterByPickUpDate(Byte workshopId, Long userId, Byte state, Long fromDate,Long toDate, Integer offset, Integer limit);
	Collection<Order> findByUserIdAndDateAndStateAndFilterByDeliveryDate(Byte workshopId, Long userId, Byte state, Long fromDate,Long toDate, Integer offset, Integer limit);
	Collection<Order> findByUserIdAndDateAndFilterByCreateDate(Byte workshopId,Long userId, Long fromDate, Long toDate, Integer offset,Integer limit);
	Collection<Order> findByUserIdAndDateAndFilterByPickUpDate(Byte workshopId,Long userId, Long fromDate, Long toDate, Integer offset,Integer limit);
	Collection<Order> findByUserIdAndDateAndFilterByDeliveryDate(Byte workshopId, Long userId, Long fromDate, Long toDate,Integer offset, Integer limit);
	Collection<Order> findByCustomerNameAndDateAndStateAndFilterByCreateDate(Byte workshopId, String customerName, Byte state, Long fromDate,Long toDate, Integer offset, Integer limit);
	Collection<Order> findByCustomerNameAndDateAndStateAndFilterByPickUpDate(Byte workshopId, String customerName, Byte state, Long fromDate,Long toDate, Integer offset, Integer limit);
	Collection<Order> findByCustomerNameAndDateAndStateAndFilterByDeliveryDate(Byte workshopId, String customerName, Byte state, Long fromDate,	Long toDate, Integer offset, Integer limit);
	Collection<Order> findByCustomerNameAndDateAndFilterByCreateDate(Byte workshopId, String customerName,  Long fromDate,Long toDate, Integer offset, Integer limit);
	Collection<Order> findByCustomerNameAndDateAndFilterByPickUpDate(Byte workshopId, String customerName,  Long fromDate,Long toDate, Integer offset, Integer limit);
	Collection<Order> findByCustomerNameAndDateAndFilterByDeliveryDate(Byte workshopId, String customerName,  Long fromDate,Long toDate, Integer offset, Integer limit);
	Collection<Order> findByDateAndFilterByCreateDate(Byte workshopId,Long fromDate, Long toDate, Integer offset, Integer limit);
	Collection<Order> findByDateAndFilterByPickUpDate(Byte workshopId,Long fromDate, Long toDate, Integer offset, Integer limit);
	Collection<Order> findByDateAndFilterByDeliveryDate(Byte workshopId,Long fromDate, Long toDate, Integer offset, Integer limit);
	/**
	 * ******************************
	 * 	Order Item Related Methods
	 * ******************************
	 */
	
	Long saveItem(Byte workshopId, Long orderId, Item item);
	void updateItem(Byte workshopId, Item item);
	void updateItemState(Byte workshopId, Long itemId, Byte state);
	void updateItemsStateByOrder(Byte workshopId, Long orderId, Byte state);
	Optional<Item> findItemById(Byte workshopId, Long itemId);
	Collection<Item> findItemsByOrderId(Byte workshopId, Long orderId);
	List<Long> findItemIdsByOrderId(Byte workshopId, Long orderId);
	Collection<Item> findItemsByOrderId(Byte workshopId, Long orderId, Integer offset, Integer limit);
	Collection<Item> findItemsByOrderIdAndDeliveryDate(Byte workshopId, Long orderId, Long deliveryDate, 
			Integer offset, Integer limit);
	Collection<Item> findItemsByOrderIdAndItemState(Byte workshopId, Long orderId, Byte state, 
			Integer offset, Integer limit);
	
	Collection<Item> findItemsByDeliveryDate(Byte workshopId,Long deliveryDate, Integer offset, Integer limit);
	Collection<Item> findItemsByItemState(Byte workshopId,Byte state, Integer offset, Integer limit);
	Collection<Item> findItemsByItemStateAndDate(Byte workshopId,Byte state, Long date, Integer offset, Integer limit);
	Collection<Item> findItemsByOrderIdAndItemState(Byte workshopId, Long orderId, Byte state);	
	void applyCouponOnItem(Byte workshopId,Long orderId, Long itemId, Coupon coupon);	
	void removeAppliedCouponOnItem(Long itemId, Integer discountid);
	void addItemPaymentAdjustment(Byte workshopId, Long itemId,BigDecimal amount, Integer adjustmentType);

	
	/**
	 * ************************************
	 * 	Order Item Service Related Methods
	 * ************************************
	 */

	void saveItemServices(Byte workshopId, Long itemId,	List<WedoService> services);
	void updateItemService(Byte workshopId, Long itemId, Long serviceId, WedoService wedoService);
	void deleteItemService(Byte workshopId, Long itemId, Long serviceId);
	void deleteItemServicesByItem(Byte workshopId, Long itemId);
	Collection<WedoService> findItemServicesByItem(Byte workshopId, Long itemId);
	Optional<WedoService> findItemService(Byte workshopId, Long itemId,Long serviceId);
	
	
	/**
	 * ************************************
	 * 	Order Item Image Related Methods
	 * ************************************
	 */
	
	void saveItemImages(Byte workshopId, Long itemId, List<Image> images);
	void updateItemImage(Byte workshopId, Long itemId, Image images);
	void deleteItemImage(Byte workshopId, Long imageId);
	void deleteItemImagesByItem(Byte workshopId, Long itemId);
	Optional<Image> findItemImage(Byte workshopId, Long imageId);
	Collection<Image> findItemImagesByItem(Byte workshopId, Long itemId);
	Collection<Image> findItemImagesByItem(Byte workshopId, Long itemId, ImageType imageType);
	
	
	/**
	 * ************************************
	 * 	Order Payment Detail Related Methods
	 * ************************************
	 */
	void addAdvancedPayment(Byte workshopId, Long orderId, BigDecimal amount);
	Long addPaymentTransaction(Byte workshopId, Long orderId,Long itemId,Payment payment);
	void addItemPayment(Byte workshopId, Long itemId, BigDecimal amount);
	Collection<Payment> findOrderPayments(Byte workshopId, Long orderId);
	Collection<PaymentTransaction> findPaymentTransactionsByStartAndEndDate(Byte workshopId,Long startDate, Long endDate,
			Integer offset, Integer limit);
	Collection<PaymentTransaction> findPaymentTransactionsByOrderId(
			Byte workshopId, Long orderId, Integer offset, Integer limit);
	Collection<PaymentTransaction> findPaymentTransactionsByItemId(
			Byte workshopId, Long itemId, Integer offset, Integer limit);
	Collection<PaymentTransaction> findPaymentTransactionsByOrderIdAndDate(
			Byte workshopId, Long orderId, Long startDate, Long endDate, Integer offset, Integer limit);
	Collection<PaymentTransaction> findPaymentTransactionsByUserIdAndDate(Byte workshopId, Long userId, Long startDate, Long endDate,
			Integer offset, Integer limit);
	Collection<PaymentTransaction> findPaymentTransactionsByUserId(Byte workshopId, Long userId, Integer offset, Integer limit);
	
	/**
	 * ************************************
	 * 	Item Discount Related Methods
	 * ************************************
	 */
	
	void saveItemDiscount(Byte workshopId, Long itemId, List<Discount> discount);
	Collection<Discount> findItemDiscountByItem(Byte workshopId, Long itemId);
	
	
	/**
	 * ************************************
	 * 	Item State transition
	 * ************************************
	 */
	Long saveItemStateTransition(Byte workshopId, Long itemId, StateTransition transition);
	void updateItemStateTransition(Byte workshopId, Long itemId, Long transitionId, StateTransition transition);
	Optional<StateTransition> findItemStateTransition(Byte workshopId, Long itemId, Long transitionId);
	Collection<StateTransition> findItemStateTransitions(Byte workshopId, Long itemId);
	List<StateTransition> findItemExternalStateTransitions(Byte workshopId, Long itemId);
	Optional<StateTransition> findItemStateTransition(Byte workshopId, Long itemId, Byte state);
	
	/**
	 * ************************************
	 * 	Invoice Related Methods
	 * ************************************
	 */
	void saveInvoice(Byte workshopId, Long orderId, Long itemId);
	Collection<Invoice> getInvoicesByItemId(Long itemId);
	Collection<Invoice> getInvoicesByOrderId(Byte workshopId, Long orderId);
	Collection<Invoice> getInvoicesByDateAndDuration(Long date, Integer duration);
	Collection<Invoice> getCurrentDateInvoices();
	
	/**
	 * ************************************
	 * 	Order Item Defects Related Methods
	 * ************************************
	 */
	void saveItemDefects(Byte workshopId, Long itemId, List<Byte> defects);
	Collection<Byte> findItemDefectsByItem(Byte workshopId, Long itemId);

	/**
	 * ************************************
	 * 	Order Feedback Related Methods
	 * ************************************
	 */
	Long saveFeedback(Long userId, Feedback feedback, FeedbackType type);
	Optional<Feedback> getFeedback(Long userId, Byte workshopId, Long orderId, Long itemId);
	Optional<LastDeliveredItem> getLastDeliveredItem(Long userId, Byte workshopId);
	void saveUsersLocation(Long itemStateTransactionId, Long userId, UsersLocation usersLocation);
	Integer findUserOrderCounts(Long userId);
	
	
	BigDecimal getTotalOrderTxn(Long orderId, Long shipmentDate);


	void updateItemRating(Byte workshopId, Long orderId, Long itemId,
			Short rating);
	
	BigDecimal getTotalTxnAmountOfDayByUserId(Long userId, Long shipmentDate);
	
	
	/**
	 * ************************************
	 * 	Orders Report Related Methods
	 * ************************************
	 */
	Collection<Order> getTaxReports(Byte workshopId, String state, Long fromDate, Long toDate);
	
	Collection<Order> getTaxReportsByDateRange(Byte workshopId, Long fromDate, Long toDate);
}