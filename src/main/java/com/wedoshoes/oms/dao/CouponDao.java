package com.wedoshoes.oms.dao;

import java.util.Collection;
import java.util.Optional;

import com.wedoshoes.oms.model.Coupon;
import com.wedoshoes.oms.model.CouponSource;

public interface CouponDao {

	Integer save(Coupon coupon);
	void update(Coupon coupon);
	void delete(Integer id);
	Optional<Coupon> findById(Integer id);
	Optional<Coupon> findDetailById(Integer id);
	Optional<Coupon> getCouponByCode(String code);
	Optional<Coupon> getCouponDetailByCode(String code);
	void applyOnUserType(Integer userTypeId, Integer couponId);
	void applyOnService(Integer serviceId, Integer couponId);
	void applyOnCategory(Integer categoryId, Integer couponId);
	void applyOnProduct(Integer productId, Integer couponId);	
	void apply(Coupon coupon);

	Collection<Coupon> listByUserType(Byte userTypeId, Integer offSet, Integer limit) ;
	Collection<Coupon> listByProduct(Integer productId, Integer offSet, Integer limit);
	Collection<Coupon> listByCategory(Integer categoryid, Integer offSet, Integer limit);
	Collection<Coupon> listDefaultCoupon( Integer offSet, Integer limit);
	
	
	Integer save(CouponSource couponSource);
	void deleteSource(Integer sourceId);
	void update(CouponSource couponSource);
	Optional<CouponSource> findSourceDetailById(Integer sourceId);
	Optional<CouponSource> findSourceById(Integer sourceId);
	Collection<CouponSource> list();

}
