package com.wedoshoes.oms.dao.rowmapper.order;

import static com.wedoshoes.oms.dao.impl.DefaultUtils.nullIfDefaultBigDecimal;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.nullIfDefaultByte;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.nullIfDefaultInteger;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wedoshoes.oms.model.Size;
import com.wedoshoes.oms.model.order.GST;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.payment.PaymentAdjustmentType;

public class ItemRowMapper implements RowMapper<Item> {

	@Override
	public Item mapRow(final ResultSet rs, int rowNum) throws SQLException {
		return Item.createItemSummay(rs.getLong("id"), rs.getLong("order_id"),
				rs.getByte("state"),  new GST(rs.getBigDecimal("igst"), rs.getBigDecimal("cgst"), 
				rs.getBigDecimal("sgst"), null), rs.getLong("create_date"),
				rs.getInt("create_time"),rs.getLong("delivery_date"),rs.getInt("delivery_time"),
				rs.getLong("est_delivery_date"),rs.getInt("est_delivery_time"),
				rs.getByte("delivery_timeslot"),
				rs.getBigDecimal("convenience_charge"),
				nullIfDefaultBigDecimal(rs.getBigDecimal("express_processing_charge")),
				rs.getInt("parent_service_id"), rs.getInt("category_id"),
				rs.getInt("product_id"), nullIfDefaultInteger(rs.getInt("size")),
				rs.getInt("brand_id"), nullIfDefaultInteger(rs.getInt("item_size_type")) == null ? null : 
					Size.codeToType(rs.getInt("item_size_type")),
				rs.getBoolean("is_express_processing"), rs.getBoolean("packing"),
				nullIfDefaultBigDecimal(rs.getBigDecimal("packing_charge")),
				nullIfDefaultBigDecimal(rs.getBigDecimal("cost_credited")),
				nullIfDefaultBigDecimal(rs.getBigDecimal("bad_debts")),
				nullIfDefaultBigDecimal(rs.getBigDecimal("adjustment")),
				rs.getString("adjustment_type") == null ? null : 
					PaymentAdjustmentType.codeToType(rs.getInt("adjustment_type")), 
					nullIfDefaultByte(rs.getByte("rating")),nullIfDefaultByte(rs.getByte("priority")));		
	}

}
