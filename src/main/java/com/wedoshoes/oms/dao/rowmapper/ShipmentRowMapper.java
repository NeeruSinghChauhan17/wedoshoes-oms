package com.wedoshoes.oms.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wedoshoes.oms.model.shipment.Shipment;

public class ShipmentRowMapper implements RowMapper<Shipment>{

	@Override
	public Shipment mapRow(ResultSet rs, int rowNum) throws SQLException {
	return new Shipment(rs.getLong("id"), rs.getLong("user_id"), 
			rs.getLong("order_id"), rs.getLong("item_id"), null, rs.getByte("current_state"),
			rs.getLong("shipment_date"),
			rs.getLong("last_updated_date"), null);
	}
}
