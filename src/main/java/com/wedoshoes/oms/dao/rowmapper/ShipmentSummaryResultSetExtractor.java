package com.wedoshoes.oms.dao.rowmapper;

import static com.wedoshoes.oms.model.order.ItemState.ANALYSED;
import static com.wedoshoes.oms.model.order.ItemState.ANALYSIS;
import static com.wedoshoes.oms.model.order.ItemState.CANCELLED;
import static com.wedoshoes.oms.model.order.ItemState.DELIVERED;
import static com.wedoshoes.oms.model.order.ItemState.INVOICE_GENERATED;
import static com.wedoshoes.oms.model.order.ItemState.IN_PROGRESS;
import static com.wedoshoes.oms.model.order.ItemState.MARK_FOR_PACKING;
import static com.wedoshoes.oms.model.order.ItemState.MARK_FOR_QUALITY_CHECK;
import static com.wedoshoes.oms.model.order.ItemState.MISSED;
import static com.wedoshoes.oms.model.order.ItemState.PICKED;
import static com.wedoshoes.oms.model.order.ItemState.PROCESSED;
import static com.wedoshoes.oms.model.order.ItemState.QA_REJECTED;
import static com.wedoshoes.oms.model.order.ItemState.READY_FOR_DELIVERY;
import static com.wedoshoes.oms.model.order.ItemState.RECEIVED_AT_CP;
import static com.wedoshoes.oms.model.order.ItemState.RECEIVED_AT_WORKSHOP;
import static com.wedoshoes.oms.model.order.ItemState.RE_SCHEDULE_FOR_DELIVERY;
import static com.wedoshoes.oms.model.order.ItemState.RE_SCHEDULE_FOR_PICKUP;
import static com.wedoshoes.oms.model.order.ItemState.RE_SERVICE;
import static com.wedoshoes.oms.model.order.ItemState.RE_WORK;
import static com.wedoshoes.oms.model.order.ItemState.SCHEDULE_FOR_DELIVERY;
import static com.wedoshoes.oms.model.order.ItemState.SCHEDULE_FOR_PICKUP;
import static com.wedoshoes.oms.model.order.ItemState.SEND_TO_WORKSHOP;
import static com.wedoshoes.oms.model.order.ItemState.WAITING_FOR_APPORVAL;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.wedoshoes.oms.model.shipment.ShipmentSummary;
/**
 * 
 * @author Alone.gk
 *
 */
public class ShipmentSummaryResultSetExtractor implements ResultSetExtractor<ShipmentSummary>{


	@Override
	public ShipmentSummary extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		final Set<Byte> states = new HashSet<>();
		// used in counts
			states.add(SCHEDULE_FOR_PICKUP);		states.add(PICKED);states.add(RECEIVED_AT_CP);
			states.add(SCHEDULE_FOR_DELIVERY);		states.add(DELIVERED);states.add(INVOICE_GENERATED);
			states.add(CANCELLED); 					states.add(RE_SCHEDULE_FOR_DELIVERY);
			states.add(RE_SCHEDULE_FOR_PICKUP);		states.add(RE_SERVICE);
			
		//un-used
			states.add(ANALYSED);					states.add(MISSED);
			states.add(PROCESSED);					states.add(SEND_TO_WORKSHOP);
			states.add(QA_REJECTED);				states.add(RECEIVED_AT_WORKSHOP);
			states.add(ANALYSIS);					states.add(WAITING_FOR_APPORVAL);
			states.add(IN_PROGRESS);				states.add(MARK_FOR_QUALITY_CHECK);
			states.add(MARK_FOR_PACKING);			states.add(READY_FOR_DELIVERY);
			states.add(RE_WORK);
			
		final Set<Long> orders = new HashSet<>();
		
		final Map<Byte, Integer> map = new HashMap<>();
			map.put(SCHEDULE_FOR_PICKUP, 0);		map.put(PICKED, 0);
			map.put(SCHEDULE_FOR_DELIVERY, 0);		map.put(DELIVERED, 0);
			map.put(CANCELLED, 0); 					map.put(RE_SERVICE, 0);
			map.put(RE_SCHEDULE_FOR_PICKUP, 0);		map.put(RE_SCHEDULE_FOR_DELIVERY, 0);
			map.put(RECEIVED_AT_CP, 0);				map.put(INVOICE_GENERATED, 0);
			map.put(ANALYSED, 0);					map.put(RE_WORK, 0);		
			map.put(PROCESSED, 0);					map.put(MISSED, 0);		
			map.put(QA_REJECTED, 0);				map.put(SEND_TO_WORKSHOP, 0);		
			map.put(ANALYSIS, 0);					map.put(RECEIVED_AT_WORKSHOP, 0);		
			map.put(IN_PROGRESS, 0);				map.put(WAITING_FOR_APPORVAL, 0);		
			map.put(MARK_FOR_PACKING, 0);			map.put(READY_FOR_DELIVERY, 0);	
			map.put(MARK_FOR_QUALITY_CHECK, 0);	
		
		while(rs.next()){
			// count total pickups and deliveries
			final Byte initialState = rs.getByte("initial_state");
			map.put(initialState, states.add(initialState)?map.get(initialState):map.get(initialState)+1);
			
			final Byte currentState = rs.getByte("current_state");
			if(currentState!=SCHEDULE_FOR_PICKUP && currentState!=SCHEDULE_FOR_DELIVERY){
				map.put(currentState, states.add(currentState)?map.get(currentState):map.get(currentState)+1);
			}
			// count orders
			orders.add(rs.getLong("order_id"));
			
		}
		
		return new ShipmentSummary.Builder().pickupItems(map.get(SCHEDULE_FOR_PICKUP))
			.pickedItem(map.get(PICKED)+map.get(RECEIVED_AT_CP)).deliveryItems(map.get(SCHEDULE_FOR_DELIVERY))
			.deliveredItems(map.get(DELIVERED)+map.get(INVOICE_GENERATED)).canceledItems(map.get(CANCELLED))
			.reScheduleForDelivery(map.get(RE_SCHEDULE_FOR_DELIVERY)).reScheduleForPickup(map.get(RE_SCHEDULE_FOR_PICKUP))
			.reServiceItems(map.get(RE_SERVICE)).orders(orders)
			.build();
	
	}

}
