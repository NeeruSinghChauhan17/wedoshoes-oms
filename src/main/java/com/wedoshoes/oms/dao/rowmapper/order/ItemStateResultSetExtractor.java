package com.wedoshoes.oms.dao.rowmapper.order;

import static com.wedoshoes.oms.dao.impl.DefaultUtils.nullIfDefaultByte;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.nullIfDefaultString;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.ResultSetExtractor;

import com.wedoshoes.oms.model.state.StateTransition;
import com.wedoshoes.oms.model.state.UsersLocation;

public class ItemStateResultSetExtractor implements ResultSetExtractor<List<StateTransition>> {
/**
 * map was replaced by arraylist because we have to show all the repeated states of an items in history.
 */
	
//	@Override
//	public Map<Byte, StateTransition> extractData(final ResultSet rs) throws SQLException {
//		final Map<Byte, StateTransition> states = new HashMap<>();
//		while(rs.next()) {
//			final Byte state = rs.getByte("state");
//			states.put(state, new StateTransition(rs.getLong("id"), rs.getLong("user_id"), rs.getLong("create_date"),
//					state, nullIfDefaultString(rs.getString("comment")),
//					nullIfDefaultByte(rs.getByte("col_point_id")), nullIfDefaultByte(rs.getByte("workshop_id")), null));
//		}
//		return states;
//	}

	
	@Override
	public List<StateTransition> extractData(final ResultSet rs) throws SQLException {
		List<StateTransition> states= new ArrayList<>(); 
		while(rs.next()) {
			states.add(new StateTransition(rs.getLong("id"), rs.getLong("user_id"), rs.getLong("create_date"),
					rs.getByte("state"), nullIfDefaultString(rs.getString("comment")),
					nullIfDefaultByte(rs.getByte("col_point_id")), nullIfDefaultByte(rs.getByte("workshop_id")), 
					null, new UsersLocation(null,rs.getDouble("latitude"), rs.getDouble("longitude"))));  
		}
		return states;
	}	
	
}
