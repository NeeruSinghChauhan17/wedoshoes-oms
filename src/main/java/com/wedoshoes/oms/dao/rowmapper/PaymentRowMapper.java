package com.wedoshoes.oms.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wedoshoes.oms.model.order.Payment;
import com.wedoshoes.oms.model.order.PaymentMode;
import com.wedoshoes.oms.model.order.TransactionType;

public class PaymentRowMapper implements RowMapper<Payment> {

	@Override
	public Payment mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Payment(null, null, rs.getString("txn_id"), PaymentMode.codeToType(rs.getInt("mode")),
				rs.getBigDecimal("amount"), rs.getLong("date"), TransactionType.codeToType(rs.getInt("type")),
				rs.getString("description"),null,null,null);
	}
}
