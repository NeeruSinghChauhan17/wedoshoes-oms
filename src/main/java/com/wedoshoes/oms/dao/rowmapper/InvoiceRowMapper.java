package com.wedoshoes.oms.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wedoshoes.oms.invoice.Invoice;


public class InvoiceRowMapper implements RowMapper<Invoice>{

	@Override
	public Invoice mapRow(ResultSet rs, int rowNumber) throws SQLException {
		return new Invoice(rs.getString("id"), rs.getByte("workshop_id"),  rs.getLong("order_id"), rs.getLong("item_id"), rs.getLong("created_date"), rs.getLong("created_time"));
	}
}
