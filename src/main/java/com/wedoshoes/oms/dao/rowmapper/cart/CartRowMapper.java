package com.wedoshoes.oms.dao.rowmapper.cart;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wedoshoes.oms.model.order.Cart;
import com.wedoshoes.oms.model.order.CartState;

public class CartRowMapper implements RowMapper<Cart> {

	@Override
	public Cart mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Cart(rs.getLong("id"), rs.getLong("user_id"), rs.getLong("create_date"), null,
				CartState.codeToType(rs.getInt("active")), rs.getInt("size")); 
	}

}
