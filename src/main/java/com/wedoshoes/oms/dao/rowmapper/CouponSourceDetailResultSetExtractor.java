package com.wedoshoes.oms.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.wedoshoes.oms.model.CouponSource;

public class CouponSourceDetailResultSetExtractor implements ResultSetExtractor<Collection<CouponSource>>{

	@Override
	public Collection<CouponSource> extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		final Map<Integer,CouponSource> sources = new HashMap<>();
		while(rs.next()) {
			final Integer sourceId = rs.getInt("id");
			if(!sources.containsKey(sourceId)){
				sources.put(sourceId, new CouponSource(sourceId, rs.getString("name")));
			}
		}
		return sources.values();
	}

}
