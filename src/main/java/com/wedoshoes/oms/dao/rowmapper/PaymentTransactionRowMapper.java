package com.wedoshoes.oms.dao.rowmapper;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.nullIfDefaultLong;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wedoshoes.oms.model.PaymentTransaction;
import com.wedoshoes.oms.model.order.PaymentMode;
import com.wedoshoes.oms.model.order.TransactionType;

public class PaymentTransactionRowMapper implements
        RowMapper<PaymentTransaction> {

    @Override
    public PaymentTransaction mapRow(ResultSet rs, int rowNum)
            throws SQLException {
        return new PaymentTransaction(null,
                nullIfDefaultLong(rs.getLong("user_id")),
                rs.getString("txn_id"), PaymentMode.codeToType(rs
                        .getInt("mode")), rs.getBigDecimal("amount"),
                rs.getLong("date"), TransactionType.codeToType(rs
                        .getInt("type")), rs.getString("description"),
                rs.getLong("order_id"),
                nullIfDefaultLong(rs.getLong("item_id")));
    }

}

