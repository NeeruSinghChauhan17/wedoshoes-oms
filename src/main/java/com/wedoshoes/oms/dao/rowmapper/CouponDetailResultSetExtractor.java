package com.wedoshoes.oms.dao.rowmapper;

import static com.wedoshoes.oms.dao.impl.DefaultUtils.nullIfDefaultInteger;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.nullIfDefaultLong;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.nullIfDefaultString;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.jdbc.core.ResultSetExtractor;

import com.wedoshoes.oms.model.Coupon;
import com.wedoshoes.oms.model.CouponSource;
import com.wedoshoes.oms.model.order.PriceValueType;

public class CouponDetailResultSetExtractor implements ResultSetExtractor<Collection<Coupon>>{

	@Override
	public Collection<Coupon> extractData(final ResultSet rs) throws SQLException {
		
		final Map<Integer, Coupon> coupons = new HashMap<>();
		final Map<Integer, Set<Integer>> services = new HashMap<>();
		final Map<Integer, Set<Integer>> userTypes = new HashMap<>();
		final Map<Integer, Set<Integer>> products = new HashMap<>();
		final Map<Integer, Set<Integer>> categories = new HashMap<>();
		
		while(rs.next()) {
			final Integer couponId = rs.getInt("id");
			if(!coupons.containsKey(couponId)) {
				coupons.put(couponId, new Coupon(rs.getInt("id"), rs.getString("code"),
						nullIfDefaultString(rs.getString("description")),rs.getBigDecimal("value"),
						PriceValueType.valueOf(rs.getString("value_type")),
						nullIfDefaultLong(rs.getLong("start_date")), nullIfDefaultLong(rs.getLong("end_date")),
						nullIfDefaultInteger(rs.getInt("max_use")), rs.getBigDecimal("min_order_amount"),
						CouponSource.createWithId(rs.getInt("source_id")),
						rs.getBigDecimal("max_discount"), null, null, null, null, rs.getInt("used_count")));
				services.put(couponId, new HashSet<Integer>());
				userTypes.put(couponId, new HashSet<Integer>());
				products.put(couponId, new HashSet<Integer>());
				categories.put(couponId, new HashSet<Integer>());
			}
			
			final Integer userTypeId = nullIfDefaultInteger(rs.getInt("user_type_id"));
			
			if(userTypeId != null) {
				userTypes.get(couponId).add(userTypeId);
			}
			
			final Integer serviceId = nullIfDefaultInteger(rs.getInt("service_id"));
			if(serviceId != null) {
				services.get(couponId).add(serviceId);
			}
			final Integer productId = nullIfDefaultInteger(rs.getInt("product_id"));
			if(productId != null) {
				products.get(couponId).add(productId);
			}
			final Integer categoryId = nullIfDefaultInteger(rs.getInt("category_id"));
			if(categoryId != null){
				categories.get(couponId).add(categoryId);
			}
		}
		
		return coupons.values().stream().map(coupon -> Coupon.createWithUserTypesServicesProductsAndCategories(
				(userTypes.get(coupon.getId()) != null && !userTypes.get(coupon.getId()).isEmpty()) ? userTypes.get(coupon.getId()) : null,
				(services.get(coupon.getId()) != null && !services.get(coupon.getId()).isEmpty()) ? services.get(coupon.getId()) : null,
				(products.get(coupon.getId()) != null && !products.get(coupon.getId()).isEmpty()) ? products.get(coupon.getId()) : null,
				(categories.get(coupon.getId()) != null && !categories.get(coupon.getId()).isEmpty()) ? categories.get(coupon.getId()) : null,
						coupon)).collect(Collectors.toList());
	}

}
