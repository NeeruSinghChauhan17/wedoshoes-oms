package com.wedoshoes.oms.dao.rowmapper.order;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wedoshoes.oms.model.order.Image;
import com.wedoshoes.oms.model.order.ImageType;

public class ImageRowMapper implements RowMapper<Image>{

	@Override
	public Image mapRow(final ResultSet rs, final int rowNum) throws SQLException {
		return new Image(rs.getLong("id"), rs.getString("hres_url"), rs.getString("lres_url"),
				ImageType.codeToType(rs.getInt("type")), rs.getString("description"));
	}
}
