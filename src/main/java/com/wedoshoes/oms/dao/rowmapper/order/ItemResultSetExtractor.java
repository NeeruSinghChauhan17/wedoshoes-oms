package com.wedoshoes.oms.dao.rowmapper.order;

import static com.wedoshoes.oms.dao.impl.DefaultUtils.nullIfDefaultBigDecimal;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.nullIfDefaultByte;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.nullIfDefaultInteger;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.nullIfDefaultLong;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.jdbc.core.ResultSetExtractor;

import com.google.common.collect.HashBasedTable;
import com.wedoshoes.oms.model.Size;
import com.wedoshoes.oms.model.order.Discount;
import com.wedoshoes.oms.model.order.GST;
import com.wedoshoes.oms.model.order.Image;
import com.wedoshoes.oms.model.order.ImageType;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.PriceValueType;
import com.wedoshoes.oms.model.order.WedoService;
import com.wedoshoes.oms.model.payment.PaymentAdjustmentType;

public class ItemResultSetExtractor implements ResultSetExtractor<Collection<Item>> {

	@Override
	public Collection<Item> extractData(final ResultSet rs) throws SQLException {
		final Map<Long, Item> items = new HashMap<>();
		final HashBasedTable<Long, Integer, Discount> itemDiscounts = HashBasedTable.create();
		final HashBasedTable<Long, Integer, WedoService> itemServices = HashBasedTable.create();
		final HashBasedTable<Long, Long, Image> itemImages = HashBasedTable.create();
		
		while(rs.next()) {
			final Long itemId = rs.getLong("id");
			final Integer discountId = nullIfDefaultInteger(rs.getInt("discount_id"));
			final Integer serviceId = nullIfDefaultInteger(rs.getInt("service_id"));
			final Long imageId = nullIfDefaultLong(rs.getLong("image_id"));
			/**
			 * Create and Store Item if not exist.
			 */
			if(!items.containsKey(itemId)) {
				items.put(itemId, Item.createItemSummay(itemId, rs.getLong("order_id"), 
						rs.getByte("state"), new GST(rs.getBigDecimal("igst"), rs.getBigDecimal("cgst"), rs.getBigDecimal("sgst"),null),
						rs.getLong("create_date"),
						rs.getInt("create_time"),rs.getLong("delivery_date"),rs.getInt("delivery_time"),
						rs.getLong("est_delivery_date"),rs.getInt("est_delivery_time"),
						rs.getByte("delivery_timeslot"),
						rs.getBigDecimal("convenience_charge"),
						nullIfDefaultBigDecimal(rs.getBigDecimal("express_processing_charge")),
						rs.getInt("parent_service_id"),rs.getInt("category_id"), rs.getInt("product_id"),
						rs.getInt("size"),rs.getInt("brand_id"),
						nullIfDefaultInteger(rs.getInt("item_size_type")) == null 
						? null :Size.codeToType(rs.getInt("item_size_type")),
								rs.getBoolean("is_express_processing"), rs.getBoolean("packing"),
								nullIfDefaultBigDecimal(rs.getBigDecimal("packing_charge")),
								nullIfDefaultBigDecimal(rs.getBigDecimal("cost_credited")),
								nullIfDefaultBigDecimal(rs.getBigDecimal("bad_debts")), 
								nullIfDefaultBigDecimal(rs.getBigDecimal("adjustment")),
								rs.getString("adjustment_type") == null ? null : 
									PaymentAdjustmentType.codeToType(rs.getInt("adjustment_type")),
									nullIfDefaultByte(rs.getByte("rating")),
									nullIfDefaultByte(rs.getByte("priority"))));
				}
			
			/**
			 * Add item discount if not added. 
			 */
			if(discountId != null && !itemDiscounts.contains(itemId, discountId)) {
				itemDiscounts.put(itemId, discountId, extractDiscount(discountId, rs));
			}
			
			/**
			 * Add service if not added. 
			 */
			if(serviceId != null && !itemServices.contains(itemId, serviceId)) {
				itemServices.put(itemId, serviceId, extractWedoservice(serviceId, rs));
			}
			
			/**
			 * Add Item Image if item has image and not added in itemImages 
			 */
			
			if(imageId != null && !itemImages.contains(itemId, imageId)) {
				itemImages.put(itemId, imageId, extractImage(imageId, rs));
			}
		}
		if(items.isEmpty()) { 
			return Collections.emptyList();
		}
		
		return items.values().stream().map(item -> Item.createWithServicesAndDiscountsAndImages(item, 
				itemServices.row(item.getId()).values(), 
				itemDiscounts.row(item.getId()).values(), itemImages.row(item.getId()).values()))
				.collect(Collectors.toList());
	}
	
		
	private Discount extractDiscount(final Integer discountId, final ResultSet rs) throws SQLException {
		return new Discount(discountId,
				rs.getBigDecimal("discount_value"), 
				PriceValueType.valueOf(rs.getString("discount_value_type")));
	}
	
	private WedoService extractWedoservice(final Integer serviceId, final ResultSet rs) throws SQLException {
		return new WedoService(serviceId,
				rs.getShort("quantity"), rs.getBigDecimal("cost"));
	}
	
	private Image extractImage(final Long imageId, final ResultSet rs) throws SQLException {
		return new Image(imageId, rs.getString("hres_url"), rs.getString("lres_url"),
				ImageType.codeToType(rs.getInt("type")), null);
	}
	
}
