package com.wedoshoes.oms.dao.rowmapper.order;

import static com.wedoshoes.oms.dao.impl.DefaultUtils.nullIfDefaultBigDecimal;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.nullIfDefaultByte;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.nullIfDefaultInteger;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.nullIfDefaultLong;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.nullIfDefaultString;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.jdbc.core.ResultSetExtractor;

import com.google.common.collect.HashBasedTable;
import com.wedoshoes.oms.model.Size;
import com.wedoshoes.oms.model.order.Address;
import com.wedoshoes.oms.model.order.Discount;
import com.wedoshoes.oms.model.order.GST;
import com.wedoshoes.oms.model.order.Image;
import com.wedoshoes.oms.model.order.ImageType;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.Order;
import com.wedoshoes.oms.model.order.PriceValueType;
import com.wedoshoes.oms.model.order.WedoService;
import com.wedoshoes.oms.model.payment.PaymentAdjustmentType;

public class OrdersDetailResultSetExtractor implements ResultSetExtractor<Collection<Order>> {

	@Override
	public Collection<Order> extractData(ResultSet rs) throws SQLException {
		
		final Map<Long, Order> orders = new HashMap<>();
		final HashBasedTable<Long, Long, Item> orderItems = HashBasedTable.create();
		final HashBasedTable<Long, Integer, Discount> itemDiscounts = HashBasedTable.create();
		final HashBasedTable<Long, Integer, WedoService> itemServices = HashBasedTable.create();
		final HashBasedTable<Long, Long, Image> itemImages = HashBasedTable.create();
		
		while(rs.next()) {
			final Long orderId = rs.getLong("id");
			final Long itemId = rs.getLong("item_id");
			final Integer discountId = nullIfDefaultInteger(rs.getInt("discount_id"));
			final Integer serviceId = nullIfDefaultInteger(rs.getInt("service_id"));
			final Long imageId = nullIfDefaultLong(rs.getLong("image_id"));
			/**
			 * Create and Store order if not exist.
			 */
			if(!orders.containsKey(orderId)) {
				orders.put(orderId, extractOrder(orderId, rs));
			}
			
			/**
			 * Add item if not added. 
			 */
			if(!orderItems.contains(orderId, itemId)) {
				orderItems.put(orderId, itemId, extractOrderItem(orderId, itemId, rs));
			}
			
			
			/**
			 * Add discount if not added. 
			 */
			if(discountId != null && !itemDiscounts.contains(itemId, discountId)) {
				itemDiscounts.put(itemId, discountId, extractDiscount(discountId, rs));
			}
			
			/**
			 * Add service if not added. 
			 */
			if(serviceId != null && !itemServices.contains(itemId, serviceId)) {
				itemServices.put(itemId, serviceId, extractWedoservice(serviceId, rs));
			}
			
			/**
			 * Add Item Image if item has image and not added in itemImages 
			 */
			
			if(imageId != null && !itemImages.contains(itemId, imageId)) {
				itemImages.put(itemId, imageId, extractImage(imageId, rs));
			}
		}
		
		if(orders.isEmpty()) { 
			return Collections.emptyList();
		}
		
		return orders.values().stream().map(order -> Order.createWithItems(order, 
				getItemsForOrder(orderItems.row(order.getId()).values(),itemServices,itemDiscounts, itemImages)))
				.collect(Collectors.toList());
	}
	
	private List<Item> getItemsForOrder(final Collection< Item> items,
			final HashBasedTable<Long, Integer, WedoService> itemServices,
			final HashBasedTable<Long, Integer, Discount> itemDiscounts,
			final HashBasedTable<Long, Long, Image> itemImages) {
		return items.stream().map(item -> Item.createWithServicesAndDiscountsAndImages(item, 
				itemServices.row(item.getId()).values(), 
				itemDiscounts.row(item.getId()).values(), itemImages.row(item.getId()).values()))
				.collect(Collectors.toList());
		
	}
	private Item extractOrderItem(final Long orderId, final Long itemId, final ResultSet rs ) throws SQLException {
		return new Item(itemId, orderId, rs.getByte("state"),
				 new GST(rs.getBigDecimal("igst"), rs.getBigDecimal("cgst"), rs.getBigDecimal("sgst"),null), 
				rs.getLong("create_date"),
				rs.getInt("create_time"),rs.getLong("delivery_date"),rs.getInt("delivery_time"),
				rs.getLong("est_delivery_date"),rs.getInt("est_delivery_time"),
				rs.getByte("delivery_timeslot"), rs.getBigDecimal("convenience_charge"),
				nullIfDefaultBigDecimal(rs.getBigDecimal("express_processing_charge")),
				rs.getInt("parent_service_id"),rs.getInt("category_id"), rs.getInt("product_id"),
				rs.getInt("size"),rs.getInt("brand_id"),
				nullIfDefaultInteger(rs.getInt("item_size_type")) == null 
				? null :Size.codeToType(rs.getInt("item_size_type")), null, null, null,
						rs.getBoolean("is_express_processing"), rs.getBoolean("packing"),
						nullIfDefaultBigDecimal(rs.getBigDecimal("packing_charge")),
						nullIfDefaultBigDecimal(rs.getBigDecimal("cost_credited")),
						nullIfDefaultBigDecimal(rs.getBigDecimal("bad_debts")), 
						nullIfDefaultBigDecimal(rs.getBigDecimal("adjustment")),
						rs.getString("adjustment_type") == null ? null : 
							PaymentAdjustmentType.codeToType(rs.getInt("adjustment_type")), /*comments*/ null,
							nullIfDefaultByte(rs.getByte("rating")),nullIfDefaultByte(rs.getByte("priority")));
	}
	
	
	private Discount extractDiscount(final Integer discountId, final ResultSet rs) throws SQLException {
		return new Discount(discountId,
				rs.getBigDecimal("discount_value"), 
				PriceValueType.valueOf(rs.getString("discount_value_type")));
	}
	
	private WedoService extractWedoservice(final Integer serviceId, final ResultSet rs) throws SQLException {
		return new WedoService(serviceId,
				rs.getShort("quantity"), rs.getBigDecimal("cost"));
	}
	
	private Image extractImage(final Long imageId, final ResultSet rs) throws SQLException {
		return new Image(imageId, rs.getString("hres_url"), rs.getString("lres_url"),
				ImageType.codeToType(rs.getInt("type")), null);
	}
	
	private Order extractOrder(final Long orderId, final ResultSet rs) throws SQLException {
		return new Order(orderId, rs.getLong("user_id"), 
				nullIfDefaultString(rs.getString("customer_name")),rs.getString("last_name"),rs.getString("cc"),
				rs.getLong("phone"), nullIfDefaultLong(rs.getLong("alternate_phone")), 
				rs.getByte("pick_up_timeslot"), rs.getLong("pick_up_date"), null,
				rs.getLong("create_date"),rs.getInt("create_time"),
				rs.getLong("creater_user_id"), null, 
					new Address(nullIfDefaultString(rs.getString("pick_up_street_address")),
							nullIfDefaultString(rs.getString("pick_up_city")),
							nullIfDefaultString(rs.getString("pick_up_state")),
							nullIfDefaultString(rs.getString("pick_up_pin")),
							nullIfDefaultString(rs.getString("pick_up_landmark")),
							(rs.getInt("pick_up_locality")),
							rs.getDouble("pick_up_latitude"),
							rs.getDouble("pick_up_longitude")),
							new Address(nullIfDefaultString(rs.getString("delivery_street_address")),
									nullIfDefaultString(rs.getString("delivery_city")),
									nullIfDefaultString(rs.getString("delivery_state")),
									nullIfDefaultString(rs.getString("delivery_pin")),
									nullIfDefaultString(rs.getString("delivery_landmark")),
									(rs.getInt("delivery_locality")),
									rs.getDouble("delivery_latitude"),
									rs.getDouble("delivery_longitude")),
									rs.getBigDecimal("advance_payment"),rs.getLong("referred_by_user_id"),
									rs.getByte("workshop_id"));
	}

}
