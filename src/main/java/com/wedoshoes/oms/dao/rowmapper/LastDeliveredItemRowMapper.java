package com.wedoshoes.oms.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wedoshoes.oms.model.LastDeliveredItem;

public class LastDeliveredItemRowMapper implements RowMapper<LastDeliveredItem>{

	@Override
	public LastDeliveredItem mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new LastDeliveredItem(rs.getLong("order_id"), rs.getLong("item_id"), rs.getLong("create_date"), null);			
	}

}
