package com.wedoshoes.oms.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.wedoshoes.oms.model.shipment.Shipment;

public class ShipmentResultSetExtractor implements ResultSetExtractor<Collection<Shipment>>{

	@Override
	public Collection<Shipment> extractData(ResultSet rs) throws SQLException, DataAccessException {

		final Map<Long, Shipment> shipments = new HashMap<>();
		while(rs.next()){
		final Long id = rs.getLong("id");
			if(!shipments.containsKey(id)){
				shipments.put(id, new Shipment(id, rs.getLong("user_id"),
						rs.getLong("order_id"), rs.getLong("item_id"), rs.getByte("initial_state"),
						rs.getByte("current_state"), rs.getLong("shipment_date"),
						rs.getLong("last_updated_date"), null));
			}
		}
		return shipments.values();
	}

}
