package com.wedoshoes.oms.dao.rowmapper.order;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wedoshoes.oms.model.order.WedoService;

public class WedoServiceRowMapper implements RowMapper<WedoService> {

	@Override
	public WedoService mapRow(final ResultSet rs, final int rowNum) throws SQLException {
		return new WedoService(rs.getInt("service_id"), rs.getShort("quantity"), rs.getBigDecimal("cost"));
	}
}
