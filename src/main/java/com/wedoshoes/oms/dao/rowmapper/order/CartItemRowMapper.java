package com.wedoshoes.oms.dao.rowmapper.order;

import static com.wedoshoes.oms.dao.impl.DefaultUtils.nullIfDefaultInteger;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wedoshoes.oms.model.Size;
import com.wedoshoes.oms.model.order.Item;
public class CartItemRowMapper implements RowMapper<Item>{

	@Override
	public Item mapRow(ResultSet rs, int rowNum) throws SQLException {
		return Item.createItemSummay(rs.getLong("id"), null,
				null, null, null, null,null,null, null,null,null, null, null,
				rs.getInt("parent_service_id"), rs.getInt("category_id"),
				rs.getInt("product_id"), nullIfDefaultInteger(rs.getInt("size")),
				rs.getInt("brand_id"), nullIfDefaultInteger(rs.getInt("item_size_type")) == null ? null : 
					Size.codeToType(rs.getInt("item_size_type")),
				rs.getBoolean("is_express_processing"), rs.getBoolean("packing"), null, null, null, null, null, null,null);		
	}
}
