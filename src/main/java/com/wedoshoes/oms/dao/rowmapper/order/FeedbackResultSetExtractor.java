package com.wedoshoes.oms.dao.rowmapper.order;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.wedoshoes.oms.model.order.Feedback;
import com.wedoshoes.oms.model.order.FeedbackType;

public class FeedbackResultSetExtractor implements ResultSetExtractor<Collection<Feedback>> {

	@Override
	public Collection<Feedback> extractData(ResultSet rs) throws SQLException, DataAccessException {
		final Map<Long, Feedback> map = new HashMap<>();
		
		while(rs.next()) {
			final Long itemId = rs.getLong("item_id");
			/**
			 * Create and Store Item if not exist.
			 */
			if(!map.containsKey(itemId)) {
				map.put(itemId, new Feedback(null, rs.getLong("user_id"), rs.getLong("order_id"), itemId,
						rs.getString("name"), rs.getString("email"), rs.getString("cc"),
						rs.getString("phone"),
						rs.getString("feedback"), rs.getLong("create_date"), rs.getShort("rating"),
						new ArrayList<FeedbackType>()));
			}
			map.get(itemId).getTypes().add(FeedbackType.codeToCategory((rs.getInt("type"))));
		}
		 
		return map.values();
	}

}
