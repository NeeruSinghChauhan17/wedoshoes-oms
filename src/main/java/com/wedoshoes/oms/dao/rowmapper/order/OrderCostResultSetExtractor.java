package com.wedoshoes.oms.dao.rowmapper.order;

import static com.wedoshoes.oms.dao.impl.DefaultUtils.nullIfDefaultBigDecimal;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.nullIfDefaultInteger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.jdbc.core.ResultSetExtractor;

import com.google.common.collect.HashBasedTable;
import com.wedoshoes.oms.model.order.Discount;
import com.wedoshoes.oms.model.order.GST;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.Order;
import com.wedoshoes.oms.model.order.PriceValueType;
import com.wedoshoes.oms.model.order.WedoService;
import com.wedoshoes.oms.model.payment.PaymentAdjustmentType;

public class OrderCostResultSetExtractor implements ResultSetExtractor<Optional<Order>> {

	@Override
	public Optional<Order> extractData(final ResultSet rs) throws SQLException {
		
		final Map<Long, Order> orders = new HashMap<>();
		final HashBasedTable<Long, Long, Item> orderItems = HashBasedTable.create();
		final HashBasedTable<Long, Integer, Discount> itemDiscounts = HashBasedTable.create();
		final HashBasedTable<Long, Integer, WedoService> itemServices = HashBasedTable.create();
		while(rs.next()) {
			final Long orderId = rs.getLong("id");
			final Long itemId = rs.getLong("item_id");
			final Integer discountId = nullIfDefaultInteger(rs.getInt("discount_id"));
			final Integer serviceId = nullIfDefaultInteger(rs.getInt("service_id"));
			/**
			 * Create and Store order if not exist.
			 */
			if(!orders.containsKey(orderId)) {
				orders.put(orderId, Order.createWithAdvancePayment(orderId, 
						rs.getBigDecimal("advance_payment"))); 
			}
			
			/**
			 * Add item if not added. 
			 */
			if(!orderItems.contains(orderId, itemId)) {
				orderItems.put(orderId, itemId, extractOrderItem(orderId, itemId, rs));
			}
			
			
			/**
			 * Add discount if not added. 
			 */
			if(discountId != null && !itemDiscounts.contains(itemId, discountId)) {
				itemDiscounts.put(itemId, discountId, extractDiscount(discountId, rs));
			}
			
			/**
			 * Add service if not added. 
			 */
			if(serviceId != null && !itemServices.contains(itemId, serviceId)) {
				itemServices.put(itemId, serviceId, extractWedoservice(serviceId, rs));
			}
			
		}
		
		if(orders.isEmpty()) { 
			return Optional.empty();
		}
		
		return orders.values().stream().map(order -> Order.createWithItems(order, 
				getItemsForOrder(orderItems.row(order.getId()).values(),itemServices,itemDiscounts)))
				.collect(Collectors.toList()).stream().findFirst();
	}
	
	private List<Item> getItemsForOrder(final Collection< Item> items,
			final HashBasedTable<Long, Integer, WedoService> itemServices,
			final HashBasedTable<Long, Integer, Discount> itemDiscounts) {
		return items.stream().map(item -> Item.createWithServicesAndDiscounts(item, 
				itemServices.row(item.getId()).values(), 
				itemDiscounts.row(item.getId()).values()))
				.collect(Collectors.toList());
		
	}
	private Item extractOrderItem(final Long orderId, final Long itemId, 
			final ResultSet rs ) throws SQLException {
		return Item.createWithCostingDetail(itemId, nullIfDefaultBigDecimal(
				rs.getBigDecimal("adjustment")), rs.getString("adjustment_type") == null ? null : 
					PaymentAdjustmentType.codeToType(rs.getInt("adjustment_type")), 
					nullIfDefaultBigDecimal(rs.getBigDecimal("cost_credited")), 
					nullIfDefaultBigDecimal(rs.getBigDecimal("bad_debts")), 
					 new GST(rs.getBigDecimal("igst"), rs.getBigDecimal("cgst"), rs.getBigDecimal("sgst"), null), rs.getBigDecimal("convenience_charge"), 
					nullIfDefaultBigDecimal(rs.getBigDecimal("express_processing_charge")), 
					nullIfDefaultBigDecimal(rs.getBigDecimal("packing_charge")),
					rs.getByte("state"));
	}
	
	
	private Discount extractDiscount(final Integer discountId, final ResultSet rs) throws SQLException {
		return new Discount(discountId,
				rs.getBigDecimal("discount_value"), 
				PriceValueType.valueOf(rs.getString("discount_value_type")));
	}
	
	private WedoService extractWedoservice(final Integer serviceId, final ResultSet rs) throws SQLException {
		return new WedoService(serviceId,
				rs.getShort("quantity"), rs.getBigDecimal("cost"));
	}

}
