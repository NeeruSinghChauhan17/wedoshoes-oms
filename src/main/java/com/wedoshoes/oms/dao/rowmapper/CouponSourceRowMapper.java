package com.wedoshoes.oms.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wedoshoes.oms.model.CouponSource;

public class CouponSourceRowMapper implements RowMapper<CouponSource>{

	@Override
	public CouponSource mapRow(ResultSet rs, int arg1) throws SQLException {
		
		return new CouponSource(rs.getInt("id"), null);
	}

}
