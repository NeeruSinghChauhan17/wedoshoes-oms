package com.wedoshoes.oms.dao.rowmapper.order;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wedoshoes.oms.model.order.Discount;
import com.wedoshoes.oms.model.order.PriceValueType;

public class DiscountRowMapper implements RowMapper<Discount> {

	@Override
	public Discount mapRow(final ResultSet rs, int arg1) throws SQLException {
		return new Discount(rs.getInt("id"), rs.getBigDecimal("value"), 
				PriceValueType.valueOf(rs.getString("value_type")));
	}
}
