package com.wedoshoes.oms.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.jdbc.core.ResultSetExtractor;

import com.wedoshoes.oms.model.shipment.ShipmentItem;
import com.wedoshoes.oms.model.shipment.ShipmentOrder;

public class ShipmentOrderResultSetExtractor implements ResultSetExtractor<Collection<ShipmentOrder>> {

	@Override
	public Collection<ShipmentOrder> extractData(final ResultSet rs) throws SQLException {
		final HashMap<Long,List<ShipmentItem>> orders = new HashMap<>();
		while(rs.next()) {
			final Long orderId = rs.getLong("order_id");
			final Long itemId = rs.getLong("item_id");
			final Byte state = rs.getByte("current_state");
			final Long shipmentId = rs.getLong("id");
			
			if(!orders.containsKey(orderId)) {
				orders.put(orderId, new ArrayList<>());
			}
			orders.get(orderId).add(new ShipmentItem(itemId, shipmentId, state));
		}
		return orders.entrySet().stream().map( entry -> new ShipmentOrder(entry.getKey(), 
				entry.getValue())).collect(Collectors.toList());
	}

}
