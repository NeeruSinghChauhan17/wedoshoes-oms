package com.wedoshoes.oms.dao.rowmapper.order;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import static com.wedoshoes.oms.dao.impl.DefaultUtils.nullIfDefaultString;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.nullIfDefaultByte;
import com.wedoshoes.oms.model.state.StateTransition;
import com.wedoshoes.oms.model.state.UsersLocation;

public class ItemStateTransitionRowMapper implements RowMapper<StateTransition> {

	@Override
	public StateTransition mapRow(final ResultSet rs, int rowNum) throws SQLException {
		return new StateTransition(rs.getLong("id"), rs.getLong("user_id"), rs.getLong("create_date"),
				rs.getByte("state"), nullIfDefaultString(rs.getString("comment")),
				nullIfDefaultByte(rs.getByte("col_point_id")), nullIfDefaultByte(rs.getByte("workshop_id")), null,  new UsersLocation(null,rs.getDouble("latitude"), rs.getDouble("longitude")));
	}

}
