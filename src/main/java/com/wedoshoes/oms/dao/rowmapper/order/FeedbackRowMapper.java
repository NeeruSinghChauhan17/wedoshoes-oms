package com.wedoshoes.oms.dao.rowmapper.order;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

import org.springframework.jdbc.core.RowMapper;

import com.wedoshoes.oms.model.order.Feedback;
import com.wedoshoes.oms.model.order.FeedbackType;

public class FeedbackRowMapper  implements RowMapper<Feedback>{

	@Override
	public Feedback mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Feedback(rs.getLong("id"), rs.getLong("user_id"), rs.getLong("order_id"), 
				rs.getLong("item_id"), rs.getString("name"), rs.getString("email"),rs.getString("cc"), rs.getString("phone"),
				rs.getString("feedback"), rs.getLong("create_date"), rs.getShort("rating"), 
				Arrays.asList(FeedbackType.valueOf(rs.getString("type"))));
	} 

}
