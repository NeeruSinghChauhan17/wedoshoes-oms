package com.wedoshoes.oms.dao.rowmapper.cart;

import static com.wedoshoes.oms.dao.impl.DefaultUtils.nullIfDefault;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.jdbc.core.ResultSetExtractor;

import com.google.common.collect.Lists;
import com.wedoshoes.oms.model.Size;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.WedoService;

public class CartItemResultSetExtractor implements ResultSetExtractor<List<Item>> {

	@Override
	public List<Item> extractData(ResultSet rs) throws SQLException {
		final Map<Long, Item> items = new LinkedHashMap<>();
		while(rs.next()) {
			final Long itemId = rs.getLong("item_id");
			final WedoService service = new WedoService(rs.getInt("service_id"),
					rs.getShort("quantity"), null);
			if(!items.containsKey(itemId)) {
				items.put(itemId, new Item(itemId, null, null, null, rs.getLong("item_create_date"),null,
						null,null,null,null,null,null, null, rs.getInt("parent_service_id"), rs.getInt("category_id"),
						rs.getInt("product_id"), rs.getInt("size"), rs.getInt("brand_id"),
						nullIfDefault(Size.codeToType(rs.getInt("item_size_type")), Size.NO_SIZE),
						Lists.newArrayList(), null, null, rs.getBoolean("is_express_processing"),
						rs.getBoolean("packing"), null,null,null,null,null, /*comments*/ null, null, null));
			}
			items.get(itemId).getServices().add(service);
		}
		return items.values().stream().collect(Collectors.toList());
	}

}
