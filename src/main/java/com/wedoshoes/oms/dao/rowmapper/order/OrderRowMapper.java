package com.wedoshoes.oms.dao.rowmapper.order;

import static com.wedoshoes.oms.dao.impl.DefaultUtils.nullIfDefaultString;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.nullIfDefaultLong;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wedoshoes.oms.model.order.Address;
import com.wedoshoes.oms.model.order.Order;

public class OrderRowMapper implements RowMapper<Order> {

	@Override
	public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Order(rs.getLong("id"), rs.getLong("user_id"), 
				nullIfDefaultString(rs.getString("customer_name")),rs.getString("last_name"),rs.getString("cc"),
				rs.getLong("phone"), nullIfDefaultLong(rs.getLong("alternate_phone")), 
				rs.getByte("pick_up_timeslot"), rs.getLong("pick_up_date"), null,
				rs.getLong("create_date"),rs.getInt("create_time"),
				rs.getLong("creater_user_id"), null, 
					new Address(nullIfDefaultString(rs.getString("pick_up_street_address")),
							nullIfDefaultString(rs.getString("pick_up_city")),
							nullIfDefaultString(rs.getString("pick_up_state")),
							nullIfDefaultString(rs.getString("pick_up_pin")),
							nullIfDefaultString(rs.getString("pick_up_landmark")),
							rs.getInt("pick_up_locality"),
							rs.getDouble("pick_up_latitude"),
							rs.getDouble("pick_up_longitude")),
							new Address(nullIfDefaultString(rs.getString("delivery_street_address")),
									nullIfDefaultString(rs.getString("delivery_city")),
									nullIfDefaultString(rs.getString("delivery_state")),
									nullIfDefaultString(rs.getString("delivery_pin")),
									nullIfDefaultString(rs.getString("delivery_landmark")),
									rs.getInt("delivery_locality"),
									rs.getDouble("delivery_latitude"),
									rs.getDouble("delivery_longitude")), null,
									rs.getLong("referred_by_user_id"),
									rs.getByte("workshop_id"));
	}
}
