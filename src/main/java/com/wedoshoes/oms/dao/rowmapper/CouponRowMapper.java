package com.wedoshoes.oms.dao.rowmapper;

import static com.wedoshoes.oms.dao.impl.DefaultUtils.*;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wedoshoes.oms.model.Coupon;
import com.wedoshoes.oms.model.order.PriceValueType;

public class CouponRowMapper implements RowMapper<Coupon> {

	@Override
	public Coupon mapRow(ResultSet rs, int arg1) throws SQLException {
		
		return new Coupon(rs.getInt("id"), rs.getString("code"),null,
				rs.getBigDecimal("value"), PriceValueType.valueOf(rs.getString("value_type")),
				nullIfDefaultLong(rs.getLong("start_date")), nullIfDefaultLong(rs.getLong("end_date")),
				null, null,	null, null, null, null, null, null, null);
	}

}
