package com.wedoshoes.oms.dao;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;

/**
 * 
 * @author Navrattan Yadav
 *
 */
public interface BaseDao {

	NamedParameterJdbcOperations getJdbcTemplate();

	String getQueryById(String id);
}
