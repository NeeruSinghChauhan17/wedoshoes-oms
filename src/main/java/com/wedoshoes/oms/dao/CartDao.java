package com.wedoshoes.oms.dao;

import java.util.List;
import java.util.Optional;

import com.wedoshoes.oms.model.order.Cart;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.WedoService;

public interface CartDao {

	Long saveCart(Cart cart);

	Long saveCartItem(Long cartId, Item item);
	
	List<Item> findCartItemsByCartId(Long cartId);

	void saveCartItemServices(Long itemId, List<WedoService> services);
	
	void deleteServiceByServiceId(Long itemId, Long serviceId);
	
	void deleteServiceByCartId(Long cartId);
	
	void deleteServiceByItemId(Long itemId);
	
	void deleteItemByItemId(Long itemId);
	
	void deleteItemsBycartId(Long cartId);

	Optional<Cart> findCartByUserId(Long userId);
	
	Optional<Cart> findCartById(Long id);

	List<Cart> findCarts(Integer offset, Integer limit);

	void deActivateCartById(Long cartId);

	void increaseCartSize(Long cartId, Integer size);
	
	void decreaseCartSize(Long cartId, Integer size);
	
	void updateItem(Byte workshopId, Long cartId, Item item);
}