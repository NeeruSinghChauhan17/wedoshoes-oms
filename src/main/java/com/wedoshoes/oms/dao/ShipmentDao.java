package com.wedoshoes.oms.dao;

import java.util.Collection;
import java.util.Optional;

import com.wedoshoes.oms.model.shipment.Shipment;
import com.wedoshoes.oms.model.shipment.ShipmentOrder;
import com.wedoshoes.oms.model.shipment.ShipmentSummary;
import com.wedoshoes.oms.model.state.StateTransition;

public interface ShipmentDao {

	Long save(Shipment shipment);
	void update(Long id, Shipment shipment);
	void updateShipmentItemState(Byte state, Long itemId, Long shipmentId);
	void updateShipmentOrderItemState(Byte state, Long orderId);
	Optional<Shipment> find(Long id);
	Collection<ShipmentOrder> findOrderByDateAndUser(Long date, Long userId);
	Optional<Shipment> findShipmentByDate(Long date, Long orderId, Long itemId);
	void saveCustomersSignature(Byte workshopId, Long shipmentId,
			StateTransition transition);
	
	Optional<Shipment> findShipmentByItemId(Long itemId);
	Collection<Shipment> findShipmentByOrderId(Long orderId);
	Optional<ShipmentSummary> findShipmentSummary(Long userId, Long shipmentDate);
}
