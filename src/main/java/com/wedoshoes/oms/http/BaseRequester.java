package com.wedoshoes.oms.http;

import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

public interface BaseRequester {

    RestTemplate getRestTemplate();
	String getURLById(String id);
	HttpHeaders getBasicHeaders();
}
