package com.wedoshoes.oms.http.impl;

import static com.wedoshoes.oms.dao.impl.DefaultUtils.defaultStringIfNull;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.ServerErrorException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.HttpStatusCodeException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wedoshoes.oms.exception.CustomException;
import com.wedoshoes.oms.http.BaseRequester;
import com.wedoshoes.oms.http.CoreServerHttpRequester;
import com.wedoshoes.oms.http.impl.model.core.User;
import com.wedoshoes.oms.model.notification.Notification;
import com.wedoshoes.oms.security.Token;

@Repository
public class CoreServerHttpRequesterImpl implements CoreServerHttpRequester {

	private static final Logger LOGGER = LoggerFactory.getLogger(CoreServerHttpRequesterImpl.class);
	
	private final BaseRequester baseRequester;
	
	@Autowired
	public CoreServerHttpRequesterImpl(@Value("${core.root.url}") final String rootUrl,
			final BaseRequesterFactory baseRequesterFactory) {
		this.baseRequester = baseRequesterFactory.createBaseHttpReqester("core_api.xml",
				rootUrl);
	}

	@Override
	public Token authenticate(final Long userId, final Token token) {
		
		final Map<String, Object> urlParam = new HashMap<>();
		urlParam.put("userId", userId);
		final String url = URLParamMapper.map(baseRequester.
				getURLById("authenticate"), urlParam);
		
		final HttpEntity<Token> entity = new HttpEntity<>(token,baseRequester.getBasicHeaders());
		try{
			final ResponseEntity<Token> response = baseRequester.getRestTemplate().
					exchange(url, HttpMethod.POST, entity, Token.class);
			if (response.getStatusCode() == HttpStatus.OK) {
				return response.getBody();
			}
		}catch (Exception e) {
			LOGGER.error("Authorization Error : userId = {},Token= {},Status= {} ",userId,
					token.getAuthToken(),e.getMessage());
		}
		throw new UnAuthorizedException();
	}

	@Override
	public Token authorize(final Long userId, final String action, final Token token) {
		final Map<String, Object> urlParam = new HashMap<>();
		urlParam.put("userId", userId);
		urlParam.put("action", action);
		final String url = URLParamMapper.map(baseRequester.
				getURLById("authorize"), urlParam);
		final HttpEntity<Token> entity = new HttpEntity<>(token,baseRequester.getBasicHeaders());
		try{
			final ResponseEntity<Token> response = baseRequester.getRestTemplate().
					exchange(url, HttpMethod.POST, entity, Token.class);
			if (response.getStatusCode() == HttpStatus.OK) {
				return response.getBody();
			}
		}catch(HttpStatusCodeException e){
			if (e.getStatusCode()==HttpStatus.FORBIDDEN){
				throw  new CustomException(HttpStatus.FORBIDDEN.value(),"This account does not have access !!");
			}else if(e.getStatusCode()==HttpStatus.UNAUTHORIZED){
				throw new UnAuthorizedException();
			}
		}catch (Exception e) {
			LOGGER.error("Authorization Error : userId = {},Token= {},Status={} ",userId,
					token.getAuthToken(),e.getMessage());
		}
		throw new UnAuthorizedException();
	}

	@Override
	public void sendNotification(final Byte workshopId, final Long userId,
			final Notification notification) {
		
		try {
			LOGGER.info("Notification:  " +new ObjectMapper().writeValueAsString(notification));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		final Map<String, Object> urlParam = new HashMap<>();
		urlParam.put("workshopId", workshopId);
		urlParam.put("userId", userId);
		final String url = URLParamMapper.map(baseRequester.
				getURLById("send.notification"), urlParam);
		
		final HttpEntity<Notification> entity = new HttpEntity<>(notification, 
				baseRequester.getBasicHeaders());

		final ResponseEntity<Token> response = baseRequester.getRestTemplate().
				exchange(url, HttpMethod.PUT, entity, Token.class);
		if (response.getStatusCode() == HttpStatus.NO_CONTENT) {
			LOGGER.error("Notification Sent. {} ",notification);
		} else {
			LOGGER.error("Error while sending notificaion to : userId = {}, workshopId = {}, Notification= {} ",
					workshopId, userId, notification);
		}
	}
	
	@Cacheable(cacheNames="usersName", key="#userId")
	@Override
	public String getUserNameById(final Long userId) {
		try {
			final Map<String, Object> params = new HashMap<>();
			params.put("userId", userId);
			final ResponseEntity<User> response = baseRequester.getRestTemplate().
					getForEntity(URLParamMapper.map(baseRequester
							.getURLById("get.customer.by.id"), params), User.class);
			if (response.getStatusCode() != HttpStatus.OK) {
				LOGGER.error("Error while get Customer Name for id ={} status={}",
						userId, response.getStatusCode());
				throw new ServerErrorException(response.getStatusCode().value());
			}
			final User user=response.getBody();
			return user.getName()+" "+defaultStringIfNull(user.getLastName());
		} catch (Exception e) {
			LOGGER.error("Error while get Customer Name for id ={} due to = {}",userId, e);
			throw new ServerErrorException(500, e);
		}
	}
	
	@Cacheable(cacheNames="usersType", key="#userId")
	@Override
	public Byte getUserTypeById(final Long userId) {
		try {
			final Map<String, Object> params = new HashMap<>();
			params.put("userId", userId);
			final ResponseEntity<User> response = baseRequester.getRestTemplate().
					getForEntity(URLParamMapper.map(baseRequester
							.getURLById("get.customer.by.id"), params), User.class);
			if (response.getStatusCode() != HttpStatus.OK) {
				LOGGER.error("Error while get Customer Type for id ={} status={}",
						userId, response.getStatusCode());
				throw new ServerErrorException(response.getStatusCode().value());
			}
			return response.getBody().getType();
		} catch (Exception e) {
			LOGGER.error("Error while get Customer Type for id ={} due to = {}",userId, e);
			throw new ServerErrorException(500, e);
		}
	}

//	@Override
//	public void notifyMail(final Notification notification) {
//		
//		final Map<String, Object> urlParam = new HashMap<>();
//		urlParam.put("userId", userId);
//		final String url = URLParamMapper.map(baseRequester.
//				getURLById("authenticate"), urlParam);
//		
//		final HttpEntity<Token> entity = new HttpEntity<>(token,baseRequester.getBasicHeaders());
//
//		final ResponseEntity<Token> response = baseRequester.getRestTemplate().
//				exchange(url, HttpMethod.POST, entity, Token.class);
//		if (response.getStatusCode() == HttpStatus.OK) {
//			return response.getBody();
//		}
//		
//		LOGGER.error("Error : userId = {}, State = {}, Token= {} ",
//				userId,response.getStatusCode(), token);
//		throw new CustomException(response.getStatusCode().value(), "UnAuthorized");
//		
//	}

}
