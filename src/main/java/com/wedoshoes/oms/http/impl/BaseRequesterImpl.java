package com.wedoshoes.oms.http.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import com.wedoshoes.oms.http.BaseRequester;
import com.wedoshoes.oms.provider.filter.HeaderParam;

/**
 * 
 * @author Navrattan Yadav
 *
 */
public class BaseRequesterImpl implements BaseRequester {

	private final RestTemplate restTemplate;
	private final Map<String, String> urlMap;
	private final HttpHeaders HEADERS = new HttpHeaders();
	
	
	
	public BaseRequesterImpl(final RestTemplate restTemplate, final Map<String, String> urlMap,
			final String serverId, final String secret) {
		this.restTemplate = restTemplate;
		this.urlMap = Collections.unmodifiableMap(urlMap);
		HEADERS.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HEADERS.setContentType(MediaType.APPLICATION_JSON);
		HEADERS.add(HeaderParam.SERVER_ID, serverId);
		HEADERS.add(HeaderParam.SECRET, secret);
	}

	@Override
	public RestTemplate getRestTemplate() {
		return restTemplate;
	}

	@Override
	public String getURLById(final String id) {
		return urlMap.get(id);
	}

	@Override
	public HttpHeaders getBasicHeaders() {
		return HEADERS;
	}
}
