package com.wedoshoes.oms.http.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import com.wedoshoes.oms.exception.PaymentRequiredException;
import com.wedoshoes.oms.http.BaseRequester;
import com.wedoshoes.oms.http.PWServerHttpRequester;
import com.wedoshoes.oms.model.Transaction;
import com.wedoshoes.oms.model.TransactionType;
import com.wedoshoes.oms.model.Wallet;
import com.wedoshoes.oms.model.payment.PaymentStatus;
import com.wedoshoes.oms.model.payment.PaytmPaymentStatus;
import com.wedoshoes.oms.model.payment.PayuPaymentStatus;

@Repository
public class PwServerHttpRequesterImpl implements PWServerHttpRequester {

	private static final Logger LOGGER = LoggerFactory.getLogger(PwServerHttpRequesterImpl.class);
	private final BaseRequester baseHttpRequester;

	@Autowired
	public PwServerHttpRequesterImpl(@Value("${pw.root.url}") final String rootUrl,
			final BaseRequesterFactory httpRequesterFactory) {
		this.baseHttpRequester = httpRequesterFactory.createBaseHttpReqester("pw_api.xml", rootUrl);
	}

	@Override
	public void createWallet(final Long userId) {
		try {

			final Map<String, Object> urlParam = new HashMap<>();
			urlParam.put("userId", userId);
			final HttpEntity<Wallet> entity = new HttpEntity<>(Wallet.createFromBalance(BigDecimal.ZERO),
					baseHttpRequester.getBasicHeaders());

			final ResponseEntity<Wallet> response = baseHttpRequester.getRestTemplate().exchange(
					URLParamMapper.map(baseHttpRequester.getURLById("create.wallet"), urlParam), HttpMethod.POST,
					entity, Wallet.class);
			if (response.getStatusCode() != HttpStatus.OK) {
				LOGGER.error("Error while create wallet for user = {} due to status {}", userId,
						response.getStatusCodeValue());
			}
		} catch (Exception e) {
			LOGGER.error("Error while create wallet for user = {} due to {}", userId, e);
		}
	}

	@Override
	public void addCredit(final Long userId, final BigDecimal credit) {
		try {
			final Map<String, Object> urlParam = new HashMap<>();
			urlParam.put("userId", userId);
			final HttpEntity<Transaction> entity = new HttpEntity<>(
					Transaction.createFromBalanceAndType(credit, TransactionType.CREDIT),
					baseHttpRequester.getBasicHeaders());

			final ResponseEntity<Transaction> response = baseHttpRequester.getRestTemplate().exchange(
					URLParamMapper.map(baseHttpRequester.getURLById("add.credit"), urlParam), HttpMethod.POST, entity,
					Transaction.class);
			if (response.getStatusCode() != HttpStatus.OK) {
				LOGGER.error("Error while addinging create from wallet for user = {}", userId);
			}
		} catch (Exception e) {
			LOGGER.error("Error while addinging create from wallet for user = {}", userId);
		}
	}

	@Override
	public void deductCredit(Long userId, BigDecimal credit) {
		try {
			final Map<String, Object> urlParam = new HashMap<>();
			urlParam.put("userId", userId);
			final HttpEntity<Transaction> entity = new HttpEntity<>(
					Transaction.createFromBalanceAndType(credit, TransactionType.DEBIT),
					baseHttpRequester.getBasicHeaders());

			final ResponseEntity<Transaction> response = baseHttpRequester.getRestTemplate().exchange(
					URLParamMapper.map(baseHttpRequester.getURLById("deduct.credit"), urlParam), HttpMethod.POST,
					entity, Transaction.class);
			if (response.getStatusCode() != HttpStatus.OK) {
				LOGGER.error("Error while deducting create from wallet for user = {}", userId);
			}
		} catch (Exception e) {
			LOGGER.error("Error while deducting create from wallet for user = {}", userId);
		}
	}

	@Override
	public PaytmPaymentStatus checkPaytmTransactionStatus(final Long userId, final PaymentStatus paymentStatus) {
		try {
			final Map<String, Object> urlParam = new HashMap<>();
			urlParam.put("userId", userId);
			final HttpEntity<PaymentStatus> entity = new HttpEntity<>(paymentStatus,
					baseHttpRequester.getBasicHeaders());
			final ResponseEntity<PaytmPaymentStatus> response = baseHttpRequester.getRestTemplate().exchange(
					URLParamMapper.map(baseHttpRequester.getURLById("check.paytm.transaction.status"), urlParam),
					HttpMethod.POST, entity, PaytmPaymentStatus.class);
			if (response.getStatusCode() == HttpStatus.OK) {
				return response.getBody();
			}
			LOGGER.error("transaction was not success with txnid = ", paymentStatus.getTransactionId());
		} catch (Exception e) {
			LOGGER.error("Error while doing payment with txnid", paymentStatus.getTransactionId(), e);
			throw new PaymentRequiredException();
		}
		throw new PaymentRequiredException();
	}

	@Override
	public PayuPaymentStatus checkPayuTransactionStatus(final Long userId, final String transactionId) {
		try {

			final Map<String, Object> urlParam = new HashMap<>();
			urlParam.put("userId", userId);
			final HttpEntity<String> entity = new HttpEntity<>(transactionId,
					baseHttpRequester.getBasicHeaders());
			final ResponseEntity<PayuPaymentStatus> response = baseHttpRequester.getRestTemplate().exchange(
					URLParamMapper.map(baseHttpRequester.getURLById("check.payu.transaction.status")+transactionId, urlParam),
					HttpMethod.GET, entity, PayuPaymentStatus.class);
			if (response.getStatusCode() == HttpStatus.OK) {
					return response.getBody();
				
			}
			LOGGER.error("transaction was not success with txnid = ", transactionId);
		} catch (Exception e) {
			LOGGER.error("Error while doing payment with txnid", transactionId, e);
			throw new PaymentRequiredException();
		}
		throw new PaymentRequiredException();
	}
}
