package com.wedoshoes.oms.http.impl.model.cms;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class TimeSlot {

	private final Byte id;
	private final String name;
	
	public TimeSlot() {
		this(null, null);
	}
	
	public TimeSlot(final Byte id, final String name) {
		this.id = id;
		this.name = name;
	}
	
	@JsonProperty("id")
	public Byte getId() {
		return id;
	}
	
	@JsonProperty("name")
	public String getName() {
		return name;
	}
	
	public static TimeSlot createFromId(final Byte id) {
		return new TimeSlot(id, null);
	}
	
	public static TimeSlot createWithId(final Byte id, final TimeSlot timeSlot) {
		return new TimeSlot(id, timeSlot.getName());
	}
}
