package com.wedoshoes.oms.http.impl;

import java.util.Map;

import org.springframework.util.StringUtils;

public class URLParamMapper {

	public static String map(final String url, final Map<String, Object> params ) {
		if(StringUtils.isEmpty(url)) {
			throw new InvalidURLException("Empty or Null URL");
		}
		String trimedURL = url.trim();
		if(params != null ) {
			for (Map.Entry<String, Object> e : params.entrySet()) {
				trimedURL = trimedURL.replace("{"+e.getKey()+"}", e.getValue().toString());
			}
		}
		
		if(isParamMissing(trimedURL)) {
			throw new InvalidURLException("URL Query params are missing " + trimedURL);
		}
		return trimedURL;
		
	}
	
		
	private static Boolean isParamMissing(final String query) {
		if(query.matches("(.*)[{](.*.)[}](.*)")) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
