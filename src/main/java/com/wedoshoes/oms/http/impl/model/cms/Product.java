package com.wedoshoes.oms.http.impl.model.cms;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This call represent a product eg. Sports Shoes, Hand Bag, Travel Bag
 * @author Navrattan Yadav
 *
 */

@JsonInclude(Include.NON_NULL)
public class Product {
	private final Integer id;
	private final Integer categoryId;
	private final String name;
	private final String description;
	private final List<Integer> sizeTypes;
	private final List<WeDoService> services;
	private final List<Integer> brands;
	private final String imageUrl;
	
	@SuppressWarnings("unused")
	private Product() {
		this(null, null, null, null, null, null, null, null);
	}
	
	public Product(final Integer id, final Integer categoryId,
			final String name, final String description,
			final List<Integer> sizeTypes, final List<WeDoService> services,
			final List<Integer> brands , final String imageUrl) {
		this.categoryId = categoryId;
		this.id = id;
		this.name = name;
		this.description = description;
		this.sizeTypes = sizeTypes;
		this.services = services;
		this.brands = brands;
		this.imageUrl= imageUrl;
	}

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("size_types")
	public List<Integer> getSizeTypes() {
		return sizeTypes;
	}

	@JsonProperty("category_id")
	public Integer getCategoryId() {
		return categoryId;
	}

	@JsonProperty("services")
	public List<WeDoService> getServices() {
		return services;
	}
	
	@JsonProperty("brands")
	public List<Integer> getBrands() {
		return brands;
	}
	
	@JsonProperty("image_url")
	public String getImageUrl() {
		return imageUrl;
	}

	public static Product createFromId(final Integer id) {
		return new Product(id, null, null, null, null, null, null, null);
	}
	
	public static Product createWithId(final Integer id, final Product product) {
		return new Product(id, product.getCategoryId(), product.getName(), product.getDescription(),
				product.getSizeTypes(), product.getServices(), product.getBrands(), product.getImageUrl());
	}
	
	public static Product createWithBrandsAndSizes(final List<Integer> brands,
			final List<Integer> sizeTypes, final Product product) {
		return new Product(product.getId(), product.getCategoryId(), product.getName(),
				product.getDescription(), sizeTypes, product.getServices(), brands, product.getImageUrl());
	}
	
	public static Product createWithServices(final List<WeDoService> services, final Product product) {
		return new Product(product.getId(), product.getCategoryId(), product.getName(),
				product.getDescription(), product.getSizeTypes(), services, product.getBrands(), product.getImageUrl());
	}
	
}