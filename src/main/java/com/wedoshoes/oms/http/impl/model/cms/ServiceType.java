package com.wedoshoes.oms.http.impl.model.cms;

import java.util.HashMap;
import java.util.Map;

public enum ServiceType {
	ROOT(0),
	REPAIR(1),
	REPLACE(2);
	
	private static final Map<Integer, ServiceType> CODE_TO_TYPE =  new HashMap<>();
	
	static {
		for(ServiceType serviceType : ServiceType.values()) {
			CODE_TO_TYPE.put(serviceType.getCode(), serviceType);
		}
	}
	private final Integer code;

	private ServiceType(final Integer code) {
		this.code = code;
	}
	
	public Integer getCode() {
		return code;
	}
	
	public static ServiceType codeToType(final Integer code) {
		return CODE_TO_TYPE.get(code);
	}
	
	
}
