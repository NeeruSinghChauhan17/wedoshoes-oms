package com.wedoshoes.oms.http.impl.model.cms;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class SizeType {

	private final Integer id;
	private final String name;
	
	// for JAX-B
	@SuppressWarnings("unused")
	private SizeType() {
		this(null, null);
	}
	
	public SizeType(final Integer id, final String name) {
		this.id = id;
		this.name = name;
	}

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}
	
	public static SizeType createFromId(final Integer id) {
		return new SizeType(id, null);
	}
	
	public static SizeType createWithId(final Integer id, final SizeType sizeType) {
		return new SizeType(id, sizeType.getName());
	}
}
