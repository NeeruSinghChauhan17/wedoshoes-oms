package com.wedoshoes.oms.http.impl.model.cms;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class represent Price for repair price for repair services.
 * @author Navrattan Yadav
 *
 */

@JsonInclude(Include.NON_NULL)
public class Price extends AbstractPrice {
	
	private final Integer productSizeType;
	private final BigDecimal materialCost;
	private final BigDecimal laborCost;
	private final Accessory accessory;
	private final BigDecimal profit;
	
	@SuppressWarnings("unused")
	private Price() {
		this(null, null, null, null, null, null, null, null, null);
	}
	
	public Price(final Integer id, final Integer productSizeType, final BigDecimal materialCost,
			final BigDecimal laborCost, final Long startDate, final Long endDate,
			final Boolean currentPrice,final Accessory accessory,final BigDecimal profit) {
		super(id, startDate, endDate, currentPrice);
		this.productSizeType = productSizeType;
		this.materialCost = materialCost;
		this.laborCost = laborCost;
		this.accessory = accessory;
		this.profit = profit;
	}
	

	@JsonProperty("product_size_type")
	public Integer getProductSizeType() {
		return productSizeType;
	}
	
	@JsonProperty("material_cost")
	public BigDecimal getMaterialCost() {
		return materialCost;
	}

	@JsonProperty("labor_cost")
	public BigDecimal getLaborCost() {
		return laborCost;
	}
	
	@JsonProperty("profit")
	public BigDecimal getProfit() {
		return profit;
	}

	@JsonProperty("accessory")
	public Accessory getAccessory() {
		return accessory;
	}
	
	@JsonProperty("total_cost")
	public BigDecimal getTotalCost() {
		BigDecimal totalPrice = BigDecimal.ZERO;
		if(laborCost != null ) {
			totalPrice = totalPrice.add(laborCost);
		}
		if(profit != null ) {
			totalPrice = totalPrice.add(profit);
		}
		
		if(materialCost != null ) {
			totalPrice = totalPrice.add(materialCost);
		} else if(accessory != null && accessory.getPrice() != null 
				&& accessory.getPrice().getAccessoryCost() != null) {
			totalPrice = totalPrice.add(accessory.getPrice().getAccessoryCost());
		}
		
		return totalPrice;
	}

	public static Price createWithCurrrentPriceAndStartDate(final Boolean currentPrice,
			final Long startDate, final Price price) {
		return new Price(price.getId(), price.getProductSizeType(),
				price.getMaterialCost(), price.getLaborCost(), startDate,
				price.getEndDate(), currentPrice, price.getAccessory(), price.getProfit());
	}
	
	public static Price createWithCurrentPriceAndEndDate(final Boolean currentPrice,
			final Long endDate, final Price price) {
		return new Price(price.getId(), price.getProductSizeType(),
				price.getMaterialCost(), price.getLaborCost(),
				price.getStartDate(), endDate, currentPrice,price.getAccessory(), price.getProfit());
	}
}
