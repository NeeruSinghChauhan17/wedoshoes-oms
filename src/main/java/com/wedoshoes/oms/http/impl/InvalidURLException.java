package com.wedoshoes.oms.http.impl;

public class InvalidURLException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	 public InvalidURLException(final String message) {
	        super(message);
	 }
}
