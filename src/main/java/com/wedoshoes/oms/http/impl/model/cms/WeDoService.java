package com.wedoshoes.oms.http.impl.model.cms;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * This class represent a Service
 * @author Navrattan Yadav
 *
 */

@JsonInclude(Include.NON_NULL)
public class WeDoService {

	private final Integer id;
	private final Integer categoryId;
	private final Integer parentId;
	private final String name;
	private final String acronym;
	private final String description;
	private final ServiceType type;
	private final Boolean hasChildren;
	
	private final ConvenienceCharges convenienceCharges;
	private final ProcessingTime processingTime;
	
	private final List<WeDoService> subServices;
	private final List<Price> prices;

	@SuppressWarnings("unused")
	private WeDoService() {
		this(null, null, null, null, null, null, null, null, null, null, null, null);
	}

	public WeDoService(final Integer id, final Integer categoryId, final Integer parentId,
			final String name,final String acronym, final String description, final ServiceType type,
			final Boolean hasChildren, final ConvenienceCharges convenienceCharges,
			final ProcessingTime processingTime,
			final List<WeDoService> subServices, final List<Price> prices) {
		this.id = id;
		this.categoryId = categoryId;
		this.parentId = parentId;
		this.name = name;
		this.acronym = acronym;
		this.description = description;
		this.type = type;
		this.hasChildren = hasChildren;
		this.convenienceCharges = convenienceCharges;
		this.processingTime = processingTime;
		this.subServices = subServices;
		this.prices = prices;
	}

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("category_id")
	public Integer getCategoryId() {
		return categoryId;
	}

	@JsonProperty("parent_id")
	public Integer getParentId() {
		return parentId;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("type")
	public ServiceType getType() {
		return type;
	}

	@JsonProperty("has_children")
	public Boolean hasChildren() {
		return hasChildren;
	}
	
	@JsonProperty("convenience_charges")
	public ConvenienceCharges getConvenienceCharges() {
		return convenienceCharges;
	}

	@JsonProperty("processing_time")
	public ProcessingTime getProcessingTime() {
		return processingTime;
	}
	
	@JsonProperty("sub_services")
	public List<WeDoService> getSubServices() {
		return subServices;
	}

	@JsonProperty("prices")
	public List<Price> getPrices() {
		return prices;
	}
	
	@JsonProperty("acronym")
	public String getAcronym() {
		return acronym;
	}

	public static WeDoService createSubService(final Integer categoryId, final Integer parentId,
			final ServiceType type,	final WeDoService service) {
		return new WeDoService(null, categoryId, parentId, service.getName(),service.getAcronym(),
				service.getDescription(), type, false, null, null, null, null);
	}
	
	public static WeDoService createWithId(final Integer id, final WeDoService service) {
		return new WeDoService(id, service.getCategoryId(), service.getParentId(),
				service.getName(),service.getAcronym(), service.getDescription(), service.getType(),
				service.hasChildren(), service.getConvenienceCharges(),
				service.getProcessingTime(),
				service.getSubServices(), service.getPrices());
	}
	
	public static WeDoService createWithMinimalInfo(final WeDoService service) {
		return new WeDoService(service.getId(), null,null,service.getName(),null,
				null, null,null,null,null, null,null);
	}
	
	public static WeDoService createFromId(final Integer id) {
		return new WeDoService(id, null, null, null, null, null, null,
				null, null, null, null, null);
	}
	
	public static WeDoService createFromIdAndPrices(final Integer id, final List<Price> prices) {
		return new WeDoService(id, null, null, null, null, null, null,null,
				null, null, null, prices);
		
	}
}
