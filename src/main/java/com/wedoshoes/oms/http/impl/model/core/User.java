package com.wedoshoes.oms.http.impl.model.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@JsonInclude(Include.NON_NULL)
public class User {

	private final Long id;
	private final String emailId;
	private final String name;
	private final String lastName;
	private final Gender gender;
	private final String imageUrl;
	private final String referralCode;
	private final Byte type;
	private final Long phone;
	private final String cc;
	private final String password;
	private final Boolean isActive;
	private final CommSettings commSettings;
	
	@SuppressWarnings("unused")
	private User() {
		this(null, null,null, null, null, null, null, null, null, null, null, null, null);
	}
	

	public User(final Long id, final String emailId, final String name, final String lastName,final Gender gender,
			final String imageUrl, final String referralCode, final Byte type,
			final String cc, final Long phone, final String password,
			final Boolean isActive, final CommSettings commSettings) {
		this.id = id;
		this.emailId = emailId;
		this.name = name;
		this.lastName=lastName;
		this.gender = gender;
		this.imageUrl = imageUrl;
		this.referralCode = referralCode;
		this.type = type;
		this.phone = phone;
		this.cc = cc;
		this.password = password;
		this.isActive = isActive;
		this.commSettings = commSettings;
	}

	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	
	@JsonProperty("comm_settings")
	public CommSettings getCommSettings() {
		return commSettings;
	}
	
	@JsonProperty("gender")
	public Gender getGender() {
		return gender;
	}

	@JsonProperty("email_id")
	public String getEmailId() {
		return emailId;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("image_url")
	public String getImageUrl() {
		return imageUrl;
	}

	@JsonProperty("referral_code")
	public String getReferralCode() {
		return referralCode;
	}

	@JsonProperty("type")
	public Byte getType() {
		return type;
	}

	@JsonProperty("cc")
	public String getCc() {
		return cc;
	}
	
	@JsonProperty("phone")
	public Long getPhone() {
		return phone;
	}

	@JsonProperty(access = Access.WRITE_ONLY)
	public String getPassword() {
		return password;
	}

	@JsonProperty(access = Access.READ_ONLY, value = "is_active")
	public Boolean isActive() {
		return isActive;
	}

	@JsonProperty("last_name")
	public String getLastName() {
		return lastName;
	}

	
}