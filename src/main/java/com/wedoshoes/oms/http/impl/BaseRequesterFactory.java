package com.wedoshoes.oms.http.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.wedoshoes.oms.http.BaseRequester;

@Component
public class BaseRequesterFactory {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BaseRequesterFactory.class);
	private static final String HTTP_CLASS_PATH = "/http/";
	private final RestTemplate restTemplate;
	private final String serverId;
	private final String secret;
	
	@Autowired
	public BaseRequesterFactory(@Qualifier("restTemplate") final RestTemplate restTemplate,
			@Value("#{server_id}") final String serverId,
			@Value("#{secret}") final String secret) {
		this.restTemplate = restTemplate;
		this.serverId = serverId;
		this.secret = secret;
	}
	
	public BaseRequester createBaseHttpReqester(final String fileName,
			final String rootUrl) {
		try {
			final Properties properties = new Properties();
			properties.loadFromXML(this.getClass().getResourceAsStream(HTTP_CLASS_PATH + "" + fileName));
			final Map<String, String> uRLMap = new HashMap<>();
			properties.forEach((key, value) -> uRLMap.put((String)key, 
					new StringBuilder().append(rootUrl.trim()).append(value.toString().trim()).toString()));
			return new BaseRequesterImpl(restTemplate, uRLMap, serverId, secret);
		} catch ( IOException e) {
			LOGGER.error("Error while loading file {}", e, fileName);
			throw new InternalError("Error while creating HttpRequester", e);
		}
	}
	
	/*private String removeWhiteSpace(final String s) {
		s.trim().replaceAll("\n", replacement)
	}*/
}
