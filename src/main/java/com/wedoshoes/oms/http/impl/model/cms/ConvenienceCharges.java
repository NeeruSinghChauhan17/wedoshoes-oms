package com.wedoshoes.oms.http.impl.model.cms;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class represent Convinence service charges for WedoServices.
 * @author Navrattan Yadav
 *
 */

@JsonInclude(Include.NON_NULL)
public class ConvenienceCharges extends AbstractPrice {

	private final BigDecimal convenienceCharge;
	private final BigDecimal singleOrderExpressProcessingCharge;
	private final BigDecimal multiOrderExpressProcessingChargePerOrder;
	private final BigDecimal packingCharge;
	  
	@SuppressWarnings("unused")
	private ConvenienceCharges() {
		this(null, null, null, null, null, null, null,null);
	}

	public ConvenienceCharges(final Integer id, final BigDecimal convenienceCharge,
			final BigDecimal singleOrderExpressCharge, final Long startDate, final Long endDate,
			final Boolean currentPrice,final BigDecimal multiOrderExpressProcessingChargePerOrder,
			final BigDecimal packingCharge) {
		super(id,startDate, endDate, currentPrice);
		this.convenienceCharge = convenienceCharge;
		this.singleOrderExpressProcessingCharge = singleOrderExpressCharge;
		this.multiOrderExpressProcessingChargePerOrder = multiOrderExpressProcessingChargePerOrder;
		this.packingCharge = packingCharge;
	}

	@JsonProperty("convenience_charge")
	public BigDecimal getConvenienceCharge() {
		return convenienceCharge;
	}

	@JsonProperty("single_order_express_processing_charge")
	public BigDecimal getSingleOrderExpressProcessingCharge() {
		return singleOrderExpressProcessingCharge;
	}
	@JsonProperty("multi_order_express_processing_charge_per_order")
	public BigDecimal getMultiOrderExpressProcessingChargePerOrder() {
		return multiOrderExpressProcessingChargePerOrder;
	}
	
	
	@JsonProperty("packing_charge")
	public BigDecimal getPackingCharge() {
		return packingCharge;
	}

	public static ConvenienceCharges createWithEndDateAndCurrentPrice(final Long endDate,
			final Boolean isCurrentPrice, final ConvenienceCharges charges) {
		return new ConvenienceCharges(charges.getId(), charges.getConvenienceCharge(),
				charges.getSingleOrderExpressProcessingCharge(), charges.getStartDate(),
				endDate, isCurrentPrice, charges.getMultiOrderExpressProcessingChargePerOrder(),
				charges.getPackingCharge());
	}
	
	public static ConvenienceCharges createWithStartDateAndCurrentPrice(final Long startDate,
			final Boolean isCurrentPrice, final ConvenienceCharges charges) {
		return new ConvenienceCharges(charges.getId(), charges.getConvenienceCharge(),
				charges.getSingleOrderExpressProcessingCharge(), startDate, charges.getEndDate(),
				isCurrentPrice, charges.getMultiOrderExpressProcessingChargePerOrder(),
				charges.getPackingCharge());
	}
}
