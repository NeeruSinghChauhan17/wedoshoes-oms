package com.wedoshoes.oms.http.impl.model.cms;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This Class Represent a Product Category Eg. Bags, Shoes.
 * @author Navrattan Yadav
 *
 */

@JsonInclude(Include.NON_NULL)
public class Category {

	private final Integer id;
	private final String name;
	private final String imageUrl;
	private final String description;
	
	// For JaxB 
	@SuppressWarnings("unused")
	private Category() {
		this(null, null, null, null);
	}
	
	public Category(final Integer id, final String name, final String description,
			final String imageUrl) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.imageUrl = imageUrl;
	}

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}
	
	@JsonProperty("image_url")
	public String getImageUrl() {
		return imageUrl;
	}

	public static Category createWithId(final Integer id, final Category category) {
		return new Category(id, category.getName(), category.getDescription(), category.getImageUrl());
	}
	
	public static Category createFromId(final Integer id) {
		return new Category(id, null, null, null);
	}
	
	public static Category createWithMinimalInfo(final Category category) {
		return new Category(category.getId(), category.getName(), null, category.getImageUrl());
	}
	
}
