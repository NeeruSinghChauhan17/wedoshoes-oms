package com.wedoshoes.oms.http.impl.model.cms;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class represent an Accessory
 * @author Navrattan Yadav
 *
 */

@JsonInclude(Include.NON_NULL)
public class Accessory {

	private final Integer id;
	private final Integer categoryId;
	private final String name;
	private final String description;
	private final AccessoryPrice price;
	private final Integer sizeType;
	
	@SuppressWarnings("unused")
	private Accessory() {
		this(null, null, null, null, null,null);
	}

	public Accessory(final Integer id, final Integer categoryId, final String name,
			final String description, final AccessoryPrice price, final Integer sizeType) {
		this.id = id;
		this.categoryId = categoryId;
		this.name = name;
		this.description = description;
		this.price = price;
		this.sizeType = sizeType;
	}

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("category_id")
	public Integer getCategoryId() {
		return categoryId;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("price")
	public AccessoryPrice getPrice() {
		return price;
	}

	@JsonProperty("size_type")
	public Integer getSizeType() {
		return sizeType;
	}
	
	public static Accessory createWithCategoryId(final Integer categoryId, final Accessory accessory) {
		return new Accessory(accessory.getId(), categoryId, accessory.getName(),
				accessory.getDescription(), accessory.getPrice(), accessory.getSizeType());
	}
	
	public static Accessory createWithAccessoryIdAndCategoryId(final Integer accessoryId,
			final Integer categoryId, final Accessory accessory) {
		return new Accessory(accessoryId, categoryId, accessory.getName(),
				accessory.getDescription(), accessory.getPrice(), accessory.getSizeType());
	}
	
	public static Accessory createWithAccessoryId(final Integer accessoryId,final Accessory accessory) {
		return new Accessory(accessoryId, accessory.getCategoryId(), accessory.getName(),
				accessory.getDescription(), accessory.getPrice(), accessory.getSizeType());
	}
	
	public static Accessory createFromId(final Integer accessoryId) {
		return new Accessory(accessoryId, null, null, null, null, null);
	}
	
	public static Accessory createWithSummary(final Integer id, final Integer categoryId,
			final String name, final String description,final Integer sizeType) {
		return new Accessory(id, categoryId, name, description, null, sizeType);
	}
	
	public static Accessory createMinimalInfo(final Accessory accessory) {
		return new Accessory(accessory.getId(), null, accessory.getName(), null, null, null);
	}
}