package com.wedoshoes.oms.http.impl.model.cms;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * This call represent a Priority
 * @author Greesh Kumar
 *
 */
@JsonInclude(value=Include.NON_NULL)
public class Priority {
	private final Integer id;
	private final String name;
	
	@SuppressWarnings("unused")
	private Priority(){
		this(null,null);
	}
	public Priority(final Integer id, final String name) {
		this.id = id;
		this.name = name;
	}
	@JsonProperty("id")
	public Integer getId() {
		return id;
	}
	@JsonProperty("name")
	public String getName() {
		return name;
	}
	
	
}
