package com.wedoshoes.oms.http.impl.model.core;

public enum Gender {

	M,
	F,
	U;
}
