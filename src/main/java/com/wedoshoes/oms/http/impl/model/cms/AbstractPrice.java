package com.wedoshoes.oms.http.impl.model.cms;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public abstract class AbstractPrice {

	private final Integer id;
	private final Long startDate;
	private final Long endDate;
	private final Boolean currentPrice;
	
	@SuppressWarnings("unused")
	private AbstractPrice() {
		this(null, null, null, null);
	}
	
	public AbstractPrice(final Integer id,final Long startDate, final Long endDate,
			final Boolean currentPrice) {
		this.id = id;
		this.startDate = startDate;
		this.endDate = endDate;
		this.currentPrice = currentPrice;
	}
	
	@JsonProperty("id")
	public Integer getId() {
		return id;
	}
	
	@JsonProperty("start_date")
	public Long getStartDate() {
		return startDate;
	}

	@JsonProperty("end_date")
	public Long getEndDate() {
		return endDate;
	}

	@JsonProperty("is_current_price")
	public Boolean isCurrentPrice() {
		return currentPrice;
	}
	
	
}
