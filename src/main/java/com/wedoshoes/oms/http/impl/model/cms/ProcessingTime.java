package com.wedoshoes.oms.http.impl.model.cms;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This Class represent processing time for a weso service.
 * @author Navrattan Yadav
 *
 */

@JsonInclude(Include.NON_NULL)
public class ProcessingTime {

	private final Integer noramlProcessingDays;
	private final Integer expressProcessingDays;
	
	
	public ProcessingTime() {
		this(null, null);
	}
	
	public ProcessingTime(final Integer noramlProcessingDays, final Integer expressProcessingDays) {
		this.noramlProcessingDays = noramlProcessingDays;
		this.expressProcessingDays = expressProcessingDays;
	}
	
	@JsonProperty("normal_processing_days")
	public Integer getNoramlProcessingDays() {
		return noramlProcessingDays;
	}

	@JsonProperty("express_processing_days")
	public Integer getExpressProcessingDays() {
		return expressProcessingDays;
	}
	
}
