package com.wedoshoes.oms.http.impl;

import javax.ws.rs.core.Response.Status;

import com.wedoshoes.oms.exception.CustomException;

public class UnAuthorizedException extends CustomException {

	private static final long serialVersionUID = 1L;
	
	public UnAuthorizedException() {
		super(Status.UNAUTHORIZED.getStatusCode(), "UnAuthorized.");
	}
	
	public UnAuthorizedException( final String message) {
		super(Status.UNAUTHORIZED.getStatusCode(), message);
	}

}
