package com.wedoshoes.oms.http.impl.model.cms;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class represent accessory replace price. It include accessory cost and labor cost.
 * @author Navrattan Yadav
 *
 */

@JsonInclude(Include.NON_NULL)
public class AccessoryPrice extends AbstractPrice {

	private final BigDecimal accessoryCost;

	@SuppressWarnings("unused")
	private AccessoryPrice() {
		this(null, null, null, null, null);
	}
	
	public AccessoryPrice(final Integer id, final BigDecimal accessoryCost, final Long startDate,
			final Long endDate, final Boolean currentPrice) {
		super(id, startDate, endDate, currentPrice);
		this.accessoryCost = accessoryCost;
	}
	
	@JsonProperty("accessory_cost")
	public BigDecimal getAccessoryCost() {
		return accessoryCost;
	}
	
	public static AccessoryPrice createWithEndDateAndCurrentPrice(final Long endDate,
			final Boolean isCurrentPrice, final AccessoryPrice price) {
		return new AccessoryPrice(price.getId(),price.getAccessoryCost(),
				price.getStartDate(), endDate, isCurrentPrice);
	}
	

	public static AccessoryPrice createWithStartDateAndCurrentPrice(final Long startDate,
			final Boolean isCurrentPrice, final AccessoryPrice price) {
		return new AccessoryPrice(price.getId(),price.getAccessoryCost(),
				startDate, price.getEndDate(), isCurrentPrice);
	}
}