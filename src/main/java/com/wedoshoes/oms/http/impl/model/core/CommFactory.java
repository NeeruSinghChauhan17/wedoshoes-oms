package com.wedoshoes.oms.http.impl.model.core;

import com.wedoshoes.oms.http.impl.model.core.CommSettings;

public class CommFactory {
	
	private static final Integer SMS_ONLY = 1;
	private static final Integer MAIL_ONLY = 2;
	private static final Integer BOTH = 3;
	
	public static CommSettings createCommSettings(final Integer commSett){
		
		return commSett==SMS_ONLY?new CommSettings(Boolean.TRUE, Boolean.FALSE):
			commSett==MAIL_ONLY?new CommSettings(Boolean.FALSE, Boolean.TRUE):
				commSett==BOTH?new CommSettings(Boolean.TRUE, Boolean.TRUE):null;	
	}
	
	/*public static void main(String[] args) {
		CommSettings cs = createCommSettings(3);
		System.out.println("mail "+cs.isEmail());
		System.out.println("sms "+cs.isSms());
	}*/
}
