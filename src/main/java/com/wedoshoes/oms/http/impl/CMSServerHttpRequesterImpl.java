package com.wedoshoes.oms.http.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.ws.rs.ServerErrorException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import com.wedoshoes.oms.http.BaseRequester;
import com.wedoshoes.oms.http.CMSServerHttpRequester;
import com.wedoshoes.oms.http.impl.model.cms.Category;
import com.wedoshoes.oms.http.impl.model.cms.Locality;
import com.wedoshoes.oms.http.impl.model.cms.Priority;
import com.wedoshoes.oms.http.impl.model.cms.Product;
import com.wedoshoes.oms.http.impl.model.cms.TimeSlot;
import com.wedoshoes.oms.http.impl.model.cms.WeDoService;
import com.wedoshoes.oms.model.NationState;
import com.wedoshoes.oms.model.Pin;
import com.wedoshoes.oms.model.Workshop;
import com.wedoshoes.oms.model.cost.EstimatedCost;
import com.wedoshoes.oms.model.cost.ItemSummary;
import com.wedoshoes.oms.model.order.ItemDefect;

@Repository
public class CMSServerHttpRequesterImpl implements CMSServerHttpRequester {

	private static final Logger LOGGER = LoggerFactory.getLogger(CMSServerHttpRequesterImpl.class);
	
	private final BaseRequester baseHttpRequester;
	
	@Autowired
	public CMSServerHttpRequesterImpl(@Value("${cms.root.url}") final String rootUrl,
			final BaseRequesterFactory httpRequesterFactory) {
		this.baseHttpRequester = httpRequesterFactory.createBaseHttpReqester("cms_api.xml",
				rootUrl);
	}

	@Override
	public EstimatedCost getEstimatedCost(final ItemSummary itemSummary,final Long referreByUserId, final Boolean summary) {
		try {
			final HttpEntity<ItemSummary> entity = new HttpEntity<>(itemSummary,
					baseHttpRequester.getBasicHeaders());
			final Map<String, Object> params = new HashMap<>();
			params.put("referreByUserId", referreByUserId);
			params.put("summary", summary);
			final ResponseEntity<EstimatedCost> response = baseHttpRequester.getRestTemplate().
				exchange(URLParamMapper.map(baseHttpRequester.getURLById("get.estimated.cost"),params),
					HttpMethod.POST, entity, EstimatedCost.class);
			if (response.getStatusCode() != HttpStatus.OK) {
				LOGGER.error("Error while get estimated cost from CMS State Code {1} ",
						response.getStatusCode());
				return null;
			}
			return response.getBody();

		} catch (Exception e) {
			LOGGER.error("Error while get Estimated Cost", e);
			return null;
		}
	}

	@Override
	public Long getDeliveryDate(final Integer parentServiceId, final Long pickupDate,
			final Boolean expressDelivery) {
		try {
			
			final Map<String, Object> params = new HashMap<>();
			params.put("parentServiceId", parentServiceId);
			params.put("pickupDate", pickupDate);
			params.put("isExpressDelivery", expressDelivery);
			final ResponseEntity<Long> response = baseHttpRequester.getRestTemplate().
					getForEntity(URLParamMapper.map(baseHttpRequester.getURLById("get.delivery.date"), params), Long.class);
			if (response.getStatusCode() != HttpStatus.OK) {
				LOGGER.error("Error while get delivery date for ParentServiceId {} PickupDate {} Express Delivery {}"
						,parentServiceId, pickupDate ,expressDelivery);
				return null;
			}
			return response.getBody();
		} catch (Exception e) {
			LOGGER.error("Error while get Delivery Date ", e);
		}
		return null;
	}

	@Cacheable(cacheNames="servicesName", key="#serviceId")
	@Override
	public String getServiceName(final Integer serviceId) {
		try {
			final Map<String, Object> params = new HashMap<>();
			params.put("serviceId", serviceId);
			final ResponseEntity<WeDoService> response = baseHttpRequester.getRestTemplate().
					getForEntity(URLParamMapper.map(baseHttpRequester
							.getURLById("get.service.name"), params), WeDoService.class);
			if (response.getStatusCode() != HttpStatus.OK) {
				LOGGER.error("Error while get service Name for id ={} status={}",
						serviceId, response.getStatusCode());
				throw new ServerErrorException(response.getStatusCode().value());
			}
			return response.getBody().getName();
		} catch (Exception e) {
			LOGGER.error("Error while get service Name for id ={} due to = {}",serviceId, e);
			throw new ServerErrorException(500, e);
		}
	}
	
	@Cacheable(cacheNames="servicesName", key="#serviceId")
	@Override
	public String getServiceNameAcronym(final Integer serviceId) {
		try {
			final Map<String, Object> params = new HashMap<>();
			params.put("serviceId", serviceId);
			final ResponseEntity<WeDoService> response = baseHttpRequester.getRestTemplate().
					getForEntity(URLParamMapper.map(baseHttpRequester
							.getURLById("get.service.name"), params), WeDoService.class);
			if (response.getStatusCode() != HttpStatus.OK) {
				LOGGER.error("Error while get service Name Acronym for id ={} status={}",
						serviceId, response.getStatusCode());
				throw new ServerErrorException(response.getStatusCode().value());
			}
			return response.getBody().getAcronym();
		} catch (Exception e) {
			LOGGER.error("Error while get service Name Acronym for id ={} due to = {}",serviceId, e);
			throw new ServerErrorException(500, e);
		}
	}

	@Cacheable(cacheNames="productsName", key="#productId")
	@Override
	public String getProductName(final Integer productId) {
		try {
			final Map<String, Object> params = new HashMap<>();
			params.put("productId", productId);
			final ResponseEntity<Product> response = baseHttpRequester.getRestTemplate().
					getForEntity(URLParamMapper.map(baseHttpRequester
							.getURLById("get.product.name"), params), Product.class);
			if (response.getStatusCode() != HttpStatus.OK) {
				LOGGER.error("Error while get product Name for id ={} status= {}",
						productId,response.getStatusCode());
				throw new ServerErrorException(response.getStatusCode().value());
			}
			return response.getBody().getName();
		} catch (Exception e) {
			LOGGER.error("Error while get product Name for id ={} due to = {}",productId, e);
			throw new ServerErrorException(500, e);
		}
	}
	
	@Cacheable(cacheNames="categoriesName", key="#categoryId")
	@Override
	public String getCategoryName(final Integer categoryId) {
		try {
			final Map<String, Object> params = new HashMap<>();
			params.put("categoryId", categoryId);
			final ResponseEntity<Category> response = baseHttpRequester.getRestTemplate().
					getForEntity(URLParamMapper.map(baseHttpRequester
							.getURLById("get.category.name"), params), Category.class);
			if (response.getStatusCode() != HttpStatus.OK) {
				LOGGER.error("Error while get category Name for id ={} status= {}",
						categoryId,response.getStatusCode());
				throw new ServerErrorException(response.getStatusCode().value());
			}
			return response.getBody().getName();
		} catch (Exception e) {
			LOGGER.error("Error while get category Name for id ={} due to = {}",categoryId, e);
			throw new ServerErrorException(500, e);
		}
	}

	@Cacheable(cacheNames="timeSlotName", key="#timeSlotId")
	@Override
	public String getTimeSlotName(final Byte timeSlotId) {
		try {
			final Map<String, Object> params = new HashMap<>();
			params.put("timeSlotId", timeSlotId);
			final ResponseEntity<TimeSlot> response = baseHttpRequester.getRestTemplate().
					getForEntity(URLParamMapper.map(baseHttpRequester
							.getURLById("get.time.slot.name"), params), TimeSlot.class);
			if (response.getStatusCode() != HttpStatus.OK) {
				LOGGER.error("Error while get time-slot Name for id ={} status = {}",
						timeSlotId, response.getStatusCode());
				throw new ServerErrorException(response.getStatusCode().value());
			}
			return response.getBody().getName();
		} catch (Exception e) {
			LOGGER.error("Error while get time-slot Name for id ={} due to = {}",timeSlotId, e);
			throw new ServerErrorException(500, e);
		}
	}
	
	@Cacheable(cacheNames="LocalityName", key="#localityId")
	@Override
	public String getLocalityName(final Integer localityId) {
		try {
			final Map<String, Object> params = new HashMap<>();
			params.put("localityId", localityId);
			final ResponseEntity<Locality> response = baseHttpRequester.getRestTemplate().
					getForEntity(URLParamMapper.map(baseHttpRequester
							.getURLById("get.locality.name"), params), Locality.class);
			if (response.getStatusCode() != HttpStatus.OK) {
				LOGGER.error("Error while get locality Name for id ={} status= {}",
						localityId,response.getStatusCode());
				throw new ServerErrorException(response.getStatusCode().value());
			}
			return response.getBody().getName();
		} catch (Exception e) {
			LOGGER.error("Error while get locality Name for id ={} due to = {}",localityId, e);
			throw new ServerErrorException(500, e);
		}
	}
	
	@Cacheable(cacheNames="PriorityName", key="#priorityId")
	@Override
	public String getPriorityName(final Byte priorityId) {
		try {
			final Map<String, Object> params = new HashMap<>();
			params.put("priorityId", priorityId);
			final ResponseEntity<Priority> response = baseHttpRequester.getRestTemplate().
					getForEntity(URLParamMapper.map(baseHttpRequester
							.getURLById("get.priority.name"), params), Priority.class);
			if (response.getStatusCode() != HttpStatus.OK) {
				LOGGER.error("Error while get priority Name for id ={} status= {}",
						priorityId,response.getStatusCode());
				throw new ServerErrorException(response.getStatusCode().value());
			}
			return response.getBody().getName();
		} catch (Exception e) {
			LOGGER.error("Error while get priority Name for id ={} due to = {}",priorityId, e);
			throw new ServerErrorException(500, e);
		}
	}

	@Cacheable(cacheNames="DefectsName", key="#defectId")
	@Override
	public String getItemDefectName(final Byte defectId) {
		try {
			final Map<String, Object> params = new HashMap<>();
			params.put("defectId", defectId);
			final ResponseEntity<ItemDefect> response = baseHttpRequester.getRestTemplate().
					getForEntity(URLParamMapper.map(baseHttpRequester
							.getURLById("get.defect.name"), params), ItemDefect.class);
			if (response.getStatusCode() != HttpStatus.OK) {
				LOGGER.error("Error while get defect Name for id ={} status={}",
						defectId, response.getStatusCode());
				throw new ServerErrorException(response.getStatusCode().value());
			}
			return response.getBody().getName();
		} catch (Exception e) {
			LOGGER.error("Error while get defect Name for id ={} due to = {}",defectId, e);
			throw new ServerErrorException(500, e);
		}
	}

	
//	@Cacheable(cacheNames="LocalityName", key="#localityId")
	@Override
	public Optional<Locality>  getLocalityByPin(final Integer pin) {
		try {
			final Map<String, Object> params = new HashMap<>();
			params.put("pin", pin);
			final ResponseEntity<Locality> response = baseHttpRequester.getRestTemplate().
					exchange(URLParamMapper.map(baseHttpRequester
							.getURLById("get.locality.name.by.pin"), params),HttpMethod.GET, null, Locality.class);
			
			if (response.getStatusCode() == HttpStatus.OK) {				
				return Optional.ofNullable(response.getBody());
			}else if(response.getStatusCode()==HttpStatus.NO_CONTENT){
				return Optional.empty();
			}
			LOGGER.error("Error while get locality Name for pin ={} status= {}",
					pin,response.getStatusCode());
			throw new ServerErrorException(response.getStatusCode().value());
		} catch (Exception e) {
			LOGGER.error("Error while get locality Name for id ={} due to = {}",pin, e);
			throw new ServerErrorException(500, e);
		}
	}

	@Override
	public Optional<WeDoService> getServiceDetailByParentId(Integer serviceId) {
		return null;
	}

	@Override
	public Optional<Workshop> getWorkshopById(final Byte workshopId) {
		try {
			final Map<String, Object> params = new HashMap<>();
			params.put("workshopId", workshopId);
			final ResponseEntity<Workshop> response = baseHttpRequester.getRestTemplate().
					exchange(URLParamMapper.map(baseHttpRequester
							.getURLById("get.workshop.by.id"), params),HttpMethod.GET, null, Workshop.class);
			
			if (response.getStatusCode() == HttpStatus.OK) {				
				return Optional.ofNullable(response.getBody());
			}else if(response.getStatusCode()==HttpStatus.NO_CONTENT){
				return Optional.empty();
			}
			LOGGER.error("Error while fetching workshop detail by id ={} status= {}",
					workshopId,response.getStatusCode());
			throw new ServerErrorException(response.getStatusCode().value());
		} catch (Exception e) {
			LOGGER.error("Error while fetching workshop detail by id ={} due to = {}",workshopId, e);
			throw new ServerErrorException(500, e);
		}
	}

	@Override
	public Optional<NationState> getStateNameById(final Byte stateId) {
		try {
			final Map<String, Object> params = new HashMap<>();
			params.put("stateId", stateId);
			final ResponseEntity<NationState> response = baseHttpRequester.getRestTemplate().
					exchange(URLParamMapper.map(baseHttpRequester
							.getURLById("get.state.name.id"), params),HttpMethod.GET, null, NationState.class);
			
			if (response.getStatusCode() == HttpStatus.OK) {				
				return Optional.ofNullable(response.getBody());
			}else if(response.getStatusCode()==HttpStatus.NO_CONTENT){
				return Optional.empty();
			}
			LOGGER.error("Error while fetching nation state by id ={} status= {}",
					stateId,response.getStatusCode());
			throw new ServerErrorException(response.getStatusCode().value());
		} catch (Exception e) {
			LOGGER.error("Error while fetching nation state by id ={} due to = {}",stateId, e);
			throw new ServerErrorException(500, e);
		}
	}

	@Override
	public List<Byte> getItemStatesByStateType(Integer stateType) {
		try{
			final Map<String, Object> params = new HashMap<>();
			params.put("stateType", stateType);
		final ResponseEntity<List<Byte>> response = baseHttpRequester.getRestTemplate().
				exchange(URLParamMapper.map(baseHttpRequester
						.getURLById("get.item.states.by.state.type"), params),HttpMethod.GET, null, new ParameterizedTypeReference<List<Byte>>(){});
			
		if (response.getStatusCode() == HttpStatus.OK) {
			return response.getBody();
		}else if(response.getStatusCode()==HttpStatus.NO_CONTENT){
			return null;
		}
		LOGGER.error("Error while fetching item-States by state-type= {}",
				stateType,response.getStatusCode());
		throw new ServerErrorException(response.getStatusCode().value());
			
		}
		catch(Exception e){
			LOGGER.error("Error while fetching item-States by state-type ={} ",stateType, e);
			throw new ServerErrorException(500, e);
		}
		
	}
	
	@Override
	public void addUndefinedPin(String pinCode) {
		try {
			final Map<String, Object> params = new HashMap<>();
			final HttpEntity<Pin> entity = new HttpEntity<>(
					Pin.createFromCode(pinCode),baseHttpRequester.getBasicHeaders());

			final ResponseEntity<String> response = baseHttpRequester.getRestTemplate().exchange(
					URLParamMapper.map(baseHttpRequester.getURLById("add.undefined.pin"), params), HttpMethod.POST, entity,
					String.class);
			if (response.getStatusCode() != HttpStatus.OK) {
				LOGGER.error("Error while adding undefined pin = {}", pinCode);
			}
		} catch (Exception e) {
			LOGGER.error("Error while adding undefined pin = {}", pinCode);
		}
		
	}

}
