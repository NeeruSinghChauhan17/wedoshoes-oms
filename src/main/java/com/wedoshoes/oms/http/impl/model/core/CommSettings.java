package com.wedoshoes.oms.http.impl.model.core;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class CommSettings {

	
	private final Boolean sms ;
	private final Boolean email;
	
	@SuppressWarnings("unused")
	private CommSettings(){
		this(null, null);
	}
	
	public CommSettings(final Boolean sms, final Boolean email) {
		super();
		this.sms = sms;
		this.email = email;
	}

	
	@JsonProperty("is_sms")
	public Boolean isSms() {
		return sms;
	}
	
	@JsonProperty("is_email")
	public Boolean isEmail() {
		return email;
	}

	public static CommSettings createWithUserId(final CommSettings cs) {
		
		return new CommSettings(cs.isSms(), cs.isEmail());
	}
	
	public static CommSettings createDefault() {
		return new CommSettings(Boolean.TRUE, Boolean.TRUE);
	}
}
