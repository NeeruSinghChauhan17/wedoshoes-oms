package com.wedoshoes.oms.http.impl.model.cms;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
/**
 * This call represent a Locality
 * @author Greesh Kumar
 *
 */
@JsonInclude(value=Include.NON_NULL)
public class Locality {
	
	private final Integer id;
	private final String name;
	private final String pin;
	private final Byte workshopId;
	private final Short districtId;

	
	@SuppressWarnings("unused")
	private Locality(){
		this(null, null, null, null, null);
	}

	public Locality(final Integer id, final String name, final String pin, final Byte workshopId,
			final Short districtId) {
		this.id = id;
		this.name = name;
		this.pin = pin;
		this.workshopId = workshopId;
		this.districtId = districtId;
	}

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}
	
	@JsonProperty("pin")
	public String getPin() {
		return pin;
	}

	@JsonProperty("workshop_id")
	public Byte getWorkshopId() {
		return workshopId;
	}


	@JsonProperty("district_id")
	public Short getDistrictId() {
		return districtId;
	}

	public static Locality createFromId(final Integer id) {
		return new Locality(id, null, null, null, null);
	}

	public static Locality createWithId(final Integer id, final Locality locality) {
		return new Locality(id, locality.getName(), locality.getPin(), 
				locality.getWorkshopId(), locality.getDistrictId());
	}
	
}