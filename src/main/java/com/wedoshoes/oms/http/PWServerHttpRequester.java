package com.wedoshoes.oms.http;

import java.math.BigDecimal;

import com.wedoshoes.oms.model.payment.PaymentStatus;
import com.wedoshoes.oms.model.payment.PaytmPaymentStatus;
import com.wedoshoes.oms.model.payment.PayuPaymentStatus;

public interface PWServerHttpRequester {

	void createWallet(Long userId);
	void addCredit(Long userId, BigDecimal credit);
	void deductCredit(Long userId, BigDecimal credit);
	PaytmPaymentStatus checkPaytmTransactionStatus(Long userId,PaymentStatus paymentStatus);
	PayuPaymentStatus checkPayuTransactionStatus(Long userId, String transactionId);
}
