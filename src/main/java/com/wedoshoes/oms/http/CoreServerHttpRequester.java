package com.wedoshoes.oms.http;

import com.wedoshoes.oms.model.notification.Notification;
import com.wedoshoes.oms.security.Token;

public interface CoreServerHttpRequester {

	/**
	 * Authenticate user by validating token and userId
	 * @param userId
	 * @param token
	 * @return {@link Token}
	 */
	Token authenticate(Long userId,Token token);
	
	/**
	 * Authenticate user by validating token and userId and check user permission for action
	 * @param userId
	 * @param action
	 * @param token
	 * @return {@link Token}
	 */
	Token authorize(Long userId,String action, Token token);
	
	void sendNotification(Byte workshopId, Long userId, Notification notification);

	/**
	 * Get Customer Name by ID.
	 * @param userId
	 * @return
	 */
	String getUserNameById(Long userId);

	/**
	 * Get Customer Type by ID.
	 * @param userId
	 * @return
	 */
	Byte getUserTypeById(Long userId);
}
