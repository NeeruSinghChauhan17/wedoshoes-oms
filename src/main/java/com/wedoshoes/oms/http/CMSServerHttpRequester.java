package com.wedoshoes.oms.http;

import java.util.List;
import java.util.Optional;

import com.wedoshoes.oms.http.impl.model.cms.Locality;
import com.wedoshoes.oms.http.impl.model.cms.WeDoService;
import com.wedoshoes.oms.model.NationState;
import com.wedoshoes.oms.model.Workshop;
import com.wedoshoes.oms.model.cost.EstimatedCost;
import com.wedoshoes.oms.model.cost.ItemSummary;

public interface CMSServerHttpRequester {

	EstimatedCost getEstimatedCost(ItemSummary itemSummary, Long referredByUserId, Boolean summary);
	Long getDeliveryDate(Integer parentServiceId, Long pickupDate,
			Boolean expressDelivery);
	
	String getServiceName(Integer serviceId);
	String getProductName(Integer productId);
	String getTimeSlotName(Byte timeSlotId);
	String getLocalityName(Integer localityId);
	String getPriorityName(Byte priorityId);
	String getServiceNameAcronym(Integer serviceId);
	String getItemDefectName(Byte defectId);
	String getCategoryName(Integer categoryId);
	Optional<Locality>  getLocalityByPin(Integer pin);
	Optional<WeDoService> getServiceDetailByParentId(Integer serviceId);
	Optional<Workshop> getWorkshopById(Byte workshopId);
	Optional<NationState> getStateNameById(Byte stateId);

	List<Byte> getItemStatesByStateType(Integer stateType);
	
	
	

	void addUndefinedPin(String pinCode);

}
