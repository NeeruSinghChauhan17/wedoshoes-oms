package com.wedoshoes.oms.service;

import java.io.File;

import com.wedoshoes.oms.model.order.Order;


public interface InvoiceService {

	File createInvoices(Byte workshoeId, Long orderId, Long itemId);

	File getInvoices(Byte workshopId, Long orderId, Long itemId,
			Long date, Integer duration);

	File createChallan(Byte workshopId, Long orderId,
			Long itemId);
	
	File testIncvoices(Byte workshopId, Order order);

	File sendInvoices(Long userId, Byte workshopId, Long orderId, Long itemId);
    
    File sendChallan(Long userId, Byte workshopId, Long orderId, Long itemId);
}
