package com.wedoshoes.oms.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.wedoshoes.oms.model.order.FeedbackType;

public interface FeedbackService {
	Map<Integer, ArrayList<FeedbackType>> getFeedbackCategory( Integer ratingPoint);

	List<String> getFeedbackCategoryByRatingPoint(Integer ratingPoint);
}
