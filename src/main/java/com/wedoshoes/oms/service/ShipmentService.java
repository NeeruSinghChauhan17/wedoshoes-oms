package com.wedoshoes.oms.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.wedoshoes.oms.model.shipment.Shipment;
import com.wedoshoes.oms.model.shipment.ShipmentPoint;
import com.wedoshoes.oms.model.shipment.ShipmentOrder;
import com.wedoshoes.oms.model.shipment.ShipmentSummary;
import com.wedoshoes.oms.model.state.StateTransition;

public interface ShipmentService {

	void addShipmentOrder(Byte workshopId, Long userId, Long orderId, StateTransition transition);
	Shipment addShipmentItem(Byte workshopId, Long userId, Long orderId,
			Long itemId,StateTransition transition);
	void changeShipmentItemState(Byte workshopId, Long userId, Long shipmentId, StateTransition transition);
	void update(Byte workshopId,Long id, Shipment shipment);
	Optional<Shipment> find(Byte workshopId,Long id);
	Collection<ShipmentOrder> findOrderByDateAndUser(Byte workshopId,Long date, Long userId);
	Optional<Shipment> findShipmentByDate(Byte workshopId,Long date, Long orderId, Long itemId);
	Collection<Shipment> getShipmentOrderAssignee(Byte workshopId, Long orderId,
			Long itemId);
	Optional<ShipmentSummary> getShipmentSummary(Byte workshopId, Long userId, Long shipmentDate);
	Collection<ShipmentPoint> getShipmentRoutes(Byte workshopId,
			Long userId, Long shipmentDate, Double latitude, Double longitude, Byte currentTimeSlot);
	void updateShipments(Byte workshopId, List<Shipment> shipment);
}
