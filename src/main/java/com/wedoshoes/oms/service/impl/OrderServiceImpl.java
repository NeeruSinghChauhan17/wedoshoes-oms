package com.wedoshoes.oms.service.impl;

import java.awt.image.BufferedImage;
import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.wedoshoes.oms.http.CMSServerHttpRequester;
import com.wedoshoes.oms.http.PWServerHttpRequester;
import com.wedoshoes.oms.http.impl.model.cms.Locality;
import com.wedoshoes.oms.http.impl.model.core.CommSettings;
import com.wedoshoes.oms.manager.OrderManager;
import com.wedoshoes.oms.model.Items;
import com.wedoshoes.oms.model.LastDeliveredItem;
import com.wedoshoes.oms.model.NationState;
import com.wedoshoes.oms.model.PaymentTransaction;
import com.wedoshoes.oms.model.TransactionStatus;
import com.wedoshoes.oms.model.Workshop;
import com.wedoshoes.oms.model.cost.EstimatedCost;
import com.wedoshoes.oms.model.order.Address;
import com.wedoshoes.oms.model.order.Discount;
import com.wedoshoes.oms.model.order.Feedback;
import com.wedoshoes.oms.model.order.GST;
import com.wedoshoes.oms.model.order.Image;
import com.wedoshoes.oms.model.order.ImageType;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.ItemDefect;
import com.wedoshoes.oms.model.order.ItemState;
import com.wedoshoes.oms.model.order.Order;
import com.wedoshoes.oms.model.order.Payment;
import com.wedoshoes.oms.model.order.PaymentMode;
import com.wedoshoes.oms.model.order.WedoService;
import com.wedoshoes.oms.model.payment.PaymentAdjustment;
import com.wedoshoes.oms.model.state.StateTransition;
import com.wedoshoes.oms.service.CostService;
import com.wedoshoes.oms.service.InvoiceService;
import com.wedoshoes.oms.service.NotificationService;
import com.wedoshoes.oms.service.OrderService;

/**
 * 
 * @author Navrattan Yadav
 *
 */
@Service
public class OrderServiceImpl implements OrderService {

	public static final Logger LOGGER = LoggerFactory.getLogger(OrderServiceImpl.class);

	private static final Byte DEFAULT_WORKSHOP_ID = 1;
	private final OrderManager orderManager;
	private final CostService costVerifier;
	private final NotificationService notificationService;
	private final BarcodeHelper barcodeHelper;
	private final InvoiceService invoiceService;
	private final PWServerHttpRequester pwServerHttpRequester;
	private final CMSServerHttpRequester cmsRequester;
	private final Integer DEFAULT_LOCALITY_ID = 0;

	@Autowired
	public OrderServiceImpl(final OrderManager orderManager, final CostService costVerifier,
			final NotificationService notificationService, final BarcodeHelper barcodeHelper,
			final InvoiceService invoiceService, final PWServerHttpRequester pwServerHttpRequester,
			final CMSServerHttpRequester cmsRequester) {
		this.orderManager = orderManager;
		this.costVerifier = costVerifier;
		this.notificationService = notificationService;
		this.barcodeHelper = barcodeHelper;
		this.invoiceService = invoiceService;
		this.pwServerHttpRequester = pwServerHttpRequester;
		this.cmsRequester = cmsRequester;
	}

	/**
	 * Create New Order . Take following action : 1. Verify Order Cost. 2.
	 * Verify Coupon applied on items. 3. If order has payment except cash then
	 * verify payment.
	 */
	@Override
	public Order createOrder(final Order order, final CommSettings commSett, final Boolean saveSettings) {
		// TODO: find workshop id based on PickUp Address state.
		return createOrder(DEFAULT_WORKSHOP_ID, ordersWithGST(order), commSett, saveSettings);
	}

	@Transactional
	@Override
	public Order createOrder(final Byte workshopId, Order order, final CommSettings commSett,
			final Boolean saveSettings) {


		 if(!costVerifier.verify(order.getUserId(), order.getItems(),order.getReferredByUserId())) {
			 LOGGER.error("Order Cost not match for userId "+ order.getId());
			 throw new ForbiddenException("Invalid order cost.");
		 }

		if (order.getPayments() != null && !order.getPayments().isEmpty()) {


			order.getPayments().stream().forEach(payment -> 
				{
				if(payment.getMode().equals(PaymentMode.WEDO_CREDIT)){
				pwServerHttpRequester.deductCredit(order.getUserId(),payment.getAmount()); 

				}
			}); 

		}

		final Long orderId = orderManager.createOrder(workshopId,
				Order.createWithDeliveryAndPickupAddress(addLocalityInAddress(order.getPickUpAddress()),
						addLocalityInAddress(order.getDeliveryAddress()), order));

		notificationService.notifyOrderStateChanged(workshopId, orderId, ItemState.NEW, commSett, saveSettings);

		return Order.createFromId(orderId);
	}

	private final Order ordersWithGST(final Order order) {
		Boolean isIntraState = isIntrastateOrder(order.getPickUpAddress().getState(), DEFAULT_WORKSHOP_ID);
		List<Item> itemsWithGST = new ArrayList<Item>();
		List<Item> items = order.getItems();
		items.stream().forEach(item -> {
			if (isIntraState) {
				itemsWithGST.add(Item.createWithGST(item,
						new GST(BigDecimal.valueOf(0), BigDecimal.valueOf(9), BigDecimal.valueOf(9), null)));
			} else {
				itemsWithGST.add(Item.createWithGST(item,
						new GST(BigDecimal.valueOf(18), BigDecimal.valueOf(0), BigDecimal.valueOf(0), null)));
			}
		});
		return Order.createWithItems(order, itemsWithGST);
	}

	private Boolean isIntrastateOrder(final String orderState, final Byte workshopId) {
		Optional<Workshop> optional = cmsRequester.getWorkshopById(workshopId);
		if (optional.isPresent()) {
			Optional<NationState> optState = cmsRequester.getStateNameById(optional.get().getState());
			if (optState.get().getName().toString().equalsIgnoreCase(orderState)) {
				return Boolean.TRUE;
			}
			return Boolean.FALSE;
		}
		return Boolean.FALSE;
	}


	
	private Address addLocalityInAddress(final Address address){
			Optional<Locality> loclaity=cmsRequester.getLocalityByPin(Integer.parseInt((address.getPin())));
			if(!loclaity.isPresent()){
				cmsRequester.addUndefinedPin(address.getPin());
			}
		return Address.createWithLocality(loclaity.isPresent() ? loclaity.get().getId() : DEFAULT_LOCALITY_ID, address); 

	}

	@Override
	public void updateOrder(final Byte workshopId, final Order order) {
		final Order ordersWithLocality = Order.createWithDeliveryAndPickupAddress(
				addLocalityInAddress(order.getPickUpAddress()), addLocalityInAddress(order.getDeliveryAddress()),
				order);
		final Collection<Item> itemList = orderManager.listOrderItems(workshopId, order.getId(), null, null, 0, 100);
		for (Item item : itemList) {
			final Long deliveryDate = cmsRequester.getDeliveryDate(item.getParentServiceId(), order.getPickUpDate(),
					item.isExpressProcessing());
			orderManager.updateItem(workshopId, Item.createWithDeliveryDate(item, deliveryDate));
		}
		orderManager.updateOrder(workshopId, ordersWithLocality);
	}

	@Override
	public Optional<Order> getOrder(final Byte workshopId, final Long orderId, final Boolean summary) {
		if (summary) {
			return orderManager.getOrder(workshopId, orderId);
		}
		return orderManager.getOrderDetail(workshopId, orderId);
	}

	@Override
	public Collection<Order> listByUser(final Long userId, final Integer offset, final Integer limit) {
		return orderManager.listByUser(userId, offset, limit);
	}

	@Override
	public Item addItem(final Byte workshopId, final Long orderId, final Item item) {
		Optional<Order> orderOptional = orderManager.getOrder(workshopId, orderId);
		if (isIntrastateOrder(orderOptional.get().getPickUpAddress().getState(), workshopId)) {
			return orderManager.addItem(workshopId, orderId, Item.createWithGST(item,
					new GST(BigDecimal.valueOf(0.00), BigDecimal.valueOf(9.00), BigDecimal.valueOf(9.00), null)));
		}
		return orderManager.addItem(workshopId, orderId, Item.createWithGST(item,
				new GST(BigDecimal.valueOf(18.00), BigDecimal.valueOf(0.00), BigDecimal.valueOf(0.00), null)));
	}

	@Override
public void updateItem(final Byte workshopId, final Long orderId,
			final Long itemId, final Item item) {
		final Optional<Order> order=orderManager.getOrder(workshopId, orderId);
		if(order.isPresent()){
		   if(item.isExpressProcessing()){
			orderManager.updateItem(workshopId,
			Item.createWithItemIdAndOrderId(itemId, orderId, Item.createWithEstimatedCost(item,cmsRequester.getEstimatedCost(Items.asItemSummary(item),order.get().getReferredByUserId(), Boolean.FALSE))));
		   }else{
	 		orderManager.updateItem(workshopId,
			Item.createWithItemIdAndOrderId(itemId, orderId, Item.createWithEstimatedCost(item,new EstimatedCost(null, BigDecimal.ZERO, null, null, null, null))));
		   }

		}
	}

	@Override
	public Optional<Item> getItem(final Byte workshopId, final Long orderId, final Long itemId, final Boolean summary) {
		return orderManager.getItem(workshopId, itemId);
	}

	@Override
	public void addItemServices(final Byte workshopId, final Long orderId, final Long itemId,
			final List<WedoService> services) {
		orderManager.addItemServices(workshopId, itemId, services);
	}

	@Override
	public void updateItemService(final Byte workshopId, final Long orderId, final Long itemId, final Long serviceId,
			final WedoService wedoService) {
		orderManager.updateItemService(workshopId, itemId, serviceId, wedoService);
	}

	@Override
	public void removeItemService(final Byte workshopId, final Long orderId, final Long itemId, final Long serviceId) {
		orderManager.removeItemService(workshopId, itemId, serviceId);
	}

	@Override
	public void removeItemServices(final Byte workshopId, final Long orderId, final Long itemId) {
		orderManager.removeItemServices(workshopId, itemId);
	}

	@Override
	public Collection<WedoService> listItemServices(final Byte workshopId, final Long orderId, final Long itemId,
			final Boolean summary) {
		return orderManager.listItemServices(workshopId, itemId);
	}

	@Override
	public Optional<WedoService> getItemService(final Byte workshopId, final Long orderId, final Long itemId,
			final Long serviceId, final Boolean summary) {
		return orderManager.getItemService(workshopId, itemId, serviceId);
	}

	@Override
	public void addItemImages(final Byte workshopId, final Long orderId, final Long itemId, final List<Image> images) {
		orderManager.addItemImages(workshopId, itemId, images);
	}

	@Override
	public void updateItemImage(final Byte workshopId, final Long orderId, final Long itemId, final Long imageId,
			final Image image) {
		orderManager.updateItemImage(workshopId, itemId, Image.createWithId(imageId, image));
	}

	@Override
	public void removeItemImage(final Byte workshopId, final Long orderId, final Long itemId, final Long imageId) {
		orderManager.removeItemImage(workshopId, imageId);
	}

	@Override
	public void removeItemImagesByItem(final Byte workshopId, final Long orderId, final Long itemId) {
		orderManager.removeItemImagesByItem(workshopId, itemId);
	}

	@Override
	public Optional<Image> getItemImage(final Byte workshopId, final Long orderId, final Long itemId,
			final Long imageId, final Boolean summary) {
		return orderManager.getItemImage(workshopId, imageId);
	}

	@Override
	public Collection<Image> listItemImages(final Byte workshopId, final Long orderId, final Long itemId,
			final Boolean summary) {
		return orderManager.listItemImages(workshopId, itemId);
	}

	@Override
	public Collection<Image> listItemImages(final Byte workshopId, final Long orderId, final Long itemId,
			final ImageType imageType, final Boolean summary) {
		return orderManager.listItemImages(workshopId, itemId, imageType);
	}

	/**
	 * Add Order Advance Payment with transaction detail. Except Cash verify
	 * payment from payment source.
	 */
	@Override
	public Payment addAdvancedPayment(final Byte workshopId, final Long userId, final Long orderId, Payment payment) {
		// TODO need to verify payment
		String paymentStatus = TransactionStatus.SUCCESS.toString();
		if (payment.getMode().equals(PaymentMode.PAYTM) || payment.getMode().equals(PaymentMode.PAYU)) {
			paymentStatus = orderManager.checkPaymentStatus(payment, userId);
		}
		payment = Payment.createWithStatus(payment, paymentStatus);
		return Payment.createWithId(orderManager.addAdvancedPayment(workshopId, userId, orderId, payment), payment);
	}

	/**
	 * Add Order item Payment with transaction detail. Except Cash verify
	 * payment from payment source.
	 */
	@Override
	public Payment addItemPayment(final Byte workshopId, final Long userId, final Long orderId, final Long itemId,
			Payment payment) {
		// TODO need to verify payment
		String paymentStatus = TransactionStatus.SUCCESS.toString();
		if (payment.getMode().equals(PaymentMode.PAYTM) || payment.getMode().equals(PaymentMode.PAYU)) {
			paymentStatus = orderManager.checkPaymentStatus(payment, userId);
		}
		payment = Payment.createWithStatus(payment, paymentStatus);
		return Payment.createWithId(orderManager.addItemPayment(workshopId, userId, orderId, itemId, payment), payment);
	}

	/**
	 * Get Order Advance Payments details
	 */
	@Override
	public Collection<Payment> listOrderPayments(final Byte workshopId, final Long userId, final Long orderId) {
		return orderManager.listOrderPayments(workshopId, userId, orderId);
	}

	@Override

	public void updateOrderState(final Byte workshopId,	final Long orderId,
			final StateTransition transition) {
		orderManager.changeOrderItemsState(workshopId, orderId,transition);
		notificationService.notifyOrderStateChanged(workshopId, orderId,
				transition.getState(),null,null);
	  notificationService.notifyShipmentOrderStateChanged(workshopId, orderId, transition);

	}

	/**
	 *  
	 */
	@Override
	public void updateItemState(final Byte workshopId, final Long orderId, final Long itemId,
			final StateTransition transition) {
		orderManager.changeItemState(workshopId, orderId, itemId, transition);

		if (ItemState.ANALYSED == transition.getState() || ItemState.DELIVERED == transition.getState()) {
			if (ItemState.ANALYSED == transition.getState()) {
				File file = invoiceService.createChallan(workshopId, orderId, itemId);
				notificationService.notifyItemStateChanged(workshopId, orderId, itemId, transition.getState(), file,
						null, null);
			} else {
				File file = invoiceService.createInvoices(workshopId, orderId, itemId);
				notificationService.notifyItemStateChanged(workshopId, orderId, itemId, transition.getState(), file,
						null, null);
			}
		} else {
			notificationService.notifyItemStateChanged(workshopId, orderId, itemId, transition.getState(), null, null,
					null);
		}


	/*
	 * private void refundPaidAmount(final Byte workshopId,final Long orderId,
	 * final Long itemId){ Optional<Order>
	 * orderOptional=orderManager.getOrderDetail(workshopId, orderId);
	 * Optional<Item> itemOptional=orderManager.getItem(workshopId, itemId);
	 * orderOptional.get().getPayments().stream().forEach(payment -> {
	 * if(payment.getMode().equals(PaymentMode.WEDO_CREDIT.toString()) ||
	 * !payment.getMode().equals(PaymentMode.COD.toString())){
	 * payment.getAmount(); //
	 * pwServerHttpRequester.deductCredit(order.getUserId(),payment.getAmount())
	 * ; // TODO : how to deduct wedocredit on item cancellation. Needs to
	 * deduct manually. }
	 * 
	 * }); }
	 */


		//TODO : Implement refund wedo-credit feature.
		notificationService.notifyShipmentItemStateChanged(workshopId, orderId, itemId, transition);
	}
	
	

	@Override
	public Collection<StateTransition> listItemStateTransitions(final Byte workshopId, final Long itemId) { //check same
		return orderManager.listStateTransition(workshopId, itemId);
	}

	@Override
	public Collection<Order> search(final Byte workshopId, final Long userId, final Long createrUserId,
			final Long referredByUserId, final String customerName, final Long orderId, final Long itemId,
			final String cc, final Long phone, final Byte state, final Long createDate, final Long pickUpDate,
			final Long deliveryDate, final Long fromDate, final Long toDate, final Integer offset,
			final Integer limit) {
		return orderManager.search(workshopId, userId, createrUserId, referredByUserId, customerName, orderId, itemId,
				cc, phone, state, createDate, pickUpDate, deliveryDate, fromDate, toDate, offset, limit);
	}

	@Override
	public Collection<Item> listOrderItems(final Byte workshopId, final Long orderId, final Byte state,
			final Long deliveryDate, final Integer offset, final Integer limit) {
		return orderManager.listOrderItems(workshopId, orderId, state, deliveryDate, offset, limit);
	}

	@Override
	public Collection<Item> searchItems(final Byte workshopId, final Byte state, final Long deliveryDate,
			final Long date, final Integer offset, final Integer limit) {
		return orderManager.searchItems(workshopId, state, deliveryDate, date, offset, limit);
	}

	@Override
	public Collection<Discount> listDiscountsOnItem(final Byte workshopId, final Long itemId) {
		return orderManager.listDiscountsOnItem(workshopId, itemId);
	}

	@Override
	public Optional<Order> getOrderCostingDetail(final Byte workshopId, final Long orderId) {
		return orderManager.getOrderCostingDetail(workshopId, orderId);
	}

	@Override
	public List<StateTransition> listExternalStateTransition(final Byte workshopId, final Long itemId,
			final Integer stateType) {
		if(stateType!=null){
			return	itemStateTransitionFilter(orderManager.listExternalStateTransition(workshopId, itemId), stateType);  //added itemStateFilter
	
		}
		return orderManager.listExternalStateTransition(workshopId, itemId);
	}

	private List<StateTransition> itemStateTransitionFilter(List<StateTransition> stateTransitions, final Integer stateType) {
		List<StateTransition> filteredTransitionStates=Lists.newArrayList();
		
		List<Byte> itemStates=cmsRequester.getItemStatesByStateType(stateType);
	
		
		if (itemStates==null || itemStates.isEmpty()) {
			return stateTransitions;
		}
		
		stateTransitions.stream().forEach(transition->{
			if(itemStates.contains(transition.getState())){
			
				filteredTransitionStates.add(transition);
			}
		});
		
		return filteredTransitionStates;
	}

	@Override
	public Optional<BufferedImage> generateBarcodeWithInfo(final Byte workshopId, final Long orderId,
			final Long itemId) {
		final Optional<Order> optional = orderManager.getOrder(workshopId, orderId);
		if (optional.isPresent()) {
			final Optional<Item> optinalItem = orderManager.getItem(workshopId, itemId);
			if (optinalItem.isPresent()) {
				final Order order = Order.createWithItems(optional.get(), Lists.newArrayList(optinalItem.get()));
				final List<Long> list = orderManager.listOrderItemIds(workshopId, orderId);
				final Integer itemNumber = list.indexOf(itemId);
				if (itemNumber.intValue() != -1) {
					return Optional
							.ofNullable(barcodeHelper.generateBarcodeWithInfo(order, itemNumber + 1, list.size()));
				}
			}
		}
		return Optional.empty();
	}

	@Override
	public Optional<BufferedImage> generateBarcode(final Byte workshopId, final Long orderId, final Long itemId) {
		return Optional.ofNullable(barcodeHelper.generateBarcode(workshopId, orderId, itemId));
	}

	@Override
	public void addItemDefects(final Byte workshopId, final Long orderId, final Long itemId, final List<Byte> defects) {
		orderManager.addItemDefects(workshopId, itemId, defects);
	}

	@Override
	public Collection<ItemDefect> listItemDefects(final Byte workshopId, final Long orderId, final Long itemId) {
		final Collection<Byte> defectIds = orderManager.listItemDefects(workshopId, itemId);
		final Collection<ItemDefect> defects = new ArrayList<>();
		defectIds.forEach(defectId -> defects.add(new ItemDefect(defectId, cmsRequester.getItemDefectName(defectId))));
		return defects;
	}

	@Override
	public Feedback addFeedback(final Long userId, final Byte workshopId, final Long orderId, final Long itemId,
			final Feedback feedback) {
		final Long fedbackId = orderManager.addFeedback(userId,
				Feedback.createWithOrderIdAndItemId(orderId, itemId, feedback));
		orderManager.updateItemRating(workshopId, orderId, itemId, feedback.getRating());
		return Feedback.createFromId(fedbackId);
	}

	@Override
	public Optional<Feedback> getFeedback(final Long userId, final Byte workshopId, final Long orderId,
			final Long itemId) {
		return orderManager.getFeedback(userId, workshopId, orderId, itemId);
	}

	@Override
	public Optional<LastDeliveredItem> getLastDeliveredItem(final Long userId, final Byte workshopId) {
		return orderManager.getLastDeliveredItem(userId, workshopId);
	}

	@Override
	public void applyCouponOnItem(final Long userId, final Byte workshopId, final Long orderId, final Long itemId,
			final Integer couponId) {
		orderManager.applyCouponOnItem(userId, workshopId, orderId, itemId, couponId);
	}

	@Override
	public void removeAppliedCoupon(final Byte workshopId, final Long orderId, final Long itemId,
			final Integer discountid) {
		orderManager.removeAppliedCoupon(workshopId, orderId, itemId, discountid);
	}

	@Override
	public void updateOrdersLocality(final Byte workshopId, final Long localityId, final String pin) {
		orderManager.updateOrdersLocality(workshopId, localityId, pin);
	}

	@Override
	public Integer getUserOrderCounts(final Long userId) {
		return orderManager.getUserOrderCounts(userId);
	}

	@Override
   public Collection<PaymentTransaction> listTransactions(final Byte workshopId,final Long orderId,final Long itemId,final Long userId,
            final Long startDate, final Long endDate, final Integer offset,
            final Integer limit) {
        return orderManager.listTransactions(workshopId,orderId,itemId,userId,startDate, endDate, offset, limit);
    }


	@Override
	public Map<String, String> findPaymentModeById(final Integer paymentModeId) {
		final Map<String, String> paymentMode = new HashMap<String, String>();
		paymentMode.put("payment_mode_id", paymentModeId.toString());
		paymentMode.put("payment_mode", PaymentMode.codeToType(paymentModeId).name());
		return paymentMode;
	}

	@Override
	public List<Map<String, String>> listPaymentModes(final Byte workshopId) {
		final List<Map<String, String>> paymentModeList = new ArrayList<>();
		for (PaymentMode paymentMode : PaymentMode.values()) {
			final Map<String, String> payment = new HashMap<String, String>();
			payment.put("payment_mode_id", paymentMode.getCode().toString());
			payment.put("payment_mode", PaymentMode.codeToType(paymentMode.getCode()).name());
			paymentModeList.add(payment);
		}
		return paymentModeList;
	}

	@Override
	public void updateOrderItemState(final Byte workshopId,final  List<Order> order) {
		order.stream().forEach(ord->{
			ord.getItems().stream().forEach(item->{
				updateItemState(workshopId, ord.getId(), item.getId(),item.getStateTransitions().get(0));
			});
		});
	}

	@Override
	public Collection<Order> search(final Byte workshopId, final Long userId,final String customerName,final String cc, final Long phone,final  Byte state,final Long fromDate,
			final Long toDate, final String filterBy, final Integer offset, final Integer limit) {
		return orderManager.search(workshopId, userId, customerName,cc, phone, state, fromDate, toDate, filterBy,offset, limit);
	}

	@Override
	public void addItemPaymentAdjustment(final Byte workshopId,final Long userId, final Long orderId, final Long itemId,
			final PaymentAdjustment paymentAdjustment) {
		orderManager.addItemPaymentAdjustment(workshopId,
				userId, orderId, itemId, paymentAdjustment);
	}

	@Override
	public Collection<Order> getTaxReports( final Byte workshopId, final Byte state, final Long fromDate, final Long toDate) {
		if(state!=null){			
			Optional<NationState> nationStateOpt=cmsRequester.getStateNameById(state);
			if(nationStateOpt.isPresent()){			
				return orderManager.getTaxReports(workshopId, nationStateOpt.get().getName(), fromDate, toDate); 
			}
			throw new NotFoundException("Entered Nation State Does Not Exist in our Database.", Response.status(404).build());
		}
		return orderManager.getTaxReportsByDateRange(workshopId, fromDate, toDate);
	}
}
