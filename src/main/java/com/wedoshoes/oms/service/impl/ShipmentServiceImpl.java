package com.wedoshoes.oms.service.impl;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wedoshoes.oms.manager.OrderManager;
import com.wedoshoes.oms.model.order.ItemState;
import com.wedoshoes.oms.model.shipment.Shipment;
import com.wedoshoes.oms.model.shipment.ShipmentPoint;
import com.wedoshoes.oms.model.shipment.ShipmentOrder;
import com.wedoshoes.oms.model.shipment.ShipmentSummary;
import com.wedoshoes.oms.model.state.StateTransition;
import com.wedoshoes.oms.service.InvoiceService;
import com.wedoshoes.oms.service.NotificationService;
import com.wedoshoes.oms.service.ShipmentService;

@Service
public class ShipmentServiceImpl implements ShipmentService {

	private final OrderManager orderManager;
	private final NotificationService notificationService;
	private final InvoiceService invoiceService;
	
	@Autowired
	public ShipmentServiceImpl(final OrderManager orderManager,	final NotificationService notificationService, final InvoiceService invoiceService) {
		this.orderManager = orderManager;
		this.notificationService = notificationService;
		this.invoiceService=invoiceService;
	}

	@Override
	public void update(final Byte workshopId, final Long id, final Shipment shipment) {
		final Optional<Shipment> oldShipOp = orderManager.getShipment(workshopId, id);
		if(oldShipOp.isPresent()){
			orderManager.changeShipmentUser(workshopId, id, shipment);
			//send notification to both
			notificationService.notifyShipmentUserChanged(workshopId, oldShipOp.get(),
					Shipment.createWithOrderIdAndItemIdAndState(oldShipOp.get().getOrderId(), oldShipOp.get().getItemId(), null, oldShipOp.get().getCurrentState(), shipment));
		}
	}

	@Override
	public Optional<Shipment> find(final Byte workshopId, final Long id) {
		return orderManager.getShipment(workshopId, id);
	}

	@Override
	public Collection<ShipmentOrder> findOrderByDateAndUser(final Byte workshopId, 
			final Long date, final Long userId) {
		return orderManager.getShipmentOrderByDateAndUser(workshopId, date, userId);
	}

	@Override
	public Optional<Shipment> findShipmentByDate(final Byte workshopId, final Long date, 
			final Long orderId, final Long itemId) {
		return orderManager.getShipmentByDate(workshopId, date, orderId, itemId);
	}

	@Override
	@Transactional
	public void changeShipmentItemState(final Byte workshopId, final Long userId, final Long shipmentId,
			final StateTransition transition) {
		
		orderManager.updateShipment(workshopId, shipmentId, StateTransition.createWithUserId(userId, transition));
		/** 
		 * Notify user when item state get changed to cancel and delivered.
		 */
		if(transition.getState()==ItemState.DELIVERED ){
			final File file=invoiceService.createInvoices(workshopId,transition.getShipment().getOrderId(), 
					transition.getShipment().getItemId());

			notificationService.notifyItemStateChanged(workshopId, transition.getShipment().getOrderId(), 
					transition.getShipment().getItemId(), transition.getState(), file, null, null);
		}
		if(transition.getState()==ItemState.CANCELLED){
			notificationService.notifyItemStateChanged(workshopId, transition.getShipment().getOrderId(), 
					transition.getShipment().getItemId(), transition.getState(), null, null, null);
		}
	}

	@Override
	public void addShipmentOrder(final Byte workshopId,final Long userId,final Long orderId,
			final StateTransition transition) {
		orderManager.addShipmentOrder(workshopId, orderId,
				StateTransition.createWithUserId(userId, transition));
		/** 
		 * Notify POS user
		 */
		notificationService.notifyShipmentOrderAdded(workshopId, orderId, transition);
	}

	@Override
	public Shipment addShipmentItem(final Byte workshopId, final Long userId,
			final Long orderId,	final Long itemId,final StateTransition transition) {
		final Long shipmentId = orderManager.addShipmentItem(workshopId, 
				orderId,itemId,	StateTransition.createWithUserId(userId, transition));
		/** 
		 * Notify POS user
		 */
		notificationService.notifyShipmentItemAdded(workshopId, orderId, itemId, transition);
		return Shipment.createFromId(shipmentId);
		
	}

	@Override
	public Collection<Shipment> getShipmentOrderAssignee(final Byte workshopId,
			final Long orderId, final Long itemId) {
		return orderManager.getShipmentOrderAssignee(workshopId,orderId,itemId);
	}

	@Override
	public Optional<ShipmentSummary> getShipmentSummary(final Byte workshopId, final Long userId,
			final Long shipmentDate) {
		return orderManager.getShipmentSummary(userId,shipmentDate);
	}

	@Override
	public Collection<ShipmentPoint> getShipmentRoutes(final Byte workshopId,
			final Long userId,final Long shipmentDate, final Double latitude, final Double longitude,final Byte currentTimeSlot) {
		return orderManager.getShipmentRoutes(workshopId,userId,
				shipmentDate,latitude,longitude,currentTimeSlot );
	}

	@Override
	public void updateShipments(final Byte workshopId, final List<Shipment> shipment) {
	         shipment.stream().forEach(shipmnt-> update(workshopId, shipmnt.getId(), shipmnt));
	}
}
