package com.wedoshoes.oms.service.impl;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wedoshoes.oms.manager.CouponManager;
import com.wedoshoes.oms.model.Coupon;
import com.wedoshoes.oms.model.CouponSource;
import com.wedoshoes.oms.service.CouponService;
/**
 * 
 * @author Lawakush Chaudhary
 *
 */
@Service
public class CouponServiceImpl implements CouponService {

	private final CouponManager couponManager;

	@Autowired
	public CouponServiceImpl(final CouponManager couponManager) {
		this.couponManager = couponManager;
	}

	// coupon

	@Override
	public Coupon save(final Coupon coupon) {
		return Coupon.createFromId(couponManager.save(coupon));
	}

	@Override
	public void update(final Coupon coupon) {
		couponManager.update(coupon);
	}

	@Override
	public void delete(final Integer couponId) {
		couponManager.delete(couponId);
	}

	@Override
	public Optional<Coupon> getCouponById(final Integer couponId,
			Boolean summary) {
		if (summary) {
			return couponManager.findById(couponId);
		}
		return couponManager.findDetailById(couponId);
	}

	@Override
	public Optional<Coupon> validate(final Coupon coupon) {
		return couponManager.validate(coupon);

	}

	@Override
	public Optional<Coupon> apply(final Coupon coupon) {
		return couponManager.apply(coupon);
	}

	@Override
	public Optional<Coupon> getCouponByCode(final String couponCode,
			Boolean summary) {
		if (summary && couponCode != null) {
			return couponManager.getCouponByCode(couponCode);
		} else if (!summary && couponCode != null) {
			return couponManager.getCouponDetailByCode(couponCode);
		}
		return Optional.empty();
	}

	@Override
	public Collection<Coupon> list(final Byte userTypeId,
			final Integer categoryId, final Integer serviceId,
			final Integer productId, final Integer offSet, final Integer limit) {
		if (userTypeId != null) {
			return couponManager.listByUserType(userTypeId, offSet, limit);
		} else if (productId != null) {
			return couponManager.listByProduct(productId, offSet, limit);
		} else if (categoryId != null) {
			return couponManager.listByCategory(categoryId, offSet, limit);
		}
		return couponManager.listDefaultCoupon(offSet, limit);
	}

	// coupon source

	@Override
	public CouponSource save(final CouponSource couponSource) {
		return CouponSource.createFromId(couponManager.save(couponSource));
	}

	@Override
	public void update(final CouponSource couponSource) {
		couponManager.update(couponSource);
	}

	@Override
	public void deleteCouponSource(final Integer sourceId) {
		couponManager.deleteSource(sourceId);
	}

	@Override
	public Optional<CouponSource> getCouponSourceById(final Integer sourceId,
			final Boolean summary) {
		if (summary) {
			return couponManager.findSourceById(sourceId);
		}
		return couponManager.findSourceDetailById(sourceId);
	}

	@Override
	public Collection<CouponSource> list()
			{
		return couponManager.list();
	}

//	@Override
//	public void removeAppliedCoupon(Long orderId, Long itemId, Integer discountid) {
//		couponManager.removeAppliedCoupon(orderId, itemId, discountid);
//	}

}
