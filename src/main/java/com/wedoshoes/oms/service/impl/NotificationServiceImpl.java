package com.wedoshoes.oms.service.impl;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.wedoshoes.oms.http.impl.model.core.CommSettings;
import com.wedoshoes.oms.manager.NotificationManager;
import com.wedoshoes.oms.manager.OrderManager;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.ItemState;
import com.wedoshoes.oms.model.order.Order;
import com.wedoshoes.oms.model.shipment.Shipment;
import com.wedoshoes.oms.model.state.StateTransition;
import com.wedoshoes.oms.service.NotificationService;

/**
 * 
 * @author Navrattan Yadav
 *
 */
@Service
public class NotificationServiceImpl implements NotificationService {

	private final OrderManager orderManager;
	private final NotificationManager notificationManager;
	private final ThreadPoolTaskExecutor bgExecutor;
	
	/**
	 * Item State on which user need to notify by server automatically.
	 */
	public final Set<Byte> notifiableState; 
	/** 
	 * Item State on which pos user need to notify by server
	 */
	public final Set<Byte> posNotifiableState;
	
	
	@Autowired
	public NotificationServiceImpl(final OrderManager orderManager,
			final NotificationManager notificationManager,
			final ThreadPoolTaskExecutor bgExecutor,
			@Value("#{notifiableState}") final Set<Byte> notifiableState,
			@Value("#{posNotifiableState}") final Set<Byte> posNotifiableState) {
		this.orderManager = orderManager;
		this.notificationManager = notificationManager;
		this.bgExecutor = bgExecutor;
		this.notifiableState = notifiableState;
		this.posNotifiableState = posNotifiableState;
	}

	/**
	 * This function used to notify user when overall orders items state get change like New (When creating order).
	 */
	@Override
	public void notifyOrderStateChanged(final Byte workshopId, final Long orderId, final Byte state,
			final CommSettings commSett,final Boolean saveSettings) {
		if(notifiableState.contains(state)) {
			bgExecutor.execute(new Runnable() {
				@Override
				public void run() {
					final Optional<Order> optional = orderManager.getOrderDetail(workshopId, orderId);
					if(optional.isPresent()) {
						final Order order = optional.get();
						final List<Item> items  = order.getItems().stream()
								.map(item -> Item.createWithStateTransition(item, 
										getStateTransition(workshopId, item.getId(), 
												item.getState()))).collect(Collectors.toList());
						notificationManager.notifyOrderStateChanged(Order.createWithItems(order, items),
								state, null, commSett, saveSettings);
					}
				}
			});
		}
	}

	/**
	 * This function used to notify user when items state get change like Analysed, delivered, canceled, etc.
	 */
	
	@Override
	public void notifyItemStateChanged(final Byte workshopId, final Long orderId, Long itemId,
			final Byte state, final File file, final CommSettings commSett,final Boolean saveSettings) {
		if(notifiableState.contains(state)) {
			bgExecutor.execute(new Runnable() {
				@Override
				public void run() {
					final Optional<Order> optional = orderManager.getOrder(workshopId, orderId);
					if(optional.isPresent()) {
						final Optional<Item> optinalItem = orderManager.getItem(workshopId, itemId);
						if(optinalItem.isPresent()) {
							final Item item = optinalItem.get();
							final Order order = Order.createWithItems(optional.get(), 
									Lists.newArrayList(Item.createWithStateTransition(item, 
											getStateTransition(workshopId, item.getId(), item.getState()))));
							notificationManager.notifyOrderStateChanged(order, state, file, commSett,saveSettings);
						}
					}
				}
			});	
		}
	}
	
	/**
	 * Return State Transition of item current state of item. 
	 * @param workshopId
	 * @param itemId
	 * @param state
	 * @return
	 */
	private List<StateTransition> getStateTransition(final Byte workshopId,
			final Long itemId, final Byte state) {
		final Optional<StateTransition> optional = orderManager
				.getStateTransition(workshopId, itemId, state);
		if(optional.isPresent()) {
			return Lists.newArrayList(optional.get());
		}
		return null;
	}

/**
 * This function is used to notify POS users when orders added in their schedule (shipment/trip).
 */
	@Override
	public void notifyShipmentOrderAdded(final Byte workshopId, final Long orderId,
			final StateTransition transition) {
		if(posNotifiableState.contains(transition.getState())) {
			bgExecutor.execute(new Runnable() {
				@Override
				public void run() {
					final Optional<Order> optional = orderManager.getOrderDetail(workshopId, orderId);
					if(optional.isPresent()) {
						final Order order = optional.get();
						
						final List<Item> items  = order.getItems().stream()
								.map(item -> Item.createWithStateTransition(item, 
										getStateTransition(workshopId, item.getId(), 
												item.getState()))).collect(Collectors.toList());
						
						notificationManager.notifyShipmentOrderAdded(workshopId, Shipment
								.createWithOrderIdAndItemIdAndState(orderId, null,null, transition.getState(),
										transition.getShipment()), Order.createWithItems(order, items));
					}
				}
			});
		}
	}

	/**
	 * This function is used to notify POS users when orders added in their schedule (shipment/trip).
	 */

	@Override
	public void notifyShipmentItemAdded(final Byte workshopId, final Long orderId,
			final Long itemId, final StateTransition transition) {
		if(posNotifiableState.contains(transition.getState())) {
			bgExecutor.execute(new Runnable() {
				@Override
				public void run() {
					final Optional<Order> optional = orderManager.getOrder(workshopId, orderId);
					if(optional.isPresent()) {
						final Optional<Item> optinalItem = orderManager.getItem(workshopId, itemId);
						if(optinalItem.isPresent()) {
							final Item item = optinalItem.get();
							final Order order = Order.createWithItems(optional.get(), 
									Lists.newArrayList(Item.createWithStateTransition(item, 
											getStateTransition(workshopId, item.getId(), item.getState()))));
							
							notificationManager.notifyShipmentOrderAdded(workshopId, Shipment
									.createWithOrderIdAndItemIdAndState(orderId, itemId, null,transition.getState(), 
											transition.getShipment()), order);
						}
					}
				}
			});
		}
	}
	
	@Override
	public void notifyShipmentUserChanged(final Byte workshopId, final Shipment OldSpmnt, final Shipment newSpmnt) {
		if(posNotifiableState.contains(OldSpmnt.getCurrentState())) {
			bgExecutor.execute(new Runnable() {
				@Override
				public void run() {
					final Optional<Order> optional = orderManager.getOrder(workshopId, OldSpmnt.getOrderId());
					if(optional.isPresent()) {
						final Optional<Item> optinalItem = orderManager.getItem(workshopId, OldSpmnt.getItemId());
						if(optinalItem.isPresent()) {
							final Item item = optinalItem.get();
							final Order order = Order.createWithItems(optional.get(), 
									Lists.newArrayList(Item.createWithStateTransition(item, 
											getStateTransition(workshopId, item.getId(), item.getState()))));
							notificationManager.notifyShipmentUserChanged(workshopId, OldSpmnt, newSpmnt, order);
						}
					}
				}
			});
		}
	}

	
/**
 * Notify POS when orders/items (canceled or re-schedule for pickup or re-schedule for delivery)
 */
	
	@Override
	public void notifyShipmentOrderStateChanged(final Byte workshopId, final Long orderId,
			final StateTransition transition) {
		if(posNotifiableState.contains(transition.getState())) {
			bgExecutor.execute(new Runnable() {
				@Override
				public void run() {
					
					final Collection<Shipment> shipments = orderManager.listShipmentByOrderId(workshopId, orderId);
					if(!shipments.isEmpty()){
						final Optional<Order> optional = orderManager.getOrderDetail(workshopId, orderId);
						if(optional.isPresent()) {
							final Order order = optional.get();
							final List<Item> items  = order.getItems().stream()
									.map(item -> Item.createWithStateTransition(item, 
											getStateTransition(workshopId, item.getId(), 
													item.getState()))).collect(Collectors.toList());
							final Order orderWithItems = Order.createWithItems(order, items);
							
							if(transition.getState()==ItemState.RE_SCHEDULE_FOR_DELIVERY ||
									transition.getState()==ItemState.RE_SCHEDULE_FOR_PICKUP){
								shipments.forEach(shipment -> notificationManager.notifyShipmentOrderAdded(workshopId,
										shipment, orderWithItems)
										
										);
							}
							if(transition.getState()==ItemState.CANCELLED){
								shipments.forEach(shipment -> notificationManager.notifyShipmentOrderCanceled(workshopId,
										shipment, orderWithItems)
										
										);
							}
							
						}
					}
				}
			});
		}
	}

	
	@Override
	public void notifyShipmentItemStateChanged(final Byte workshopId, final Long orderId, 
			final Long itemId, final StateTransition transition) {
		if(posNotifiableState.contains(transition.getState())) {
			bgExecutor.execute(new Runnable() {
				@Override
				public void run() {
					
					final Optional<Shipment> opShip = orderManager.getShipmentByItemId(workshopId, orderId, itemId);
					if(opShip.isPresent()){
						final Optional<Order> optional = orderManager.getOrderDetail(workshopId, orderId);
						if(optional.isPresent()) {
							final Optional<Item> optinalItem = orderManager.getItem(workshopId, itemId);
							
							if(optinalItem.isPresent()) {
								final Item item = optinalItem.get();
								final Order order = Order.createWithItems(optional.get(), 
										Lists.newArrayList(Item.createWithStateTransition(item, 
												getStateTransition(workshopId, item.getId(), item.getState()))));
								
								if(transition.getState()==ItemState.CANCELLED){
									notificationManager.notifyShipmentOrderCanceled(workshopId, opShip.get(), order);
								}
								if(transition.getState()==ItemState.RE_SCHEDULE_FOR_DELIVERY ||
										transition.getState()==ItemState.RE_SCHEDULE_FOR_PICKUP){
									notificationManager.notifyShipmentOrderAdded(workshopId, opShip.get(), order);
								}
							}
						}
					}
				}
			});
		}
	}
	
	@Override
	public void notifyOnInvoiceAndChallanCreation(final Byte workshopId,final Long userId, final Long orderId, final Byte state, File file){
		final Optional<Order> order=orderManager.getOrderDetail(workshopId, orderId);
		if(order.isPresent()){
		 notificationManager.notifyOnInvoiceAndChallanCreation(userId, order.get(), state, file); 
		}
	}
}
