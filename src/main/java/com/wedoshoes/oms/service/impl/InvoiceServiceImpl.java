package com.wedoshoes.oms.service.impl;

import static com.wedoshoes.oms.dao.impl.DefaultUtils.defaultBigDecimalIfNull;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crater.pdfgenerator.Address;
import com.crater.pdfgenerator.AffiliateType;
import com.crater.pdfgenerator.DocType;
import com.crater.pdfgenerator.InvoiceDetail;
import com.crater.pdfgenerator.PdfGeneratorService;
import com.wedoshoes.oms.http.CMSServerHttpRequester;
import com.wedoshoes.oms.http.CoreServerHttpRequester;
import com.wedoshoes.oms.invoice.Invoice;
import com.wedoshoes.oms.manager.OrderManager;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.ItemState;
import com.wedoshoes.oms.model.order.Order;
import com.wedoshoes.oms.service.InvoiceService;
import com.wedoshoes.oms.service.NotificationService;
import com.wedoshoes.oms.util.DateUtil;

@Service
public class InvoiceServiceImpl implements InvoiceService {

	final Byte DELIVERED_STATE=5; 
	final Byte INVOICE_GENERATED=21; 
	private final OrderManager orderManager;
	private final String invoiceTitle;
	private final String companyStreetAddressTitle;
	private final String companyStreetAddress;
	private final String companyAddressName;
	private final String companyAddressLandmark;
	private final String companyAddressCity;
	private final String companyAddressPin;
	private final String companyAddressState;
	private final String companyAddressCountry;
	private final String companyAddressPhone;
	private final String vatTinNumber;
	private final String cstNumber;
	private final String serviceTaxNumber;
	private final String GSTNumber;
	private final String PANNumber;
	
	 String invoiceNumber;
	 String orderNumber;
	private final String digitalSignatureHeader;
	private final String termAndConditionsTitle;
	private final String termAndConditionsL1;
	private final String termAndConditionsL2;
	private final String termAndConditionsL3;
	private final String invoiceFooter;
	@SuppressWarnings("unused")
	private final CoreServerHttpRequester coreServerHttpRequester;
	private final CMSServerHttpRequester cmsServerHttpRequester;
	private final NotificationService notificationService;
	private final PdfGeneratorService pdfGenerator;
	
	@Autowired
	public InvoiceServiceImpl(final OrderManager orderManager, @Value("${invoice.title}") String invoiceTitle,   
			@Value("${company.address.title}") String companyStreetAddressTitle ,@Value("${company.address.street}") String companyStreetAddress, 
			@Value("${company.address.name}") String companyAddressName,  
			@Value("${company.address.landmark}") String companyAddressLandmark,  @Value("${company.address.city}") String companyAddressCity,  
			@Value("${company.address.pin}") String companyAddressPin,  @Value("${company.address.state}") String companyAddressState,  
			@Value("${company.address.country}") String companyAddressCountry,  @Value("${company.address.phone}") String companyAddressPhone,  
			@Value("${vat.tin.number}") String vatTinNumber,  @Value("${cst.number}") String cstNumber,  
			@Value("${service.tax.number}") String serviceTaxNumber, @Value("${gst.number}") String GSTNumber, @Value("${pan.number}") String PANNumber, 
			@Value("${invoice.number}") String invoiceNumber,  @Value("${order.number}") String orderNumber,  
			@Value("${digital.signature.header}") String digitalSignatureHeader,  @Value("${term.and.conditions.title}") String termAndConditionsTitle,  
			@Value("${term.and.conditions.l1}") String termAndConditionsL1,  @Value("${term.and.conditions.l2}") String termAndConditionsL2,  
			@Value("${term.and.conditions.l3}") String termAndConditionsL3,  @Value("${invoice.footer}") String invoiceFooter,
			final CoreServerHttpRequester coreServerHttpRequester, final CMSServerHttpRequester cmsServerHttpRequester, 
			final NotificationService notificationServicer,final PdfGeneratorService pdfGenerator) {
	
		this.orderManager = orderManager;
		this.invoiceTitle =invoiceTitle;
		this.companyStreetAddressTitle=companyStreetAddressTitle;
		this.companyStreetAddress=companyStreetAddress;
		this.companyAddressName=companyAddressName;
		this.companyAddressLandmark=companyAddressLandmark;
		this.companyAddressCity=companyAddressCity;
		this.companyAddressPin=companyAddressPin;
		this.companyAddressState=companyAddressState;
		this.companyAddressCountry=companyAddressCountry;
		this.companyAddressPhone=companyAddressPhone;
		this.vatTinNumber=vatTinNumber;
		this.cstNumber=cstNumber;
		this.invoiceNumber=invoiceNumber;
		this.orderNumber=orderNumber;
		this.digitalSignatureHeader=digitalSignatureHeader;
		this.termAndConditionsTitle=termAndConditionsTitle;
		this.termAndConditionsL1=termAndConditionsL1;
		this.termAndConditionsL2=termAndConditionsL2;
		this.termAndConditionsL3=termAndConditionsL3;
		this.invoiceFooter=invoiceFooter;
		this.cmsServerHttpRequester=cmsServerHttpRequester;
		this.coreServerHttpRequester=coreServerHttpRequester;
		this.serviceTaxNumber=serviceTaxNumber;
		this.notificationService=notificationServicer;
		this.GSTNumber=GSTNumber;
		this.PANNumber=PANNumber;
		this.pdfGenerator=pdfGenerator;
	}

	@Override
	public File testIncvoices(final Byte workshopId, final Order order){
		Collection<Item> deliveredItems = order.getItems();
	    List<InvoiceDetail> invoiceDetails= deliveredItems.stream().map(item ->
	    convertOrderToInvoiceAndSaveData(workshopId, order, item, getTermsAndConditions())).collect(Collectors.toList()); 
	    return pdfGenerator.createInvoices(invoiceDetails, true, DocType.INVOICE.toString(), workshopId, order.getId(), AffiliateType.WEDOSHOES);
	}
	
	@Override
	public File createInvoices(final Byte workshopId, final Long orderId, final Long itemId){  			
		final Optional<Order> orderResp=  orderManager.getOrderDetail(workshopId, orderId);
		Collection<Item> itemsList = orderResp.get().getItems();
		List<InvoiceDetail> invoiceDetails=new ArrayList<>();
	if(itemId!=null){		
		itemsList.stream().forEach(item->{
			if(item.getId().longValue()==itemId.longValue()){
				invoiceDetails.add(convertOrderToInvoiceAndSaveData(workshopId, orderResp.get(), item, getTermsAndConditions())); 
			}
		});
	}else{
		itemsList.stream().forEach(item->{
			if(item.getState()==ItemState.DELIVERED){
				invoiceDetails.add(convertOrderToInvoiceAndSaveData(workshopId, orderResp.get(), item, getTermsAndConditions())); 
			}
		});
	}	   
	   return  pdfGenerator.createInvoices(invoiceDetails, true, DocType.INVOICE.toString(),workshopId,orderId,AffiliateType.WEDOSHOES);
	}
	

	
	@Override
	public File createChallan(final Byte workshopId, final Long orderId,
			final Long itemId){  	
		final Optional<Order> orderResp=  orderManager.getOrderDetail(workshopId, orderId);
		Collection<Item> itemsList = orderResp.get().getItems();
		List<InvoiceDetail> invoiceDetails=new ArrayList<>();
	   if(itemId!=null){
		   itemsList.stream().forEach(item->{
				if(item.getId().longValue()==itemId.longValue()){
					invoiceDetails.add(convertOrderToInvoiceModel(workshopId, orderResp.get(), item, getTermsAndConditions())); 
				}
			});
	   }else{		   
		   itemsList.stream().forEach(item->
				invoiceDetails.add(convertOrderToInvoiceModel(workshopId, orderResp.get(), item, getTermsAndConditions())));
	   }
	   return pdfGenerator.createInvoices(invoiceDetails, true, DocType.CHALLAN.toString(),workshopId,orderId,AffiliateType.WEDOSHOES);
	}
	
	@Override
	public File getInvoices(final Byte workshopId, final Long orderId, final Long itemId,
			final Long date, final Integer duration) {
		List<InvoiceDetail> invoicesList=new ArrayList<InvoiceDetail>();
		Collection<Invoice> invoices=orderManager.getInvoices(workshopId, orderId, itemId, date, duration);
		for(Invoice invoice :invoices){
			Optional<Order> orderResp=orderManager.getOrderDetail(workshopId, invoice.getOrderId());
			Optional<Item> item=orderManager.getItem(workshopId, invoice.getItemId());      
			invoicesList.add(convertOrderToInvoiceModel(workshopId, orderResp.get(), 
					item.get(), getTermsAndConditions()));		
		}
		return pdfGenerator.createInvoices(invoicesList, false, DocType.INVOICE.toString(), workshopId, orderId,AffiliateType.WEDOSHOES);	 
	}
	
	private List<String> getTermsAndConditions(){
		List<String> termAndConditions=new ArrayList<>();
		termAndConditions.add(termAndConditionsTitle);
		termAndConditions.add(termAndConditionsL1);
		termAndConditions.add(termAndConditionsL2);
		termAndConditions.add(termAndConditionsL3);
		return termAndConditions;
	}
	
	@Transactional
	private InvoiceDetail convertOrderToInvoiceAndSaveData(final Byte workshopId, final Order order, final Item item, final List<String> termAndConditions){
	    orderManager.saveInvoice(workshopId, order.getId(), item.getId(), INVOICE_GENERATED); 
	    return convertOrderToInvoiceModel(workshopId, order, item, termAndConditions);
	}
	
	private InvoiceDetail convertOrderToInvoiceModel(final Byte workshopId, final Order order, final Item item, 
			final List<String> termAndConditions) {
		
		return new InvoiceDetail(order.getCustomerName(), order.getPhone(), ""+order.getPickUpDate(),
				""+order.getPickUpTimeSlotId(), item.getCostCreadited(), null, 
				null, null, null, null, null,null, ""+order.getId(),null, null, null, item.getId(), 
				cmsServerHttpRequester.getProductName(item.getProductId()), 
				cmsServerHttpRequester.getServiceName(item.getParentServiceId()),
				DateUtil.convertToDateString(item.getDeliveryDate()), item.getServiceCost(), 
				item.getDiscount(item.getServiceCost()).setScale(2, RoundingMode.HALF_UP), item.getGST().getIgst(),item.getGST().getSgst(),item.getGST().getCgst(),item.getGST().isIntrastate(),item.calculateTaxAmount().setScale(2, RoundingMode.HALF_UP),   
				item.getConvenienceCharge(), true, null,
				new Address(order.getCustomerName(), order.getLastName(),order.getPickUpAddress().getLandmark(), 
						order.getPickUpAddress().getCity(),
								order.getPickUpAddress().getPin(), 
								order.getPickUpAddress().getState(), null, 
								""+order.getPhone(), order.getPickUpAddress().getStreetAddress()), 
						
			    new Address(order.getCustomerName(),order.getLastName(), order.getDeliveryAddress().getLandmark(), order.getDeliveryAddress().getCity(), 
								order.getDeliveryAddress().getPin(), order.getDeliveryAddress().getState(), 
								null, ""+order.getPhone(), order.getDeliveryAddress().getStreetAddress()),
								defaultBigDecimalIfNull(item.getExpressProcessingCharge()) ,
								BigDecimal.ZERO, 
								defaultBigDecimalIfNull(item.calculateTotalCost()).setScale(2,RoundingMode.HALF_UP), 
								defaultBigDecimalIfNull(item.getDiscount(item.getServiceCost())).setScale(2,RoundingMode.HALF_UP),   
								item.isExpressProcessing(), null);
	}

	@Override
	public File sendInvoices(final Long userId, final Byte workshopId, final Long orderId, final Long itemId) {
		final File file=createInvoices(workshopId, orderId, itemId);
		final Byte  invoiceGenerated=100;
		notificationService.notifyOnInvoiceAndChallanCreation(workshopId,userId, orderId, invoiceGenerated, file); 
		return file;
	}

	@Override
	public File sendChallan(final Long userId, final Byte workshopId, final Long orderId, final Long itemId) {
		final Byte challanCreated=99;
		final File file= createChallan(workshopId, orderId, itemId);
		notificationService.notifyOnInvoiceAndChallanCreation(workshopId,userId, orderId, challanCreated, file); 
		return file;
	}
}
