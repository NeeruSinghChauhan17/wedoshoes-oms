package com.wedoshoes.oms.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wedoshoes.oms.http.CMSServerHttpRequester;
import com.wedoshoes.oms.model.Items;
import com.wedoshoes.oms.model.cost.EstimatedCost;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.service.CostService;

@Service
public class CostServiceImpl implements CostService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CostServiceImpl.class);
	
	private final CMSServerHttpRequester cmsHttpRequest;
	
	@Autowired
	public CostServiceImpl(final CMSServerHttpRequester cmsHttpRequest) {
		this.cmsHttpRequest = cmsHttpRequest;
	}

	@Override
	public Boolean verify(final Long userId, final List<Item> items,final Long referredByUserId) {
		
		for(Item item : items) {
			if(!verify(item,referredByUserId)) {
				LOGGER.info("Item Cost Not Matched for user " + userId);
				return Boolean.FALSE;
			}
		}
		return Boolean.TRUE;
	}
	
	private Boolean verify(final Item item,final Long referredByUserId) {
		if(Items.getEstimatedCost(item).equals(getEstimatedCost(item, referredByUserId,Boolean.TRUE))) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	@Override
	public EstimatedCost getEstimatedCost(final Item item,final Long referredByUserId, final Boolean summary) {
		return cmsHttpRequest.getEstimatedCost(Items.asItemSummary(item), referredByUserId, summary);
	}

	@Override
	public Long getDeliveryDate(final Integer parentServiceId, final Long pickupDate,
			final Boolean expressDelivery) {
		return cmsHttpRequest.getDeliveryDate(parentServiceId, pickupDate, expressDelivery);
	}	
}
