package com.wedoshoes.oms.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wedoshoes.oms.model.shipment.ShipmentPoint;

public class ShipmentRouteGenerator {
	
	public static  final Logger LOGGER = LoggerFactory.getLogger(ShipmentRouteGenerator.class);
	
	public static final String googleDistanceUrl="https://maps.googleapis.com/maps/api/directions/json?origin=";
	public static final String GOOGLE_API_KEY="AIzaSyDj8ZLb-gxOXrrKFxSgigbxmtVcM5v5K4g";
	

	

	public static Collection<ShipmentPoint> getShortestRoute(final List<ShipmentPoint> locationList) {
		List<ShipmentPoint> shortestLocationList=new ArrayList<ShipmentPoint>();
		shortestLocationList.add(new ShipmentPoint(locationList.get(0).getLatitude(),locationList.get(0).getLongitude(),null,null));
		
		shortestLocationList=getShortestLocationsList(locationList.get(0).getLatitude(),locationList.get(0).getLongitude(),locationList,shortestLocationList);
		return shortestLocationList;
	}

	/**
	 *This calculates list of all place locations which are shortest to each other
	 */
	public static List<ShipmentPoint> getShortestLocationsList(final Double latitude, final Double longitude,
			final List<ShipmentPoint> locationList,final  List<ShipmentPoint> sortedLocationList) {
		
		Double dist;
		Double kdist;
		Integer minDistIndex = 1;
		double lat = latitude;
		double lng =longitude;

		for(int l=locationList.size()-1;l>0;l--){
			final int size=locationList.size();
	        dist=calculateDistance(lat,lng,locationList.get(size-1).getLatitude(),locationList.get(size-1).getLongitude());
			kdist=dist;
	        for(int i=1;i<size-1;i++){
				final Double distance=calculateDistance(lat,lng,locationList.get(i).getLatitude(),locationList.get(i).getLongitude());
				if(dist > 0.0  && dist > distance ){
				   dist=distance;
				   minDistIndex=i;
				 }							
		    }
	        if(kdist==dist ){
				minDistIndex=size-1;
			}
			lat=locationList.get(minDistIndex).getLatitude();
			lng=locationList.get(minDistIndex).getLongitude();
			sortedLocationList.add(locationList.get(minDistIndex));
			locationList.remove(locationList.get(minDistIndex));
		}
		return sortedLocationList;
	}

	/**
	 *Google direction api calculates distance between  source and destination coordinates 
	 */
	public static Double calculateDistance(final Double sourceLatitude, final Double sourceLongitude,
			final Double destinationLatitude, final Double destinationLongitude) {
		
		HttpURLConnection connection;
		try {
			URL distanceURL = new URL(googleDistanceUrl+sourceLatitude+"%2C"+sourceLongitude+"&destination="+destinationLatitude+"%2C"+destinationLongitude+"&sensor="+false+"&key="+GOOGLE_API_KEY);
			LOGGER.info("url entered "+distanceURL.toString());
			connection = (HttpURLConnection) distanceURL.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			connection.setDoOutput(true);
			connection.connect();
			
			 BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
             StringBuilder sb = new StringBuilder();
             String line;
             while ((line = br.readLine()) != null) {
                 sb.append(line+"\n");
             }
             br.close();
             try {
				JSONObject obj = new JSONObject(sb.toString());
				JSONArray routesArray = obj.getJSONArray("routes");
			    JSONObject route = routesArray.getJSONObject(0);
			    JSONArray legs ;
			    JSONObject leg ;
			    if(route.has("legs")) {
			        legs = route.getJSONArray("legs");
			        leg = legs.getJSONObject(0) ;
			        JSONObject obj1=leg.getJSONObject("distance");
			    
				String dist=obj1.getString("text");
				String[] wordsplit = dist.split(" ");
				return Double.parseDouble(wordsplit[0]);
			    }
			} catch (JSONException e) {
				LOGGER.error("Query Limit Of Api exceeded or Zero Result Found  ");
			}
     		} catch (IOException e) {
			LOGGER.error("Error while calling  URL:",e);
		}
		return 0.0;
	}
	
}
