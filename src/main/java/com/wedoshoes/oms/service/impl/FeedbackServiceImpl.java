package com.wedoshoes.oms.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.wedoshoes.oms.model.order.FeedbackType;
import com.wedoshoes.oms.service.FeedbackService;
import com.wedoshoes.oms.util.FeedbackCategories;

@Service
public class FeedbackServiceImpl implements FeedbackService{

	@Override
	public Map<Integer, ArrayList<FeedbackType>> getFeedbackCategory(
			final Integer ratingPoint) {
		return FeedbackCategories.getFeedbackCategories();
	}

	@Override
    public List<String> getFeedbackCategoryByRatingPoint(final Integer ratingPoint) {
        Map<Integer, ArrayList<FeedbackType>> feedBackType=FeedbackCategories.getFeedbackCategories();
        final ArrayList<String> feedBackTypeCategoryList=new ArrayList<String>();
        for(Map.Entry<Integer, ArrayList<FeedbackType>>  entry :feedBackType.entrySet()){
            if(entry.getKey().equals(ratingPoint)){
             for(FeedbackType  feed : entry.getValue()){
                 feedBackTypeCategoryList.add(feed.toValue());
             }
          }
        }
        return feedBackTypeCategoryList;
    }
}
