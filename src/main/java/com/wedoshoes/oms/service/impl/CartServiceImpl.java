package com.wedoshoes.oms.service.impl;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.codahale.metrics.annotation.Timed;
import com.wedoshoes.oms.manager.CartManager;
import com.wedoshoes.oms.model.order.Cart;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.Order;
import com.wedoshoes.oms.model.order.WedoService;
import com.wedoshoes.oms.service.CartService;
import com.wedoshoes.oms.service.CostService;
import com.wedoshoes.oms.service.OrderService;

@Service
public class CartServiceImpl implements CartService {

	private final CartManager cartManager;
	private final OrderService orderService;
	private final CostService costService;
	
	@Autowired
	public CartServiceImpl(final CartManager cartManager,
			final OrderService orderService,final CostService costService) {
		this.cartManager = cartManager;
		this.orderService=orderService;
		this.costService = costService;
	}
	
	@Override
	public Long createCart(final Cart cart) {
		return cartManager.createCart(cart);
	}

	@Override
	public void addCartItems(final Long cartId, final List<Item> items) {
		cartManager.addCartItems(cartId, items);
	}

	@Override
	public void addCartItemServices(final Long cartId, final Long itemId,
			final List<WedoService> services) {
		cartManager.addCartItemServices(cartId, itemId, services);
	}

	@Override
	public void deleteItemServiceByServiceId(final Long cartId,final Long itemId, final Long serviceId) {
		cartManager.deleteItemServiceByServiceId(itemId, serviceId);
	}

	@Override
	public void deleteItem(final Long cartId, final Long itemId) {
		cartManager.deleteItem(cartId, itemId);
	}

	@Override
	public void clearCart(final Long cartId) {
		cartManager.clearCart(cartId);
	}
	
	@Override
	public void deActivateCart(final Long cartId) {
		cartManager.deActivateCart(cartId);
	}

	@Override
	@Timed
	public Order checkoutCart(final Long cartId, final Order order) {
		
		final List<Item> cartItems = listCartItemByCartId(cartId);
		final List<Item> items = mapOrderItemToCartItem(order.getItems(), cartItems);
		
		final List<Item> itemsWithCost = items.stream().map(item -> Item.
				createWithEstimatedCostAndDeliveryDate(item, 
				costService.getEstimatedCost(item,order.getReferredByUserId(), Boolean.FALSE),
				costService.getDeliveryDate(item.getParentServiceId(), order.getPickUpDate(), 
						item.isExpressProcessing()), LocalTime.now().toSecondOfDay()
				, item.getDeliveryTimeSlotId())).
						collect(Collectors.toList());
		// TODO get workshopId based on pickup address pin.
		final Order orderResp = orderService.createOrder(Order.createWithItems(order, itemsWithCost), null,null);
		// mark cart passive
		//deActivateCart(cartId);
		clearCart(cartId); 
		return orderResp;
	}
	
	// map discounts to cart items
	private List<Item> mapOrderItemToCartItem(final List<Item> orderItems, final List<Item> cartItems){
		if(orderItems==null || orderItems.isEmpty()){
			return cartItems;
		}
		final Map<Long,Item> map = new HashMap<>();
		final List<Item> temp = new ArrayList<>();
		orderItems.forEach(o -> map.put(o.getId(), o));  // put all order item into map<id,item>
		//map discounts
		cartItems.forEach(c -> {
				if(map.containsKey(c.getId())){
					temp.add(Item.createWithDiscounts(map.get(c.getId()).getDiscounts(), c));
				}else{
					temp.add(c);
				}
		});
		return temp;
	}

	@Override
	public List<Item> listCartItemByCartId(final Long cartId) {
		return cartManager.listCartItemByCartId(cartId);
	}

	@Override
	public Optional<Cart> getCartByUserId(final Long userId) {
		return cartManager.getCartByUserId(userId);
	}
	
	@Override
	public Optional<Cart> getCartById(final Long id) {
		return cartManager.getCartById(id);
	}

	@Override
	public List<Cart> listCarts(final Integer offset, final Integer limit) {
		return cartManager.listCarts(offset, limit);
	}

	@Override
	public Optional<Cart> getCartDetail(final Long cartId, final Boolean summary) {
		if(summary){
			return cartManager.getCartById(cartId);
		}
		return cartManager.getCartDetailById(cartId);
	}

	@Transactional
	@Override
	public void updateItem(final Byte workshopId, final Long cartId, final Long itemId, final Item item) {
		
			cartManager.updateItem(workshopId, cartId, Item.createWithItemId(itemId, item)); 
		if(!item.getServices().isEmpty()){
			cartManager.deleteItemServicesByItemId(itemId); 
			cartManager.addCartItemServices(cartId, itemId, item.getServices()); 
		}
	}
	
}
