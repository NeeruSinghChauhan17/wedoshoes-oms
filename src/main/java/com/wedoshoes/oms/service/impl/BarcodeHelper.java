package com.wedoshoes.oms.service.impl;

import java.awt.image.BufferedImage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.crater.barcodegenerator.BarcodeGenerator;
import com.crater.barcodegenerator.BarcodeInfo;
import com.crater.barcodegenerator.EncodedData;
import com.wedoshoes.oms.http.CMSServerHttpRequester;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.Order;
import com.wedoshoes.oms.util.DateUtil;

@Service
public class BarcodeHelper{

	private final BarcodeGenerator barcodeGenerator;
	private final CMSServerHttpRequester cmsRequester;
	private final String barTitle;
	
	@Autowired
	public BarcodeHelper(final BarcodeGenerator barcodeGenerator,
			final CMSServerHttpRequester cmsRequester, @Value("${barcode.title}") final String barTitle) {
		this.barcodeGenerator = barcodeGenerator;
		this.cmsRequester = cmsRequester;
		this.barTitle = barTitle;
	}

	public BufferedImage generateBarcodeWithInfo(final Order order,final Integer itemNumber,
			final Integer totalItem) {
		final Item item = order.getItems().get(0);
		final String itemId = item.isExpressProcessing()?item.getId()+"*":String.valueOf(item.getId());
		final String locality  = cmsRequester.getLocalityName(order.getDeliveryAddress().getLocality());
		final String priority  = item.getPriority()!=null?cmsRequester.getPriorityName(item.getPriority()):"";
		final StringBuilder sevices = new StringBuilder();
		
		item.getServices().forEach(e->sevices.append(",")
				.append(cmsRequester.getServiceNameAcronym(e.getServiceId())));
		final EncodedData data = new EncodedData(order.getWorkshopId(), order.getId(), item.getId());
		final BarcodeInfo barInfo = new BarcodeInfo(getBarTitle(), itemId,
			order.getDeliveryAddress().getPin(), locality, DateUtil.convertToDateString(item.getDeliveryDate())
			,priority,	sevices.toString().substring(1), order.getCustomerName(),
				String.valueOf(itemNumber), String.valueOf(totalItem));
		return barcodeGenerator.generateBarcodeWithInfo(data,barInfo);
	}

	public BufferedImage generateBarcode(final Byte workshopId, final Long orderId,
			final Long itemId) {
		return barcodeGenerator.generateBarcode(new EncodedData(workshopId, orderId, itemId));
	}

	public String getBarTitle() {
		return barTitle;
	}

}
