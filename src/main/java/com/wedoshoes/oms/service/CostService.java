package com.wedoshoes.oms.service;

import java.util.List;

import com.wedoshoes.oms.model.cost.EstimatedCost;
import com.wedoshoes.oms.model.order.Item;

public interface CostService {

	Boolean verify(Long userId, List<Item> items,Long referredByUserId);
	EstimatedCost getEstimatedCost(Item item, Long referredByUserId,Boolean summary);
	Long getDeliveryDate(Integer parentServiceId, Long pickupDate, Boolean expressDelivery);
	
	
}