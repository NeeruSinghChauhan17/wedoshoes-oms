package com.wedoshoes.oms.service;

import java.util.List;
import java.util.Optional;

import com.wedoshoes.oms.model.order.Cart;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.Order;
import com.wedoshoes.oms.model.order.WedoService;

public interface CartService {
	Long createCart(Cart cart);

	void addCartItems(Long cartId, List<Item> items);

	void addCartItemServices(Long cartId,Long itemId, List<WedoService> services);
	
	void deleteItemServiceByServiceId(Long cartId,Long itemId, Long serviceId);
	void deleteItem(Long cartId,Long itemId);
	
	Order checkoutCart(Long cartId,Order order);
	/** 
	 * Clear Items from Active Cart
	 * @param cartId
	 */
	void clearCart(Long cartId);
	/** 
	 * Makes Cart Passive
	 * @param cartId
	 */
	void deActivateCart(Long cartId);
	
	List<Item> listCartItemByCartId(Long cartId);

	Optional<Cart> getCartByUserId(Long userId);
	Optional<Cart> getCartById(Long id);
	
	List<Cart> listCarts(Integer offset, Integer limit);

	Optional<Cart> getCartDetail(Long cartId, Boolean summary);
	
	void updateItem(final Byte workshopId, final Long cartId, Long itemId, Item item);
}
