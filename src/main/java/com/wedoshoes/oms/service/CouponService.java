package com.wedoshoes.oms.service;

import java.util.Collection;
import java.util.Optional;

import com.wedoshoes.oms.model.Coupon;
import com.wedoshoes.oms.model.CouponSource;

public interface CouponService {
	
	Coupon save(Coupon coupon);
	void update(Coupon coupon);
	void delete(Integer couponId);
//	void removeAppliedCoupon(Long orderId, Long itemId, Integer discountid);

	Optional<Coupon> getCouponById(Integer couponId, Boolean summary);
	Optional<Coupon> validate(Coupon coupon);
	Optional<Coupon> apply(Coupon coupon);
	Optional<Coupon> getCouponByCode(String couponCode, Boolean summary);
	Collection<Coupon> list(Byte userTypeId, Integer categoryId,
			Integer serviceId, Integer productId, Integer offset, Integer limit);
	
	CouponSource save(CouponSource couponSource);
	void update(CouponSource couponSource);
	void deleteCouponSource(Integer sourceId);
	Optional<CouponSource> getCouponSourceById(Integer sourceId, Boolean summary);
	Collection<CouponSource> list();

}
