package com.wedoshoes.oms.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.wedoshoes.oms.model.state.StateTransition;

public interface LogisticServiceImpl {

	void addOrder(Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId, StateTransition transition);
	
	@PUT
	@Path("/{order_id}/state")
	@Consumes(MediaType.APPLICATION_JSON)
	Response updateOrder(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId, StateTransition transition);
	
	
	@POST
	@Path("/{order_id}/items/{item_id}/state")
	@Consumes(MediaType.APPLICATION_JSON)
	Response addItem(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,StateTransition transition);
	
	@POST
	@Path("/{order_id}/items/{item_id}/state")
	@Consumes(MediaType.APPLICATION_JSON)
	Response updateItem(@PathParam("user_id") Long userId,
			@PathParam("workshop_id") Byte workshopId,
			@PathParam("order_id") Long orderId,
			@PathParam("item_id") Long itemId,StateTransition transition);
	
	
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	Response getOrders(@QueryParam("date") Long date,
			@QueryParam("user_id") Long userId);
	
	@GET
	@Path("/{order_id}/items/{item_id}")
	@Consumes(MediaType.APPLICATION_JSON)
	Response getItemLogistics(@QueryParam("date") Long date);
	
	@GET
	@Path("/{order_id}")
	@Consumes(MediaType.APPLICATION_JSON)
	Response getOrderLogistics(@QueryParam("date") Long date);
}
