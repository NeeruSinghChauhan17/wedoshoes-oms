package com.wedoshoes.oms.service;

import java.io.File;

import com.wedoshoes.oms.http.impl.model.core.CommSettings;
import com.wedoshoes.oms.model.shipment.Shipment;
import com.wedoshoes.oms.model.state.StateTransition;

public interface NotificationService {

	/**
	 * Notify user when order state changes
	 */
	void notifyOrderStateChanged(Byte workshopId, Long orderId, Byte state, CommSettings commSett,
			Boolean saveSettings);
	
	/**
	 * Notify user when order-item state changes
	 */
	void notifyItemStateChanged(Byte workshopId, Long orderId, Long itemId, Byte state, File file,CommSettings commSett,
			Boolean saveSettings);
	
	/** 
	 * Notify POS user when order-item added in shipment
	 */
	void notifyShipmentOrderAdded(Byte workshopId, Long orderId, StateTransition transition);
	
	/** 
	 * Notify POS user when item added in shipment
	 */
	void notifyShipmentItemAdded(Byte workshopId, Long orderId, Long itemId, StateTransition transition);
	
	
	/** 
	 * Notify POS user when order state changed by admin / user/ cc
	 */
	void notifyShipmentOrderStateChanged(Byte workshopId, Long orderId, StateTransition transition);
	
	/** 
	 * Notify POS user when order-item state changed by admin / user/ cc
	 */
	void notifyShipmentItemStateChanged(Byte workshopId, Long orderId, Long itemId, StateTransition transition);
	
	/** 
	 * Notify POS users when shipment user change
	 */
	void notifyShipmentUserChanged(Byte workshopId, Shipment Oldship, Shipment newShip);

	void notifyOnInvoiceAndChallanCreation(Byte workshopId,Long userId, Long orderId, Byte state, File file);
	
}
