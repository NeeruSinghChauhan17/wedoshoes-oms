package com.wedoshoes.oms.service;

import java.awt.image.BufferedImage;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.wedoshoes.oms.http.impl.model.core.CommSettings;
import com.wedoshoes.oms.model.LastDeliveredItem;
import com.wedoshoes.oms.model.PaymentTransaction;
import com.wedoshoes.oms.model.order.Discount;
import com.wedoshoes.oms.model.order.Feedback;
import com.wedoshoes.oms.model.order.Image;
import com.wedoshoes.oms.model.order.ImageType;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.ItemDefect;
import com.wedoshoes.oms.model.order.Order;
import com.wedoshoes.oms.model.order.Payment;
import com.wedoshoes.oms.model.order.WedoService;
import com.wedoshoes.oms.model.payment.PaymentAdjustment;
import com.wedoshoes.oms.model.state.StateTransition;

/**
 * 
 * @author Navrattan Yadav
 *
 */
public interface OrderService {

	
	
	/**
	 * ******************************
	 * 	Order Related Methods
	 * ******************************
	 */
	
	Order createOrder(Order order, CommSettings commSett, Boolean saveSettings);
	Order createOrder(Byte workshopId,Order order,CommSettings commSett,Boolean saveSettings);
	void updateOrder(Byte workshopId, Order order);
	Optional<Order> getOrder(Byte workshopId, Long orderId, Boolean summary);
	Collection<Order> listByUser(Long userId, Integer offset, Integer limit);
	Optional<Order> getOrderCostingDetail(Byte workshopId, Long orderId);
	Collection<Order> search(Byte workshopId,Long userId, Long createrUserId, Long referredByUserId, 
			String customerName, Long orderId, Long itemId, String cc, Long phone, Byte state, 
			Long createDate, Long pickUpDate, Long deliveryDate, Long fromDate, Long toDate,
			Integer offset, Integer limit);
	Collection<Order> search(Byte workshopId, Long userId, String customerName,	String cc, Long phone, Byte state, Long fromDate, Long toDate,	String filterBy, Integer offset, Integer limit);

	
	/**
	 * ******************************
	 * 	Order Item Related Methods
	 * ******************************
	 */
	
	Item addItem(Byte workshopId, Long orderId, Item item);
	void updateItem(Byte workshopId, Long orderId,Long itemId, Item item);
	void updateOrderState(Byte workshopId, Long orderId, StateTransition transition);
	void updateItemState(Byte workshopId, Long orderId,Long itemId, StateTransition transition);
	Collection<StateTransition> listItemStateTransitions(Byte workshopId, Long itemId);
	Optional<Item> getItem(Byte workshopId, Long orderId, Long itemId, Boolean summary);
	List<StateTransition> listExternalStateTransition(Byte workshopId, Long itemId,Integer state);
	
	Collection<Item> listOrderItems(Byte workshopId, Long orderId, Byte state, Long deliveryDate, 
			Integer offset, Integer limit);
	Collection<Item> searchItems(Byte workshopId,Byte state,Long deliveryDate, Long date, Integer offset, Integer limit);
	void applyCouponOnItem(Long userId, Byte workshopId,Long orderId, Long itemId, Integer couponId);	
	void removeAppliedCoupon(Byte workshopId, Long orderId, Long itemId, Integer discountid);
	/**
	 * ************************************
	 * 	Order Item Service Related Methods
	 * ************************************
	 */

	void addItemServices(Byte workshopId, Long orderId,Long itemId,	List<WedoService> services);
	void updateItemService(Byte workshopId, Long orderId, Long itemId, Long serviceId, WedoService wedoService);
	void removeItemService(Byte workshopId, Long orderId,Long itemId, Long serviceId);
	void removeItemServices(Byte workshopId, Long orderId,Long itemId);
	Collection<WedoService> listItemServices(Byte workshopId, Long orderId,Long itemId, Boolean summary);
	Optional<WedoService> getItemService(Byte workshopId, Long orderId, Long itemId,Long serviceId, Boolean summary);
	
	
	/**
	 * ************************************
	 * 	Order Item Image Related Methods
	 * ************************************
	 */
	
	void addItemImages( Byte workshopId, Long orderId,Long itemId, List<Image> images);
	void updateItemImage( Byte workshopId, Long orderId,Long itemId, Long imageId, Image image);
	void removeItemImage(Byte workshopId, Long orderId,Long itemId,Long imageId);
	void removeItemImagesByItem(Byte workshopId, Long orderId, Long itemId);
	Optional<Image> getItemImage(Byte workshopId, Long orderId,Long itemId,Long imageId, Boolean summary);
	Collection<Image> listItemImages(Byte workshopId, Long orderId, Long itemId, Boolean summary);
	Collection<Image> listItemImages(Byte workshopId, Long orderId,Long itemId, ImageType imageType, Boolean summary);
	
	
	/**
	 * ************************************
	 * 	Order Payment Detail Related Methods
	 * ************************************
	 */
	
	Payment addAdvancedPayment(Byte workshopId, Long userId, Long orderId,Payment payment);
	Payment addItemPayment(Byte workshopId, Long userId, Long orderId,Long itemId, Payment payment);
	Collection<Payment> listOrderPayments(Byte workshopId, Long userId, Long orderId);
	void addItemPaymentAdjustment(Byte workshopId, Long userId, Long orderId, Long itemId,
			PaymentAdjustment paymentAdjustment);
	
	/**
	 * ************************************
	 * 	Item Discount Related Methods
	 * ************************************
	 */	
	Collection<Discount> listDiscountsOnItem(Byte workshopId, Long itemId);
	
	 /**
	 * ************************************
	 * 	Order Item BarCode Related Methods
	 * ************************************
	 */
	Optional<BufferedImage> generateBarcodeWithInfo(Byte workshopId, Long orderId, Long itemId);
	Optional<BufferedImage> generateBarcode(Byte workshopId, Long orderId, Long itemId);
	
	/**
	 * ************************************
	 * 	Order Item Defects Related Methods
	 * ************************************
	 */
	void addItemDefects(Byte workshopId, Long orderId, Long itemId, List<Byte> defects);
	Collection<ItemDefect> listItemDefects(Byte workshopId, Long orderId, Long itemId);
	
	/**
	 * ************************************
	 * 	Order Feedback Related Methods
	 * ************************************
	 */
	Feedback addFeedback(Long userId, Byte workshopId, Long orderId, Long itemId, Feedback feedback);

	Optional<Feedback> getFeedback(Long userId, Byte workshopId, Long orderId, Long itemId);
	
	Optional<LastDeliveredItem> getLastDeliveredItem(Long userId, Byte workshopId);


	void updateOrdersLocality(Byte workshopId, Long localityId, String pin);

	Integer getUserOrderCounts(Long userId);
	Collection<PaymentTransaction> listTransactions(Byte workshopId,Long orderId, Long itemId ,Long userId, Long startDate, Long endDate, Integer offset, Integer limit);
	Map<String, String> findPaymentModeById(Integer paymentModeId);
	List<Map<String, String>> listPaymentModes(Byte workshopId);
	void updateOrderItemState(Byte workshopId, List<Order> order);

	/**
	 * ************************************
	 * 	Orders Report Related Methods
	 * ************************************
	 */
	Collection<Order> getTaxReports(Byte workshopId, Byte state, Long fromDate, Long toDate);
	
}