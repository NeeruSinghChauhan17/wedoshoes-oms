package com.wedoshoes.oms.exception;

import javax.ws.rs.core.Response.Status;

public class ConflictException extends CustomException {

	private static final long serialVersionUID = 1L;
	
	private static final Integer STATUS_CONFLIT = Status.CONFLICT.getStatusCode();

	public ConflictException() {
		super(STATUS_CONFLIT, "Conflit.");
	}
	
	public ConflictException(final String message) {
		super(STATUS_CONFLIT, message);
	}
}
