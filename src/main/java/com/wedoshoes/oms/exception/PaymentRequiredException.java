package com.wedoshoes.oms.exception;

import javax.ws.rs.core.Response.Status;

public class PaymentRequiredException extends CustomException{

private static final long serialVersionUID = 1L;
	
	private static final Integer PAYMENT_REQUIRED = Status.PAYMENT_REQUIRED.getStatusCode();

	public PaymentRequiredException() {
		super(PAYMENT_REQUIRED, "Transaction Failure.");
	}
	
	public PaymentRequiredException(final String message) {
		super(PAYMENT_REQUIRED, message);
	}
}
