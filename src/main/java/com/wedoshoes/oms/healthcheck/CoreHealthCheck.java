package com.wedoshoes.oms.healthcheck;

import org.springframework.stereotype.Component;

import com.codahale.metrics.health.HealthCheck;
/**
 * 
 * @author Navrattan Yadav
 *
 */

@Component
public class CoreHealthCheck extends HealthCheck {

	@Override
	protected Result check() throws Exception {
		return Result.healthy();
	}

}
