package com.wedoshoes.oms.mail;

import java.util.HashMap;
import java.util.Map;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.google.common.collect.Lists;
import com.wedoshoes.oms.model.notification.Attachment;
import com.wedoshoes.oms.model.notification.Email;
import com.wedoshoes.oms.model.order.Order;

@SuppressWarnings("deprecation")
public class InvoiceMail extends Email{

	private static final String UTF_8_ENCODING = "utf-8";
	private static final String TEMPLATE="email-template/invoiceMailTemplate.vm";
	final static Byte challanState=99;
	final static Byte invoiceState=100;
	
	public InvoiceMail(final VelocityEngine velocityEngine, final String[] to, final String[] bcc, 
			final String[] cc, final String from, final Order order,final Byte state,final String subject,
			final Attachment attachment) { 
		
		super(getMailDataTemplate(velocityEngine,order,state), 
				subject, to, bcc, cc, from, Lists.newArrayList(attachment));
	}
	
	private static String getMailDataTemplate(final VelocityEngine velocityEngine,final Order order,final Byte state) {
		final Map<String, Object> model = new HashMap<String, Object>();
		if(state==challanState){
			model.put("fileName", "challan");
		}else {
			model.put("fileName", "invoice");
		}
		model.put("customerName", order.getCustomerName()+" "+order.getLastName());
		return  VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, TEMPLATE, UTF_8_ENCODING, model);

	}
}
