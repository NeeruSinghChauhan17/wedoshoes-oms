package com.wedoshoes.oms.mail;

import static com.wedoshoes.oms.dao.impl.DefaultUtils.defaultBigDecimalIfNull;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.defaultByteIfNull;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.defaultIntegerIfNull;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.defaultLongIfNull;
import static com.wedoshoes.oms.dao.impl.DefaultUtils.defaultStringIfNull;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.google.common.collect.Lists;
import com.wedoshoes.oms.http.CMSServerHttpRequester;
import com.wedoshoes.oms.model.notification.Attachment;
import com.wedoshoes.oms.model.notification.Email;
import com.wedoshoes.oms.model.order.Address;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.Order;
import com.wedoshoes.oms.model.order.WedoService;
import com.wedoshoes.oms.util.DateUtil;

@SuppressWarnings("deprecation")
public class OrderMail extends Email {

	private static final Logger LOGGER = LoggerFactory.getLogger(OrderMail.class);
	private static final String UTF_8_ENCODING = "utf-8";
	private static final String TEMPLATE="email-template/orderMailTemplate.vm";
	private static final Byte CANCEL_ITEM_STATE = 6;
	private static final Byte DELIVERED = 5;

	public OrderMail(final VelocityEngine velocityEngine, final String[] to, final String[] bcc, 
			final String[] cc, final String from, final Order order, final Byte state, final String subject,
			final CMSServerHttpRequester cmsServerRequester, final Attachment attachment) {
		
		super(getMailDataTemplate(velocityEngine, order, state, cmsServerRequester), 
				subject, to, bcc, cc, from, attachment!=null? Lists.newArrayList(attachment) : null);
	}
	
	private static String getMailDataTemplate(final VelocityEngine velocityEngine, 
			final Order order, final Byte orderState, final CMSServerHttpRequester cmsServerRequester) {

		final Map<String, Object> model = new HashMap<String, Object>();
		BigDecimal grandTotal = BigDecimal.ZERO;
		BigDecimal totalCostCredited = BigDecimal.ZERO;
		final List<Map<String, Object>> itemsData = new ArrayList<Map<String, Object>>();
		final Map<String, Object> orderData = new HashMap<String, Object>();
		orderData.put("pickupAddress", getPickupAddressData(order.getPickUpAddress()));
		orderData.put("deliveryAddress", getDeliveryAddressData(order.getDeliveryAddress()));
		orderData.put("orderDetail", getOrderData(order, orderState, cmsServerRequester));
		final List<Item> items = order.getItems();
		for (Item item : items) {
			if (item.getState() == CANCEL_ITEM_STATE) {
				itemsData.add(getItemData(item, cmsServerRequester));
				continue;
			}

			grandTotal = grandTotal.add(defaultBigDecimalIfNull(item.calculateTotalCost()));
			totalCostCredited = totalCostCredited.add(defaultBigDecimalIfNull(item.getCostCreadited()));
			itemsData.add(getItemData(item, cmsServerRequester));
		}
		model.put("grandTotal", defaultBigDecimalIfNull(grandTotal).setScale(2, RoundingMode.HALF_UP));		
		final BigDecimal sumOfAdvancePayment=defaultBigDecimalIfNull(totalCostCredited).add(defaultBigDecimalIfNull(order.getAdvancePayment())).setScale(2, RoundingMode.HALF_UP);
		model.put("advance", sumOfAdvancePayment);
		LOGGER.info("advance payment {}", sumOfAdvancePayment); 
		model.put("totalPayable",defaultBigDecimalIfNull(grandTotal.subtract(sumOfAdvancePayment))); //defaultBigDecimalIfNull(grandTotal.subtract(BigDecimal.ZERO).subtract(defaultBigDecimalIfNull(order.getAdvancePayment())))
		orderData.put("itemList", itemsData);
		model.put("orderData", orderData); 
		model.put("orderState", orderState);
		return  VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, TEMPLATE, UTF_8_ENCODING, model);
	}

	//===================  Methods to map Item amd Order data ===============
	
	private static Map<String, Object> getOrderData(final Order order, final Byte orderState,
			final CMSServerHttpRequester cmsServerRequester) {
		final Map<String, Object> orderData = new HashMap<String, Object>();
		orderData.put("name", order.getCustomerName()+" "+order.getLastName());
		orderData.put("phone", order.getPhone());
		orderData.put("orderNumber", order.getId());
		orderData.put("pickupDate", DateUtil.convertToDateString(order.getPickUpDate()));
		orderData.put("timeSlot", cmsServerRequester.getTimeSlotName(order.getPickUpTimeSlotId()));
		return orderData;
	}

	private static Map<String, Object> getItemData(final Item item,
			final CMSServerHttpRequester cmsServerRequester) {
		final List<String> productJobList = new ArrayList<String>();
		final Map<String, Object> itemData = new HashMap<String, Object>();
		itemData.put("itemId", item.getId());
		itemData.put("productId", cmsServerRequester.getProductName(item.getProductId()));
		itemData.put("serviceCharges", defaultBigDecimalIfNull(item.getServiceCost()));
		itemData.put("discount", defaultBigDecimalIfNull(item.getDiscount(item.getServiceCost())).setScale(2, RoundingMode.HALF_UP));
		itemData.put("total", defaultBigDecimalIfNull(item.getServiceCost())
				.subtract(defaultBigDecimalIfNull(item.getDiscount(item.getServiceCost()))).setScale(2, RoundingMode.HALF_UP));
		itemData.put("grossAmount", defaultBigDecimalIfNull(item.calculateTotalCost()).setScale(2, RoundingMode.HALF_UP)); 
		if(item.getState()==DELIVERED){
            itemData.put("deliveryDate", DateUtil.convertToDateString(defaultLongIfNull(item.getDeliveryDate())));
          }else{
            itemData.put("deliveryDate", DateUtil.convertToDateString(defaultLongIfNull(item.getEstDeliveryDate())));
          }
		itemData.put("size", defaultIntegerIfNull(item.getSize()));
		itemData.put("convenienceCharges", defaultBigDecimalIfNull(item.getConvenienceCharge()));
		itemData.put("expressDeliveryCharges", defaultBigDecimalIfNull(item.getExpressProcessingCharge()));
		itemData.put("itemStatus", defaultByteIfNull(item.getState()));
		itemData.put("transitionState", defaultByteIfNull(item.getStateTransitions().get(0).getState())); 
		itemData.put("description", defaultStringIfNull(item.getStateTransitions().get(0).getComment()));
		itemData.put("taxAmount", defaultBigDecimalIfNull(item.calculateTaxAmount().setScale(2, RoundingMode.HALF_UP)));  
		
		for (WedoService service : item.getServices()) {
			productJobList.add(cmsServerRequester.getServiceName(service.getServiceId()));
		}
		itemData.put("services", productJobList);
		return itemData;
	}

	private static Map<String, Object> getPickupAddressData(final Address pickupAddress) {
		final Map<String, Object> addressData = new HashMap<String, Object>();
		addressData.put("street", defaultStringIfNull(pickupAddress.getStreetAddress()));
		addressData.put("landmark", defaultStringIfNull(pickupAddress.getLandmark()));
		addressData.put("city", defaultStringIfNull(pickupAddress.getCity()));
		addressData.put("state", defaultStringIfNull(pickupAddress.getState()));
		addressData.put("pin", defaultStringIfNull(pickupAddress.getPin()));
		return addressData;
	}

	private static Map<String, Object> getDeliveryAddressData(final Address deliveryAddress) {
		final Map<String, Object> addressData = new HashMap<String, Object>();
		addressData.put("street", defaultStringIfNull(deliveryAddress.getStreetAddress()));
		addressData.put("landmark", defaultStringIfNull(deliveryAddress.getLandmark()));
		addressData.put("city", defaultStringIfNull(deliveryAddress.getCity()));
		addressData.put("state", defaultStringIfNull(deliveryAddress.getState()));
		addressData.put("pin", defaultStringIfNull(deliveryAddress.getPin()));
		return addressData;
	}

}
