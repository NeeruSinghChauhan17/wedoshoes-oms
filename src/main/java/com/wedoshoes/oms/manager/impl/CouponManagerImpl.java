package com.wedoshoes.oms.manager.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.wedoshoes.oms.dao.CouponDao;
import com.wedoshoes.oms.exception.ConflictException;
import com.wedoshoes.oms.exception.CustomException;
import com.wedoshoes.oms.manager.CouponManager;
import com.wedoshoes.oms.model.Coupon;
import com.wedoshoes.oms.model.CouponSource;
/**
 * 
 * @author Lawakush Chaudhary
 *
 */
@Component
public class CouponManagerImpl implements CouponManager {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(CouponManagerImpl.class);

	private final CouponDao couponDao;
	public final Byte DEFAULT_WORKSHOP_ID =1;

	@Autowired
	public CouponManagerImpl(final CouponDao couponDao) {
		this.couponDao = couponDao;
	}

	@Transactional
	@Override
	public Integer save(final Coupon coupon) {
		try {
			final Integer createdCouponId = couponDao.save(coupon);
			addCategories(coupon.getCategories(), createdCouponId);
			addServices(coupon.getServices(), createdCouponId);
			addProducts(coupon.getProducts(), createdCouponId);
			addUserTypes(coupon.getUserTypes(), createdCouponId);

			return createdCouponId;
		} catch(DuplicateKeyException e){
			LOGGER.error("error while save coupon {} ", coupon.getCode(), e);
			throw new ConflictException("Coupon with code already exist.");
		}catch (DataIntegrityViolationException e) {
			LOGGER.error("error while save coupon {}", coupon.getCode(), e);
			throw new CustomException(Status.BAD_REQUEST.getStatusCode(),"Invalid Coupon Data.");
		}
	}

	@Override
	public void update(final Coupon coupon) {
		try {
			couponDao.update(coupon);
		} catch (DuplicateKeyException e) {
			LOGGER.error("error while update coupon", e);
			throw new ConflictException("Coupon with code already exist.");
		}
	}

	@Override
	public Optional<Coupon> findById(final Integer id) {
		return couponDao.findById(id);
	}

	@Override
	public Optional<Coupon> findDetailById(final Integer id) {
		return couponDao.findDetailById(id);
	}

	@Override
	public void delete(final Integer id) {
		couponDao.delete(id);
	}

	// ======= add coupons limitations information
	// ===================

	private void addCategories(final Set<Integer> categories,
			final Integer couponId) {
		if (categories != null) {
			categories.stream().forEach(
					id -> couponDao.applyOnCategory(id, couponId));
		}

	}

	private void addServices(final Set<Integer> services, final Integer couponId) {
		if (services != null) {
			services.stream().forEach(
					id -> couponDao.applyOnService(id, couponId));
		}

	}

	private void addProducts(final Set<Integer> productIds,
			final Integer couponId) {
		if (productIds != null) {
			productIds.stream().forEach(
					id -> couponDao.applyOnProduct(id, couponId));
		}

	}

	private void addUserTypes(final Set<Integer> userTypeIds,
			final Integer couponId) {
		if (userTypeIds != null) {
			userTypeIds.stream().forEach(
					id -> couponDao.applyOnUserType(id, couponId));
		}
	}

	@Override
	public Optional<Coupon> validate(final Coupon coupon) {
		Optional<Coupon> couponResp = couponDao.getCouponDetailByCode(coupon.getCode());
		if (couponResp.isPresent()) {
			if((couponResp.get().getMaxUse() - couponResp.get().getUsedCount()) < coupon.getMaxUse()){
				return Optional.ofNullable(new Coupon(null, null, "Coupon "+ coupon.getCode()+ " is applicable for max "+(couponResp.get().getMaxUse() - couponResp.get().getUsedCount())+" Items Only.", null,
						null, null, null, couponResp.get().getMaxUse() - couponResp.get().getUsedCount(), null, null, null, null,
						null, null, null, null));				
			}
			if (couponResp.get().getUserTypes()!=null) {
				Set<Integer> userTypes = new HashSet<Integer>();
				if (coupon.getUserTypes()==null) {
					userTypes.add(1);
				} else {
					userTypes = coupon.getUserTypes();
				}
				Set<Integer> unMatchedUserType = validateUsersType(userTypes,
						couponResp.get().getUserTypes());
				if (!unMatchedUserType.isEmpty()) {
					return Optional.ofNullable(new Coupon(null, null, "Coupon "+ coupon.getCode()+ " is not valid for following user types.", null,
							null, null, null, null, null, null, null, null,
							null, unMatchedUserType, null, null));
				}
			}
			
			
			if (couponResp.get().getProducts()!=null) {
				Set<Integer> unMatchedProducts = validateProducts(
						coupon.getProducts(), couponResp.get().getProducts());
				if (!unMatchedProducts.isEmpty()) {
					return Optional.ofNullable(new Coupon(null, null, "Coupon "+ coupon.getCode()+ " is not valid for following Products.", null,
							null, null, null, null, null, null, null,
							unMatchedProducts, null, null, null, null));
				}
			}
			
			
			if (couponResp.get().getServices()!=null) {
				Set<Integer> unMatchedServices = validateServices(
						coupon.getServices(), couponResp.get().getServices());
				if (!unMatchedServices.isEmpty()) {
					return Optional.ofNullable(new Coupon(null, null, "Coupon "+ coupon.getCode()+ " is not valid for following Services.", null,
							null, null, null, null, null, null, null, null,
							unMatchedServices, null, null, null));
				}
			}
			
			if (couponResp.get().getCategories()!=null) {
				Set<Integer> unMatchedcategories = validateCategories(
						coupon.getCategories(), couponResp.get()
								.getCategories());
				if (!unMatchedcategories.isEmpty()) {
					return Optional.ofNullable(new Coupon(null, null, "Coupon "+ coupon.getCode()+ " is not valid for following Categories.", null,
							null, null, null, null, null, null, null, null,
							null, null, unMatchedcategories, null));
				}
			}
			return Optional
					.ofNullable(new Coupon(couponResp.get().getId(), couponResp.get().getCode(), null,couponResp.get().getValue(), couponResp.get()
									.getValueType(), null, null, null, null,
							null, null, null, null, null, null, null));
		}
		return Optional.empty();
	}

	
	@Override
	public Optional<Coupon> apply(final Coupon coupon) {
		Optional<Coupon> couponResp = validate(coupon);
		if (couponResp.isPresent()) {
			couponDao.apply(coupon);
		}
		return couponResp;
	}

	@Override
	public Optional<Coupon> getCouponByCode(final String code) {
		return couponDao.getCouponByCode(code);
	}

	@Override
	public Optional<Coupon> getCouponDetailByCode(final String code) {
		return couponDao.getCouponDetailByCode(code);
	}

	@Override
	public Collection<Coupon> listByUserType(final Byte userTypeId,
			final Integer offSet, final Integer limit) {
		return couponDao.listByUserType(userTypeId, offSet, limit);
	}

	@Override
	public Collection<Coupon> listByProduct(final Integer productId,
			final Integer offSet, final Integer limit) {
		return couponDao.listByProduct(productId, offSet, limit);
	}

	@Override
	public Collection<Coupon> listByCategory(final Integer categoryId,
			final Integer offSet, final Integer limit) {
		return couponDao.listByCategory(categoryId, offSet, limit);
	}

	@Override
	public Collection<Coupon> listDefaultCoupon(final Integer offSet,
			final Integer limit) {
		return couponDao.listDefaultCoupon(offSet, limit);
	}

	// ========== validations of coupons ============

	private Set<Integer> validateUsersType(Set<Integer> requestedSet,
			Set<Integer> existingSet) {
		return requestedSet.stream()
				.filter(categoryId -> !existingSet.contains(categoryId))
				.collect(Collectors.toSet());
	}

	private Set<Integer> validateCategories(Set<Integer> requestedSet,
			Set<Integer> existingSet) {
		return requestedSet.stream()
				.filter(categoryId -> !existingSet.contains(categoryId))
				.collect(Collectors.toSet());
	}

	private Set<Integer> validateProducts(Set<Integer> requestedSet,
			Set<Integer> existingSet) {
		Set<Integer> result = requestedSet.stream()
				.filter(categoryId -> !existingSet.contains(categoryId))
				.collect(Collectors.toSet());
		return result;
	}

	private Set<Integer> validateServices(Set<Integer> requestedSet,
			Set<Integer> existingSet) {
		return requestedSet.stream()
				.filter(categoryId -> !existingSet.contains(categoryId))
				.collect(Collectors.toSet());
	}

	// Coupon Source
	@Override
	public Integer save(final CouponSource couponSource) {
		try {
			return couponDao.save(couponSource);
		} catch (DataIntegrityViolationException e) {
			LOGGER.error("error while save coupon source", e);
			throw new ConflictException("Coupon Source already exist.");
		}
	}

	@Override
	public void update(final CouponSource couponSource) {
		try {
			couponDao.update(couponSource);
		} catch (DuplicateKeyException e) {
			LOGGER.error("error while update coupon source", e);
			throw new ConflictException("Coupon Source already exist.");
		}
	}

	@Override
	public void deleteSource(final Integer sourceId) {
		couponDao.deleteSource(sourceId);
	}

	@Override
	public Optional<CouponSource> findSourceById(final Integer sourceId) {
		return couponDao.findSourceById(sourceId);
	}

	@Override
	public Optional<CouponSource> findSourceDetailById(final Integer sourceId) {
		return couponDao.findSourceDetailById(sourceId);
	}
	
	@Override
	public Collection<CouponSource> list(){
		return couponDao.list();
	}

	
}
