package com.wedoshoes.oms.manager.impl;

import static com.wedoshoes.oms.model.order.ItemState.NEW;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.google.common.collect.Lists;
import com.wedoshoes.oms.dao.OrderDao;
import com.wedoshoes.oms.dao.ShipmentDao;
import com.wedoshoes.oms.exception.PaymentRequiredException;
import com.wedoshoes.oms.http.CoreServerHttpRequester;
import com.wedoshoes.oms.http.PWServerHttpRequester;
import com.wedoshoes.oms.invoice.Invoice;
import com.wedoshoes.oms.manager.CouponManager;
import com.wedoshoes.oms.manager.OrderManager;
import com.wedoshoes.oms.model.Coupon;
import com.wedoshoes.oms.model.LastDeliveredItem;
import com.wedoshoes.oms.model.PaymentTransaction;
import com.wedoshoes.oms.model.TransactionStatus;
import com.wedoshoes.oms.model.order.Discount;
import com.wedoshoes.oms.model.order.Feedback;
import com.wedoshoes.oms.model.order.FeedbackType;
import com.wedoshoes.oms.model.order.Image;
import com.wedoshoes.oms.model.order.ImageType;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.ItemState;
import com.wedoshoes.oms.model.order.Order;
import com.wedoshoes.oms.model.order.Payment;
import com.wedoshoes.oms.model.order.PaymentMode;
import com.wedoshoes.oms.model.order.TransactionType;
import com.wedoshoes.oms.model.order.WedoService;
import com.wedoshoes.oms.model.payment.PaymentAdjustment;
import com.wedoshoes.oms.model.payment.PaymentAdjustmentType;
import com.wedoshoes.oms.model.payment.PaymentState;
import com.wedoshoes.oms.model.payment.PaymentStatus;
import com.wedoshoes.oms.model.payment.PaytmPaymentStatus;
import com.wedoshoes.oms.model.payment.PayuPaymentStatus;
import com.wedoshoes.oms.model.shipment.Shipment;
import com.wedoshoes.oms.model.shipment.ShipmentItem;
import com.wedoshoes.oms.model.shipment.ShipmentOrder;
import com.wedoshoes.oms.model.shipment.ShipmentPoint;
import com.wedoshoes.oms.model.shipment.ShipmentSummary;
import com.wedoshoes.oms.model.state.StateTransition;
import com.wedoshoes.oms.service.impl.ShipmentRouteGenerator;

@Component
public class OrderManagerImpl implements OrderManager {

	private static final Logger LOGGER = LoggerFactory.getLogger(OrderManagerImpl.class);

	private final OrderDao orderDao;
	private final ShipmentDao shipmentDao;
	private final CouponManager couponManager;
	private final PWServerHttpRequester pwServerHttpRequester;
	private final CoreServerHttpRequester coreServerHttpRequester;

	/*** prod wedos admin id is 342l where stage admin id is 353 */
	private static final Long WEDO_ADMIN_ID=342l; 
	
	private static final Byte ADMIN_TYPE=2;
	private static final Byte CC_TEAM_TYPE=3;
	private static final Byte POS_TYPE=4;
	
	/**
	 * List of state on which need to update shipments item state.
	 */
	private final Set<Byte> stateToUpdateShipments;

	@Autowired
	public OrderManagerImpl(final OrderDao orderDao, final ShipmentDao shipmentDao, final CouponManager couponManager,
			final PWServerHttpRequester pwServerHttpRequester, 
			@Value("#{statesToUpdateShipments}") final Set<Byte> stateToUpdateShipments,
			final CoreServerHttpRequester coreServerHttpRequester) {
		this.orderDao = orderDao;
		this.shipmentDao = shipmentDao;
		this.couponManager = couponManager;
		this.pwServerHttpRequester = pwServerHttpRequester;
		this.stateToUpdateShipments=stateToUpdateShipments;
		this.coreServerHttpRequester=coreServerHttpRequester;
	}

	/**
	 * *****************************************************************************************************
	 * Order Related Methods
	 * *****************************************************************************************************
	 */

	@Override
	@Transactional
	public Long createOrder(final Byte workshopId, final Order order) {
		LOGGER.info("******************************* Processing Order *****************************");
		final Long orderId = orderDao.save(workshopId, order);
		LOGGER.info(" Order Id = {} Order Detail : = {}",orderId, order.toString());
		
		final List<Item> items = order.getItems().stream().map(item ->addItem(workshopId, orderId, item))
		.collect(Collectors.toList());
		
		// Save Item StateTransition and Comment (Comment only added if exist)
		items.stream()
				.forEach(item -> orderDao.saveItemStateTransition(workshopId, item.getId(),
						StateTransition.createWithUserIdAndState(order.getCreaterUserId(), NEW,
								(item.getStateTransitions() != null && item.getStateTransitions().size() > 0)
										? item.getStateTransitions().get(0).getComment() : null)));

		// if order has payment then process payment
		LOGGER.info("******** Processing Order Payment. *******");
		final List<Payment> payments = order.getPayments();
		if (payments != null && !payments.isEmpty()) {
			final BigDecimal totalPaid = BigDecimal
					.valueOf(payments.stream().mapToDouble(payment -> payment.getAmount().doubleValue()).sum());
			/**
			 * 
			 * if order complete payment is credited then save item with cost
			 * credited info. else if it has partial payment then add payment in
			 * order advance payment.
			 */
			System.out.println(order.calculateTotalCost());
			if (totalPaid.compareTo(order.calculateTotalCost()) == 0) {
				LOGGER.info("Payment State : = Complete Advanced Payment = {}. Adding payment in all item. ",
						totalPaid.doubleValue());
				// Add All item payments
				items.stream()
						.forEach(item -> orderDao.addItemPayment(workshopId, item.getId(), item.calculateTotalCost()));
				// save all transaction detail.
				payments.stream().forEach(payment -> {
					String paymentStatus = TransactionStatus.SUCCESS.toString();
					if (payment.getMode().equals(PaymentMode.PAYTM) || payment.getMode().equals(PaymentMode.PAYU)) {
					//	paymentStatus = checkPaymentStatus(payment, order.getUserId());
					}
					payment=Payment.createWithStatus(payment, paymentStatus);
					// if paymentMode is WEDO_CREDIT OR COD add txn id 
					if(payment.getMode().equals(PaymentMode.WEDO_CREDIT) || payment.getMode().equals(PaymentMode.COD)){
						payment=Payment.createWithTransactionId(payment, getTxnId());
					}
					orderDao.addPaymentTransaction(workshopId, orderId, null,
							Payment.createWithTransactionTypeAndUserId(TransactionType.CREDIT, order.getCreaterUserId(),
									payment));
				});

			} else {
				LOGGER.info("Payment State : = Parital Advanced Payment = {}. Add payment in Order Advance payment",
						totalPaid.doubleValue());
				payments.stream().forEach(payment -> { 
					String paymentStatus = TransactionStatus.SUCCESS.toString();
					if (payment.getMode().equals(PaymentMode.PAYTM) || payment.getMode().equals(PaymentMode.PAYU)) {
						//paymentStatus = checkPaymentStatus(payment, order.getUserId());
					}
					payment=Payment.createWithStatus(payment, paymentStatus);
					addAdvancedPayment(workshopId, order.getCreaterUserId(), orderId, payment);
				});
			}
		} else {
			LOGGER.info("Payment State : = No Advanced Payment. ");
		}
		return orderId;
	}

	@Override
	public void updateOrder(final Byte workshopId, final Order order) {
		orderDao.update(workshopId, order);
	}

	@Override
	public Optional<Order> getOrder(final Byte workshopId, final Long orderId) {
		return orderDao.findById(workshopId, orderId);
	}

	@Override
	public Optional<Order> getOrderDetail(final Byte workshopId, final Long orderId) {
		return orderDao.findDetailById(workshopId, orderId);
	}

	@Override
	public Collection<PaymentTransaction> listTransactions(final Byte workshopId, final Long orderId, final Long itemId,final Long userId,
			final Long startDate, final Long endDate, final Integer offset, final Integer limit) {
		if (orderId != null) {
			if (startDate != null && endDate != null) {
				return orderDao.findPaymentTransactionsByOrderIdAndDate(workshopId, orderId, startDate, endDate, offset,
						limit);
			}
			return orderDao.findPaymentTransactionsByOrderId(workshopId, orderId, offset, limit);
		} else if (itemId != null) {
			return orderDao.findPaymentTransactionsByItemId(workshopId, itemId, offset, limit);
		}else if (userId !=null){
			return findPaymentTransactionsByUserId(workshopId, userId, startDate, endDate, offset,limit);
		}
		return orderDao.findPaymentTransactionsByStartAndEndDate(workshopId, startDate, endDate, offset, limit);
	}

	private Collection<PaymentTransaction> findPaymentTransactionsByUserId(final Byte workshopId, final Long userId, final Long startDate, final Long endDate,
			final Integer offset,final  Integer limit) {
		if (startDate != null && endDate != null) {
			return orderDao.findPaymentTransactionsByUserIdAndDate(workshopId, userId, startDate, endDate, offset,
					limit);
		}
		return orderDao.findPaymentTransactionsByUserId(workshopId, userId, offset, limit);
	}

	/**
	 * *****************************************************************************************************
	 * Order Item Related Methods
	 * *****************************************************************************************************
	 */

	@Transactional
	@Override
	public Item addItem(final Byte workshopId, final Long orderId, final Item item) {
		Long itemId=null;
			//Save Items 
			  itemId = orderDao.saveItem(workshopId, orderId, Item.createWithState(NEW, item)); 			  

			  // Save Item Services
		if(item.getServices() != null) {
			orderDao.saveItemServices(workshopId, itemId, item.getServices());
		}

		// Save Item Discounts if exist
		if (item.getDiscounts() != null) {
			orderDao.saveItemDiscount(workshopId, itemId, item.getDiscounts());

		}

		// Save Item Images if exist
		if (item.getImages() != null) {
			orderDao.saveItemImages(workshopId, itemId, item.getImages());
		}

		return Item.createWithItemId(itemId, item);
	}

	@Override
	public void updateItem(final Byte workshopId, final Item item) {
		orderDao.updateItem(workshopId, item);
	}

	/*
	 * @Override public void cancelOrder(final Byte workshopId, final Long
	 * orderId) { orderDao.updateItemsStateByOrder(workshopId, orderId,
	 * CANCELED);
	 * 
	 * }
	 * 
	 * @Override public void cancelOrderItem(final Byte workshopId, final Long
	 * orderId, final Long itemId) { orderDao.updateItemState(workshopId,
	 * itemId, CANCELED); }
	 */

	@Override
	public Optional<Item> getItem(final Byte workshopId, final Long itemId) {
		return orderDao.findItemById(workshopId, itemId);
	}

	/**
	 * *****************************************************************************************************
	 * Order Item Services Related Methods
	 * *****************************************************************************************************
	 */

	@Override
	public void addItemServices(final Byte workshopId, final Long itemId, final List<WedoService> services) {
		orderDao.saveItemServices(workshopId, itemId, services);
	}

	@Override
	public void updateItemService(final Byte workshopId, final Long itemId, final Long serviceId,
			final WedoService wedoService) {
		orderDao.updateItemService(workshopId, itemId, serviceId, wedoService);
	}

	@Override
	public void removeItemService(final Byte workshopId, final Long itemId, final Long serviceId) {
		orderDao.deleteItemService(workshopId, itemId, serviceId);
	}

	@Override
	public void removeItemServices(final Byte workshopId, final Long itemId) {
		orderDao.deleteItemServicesByItem(workshopId, itemId);
	}

	@Override
	public Collection<WedoService> listItemServices(final Byte workshopId, final Long itemId) {
		return orderDao.findItemServicesByItem(workshopId, itemId);
	}

	@Override
	public Optional<WedoService> getItemService(final Byte workshopId, final Long itemId, final Long serviceId) {
		return orderDao.findItemService(workshopId, itemId, serviceId);
	}

	/**
	 * *****************************************************************************************************
	 * Order Item Images Related Methods
	 * *****************************************************************************************************
	 */

	@Override
	public void addItemImages(final Byte workshopId, final Long itemId, final List<Image> images) {
		orderDao.saveItemImages(workshopId, itemId, images);
	}

	@Override
	public void updateItemImage(final Byte workshopId, final Long itemId, final Image image) {
		orderDao.updateItemImage(workshopId, itemId, image);
	}

	@Override
	public void removeItemImage(final Byte workshopId, final Long imageId) {
		orderDao.deleteItemImage(workshopId, imageId);
	}

	@Override
	public void removeItemImagesByItem(final Byte workshopId, final Long itemId) {
		orderDao.deleteItemImagesByItem(workshopId, itemId);
	}

	@Override
	public Optional<Image> getItemImage(final Byte workshopId, final Long imageId) {
		return orderDao.findItemImage(workshopId, imageId);
	}

	@Override
	public Collection<Image> listItemImages(final Byte workshopId, final Long itemId) {
		return orderDao.findItemImagesByItem(workshopId, itemId);
	}

	@Override
	public Collection<Image> listItemImages(final Byte workshopId, final Long itemId, final ImageType imageType) {
		return orderDao.findItemImagesByItem(workshopId, itemId, imageType);
	}

	@Override
	public void addDiscountOnOrderItem(final Byte workshopId, final Long orderId, final Long itemId,
			final List<Discount> discounts) {
		orderDao.saveItemDiscount(workshopId, itemId, discounts);
	}

	@Override
	public Collection<Order> listByUser(final Long userId, final Integer offset, final Integer limit) {
		// TODO need to get all workshop id and pass in this
		return orderDao.findByUserId((byte) 1, userId, offset, limit);
	}

	@Override
	public Collection<Discount> listDiscountsOnItem(final Byte workshopId, final Long itemId) {
		return orderDao.findItemDiscountByItem(workshopId, itemId);
	}

	@Override
	@Transactional
	public Long addAdvancedPayment(final Byte workshopId, Long userId, final Long orderId,
			final Payment payment) {
		LOGGER.info("Adding Advnace payment for OrderId = {} with payment detail = {}", orderId, payment.toString());
		// Save order advance payment
		orderDao.addAdvancedPayment(workshopId, orderId, payment.getAmount());
		
		final Byte userType=coreServerHttpRequester.getUserTypeById(userId);
		if (userType!=ADMIN_TYPE && userType!=POS_TYPE && userType!=CC_TEAM_TYPE) {
			userId=WEDO_ADMIN_ID;
		}
		// if payment mode is WEDO_CREDIT or COD ,create and stores txn id with transaction details
		if(payment.getMode().equals(PaymentMode.WEDO_CREDIT) ||payment.getMode().equals(PaymentMode.COD) ){
			return orderDao.addPaymentTransaction(workshopId, orderId, null,
					Payment.createWithTransactionTypeAndUserId(TransactionType.CREDIT, userId, Payment.createWithTransactionId(payment, getTxnId())));
		}
		return orderDao.addPaymentTransaction(workshopId, orderId, null,
				Payment.createWithTransactionTypeAndUserId(TransactionType.CREDIT, userId, payment));
	}

	//create transaction id for wedo_credit and COD payment case
	private String getTxnId() {
		     Random rand = new Random();
			String rndm = Integer.toString(rand.nextInt())+(System.currentTimeMillis() / 1000L);
			return hashCal("SHA-256",rndm).substring(0,20);
	}
	private static String hashCal(String type,String str){
		byte[] hashseq=str.getBytes();
		StringBuffer hexString = new StringBuffer();
		try{
		MessageDigest algorithm = MessageDigest.getInstance(type);
		algorithm.reset();
		algorithm.update(hashseq);
		byte messageDigest[] = algorithm.digest();
            
		
		for (int i=0;i<messageDigest.length;i++) {
			String hex=Integer.toHexString(0xFF & messageDigest[i]);
			if(hex.length()==1) hexString.append("0");
			hexString.append(hex);
		}
		}catch(NoSuchAlgorithmException nsae){ }
		
		return hexString.toString();
	}
	

	@Override
	@Transactional
	public Long addItemPayment(final Byte workshopId, Long userId, final Long orderId, final Long itemId,
			final Payment payment) {
		LOGGER.info("Adding Item payment for OrderId = {}, itemId = {} with payment detail = {}", orderId, itemId,
				payment.toString());
		// save item payment
		orderDao.addItemPayment(workshopId, itemId, payment.getAmount());
		if(payment.getAdjustment()!=null){
			if(!payment.getAdjustment().getAdjustmentType().equals(PaymentAdjustmentType.LEAVE_AS_BALANCE)){
				orderDao.addItemPaymentAdjustment(workshopId,itemId,payment.getAdjustment().getAmount(),payment.getAdjustment().getAdjustmentType().getCode());
		    }else{
			orderDao.addItemPaymentAdjustment(workshopId,itemId,BigDecimal.ZERO,payment.getAdjustment().getAdjustmentType().getCode());
		 } 
	   }
		
		final Byte userType=coreServerHttpRequester.getUserTypeById(userId);
		if (userType!=ADMIN_TYPE && userType!=POS_TYPE && userType!=CC_TEAM_TYPE) {
			userId=WEDO_ADMIN_ID;
		}
		// if payment mode is WEDO_CREDIT or COD ,create and stores txn id with transaction details
		if(payment.getMode().equals(PaymentMode.WEDO_CREDIT) ||payment.getMode().equals(PaymentMode.COD) ){
			return orderDao.addPaymentTransaction(workshopId, orderId, itemId,
					Payment.createWithTransactionTypeAndUserId(TransactionType.CREDIT, userId,Payment.createWithTransactionId(payment, getTxnId())));
		}
		return orderDao.addPaymentTransaction(workshopId, orderId, itemId,
				Payment.createWithTransactionTypeAndUserId(TransactionType.CREDIT, userId, payment));
	}
	
	@Override
	public void addItemPaymentAdjustment(final Byte workshopId, final Long userId,final  Long orderId,final  Long itemId,
			final PaymentAdjustment paymentAdjustment) {
			if(!paymentAdjustment.getAdjustmentType().equals(PaymentAdjustmentType.LEAVE_AS_BALANCE)){
				orderDao.addItemPaymentAdjustment(workshopId,itemId,paymentAdjustment.getAmount(),paymentAdjustment.getAdjustmentType().getCode());
		    }else{
			orderDao.addItemPaymentAdjustment(workshopId,itemId,BigDecimal.ZERO,paymentAdjustment.getAdjustmentType().getCode());
	   }
	}

	@Override
	public void changeOrderItemsState(final Byte workshopId, final Long orderId, final StateTransition transition) {
		orderDao.findItemsByOrderId(workshopId, orderId).stream()
				.forEach(item -> changeItemState(workshopId, orderId, item.getId(), transition));
	}

	@Override
	@Transactional
	public void changeItemState(final Byte workshopId, final Long orderId, final Long itemId,
			final StateTransition transition) {
		orderDao.updateItemState(workshopId, itemId, transition.getState());
		Long stateTransitionId = orderDao.saveItemStateTransition(workshopId, itemId, transition);
		/**
		 * add users location who is changing the items state.
		 */
		if (transition.getUsersLocation() != null) {
			orderDao.saveUsersLocation(stateTransitionId, transition.getUserId(), transition.getUsersLocation());
		}
		/**
		 * Update ItemState in Shipments if exist and if current state is related with POS.
		 */

		if(stateToUpdateShipments.contains(transition.getState())){			
			updateShipmentItemState(transition.getState(), itemId);
		}
	}

	/**
	 * *****************************************************************************************************
	 * Shipment Related Methods
	 * *****************************************************************************************************
	 */

	@Override
	@Transactional
	public void updateShipment(final Byte workshopId, final Long shipmentId, final StateTransition transition) {

		
		/**
		 * Update State of existing Schedule shipment.
		 */
		shipmentDao.update(shipmentId, Shipment.createWithState(transition.getState(), transition.getShipment()));
		
		
		changeItemState(workshopId, transition.getShipment().getOrderId(), transition.getShipment().getItemId(),
				transition);
		
		/**
		 * 
		 * Add Customers signature in picked and delivered state.
		 *
		 */
		if (transition.getState() == ItemState.PICKED || transition.getState() == ItemState.DELIVERED) {
			shipmentDao.saveCustomersSignature(workshopId, shipmentId, transition);
		}

	}

	@Override
	public void updateShipmentItemState(final Byte state, final Long itemId) {
		Optional<Shipment> shipment=shipmentDao.findShipmentByItemId(itemId);
		if(shipment.isPresent()){
			shipmentDao.updateShipmentItemState(state, itemId,shipment.get().getId());
		}
	}

	@Override
	public void updateShipmentOrderState(final Byte state, final Long orderId) {
		shipmentDao.updateShipmentOrderItemState(state, orderId);
	}

	@Override
	public Optional<Shipment> getShipment(final Byte workshopId, final Long id) {
		return shipmentDao.find(id);
	}

	@Override
	public Collection<ShipmentOrder> getShipmentOrderByDateAndUser(final Byte workshopId, final Long date,
			final Long userId) {
		return shipmentDao.findOrderByDateAndUser(date, userId);
	}

	@Override
	public Optional<Shipment> getShipmentByDate(final Byte workshopId, final Long date, final Long orderId,
			final Long itemId) {
		return shipmentDao.findShipmentByDate(date, orderId, itemId);
	}

	@Override
	public Optional<Shipment> getShipmentByItemId(final Byte workshopId, final Long orderId, final Long itemId) {
		return shipmentDao.findShipmentByItemId(itemId);
	}

	@Override
	public Collection<Shipment> listShipmentByOrderId(final Byte workshopId, final Long orderId) {
		return shipmentDao.findShipmentByOrderId(orderId);
	}

	@Override
	public Optional<ShipmentSummary> getShipmentSummary(final Long userId, final Long shipmentDate) {

		Optional<ShipmentSummary> op = shipmentDao.findShipmentSummary(userId, shipmentDate);
		if (op.isPresent()) {
			final ShipmentSummary summry = op.get();
			summry.addCollectedAmount(orderDao.getTotalTxnAmountOfDayByUserId(userId, shipmentDate));		
			return Optional.ofNullable(summry);
		}
		return op;
	}

	@Override
	public Optional<StateTransition> getStateTransition(final Byte workshopId, final Long itemId,
			final Long transitionId) {
		return orderDao.findItemStateTransition(workshopId, itemId, transitionId);
	}

	@Override
	public Collection<StateTransition> listStateTransition(final Byte workshopId, final Long itemId) {
		return orderDao.findItemStateTransitions(workshopId, itemId);
	}

	@Override
	public List<StateTransition> listExternalStateTransition(final Byte workshopId, final Long itemId) {
		return orderDao.findItemExternalStateTransitions(workshopId, itemId);
	}

	@Override
	public Collection<Order> search(final Byte workshopId, final Long userId, final Long createrUserId,
			final Long referredByUserId, final String customerName, final Long orderId, final Long itemId,
			final String cc, final Long phone, final Byte state, final Long createDate, final Long pickUpDate,
			final Long deliveryDate, final Long fromDate, final Long toDate, final Integer offset,
			final Integer limit) {

		if (orderId != null) {
			final Optional<Order> optional = orderDao.findById(workshopId, orderId);
			if (optional.isPresent()) {
				return Lists.newArrayList(optional.get());
			}
			return Lists.newArrayList();
		} else if (itemId != null) {
			final Optional<Order> optional = orderDao.findByItemId(workshopId, itemId);
			if (optional.isPresent()) {
				return Lists.newArrayList(optional.get());
			}
			return Lists.newArrayList();
		} else if (phone != null) {
			return findByPhone(workshopId, cc, phone, state, createDate, pickUpDate, deliveryDate,offset, limit);
		} else if (userId != null) {
			return findByUserId(workshopId, userId, state, createDate, pickUpDate, deliveryDate, offset, limit);
		} else if (referredByUserId != null) {
			return findByReferredByUserId(workshopId, referredByUserId, state, createDate, pickUpDate, deliveryDate,
					offset, limit);
		} else if (createrUserId != null) {
			return findByCraterUserId(workshopId, referredByUserId, state, createDate, pickUpDate, deliveryDate, offset,
					limit);
		} else if (!StringUtils.isEmpty(customerName)) {
			return findByCustomerName(workshopId, customerName, state, createDate, pickUpDate, deliveryDate, offset,
					limit);
		} else if (createDate != null) {
			return orderDao.findByCreateDate(workshopId, createDate, offset, limit);
		} else if (pickUpDate != null) {
			return findByPickUpDate(workshopId, pickUpDate, state, offset, limit);
		} else if (deliveryDate != null) {
			return findByDeliveryDate(workshopId, deliveryDate, state, offset, limit);
		} else if (state != null) {
			return findByItemState(workshopId, fromDate, toDate, state, offset, limit);
		}
		return orderDao.findBy(workshopId, offset, limit);
	}

	@Override
	public Collection<Shipment> getShipmentOrderAssignee(final Byte workshopId, final Long orderId, final Long itemId) {
		if (orderId != null) {
			return shipmentDao.findShipmentByOrderId(orderId);
		} else if (itemId != null) {
			final Optional<Shipment> optional = shipmentDao.findShipmentByItemId(itemId);
			if (optional.isPresent()) {
				return Lists.newArrayList(optional.get());
			}
		}
		return Lists.newArrayList();
	}

	@Override
	public void updateOrdersLocality(final Byte workshopId, final Long localityId, final String pin) {
		orderDao.updateOrdersLocality(workshopId, localityId, pin);
	}

	private Collection<Order> findByPhone(final Byte workshopId, final String cc, final Long phone, final Byte state,
			final Long createDate, final Long pickUpDate, final Long deliveryDate,final Integer offset,
			final Integer limit) {
		if(state !=null){
			if(createDate!=null){
				return orderDao.findByPhoneAndCreateDateAndState(workshopId, cc, phone, createDate, state,offset, limit);
			}else if(pickUpDate !=null){
				return orderDao.findByPhoneAndPickUpDateAndState(workshopId, cc, phone, pickUpDate, state,offset, limit);
			}else if(deliveryDate !=null){
				return orderDao.findByPhoneAndDeliveryDateAndState(workshopId, cc, phone, deliveryDate, state,offset, limit);
			}
			return orderDao.findByPhone(workshopId, cc, phone, state, offset, limit);
		}
		if (createDate != null) {
			return orderDao.findByPhoneAndCreateDate(workshopId, cc, phone, createDate, offset, limit);
		} else if (pickUpDate != null) {
			return orderDao.findByPhoneAndPickUpDate(workshopId, cc, phone, pickUpDate, offset, limit);
		}  else if (deliveryDate != null) {
			return orderDao.findByPhoneAndDeliveryDate(workshopId, cc, phone, deliveryDate, offset, limit);
		}
		return orderDao.findByPhone(workshopId, cc, phone, offset, limit);
	}

	private Collection<Order> findByItemState(final Byte workshopId, final Long fromDate, final Long toDate,
			final Byte state, final Integer offset, final Integer limit) {
		if (fromDate != null && toDate != null) {
			return orderDao.findByItemStateAndDate(workshopId, state, fromDate, toDate, offset, limit);
		}
		return orderDao.findByItemState(workshopId, state, offset, limit);
	}

	private Collection<Order> findByCustomerName(final Byte workshopId, final String customerName, final Byte state,
			final Long createDate, final Long pickUpDate, final Long deliveryDate, final Integer offset,
			final Integer limit) {
		 if(state !=null){
			if(createDate !=null){
				return orderDao.findByCustomerNameAndCreateDateAndState(workshopId, customerName, createDate,state, offset, limit);
			}else if(pickUpDate !=null){
				return orderDao.findByCustomerNameAndPickUpDateAndState(workshopId, customerName, pickUpDate,state, offset, limit);
			}else if(deliveryDate !=null){
				return orderDao.findByCustomerNameAndDeliveryDateAndState(workshopId, customerName, deliveryDate,state, offset, limit);
			}
			return orderDao.findByCustomerName(workshopId, customerName, state, offset, limit);
	    } else if (createDate != null) {
			return orderDao.findByCustomerNameAndCreateDate(workshopId, customerName, createDate, offset, limit);
		} else if (pickUpDate != null) {
			return orderDao.findByCustomerNameAndPickUpDate(workshopId, customerName, pickUpDate, offset, limit);
		} else if (deliveryDate != null) {
			return orderDao.findByCustomerNameAndDeliveryDate(workshopId, customerName, deliveryDate, offset, limit);
		}
		return orderDao.findByCustomerName(workshopId, customerName, offset, limit);
	}

	private Collection<Order> findByUserId(final Byte workshopId, final Long userId, final Byte state,
			final Long createDate, final Long pickUpDate, final Long deliveryDate, final Integer offset,
			final Integer limit) {
		if(state !=null){
			if(createDate !=null){
				return orderDao.findByUserIdAndCreateDateAndState(workshopId, userId, createDate,state, offset, limit);
			}else if(pickUpDate !=null){
				return orderDao.findByUserIdAndPickUpDateAndState(workshopId, userId, pickUpDate,state, offset, limit);
			}else if(deliveryDate !=null){
				return orderDao.findByUserIdAndDeliveryDateAndState(workshopId, userId, deliveryDate,state, offset, limit);
			}
			return orderDao.findByUserId(workshopId, userId, state, offset, limit);
		} else if (createDate != null) {
			return orderDao.findByUserIdAndCreateDate(workshopId, userId, createDate, offset, limit);
		} else if (pickUpDate != null) {
			return orderDao.findByUserIdAndPickUpDate(workshopId, userId, pickUpDate, offset, limit);
		}  else if (deliveryDate != null) {
			return orderDao.findByUserIdAndDeliveryDate(workshopId, userId, deliveryDate, offset, limit);
		}
		return orderDao.findByUserId(workshopId, userId, offset, limit);
	}

	private Collection<Order> findByCraterUserId(final Byte workshopId, final Long userId, final Byte state,
			final Long createDate, final Long pickUpDate, final Long deliveryDate, final Integer offset,
			final Integer limit) {
	    if (createDate != null) {
			return orderDao.findByCreaterUserIdAndCreateDate(workshopId, userId, createDate, offset, limit);
		} else if (pickUpDate != null) {
			return orderDao.findByCreaterUserIdAndPickUpDate(workshopId, userId, pickUpDate, offset, limit);
		} else if (state != null) {
			return orderDao.findByCreaterUserId(workshopId, userId, state, offset, limit);
		} else if (deliveryDate != null) {
			return orderDao.findByCreaterUserIdAndDeliveryDate(workshopId, userId, deliveryDate, offset, limit);
		}
		return orderDao.findByCreaterUserId(workshopId, userId, offset, limit);
	}

	private Collection<Order> findByReferredByUserId(final Byte workshopId, final Long userId, final Byte state,
			final Long createDate, final Long pickUpDate, final Long deliveryDate, final Integer offset,
			final Integer limit) {
		 if (createDate != null) {
			return orderDao.findByReferredByUserIdAndCreateDate(workshopId, userId, createDate, offset, limit);
		} else if (pickUpDate != null) {
			return orderDao.findByReferredByUserIdAndPickUpDate(workshopId, userId, pickUpDate, offset, limit);
		}else if (state != null) {
			return orderDao.findByReferredByUserId(workshopId, userId, state, offset, limit);
		}else if (deliveryDate != null) {
			return orderDao.findByReferredByUserIdAndDeliveryDate(workshopId, userId, deliveryDate, offset, limit);
		}
		return orderDao.findByReferredByUserId(workshopId, userId, offset, limit);
	}

	private Collection<Order> findByDeliveryDate(final Byte workshopId, final Long deliveryDate, final Byte state,
			final Integer offset, final Integer limit) {
		if (state != null) {
			return orderDao.findByDeliveryDateAndState(workshopId, deliveryDate, state, offset, limit);
		}
		return orderDao.findByDeliveryDate(workshopId, deliveryDate, offset, limit);

	}

	private Collection<Order> findByPickUpDate(final Byte workshopId, final Long pickUpDate, final Byte state,
			final Integer offset, final Integer limit) {
		if (state != null) {
			return orderDao.findByPickUpDateAndState(workshopId, pickUpDate, state, offset, limit);
		}
		return orderDao.findByPickUpDate(workshopId, pickUpDate, offset, limit);
	}

	@Override
	public Collection<Item> listOrderItems(final Byte workshopId, final Long orderId, final Byte state,
			final Long deliveryDate, final Integer offset, final Integer limit) {
		if (state != null) {
			return orderDao.findItemsByOrderIdAndItemState(workshopId, orderId, state, offset, limit);
		} else if (deliveryDate != null) {
			return orderDao.findItemsByOrderIdAndDeliveryDate(workshopId, orderId, deliveryDate, offset, limit);
		}
		return orderDao.findItemsByOrderId(workshopId, orderId, offset, limit);
	}

	@Override
	public List<Long> listOrderItemIds(final Byte workshopId, final Long orderId) {
		return orderDao.findItemIdsByOrderId(workshopId, orderId);
	}

	@Override
	public Collection<Item> searchItems(final Byte workshopId, final Byte state, final Long deliveryDate,
			final Long date, final Integer offset, final Integer limit) {
		if (state != null) {
			if (date != null) {
				return orderDao.findItemsByItemStateAndDate(workshopId, state, date, offset, limit);
			}
			return orderDao.findItemsByItemState(workshopId, state, offset, limit);
		} else if (deliveryDate != null) {
			return orderDao.findItemsByDeliveryDate(workshopId, deliveryDate, offset, limit);
		}
		return Lists.newArrayList();
	}

	@Override
	public Optional<Order> getOrderCostingDetail(final Byte workshopId, final Long orderId) {
		return orderDao.findOrderCostDetail(workshopId, orderId);
	}

	@Override
	public Collection<Payment> listOrderPayments(final Byte workshopId, final Long userId, final Long orderId) {
		return orderDao.findOrderPayments(workshopId, orderId);
	}

	@Override
	public Optional<StateTransition> getStateTransition(final Byte workshopId, final Long itemId, final Byte state) {
		return orderDao.findItemStateTransition(workshopId, itemId, state);
	}

	@Override
	public Collection<Item> searchItems(final Byte workshopId, final Long orderId, final Byte state) {
		return orderDao.findItemsByOrderIdAndItemState(workshopId, orderId, state);
	}

	@Override
	public void changeShipmentUser(final Byte workshopId, final Long shipmentId, final Shipment shipment) {
		shipmentDao.update(shipmentId, shipment);
	}

	@Override
	public Long addShipmentItem(final Byte workshopId, final Long orderId, final Long itemId,
			final StateTransition transition) {
		changeItemState(workshopId, orderId, itemId, transition);
		return shipmentDao.save(Shipment.createWithOrderIdAndItemIdAndState(orderId, itemId, transition.getState(),
				transition.getState(), transition.getShipment()));
	}

	@Override
	public void addShipmentOrder(final Byte workshopId, final Long orderId, final StateTransition transition) {
		orderDao.findItemsByOrderId(workshopId, orderId).stream()
				.forEach(item -> addShipmentItem(workshopId, orderId, item.getId(), transition));
	}

	@Override
	public void saveInvoice(final Byte workshopId, final Long orderId, final Long itemId, final Byte itemState) {
		orderDao.saveInvoice(workshopId, orderId, itemId);
		// orderDao.updateItemState(workshopId, itemId, itemState);
	}

	@Override
	public Collection<Invoice> getInvoices(Byte workshopId, Long orderId, Long itemId, Long date, Integer duration) {
		if (itemId != null) {
			return orderDao.getInvoicesByItemId(itemId);
		} else if (orderId != null) {
			return orderDao.getInvoicesByOrderId(workshopId, orderId);
		} else if (date != null) {
			return orderDao.getInvoicesByDateAndDuration(date, duration);
		}
		return orderDao.getCurrentDateInvoices();
	}

	@Override
	public void addItemDefects(final Byte workshopId, final Long itemId, final List<Byte> defects) {
		orderDao.saveItemDefects(workshopId, itemId, defects);
	}

	@Override
	public Collection<Byte> listItemDefects(final Byte workshopId, final Long itemId) {
		return orderDao.findItemDefectsByItem(workshopId, itemId);
	}

	@Override
	public Long addFeedback(final Long userId, final Feedback feedback) {
		if(!feedback.getTypes().isEmpty()){			
			final List<Long> ids = feedback.getTypes().stream().map(f -> orderDao.saveFeedback(userId, feedback, f))
					.collect(Collectors.toList());
			return ids.stream().findFirst().get();
		}
		return orderDao.saveFeedback(userId, feedback, FeedbackType.OTHERS);
	}


	@Override
	public Optional<Feedback> getFeedback(Long userId, Byte workshopId, Long orderId, Long itemId) {
		return orderDao.getFeedback(userId, workshopId, orderId, itemId);
	}

	@Override
	public Optional<LastDeliveredItem> getLastDeliveredItem(Long userId, Byte workshopId) {

		Optional<LastDeliveredItem> optional = orderDao.getLastDeliveredItem(userId, workshopId);
		if (optional.isPresent()) {
			Collection<Image> images = orderDao.findItemImagesByItem(workshopId, optional.get().getItemId());
			return Optional.ofNullable(LastDeliveredItem.createWithImages(images, optional.get()));
		}
		return optional;
	}

	@Override
	public void applyCouponOnItem(Long userId, Byte workshopId, Long orderId, Long itemId, Integer couponId) {
		Optional<Coupon> optional = couponManager.findById(couponId);
		if (optional.isPresent()) {
			// TODO : verify coupon code against all the terms.
			// couponManager.apply(optional.get());
			orderDao.applyCouponOnItem(workshopId, orderId, itemId, optional.get());
		}
	}

	@Override
	public void removeAppliedCoupon(final Byte workshopId, final Long orderId, final Long itemId,
			final Integer discountId) {
		if (itemId != null) {
			orderDao.removeAppliedCouponOnItem(itemId, discountId);
		} else if (orderId != null) {
			removeAppliedCouponOnOrder(workshopId, orderId, discountId);
		}
	}

	/**
	 * remove applied coupons of each item in an order.
	 * 
	 * @param orderId
	 * @param discountId
	 */
	private void removeAppliedCouponOnOrder(final Byte workshopId, final Long orderId, final Integer discountId) {
		Optional<Order> orderOpt = getOrderDetail(workshopId, orderId);
		List<Item> items = orderOpt.get().getItems();
		items.stream().forEach(item -> orderDao.removeAppliedCouponOnItem(item.getId(), discountId));
	}

	@Override
	public Integer getUserOrderCounts(final Long userId) {
		return orderDao.findUserOrderCounts(userId);
	}

	@Override
	public String checkPaymentStatus(final Payment payment, final Long userId) {
		String status = PaymentState.SUCCESS.toString();
		if (payment.getMode().equals(PaymentMode.PAYTM)) {
			PaytmPaymentStatus paymentDetails = pwServerHttpRequester.checkPaytmTransactionStatus(userId,
					new PaymentStatus(payment.getTxnId(), payment.getChecksum()));
			if (paymentDetails.getSTATUS().equalsIgnoreCase("TXN_FAILURE")) {
				throw new PaymentRequiredException();
			}
			if (paymentDetails.getSTATUS().equalsIgnoreCase("PENDING") || paymentDetails.getSTATUS().equalsIgnoreCase("OPEN")) {
				status = PaymentState.PENDING.toString();
			}
		}

		if (payment.getMode().equals(PaymentMode.PAYU)) {
			PayuPaymentStatus paymentDetails = pwServerHttpRequester.checkPayuTransactionStatus(userId, payment.getTxnId());
			if (paymentDetails.getStatus()!=0) {
				throw new PaymentRequiredException();
			}
			if (paymentDetails.getStatus()==0 && paymentDetails.getResult().get(0).getStatus().equalsIgnoreCase("Settlement in process")){
				status = PaymentState.PENDING.toString();
			}
		}
		return status;
	}

	@Override
	public void updateItemRating(final Byte workshopId,final  Long orderId, final Long itemId,
			final Short rating) {
		 orderDao.updateItemRating(workshopId, orderId, itemId,rating);
	}


	@Override
	public Collection<ShipmentPoint> getShipmentRoutes(final Byte workshopId,
			final Long userId, final Long shipmentDate, final Double latitude, final Double longitude,final Byte currentTimeSlot) {
		final Collection<ShipmentOrder> shipmentOrderList=shipmentDao.findOrderByDateAndUser(shipmentDate, userId);
		List<ShipmentPoint> locationList=new ArrayList<ShipmentPoint>();
		locationList=getLocationList(workshopId,shipmentOrderList,latitude,longitude,currentTimeSlot);
		return ShipmentRouteGenerator.getShortestRoute(locationList);
	}

	/**
	 *Returns LocationList based on List of shipment assigned to user on particular shipment Date and TimeSlot queried
	 */
	private List<ShipmentPoint> getLocationList(final Byte workshopId,final Collection<ShipmentOrder> shipmentOrderList,
			final Double latitude, final Double longitude,final Byte currentTimeSlot) {
		List<ShipmentPoint> locationList=new ArrayList<ShipmentPoint>();
		locationList.add(new ShipmentPoint(latitude, longitude,null,null));
		if(currentTimeSlot !=null){
			for(ShipmentOrder shipment:shipmentOrderList){
				final ShipmentPoint shipmentPoint=getShipmentLocation(workshopId,shipment); 
				if(shipmentPoint!=null){
			    			  if(currentTimeSlot==1 && shipmentPoint.getTimeSlot()==1){
			    				  locationList.add(shipmentPoint);
			    			  }else if(currentTimeSlot==2 && shipmentPoint.getTimeSlot()==2){
			    				  locationList.add(shipmentPoint);
			    			  }else if(currentTimeSlot==3 && shipmentPoint.getTimeSlot()==3){
			    				  locationList.add(shipmentPoint);
			    			  }
			      }
			}
		}else {
			for(ShipmentOrder shipment:shipmentOrderList){
				final ShipmentPoint shipmentPoint=getShipmentLocation(workshopId,shipment); 		
				if(shipmentPoint!=null){
			    	 	locationList.add(shipmentPoint);
			     }
		   }
	    }
		return locationList;
	}
	
	private ShipmentPoint getShipmentLocation(final Byte workshopId, final ShipmentOrder shipment) {
		Optional<Order> order=orderDao.findById(workshopId, shipment.getId());
	    for(ShipmentItem items:shipment.getItems()){
	    	if(items !=null){
	    	Optional<Item> item=orderDao.findItemById(workshopId, items.getId());
	    		final ShipmentPoint shipmentPoint=ShipmentPoint.createWithOrder(shipment, order,item);
	    		return shipmentPoint;
	    	}
	    }
	    return null;
   }
	@Override
	public Collection<Order> search(final Byte workshopId,final Long userId,final String customerName,
			final String cc,final Long phone, final Byte state, final Long fromDate,final Long toDate,
			final  String filterBy,final Integer offset,final Integer limit) {
		    if(phone !=null){
				return findByPhone(workshopId, cc, phone, state, fromDate,toDate,filterBy,offset, limit);
			}else if(userId !=null){
				return findByUserId(workshopId,userId, state, fromDate,toDate,filterBy,offset, limit);
			}else if (!StringUtils.isEmpty(customerName)){
				return findByCustomerName(workshopId,customerName, state, fromDate,toDate,filterBy,offset, limit);
			}
		return  findByDate(workshopId,fromDate,toDate,filterBy,offset,limit);
	}

	private Collection<Order> findByDate(final Byte workshopId, final Long fromDate,
			final Long toDate, final String filterBy,final  Integer offset, final Integer limit) {
		if(filterBy.equals("create_date")){
			return orderDao.findByDateAndFilterByCreateDate(workshopId,fromDate,toDate,offset,limit);
		}else if(filterBy.equals("pick_up_date")){
			return orderDao.findByDateAndFilterByPickUpDate(workshopId,fromDate,toDate,offset,limit);
		}else if(filterBy.equals("delivery_date")){
			return orderDao.findByDateAndFilterByDeliveryDate(workshopId,fromDate,toDate,offset,limit);
		}
		return orderDao.findBy(workshopId, offset, limit);
	}

	private Collection<Order> findByCustomerName(final Byte workshopId,final String customerName,
			final Byte state,final Long fromDate,final Long toDate,final String filterBy,
			final  Integer offset,final Integer limit) {
		if(state !=null){
			if(filterBy.equals("create_date")){
				return orderDao.findByCustomerNameAndDateAndStateAndFilterByCreateDate(workshopId,customerName,state,fromDate,toDate,offset,limit);
			}else if(filterBy.equals("pick_up_date")){
				return orderDao.findByCustomerNameAndDateAndStateAndFilterByPickUpDate(workshopId,customerName, state, fromDate, toDate, offset, limit);
			}else if (filterBy.equals("delivery_date")){
				return orderDao.findByCustomerNameAndDateAndStateAndFilterByDeliveryDate(workshopId, customerName, state, fromDate, toDate, offset, limit);
			}
		}
		if(filterBy.equals("create_date")){
				return orderDao.findByCustomerNameAndDateAndFilterByCreateDate(workshopId,customerName,fromDate,toDate,offset,limit);
		}else if(filterBy.equals("pick_up_date")){
				return orderDao.findByCustomerNameAndDateAndFilterByPickUpDate(workshopId, customerName,  fromDate, toDate, offset, limit);
		}else if (filterBy.equals("delivery_date")){
				return orderDao.findByCustomerNameAndDateAndFilterByDeliveryDate(workshopId, customerName, fromDate, toDate, offset, limit);
		}
		return Lists.newArrayList();
	}

	private Collection<Order> findByUserId(final Byte workshopId, final Long userId,
			final Byte state, final Long fromDate, final Long toDate,final String filterBy,
			final Integer offset,final Integer limit) {
		if(state !=null){
			if(filterBy.equals("create_date")){
				return orderDao.findByUserIdAndDateAndStateAndFilterByCreateDate(workshopId,userId,state,fromDate,toDate,offset,limit);
			}else if(filterBy.equals("pick_up_date")){
				return orderDao.findByUserIdAndDateAndStateAndFilterByPickUpDate(workshopId,userId, state, fromDate, toDate, offset, limit);
			}else if (filterBy.equals("delivery_date")){
				return orderDao.findByUserIdAndDateAndStateAndFilterByDeliveryDate(workshopId, userId, state, fromDate, toDate, offset, limit);
			}
		}
		if(filterBy.equals("create_date")){
				return orderDao.findByUserIdAndDateAndFilterByCreateDate(workshopId,userId,fromDate,toDate,offset,limit);
		}else if(filterBy.equals("pick_up_date")){
				return orderDao.findByUserIdAndDateAndFilterByPickUpDate(workshopId, userId,  fromDate, toDate, offset, limit);
		}else if (filterBy.equals("delivery_date")){
				return orderDao.findByUserIdAndDateAndFilterByDeliveryDate(workshopId, userId, fromDate, toDate, offset, limit);
		}
		return Lists.newArrayList();
	}

	private Collection<Order> findByPhone(final Byte workshopId, final String cc,final Long phone,
			final Byte state,final  Long fromDate, final Long toDate,final String filterBy,
			final Integer offset, final Integer limit) {
		if(state !=null){
			if(filterBy.equals("create_date")){
				return orderDao.findByPhoneAndDateAndStateAndFilterByCreateDate(workshopId,cc,phone,state,fromDate,toDate,offset,limit);
			}else if(filterBy.equals("pick_up_date")){
				return orderDao.findByPhoneAndDateAndStateAndFilterByPickUpDate(workshopId, cc, phone, state, fromDate, toDate, offset, limit);
			}else if (filterBy.equals("delivery_date")){
				return orderDao.findByPhoneAndDateAndStateAndFilterByDeliveryDate(workshopId, cc, phone, state, fromDate, toDate, offset, limit);
			}
		}
		if(filterBy.equals("create_date")){
				return orderDao.findByPhoneAndDateAndFilterByCreateDate(workshopId,cc,phone,fromDate,toDate,offset,limit);
		}else if(filterBy.equals("pick_up_date")){
				return orderDao.findByPhoneAndDateAndFilterByPickUpDate(workshopId, cc, phone,  fromDate, toDate, offset, limit);
		}else if (filterBy.equals("delivery_date")){
				return orderDao.findByPhoneAndDateAndFilterByDeliveryDate(workshopId, cc, phone, fromDate, toDate, offset, limit);
		}
		return Lists.newArrayList();
	}

	@Override
	public Collection<Order> getTaxReports(final Byte workshopId, final String state, final Long fromDate, final Long toDate) {
		return orderDao.getTaxReports(workshopId, state, fromDate, toDate);
	}

	@Override
	public Collection<Order> getTaxReportsByDateRange(final Byte workshopId, final Long fromDate, final Long toDate) {
		return orderDao.getTaxReportsByDateRange(workshopId, fromDate, toDate);
	}
}
