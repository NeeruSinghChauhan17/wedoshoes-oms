package com.wedoshoes.oms.manager.impl;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.wedoshoes.oms.http.CMSServerHttpRequester;
import com.wedoshoes.oms.http.CoreServerHttpRequester;
import com.wedoshoes.oms.http.impl.model.core.CommSettings;
import com.wedoshoes.oms.mail.InvoiceMail;
import com.wedoshoes.oms.mail.InvoiceMailAffiliate;
import com.wedoshoes.oms.mail.OrderMail;
import com.wedoshoes.oms.mail.OrderMailAffiliate;
import com.wedoshoes.oms.manager.NotificationManager;
import com.wedoshoes.oms.model.notification.Attachment;
import com.wedoshoes.oms.model.notification.Notification.NotificationBuilder;
import com.wedoshoes.oms.model.notification.Push;
import com.wedoshoes.oms.model.notification.PushPayload;
import com.wedoshoes.oms.model.notification.SMS;
import com.wedoshoes.oms.model.order.ItemState;
import com.wedoshoes.oms.model.order.Order;
import com.wedoshoes.oms.model.shipment.Shipment;
import com.wedoshoes.oms.util.Base64BinaryConvertor;
import com.wedoshoes.oms.util.TemplateMapper;

@Service
public class NotificationManagerImpl implements NotificationManager {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(NotificationManagerImpl.class);
	private final static Byte AFFILIATE_TYPE = 10;
	
	private final VelocityEngine velocityEngine;
	private final String from;
	private final CoreServerHttpRequester coreServerHttpRequester;
	private final CMSServerHttpRequester cmsServerHttpRequester;
	private final Map<String, String> smsTemplates;
	private final Map<String, String> pushTemplates;
	private final Map<String, String> mailSubjects;
	private final static String PUSH_TITLE = "WEDOSHOES";
	private final Byte workshopId=1;
	private final Byte DELIVERED_STATE=5;

	
	@Autowired
	public NotificationManagerImpl(final VelocityEngine velocityEngine, 
			@Value("${email.from.senderId}") final String from, 
			final CoreServerHttpRequester coreServerHttpRequester,
			final CMSServerHttpRequester cmsServerHttpRequester,
			@Value("#{smsTemplates}") final Map<String, String> smsTemplates,
			@Value("#{pushTemplates}") final Map<String, String> pushTemplates,
			@Value("#{mailSubjects}") final Map<String, String> mailSubjects) {
		
		this.velocityEngine = velocityEngine;
		this.from=from;
		this.coreServerHttpRequester=coreServerHttpRequester;
		this.cmsServerHttpRequester = cmsServerHttpRequester;
		this.smsTemplates = smsTemplates;
		this.pushTemplates = pushTemplates;
		this.mailSubjects = mailSubjects;
	}

	@Override
	public void notifyOrderStateChanged(final Order order, final Byte state, final File file,
			final CommSettings commSett, final Boolean saveSettings) {
		final NotificationBuilder nb = new NotificationBuilder();
		final NotificationBuilder nb4Type = new NotificationBuilder();
		final Byte userType = coreServerHttpRequester.getUserTypeById(order.getReferredByUserId());
		Attachment attachment=null;
		LOGGER.info("NotiR: Refered UserId: {}, userType: {} ",order.getReferredByUserId(),userType);
		
		// set communication settings, and flag to save
		nb.commSett(commSett);
		nb.saveSettings(saveSettings);
		
		if(mailSubjects.containsKey(String.valueOf(state))) {
			
			final String text = mailSubjects.get(String.valueOf(state));
			final Map<String, Object> params = new HashMap<>();
			params.put("orderId", order.getId());
			
			if(file !=null){				
				if(ItemState.ANALYSED == state || ItemState.DELIVERED==state){
					try {
						if(ItemState.ANALYSED== state){						
							String challan=Base64BinaryConvertor.encodeFileToBase64Binary(file);
							//LOGGER.info("NotiR: fileBase64-challan={}",challan);
							attachment= new Attachment("challan_"+order.getId()+".pdf", "application/pdf", challan);
						}else{
							String invoice=Base64BinaryConvertor.encodeFileToBase64Binary(file);
							//LOGGER.info("NotiR: fileBase64-invoice={}",invoice);
							attachment= new Attachment("invoice_"+order.getId()+".pdf", "application/pdf", invoice);
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				
			}	
			
			if(ItemState.NEW == state) {
				params.put("noOfItem", order.getItems().size());				
			} else {
				if(order.getItems().size()>1){
					params.put("itemName", order.getItems().size()+" Items");
				}else{
					params.put("itemName", cmsServerHttpRequester.getProductName(order.getItems().get(0).getProductId()));
				}
			}
			if(AFFILIATE_TYPE.byteValue()==userType.byteValue()){
				
				nb4Type.email(new OrderMailAffiliate(velocityEngine, null, null, new String[]{from}, from,
						order, state, TemplateMapper.map(text, params), cmsServerHttpRequester, attachment));
		
			}else{				
				nb.email(new OrderMail(velocityEngine, null, null, new String[]{from}, from,
						order, state, TemplateMapper.map(text, params), cmsServerHttpRequester, attachment));	
			}
		}
		
		if(smsTemplates.containsKey(String.valueOf(state))) {
			final String text = smsTemplates.get(String.valueOf(state));
			final Map<String, Object> params = new HashMap<>();
			params.put("orderId", order.getId());
			if(ItemState.NEW == state) {
				params.put("noOfItem", order.getItems().size());				
			} else {
				params.put("itemName", cmsServerHttpRequester
						.getProductName(order.getItems().get(0).getProductId()));
			}
			nb.sms(new SMS(Lists.newArrayList(Long.valueOf(order.getCc()+order.getPhone())),
					TemplateMapper.map(text, params)));
					}
		
		if(pushTemplates.containsKey(String.valueOf(state))) {
			final String text = pushTemplates.get(String.valueOf(state));
			final Map<String, Object> params = new HashMap<>();
			params.put("orderId", order.getId());
			if(ItemState.NEW == state) {
				params.put("noOfItem", order.getItems().size());				
			} else {
				params.put("itemName", cmsServerHttpRequester
						.getProductName(order.getItems().get(0).getProductId()));
			}
			
			final Map<String, String>  data = new HashMap<>();
				data.put("title", PUSH_TITLE);
				data.put("body", TemplateMapper.map(text, params));
				data.put("order_id", String.valueOf(order.getId()));
			nb.push(new Push(PushPayload.createWithCustomData(data)));
		}
		coreServerHttpRequester.sendNotification(order.getWorkshopId(), 
				order.getUserId(), nb.build());
		if(AFFILIATE_TYPE.byteValue()==userType){
		coreServerHttpRequester.sendNotification(order.getWorkshopId(), 
				order.getReferredByUserId(), nb4Type.build());
		coreServerHttpRequester.sendNotification(order.getWorkshopId(), 
				order.getUserId(), nb4Type.build());
		}
		
 	}

	@Override
	public String printTemplate() {
		final String text = smsTemplates.get("1");
		final Map<String, Object> params = new HashMap<>();
		params.put("orderId", 1);
		params.put("noOfItem", 5);
		final String finalText = TemplateMapper.map(text, params);
		final NotificationBuilder nb = new NotificationBuilder();
		final SMS sms = new SMS(Lists.newArrayList(8800132375L), finalText);
		nb.sms(sms);
		Byte userId = 1;
		coreServerHttpRequester.sendNotification(userId, 1L, nb.build());
		return finalText;
	}

	@Override
	public void notifyShipmentOrderAdded(final Byte workshopId, final Shipment shipment, final Order order) {
		
		final NotificationBuilder nb = new NotificationBuilder();
		if(pushTemplates.containsKey(String.valueOf(shipment.getCurrentState()))){
			final String text = pushTemplates.get(String.valueOf(shipment.getCurrentState()));
			
			final Map<String, Object> params = new HashMap<>();
				params.put("orderId", order.getId());
				
				if(order.getItems().size()>1){
					params.put("itemName", order.getItems().size()+" Items");
				}else{
					params.put("itemName", cmsServerHttpRequester.getProductName(order.getItems().get(0).getProductId()));
				}
				params.put("customerName", coreServerHttpRequester.getUserNameById(order.getUserId()));
			
			final Map<String, String> data = new HashMap<>();
				data.put("title", PUSH_TITLE);
				data.put("body", TemplateMapper.map(text, params));
				data.put("order_id", String.valueOf(shipment.getOrderId()));
			
			nb.push(new Push(PushPayload.createWithCustomData(data)));
		}
		// send notification
		coreServerHttpRequester.sendNotification(workshopId, shipment.getUserId(), nb.build());
	}
	
	@Override
	public void notifyShipmentUserChanged(final Byte workshopId, final Shipment oldShip,
			final Shipment newShip, final Order order) {
		// send to Assigned To
		notifyShipmentOrderAdded(workshopId, newShip, order);
		// send to Assigned From
		notifyShipmentOrderRemoved(workshopId, oldShip, order, newShip.getUserId());
		
	}
	
	private void notifyShipmentOrderRemoved(final Byte workshopId, final Shipment oldShip,
			final Order order, final Long assigneePosUserId){
		final NotificationBuilder nb = new NotificationBuilder();
		if(pushTemplates.containsKey("REMOVE_SHIPMENT")){
			final String text = pushTemplates.get("REMOVE_SHIPMENT");

		final Map<String, Object> params = new HashMap<>();
				params.put("orderId", order.getId());
				
				if(order.getItems().size()>1){
					params.put("itemName", order.getItems().size()+" Items");
				}else{
					params.put("itemName", cmsServerHttpRequester.getProductName(order.getItems().get(0).getProductId()));
				}
				params.put("customerName", coreServerHttpRequester.getUserNameById(order.getUserId()));
				params.put("posUserName", coreServerHttpRequester.getUserNameById(assigneePosUserId));
			
			final Map<String, String> data = new HashMap<>();
				data.put("title", PUSH_TITLE);
				data.put("body", TemplateMapper.map(text, params));
				data.put("order_id", String.valueOf(order.getId()));
			
			nb.push(new Push(PushPayload.createWithCustomData(data)));
		}
		coreServerHttpRequester.sendNotification(workshopId, oldShip.getUserId(), nb.build());
	}

	@Override
	public void notifyShipmentOrderCanceled(final Byte workshopId, final Shipment shipment,
			final Order order) {
		final NotificationBuilder nb = new NotificationBuilder();
		if(pushTemplates.containsKey("ORDER_CANCELED_SHIPMENT")){
			final String text = pushTemplates.get("ORDER_CANCELED_SHIPMENT");
			
			final Map<String, Object> params = new HashMap<>();
				params.put("orderId", order.getId());
				if(order.getItems().size()>1){
					params.put("itemName", "Some Items");
				}else{
					params.put("itemName", cmsServerHttpRequester.getProductName(order.getItems().get(0).getProductId()));
				}
				params.put("customerName", coreServerHttpRequester.getUserNameById(order.getUserId()));
			
			final Map<String, String> data = new HashMap<>();
				data.put("title", PUSH_TITLE);
				data.put("body", TemplateMapper.map(text, params));
				data.put("order_id", String.valueOf(order.getId()));
			
			nb.push(new Push(PushPayload.createWithCustomData(data)));
		}
		// send notification
		coreServerHttpRequester.sendNotification(workshopId, shipment.getUserId(), nb.build());
	}

	@Override
	public void notifyOnInvoiceAndChallanCreation(final Long userId, final Order order, final Byte state, File file) {
		final NotificationBuilder nb = new NotificationBuilder();
		final NotificationBuilder nb4Type = new NotificationBuilder();
		final Byte userType = coreServerHttpRequester.getUserTypeById(order.getReferredByUserId());
		Attachment attachment=null;
		if(mailSubjects.containsKey(String.valueOf(state))) {
			final String text = mailSubjects.get(String.valueOf(state));
			final Map<String, Object> params = new HashMap<>();
			params.put("orderId", order.getId());
			if(order.getItems().size()>1){
				params.put("itemName", "Some Items");
			}else{
				params.put("itemName", cmsServerHttpRequester.getProductName(order.getItems().get(0).getProductId()));
			}
			
		if(file !=null){
			
			try {
				if(state==99){						
					String challan=Base64BinaryConvertor.encodeFileToBase64Binary(file);
					attachment= new Attachment("challan_"+order.getId()+".pdf", "application/pdf", challan);
				}else{
					String invoice=Base64BinaryConvertor.encodeFileToBase64Binary(file);
					attachment= new Attachment("invoice_"+order.getId()+".pdf", "application/pdf", invoice);
				}
			} catch (IOException e) {
				e.printStackTrace();
		  }
           if(AFFILIATE_TYPE.byteValue()==userType.byteValue()){
				nb4Type.email(new InvoiceMailAffiliate(velocityEngine,null , null,new String[]{from}, from,order,state,  TemplateMapper.map(text, params), attachment));
		   }else{
			nb.email(new InvoiceMail(velocityEngine,null , null, new String[]{from}, from,order,state,  TemplateMapper.map(text, params), attachment)); 
		     }
		 }
		}
		coreServerHttpRequester.sendNotification(workshopId, userId, nb.build());
		if(AFFILIATE_TYPE.byteValue()==userType){
			coreServerHttpRequester.sendNotification(workshopId, 
					order.getReferredByUserId(), nb4Type.build());
			coreServerHttpRequester.sendNotification(order.getWorkshopId(), 
					order.getUserId(), nb4Type.build());	
			}  
 	}

	@Override
    public void notifyShipmentOrderDelivered(final Byte workshopId,
            final Shipment shipment, final Order order) {
        final NotificationBuilder nb = new NotificationBuilder();

        if (pushTemplates.containsKey("ORDER_DELIVERED_SHIPMENT")) {
            final String text = pushTemplates.get("ORDER_DELIVERED_SHIPMENT");

            final Map<String, Object> params = new HashMap<>();
            params.put("orderId", order.getId());
            if (order.getItems().size() > 1) {
                params.put("itemName", "Some Items");
            } else {
                params.put(
                        "itemName",
                        cmsServerHttpRequester.getProductName(order.getItems()
                                .get(0).getProductId()));
            }
            params.put("customerName",
                    coreServerHttpRequester.getUserNameById(order.getUserId()));

            final Map<String, String> data = new HashMap<>();
            data.put("title", PUSH_TITLE);
            data.put("body", TemplateMapper.map(text, params));
            data.put("order_id", String.valueOf(order.getId()));

            nb.push(new Push(PushPayload.createWithCustomData(data)));
        }
        
        if (smsTemplates.containsKey(String.valueOf(DELIVERED_STATE))) {
            final String text = smsTemplates.get(String.valueOf(DELIVERED_STATE));
            final Map<String, Object> params = new HashMap<>();
            params.put("orderId", order.getId());
            if (order.getItems().size() > 1) {
                params.put("noOfItem", order.getItems().size());
            } else {
                params.put(
                        "itemName",
                        cmsServerHttpRequester.getProductName(order.getItems()
                                .get(0).getProductId()));
            }
            nb.sms(new SMS(Lists.newArrayList(Long.valueOf(order.getCc()+order.getPhone())), TemplateMapper
                    .map(text, params)));
        }

        if (mailSubjects.containsKey(String.valueOf("ORDER_DELIVERED_SHIPMENT"))) {
            final String text = mailSubjects.get(String
                    .valueOf("ORDER_DELIVERED_SHIPMENT"));
            final Byte DELIVERED_ITEM_STATE = 5;
            final Map<String, Object> params = new HashMap<>();
            params.put("orderId", order.getId());

            if (order.getItems().size() > 1) {
                params.put("itemName", order.getItems().size() + " Items");
            } else {
                params.put(
                        "itemName",
                        cmsServerHttpRequester.getProductName(order.getItems()
                                .get(0).getProductId()));
            }
            nb.email(new OrderMail(velocityEngine, null, null,
                    new String[] { from }, from, order, DELIVERED_ITEM_STATE,
                    TemplateMapper.map(text, params), cmsServerHttpRequester,
                    null));
        }

        // send notification
        coreServerHttpRequester.sendNotification(workshopId,
                order.getUserId(), nb.build());
    }
}
