package com.wedoshoes.oms.manager.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.wedoshoes.oms.dao.CartDao;
import com.wedoshoes.oms.manager.CartManager;
import com.wedoshoes.oms.model.order.Cart;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.WedoService;

@Component
public class CartManagerImpl implements CartManager {
	
	private final CartDao cartDao;
	@Autowired
	public CartManagerImpl(final CartDao cartDao) {
		this.cartDao = cartDao;
	}

	@Override
	public Long createCart(final Cart cart) {
		return cartDao.saveCart(cart);
	}

	@Transactional
	@Override
	public void addCartItems(final Long cartId, final List<Item> items) {
		items.stream().map(item -> Item.createWithItemId(cartDao.saveCartItem(cartId,item), item)).
		collect(Collectors.toList()).stream().forEach(item ->cartDao.saveCartItemServices(item.getId(), item.getServices()));
		cartDao.increaseCartSize(cartId, items.size()); 
	}

	@Transactional
	@Override
	public void addCartItemServices(final Long cartId, final Long itemId,
			final List<WedoService> services) {
		cartDao.saveCartItemServices(itemId, services);
	}

	@Override
	public void deleteItemServiceByServiceId(final Long itemId, final Long serviceId) {
		cartDao.deleteServiceByServiceId(itemId, serviceId);
	}

	@Override
	public void deleteItemServicesByItemId(final Long itemId) {
		cartDao.deleteServiceByItemId(itemId);
	}

	@Transactional
	@Override
	public void deleteItem(final Long cartId, final Long itemId) {
		deleteItemServicesByItemId(itemId);
		cartDao.deleteItemByItemId(itemId);
		cartDao.decreaseCartSize(cartId, 1); 
	}

	@Transactional
	@Override
	public void clearCart(final Long cartId) {
		cartDao.deleteServiceByCartId(cartId);
		cartDao.deleteItemsBycartId(cartId);
		cartDao.decreaseCartSize(cartId, 0); 
	}
	
	@Override
	public void deActivateCart(final Long cartId) {
		cartDao.deActivateCartById(cartId);
	}

	@Override
	public List<Item> listCartItemByCartId(final Long cartId) {
		return cartDao.findCartItemsByCartId(cartId);
	}

	@Override
	public Optional<Cart> getCartByUserId(final Long userId) {
		return cartDao.findCartByUserId(userId);
	}
	
	@Override
	public Optional<Cart> getCartById(final Long id) {
		return cartDao.findCartById(id);
	}

	@Override
	public List<Cart> listCarts(final Integer offset, final Integer limit) {
		return cartDao.findCarts(offset, limit);
	}

	@Transactional
	@Override
	public Optional<Cart> getCartDetailById(final Long id) {
		Optional<Cart> cartOptional= cartDao.findCartById(id);
		if(cartOptional.isPresent()){			
			return Optional.ofNullable(Cart.createWithItems(cartOptional.get(), cartDao.findCartItemsByCartId(id)));
		}
		return Optional.empty();
	}

	@Override
	public void updateItem(final Byte workshopId, final Long cartId, final Item item) {
		cartDao.updateItem(workshopId, cartId, item); 
	}

	
}
