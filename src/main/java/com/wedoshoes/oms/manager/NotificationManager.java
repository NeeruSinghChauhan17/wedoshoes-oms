package com.wedoshoes.oms.manager;

import java.io.File;

import com.wedoshoes.oms.http.impl.model.core.CommSettings;
import com.wedoshoes.oms.model.order.Order;
import com.wedoshoes.oms.model.shipment.Shipment;

public interface NotificationManager {

	/** 
	 * Send Notification to POS user (State must known)
	 */
	void notifyShipmentOrderAdded(Byte workshopId, Shipment shipment, Order order);
	
	/** 
	 * Send Notification to POS user
	 */
	void notifyShipmentUserChanged(Byte workshopId, Shipment oldShip, Shipment newShip, Order order);
	
	
	/** 
	 * Send Notification to POS user IF Order Canceled by Admin/cutomerCare/User.
	 */
	void notifyShipmentOrderCanceled(Byte workshopId, Shipment shipment, Order order);
	
	
	void notifyOrderStateChanged(Order order, Byte state, File file, CommSettings commSett, Boolean saveSettings);
	String printTemplate();
	
	public void notifyOnInvoiceAndChallanCreation(Long userId, Order order, Byte state, File file);

	void notifyShipmentOrderDelivered(Byte workshopId, Shipment shipment, Order order); 
}
