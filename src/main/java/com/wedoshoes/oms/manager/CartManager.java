package com.wedoshoes.oms.manager;

import java.util.List;
import java.util.Optional;

import com.wedoshoes.oms.model.order.Cart;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.WedoService;

public interface CartManager {

	Long createCart(Cart cart);

	void addCartItems(Long cartId, List<Item> items);

	void addCartItemServices(Long cartId,Long itemId,
			List<WedoService> services);
	
	void deleteItemServiceByServiceId(Long itemId, Long serviceId);
	void deleteItemServicesByItemId(Long itemId);
	void deleteItem(Long cartId, Long itemId);
	void clearCart(Long cartId);
	
	List<Item> listCartItemByCartId(Long cartId);

	Optional<Cart> getCartByUserId(Long userId);
	Optional<Cart> getCartById(Long id);
	
	List<Cart> listCarts(Integer offset, Integer limit);

	void deActivateCart(Long cartId);
	
	Optional<Cart> getCartDetailById(Long id);
	void updateItem(Byte workshopId, Long cartId, Item item);
}
