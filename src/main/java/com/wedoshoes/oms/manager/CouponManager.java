package com.wedoshoes.oms.manager;

import java.util.Collection;
import java.util.Optional;

import com.wedoshoes.oms.model.Coupon;
import com.wedoshoes.oms.model.CouponSource;

public interface CouponManager {

	Integer save(Coupon coupon);
	void update(Coupon coupon);
	void delete(Integer id);
	Optional<Coupon> findById(Integer id);
	Optional<Coupon> getCouponByCode(String code);
	Optional<Coupon> getCouponDetailByCode(String code);
	Optional<Coupon> findDetailById(Integer id);
	Optional<Coupon> validate(Coupon coupon);
	Optional<Coupon> apply(Coupon coupon);
	
	Collection<Coupon> listByUserType(Byte userTypeId, Integer offSet, Integer limit) ;
	Collection<Coupon> listByProduct(Integer productId, Integer offSet, Integer limit);
	Collection<Coupon> listByCategory(Integer categoryid, Integer offSet, Integer limit);
	Collection<Coupon> listDefaultCoupon( Integer offSet, Integer limit);
	
	Integer save(CouponSource couponSource);
	void update(CouponSource couponSource);
	void deleteSource(Integer sourceId);
	Optional<CouponSource> findSourceById(Integer sourceId);
	Optional<CouponSource> findSourceDetailById(Integer sourceId);
	Collection<CouponSource> list();
}
