package com.wedoshoes.oms.manager;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.wedoshoes.oms.invoice.Invoice;
import com.wedoshoes.oms.model.LastDeliveredItem;
import com.wedoshoes.oms.model.PaymentTransaction;
import com.wedoshoes.oms.model.order.Discount;
import com.wedoshoes.oms.model.order.Feedback;
import com.wedoshoes.oms.model.order.Image;
import com.wedoshoes.oms.model.order.ImageType;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.Order;
import com.wedoshoes.oms.model.order.Payment;
import com.wedoshoes.oms.model.order.WedoService;
import com.wedoshoes.oms.model.payment.PaymentAdjustment;
import com.wedoshoes.oms.model.shipment.Shipment;
import com.wedoshoes.oms.model.shipment.ShipmentPoint;
import com.wedoshoes.oms.model.shipment.ShipmentOrder;
import com.wedoshoes.oms.model.shipment.ShipmentSummary;
import com.wedoshoes.oms.model.state.StateTransition;


public interface OrderManager {
	
	/**
	 * ******************************
	 * 	Order Related Methods
	 * ******************************
	 */
	
	Long createOrder(Byte workshopId, Order order);
	void updateOrder(Byte workshopId, Order order);
	Optional<Order> getOrder(Byte workshopId, Long orderId);
	Optional<Order> getOrderDetail(Byte workshopId, Long orderId);
	Collection<Order> listByUser(Long userId, Integer offset, Integer limit);
	
	Optional<Order> getOrderCostingDetail(Byte workshopId, Long orderId);
	
	Collection<Order> search(Byte workshopId, Long userId, Long createrUserId, Long referredByUserId, 
			String customerName, Long orderId, Long itemId, String cc, Long phone, Byte state, 
			Long createDate, Long pickUpDate, Long deliveryDate,Long fromDate, Long toDate,
			Integer offset, Integer limit);
	Collection<Order> search(Byte workshopId, Long userId, String customerName,	String cc, Long phone, Byte state, Long fromDate, Long toDate,	String filterBy, Integer offset, Integer limit);

	 void updateOrdersLocality(Byte workshopId, Long localityId, String pin);
	 
	/**
	 * ******************************
	 * 	Order Item Related Methods
	 * ******************************
	 */
	
	Item addItem(Byte workshopId, Long orderId, Item item);
	void updateItem(Byte workshopId, Item item);
	
	void changeItemState(Byte workshopId, Long orderId, Long itemId, StateTransition transition);
	void changeOrderItemsState(Byte workshopId, Long orderId, StateTransition transition);
	void applyCouponOnItem(Long userId, Byte workshopId,Long orderId, Long itemId, Integer couponId);	
	void removeAppliedCoupon(Byte workshopId, Long orderId, Long itemId, Integer discountid);

	
	/**
	 * ******************************
	 * 	Shipment Related Methods
	 * ******************************
	 */
	Long addShipmentItem(Byte workshopId, Long orderId, Long itemId, StateTransition transition);
	void addShipmentOrder(Byte workshopId, Long orderId, StateTransition transition);
	
	void updateShipment(Byte workshopId, Long shipmentId, StateTransition transition);
	void updateShipmentItemState(Byte state, Long itemId);
	void updateShipmentOrderState(Byte state, Long orderId);
	
	void changeShipmentUser(Byte workshopId, Long shipmentId, Shipment shipment);
	Optional<ShipmentSummary> getShipmentSummary(Long userId, Long shipmentDate);
	Optional<Shipment> getShipment(Byte workshopId, Long id);
	Collection<ShipmentOrder> getShipmentOrderByDateAndUser(Byte workshopId, Long date, Long userId);
	Optional<Shipment> getShipmentByDate(Byte workshopId, Long date, Long orderId, Long itemId);
	Optional<Shipment> getShipmentByItemId(Byte workshopId, Long orderId, Long itemId);
	Collection<Shipment> listShipmentByOrderId(Byte workshopId, Long orderId);
	
	/**
	 * ******************************
	 * 	State-Transition Related Methods
	 * ******************************
	 */
	
	Optional<StateTransition> getStateTransition(Byte workshopId, Long itemId, Long transitionId);
	Optional<StateTransition> getStateTransition(Byte workshopId, Long itemId, Byte state);
	Collection<StateTransition> listStateTransition(Byte workshopId, Long itemId);
	List<StateTransition> listExternalStateTransition(Byte workshopId, Long itemId);
	
	
	Optional<Item> getItem(Byte workshopId, Long itemId);
	Collection<Item> listOrderItems(Byte workshopId, Long orderId, Byte state, Long deliveryDate, 
			Integer offset, Integer limit);
	List<Long> listOrderItemIds(Byte workshopId, Long orderId);
	Collection<Item> searchItems(Byte workshopId,Byte state,Long deliveryDate, Long date, Integer offset, Integer limit);
	/*void cancelOrder(Byte workshopId, Long orderId);
	void cancelOrderItem(Byte workshopId, Long orderId, Long itemId);*/
	Collection<Item> searchItems(final Byte workshopId, final Long orderId, final Byte state);
	/**
	 * ************************************
	 * 	Order Item Service Related Methods
	 * ************************************
	 */

	void addItemServices(Byte workshopId, Long itemId, List<WedoService> services);
	void updateItemService(Byte workshopId, Long itemId, Long serviceId, WedoService wedoService);
	void removeItemService(Byte workshopId, Long itemId, Long serviceId);
	void removeItemServices(Byte workshopId, Long itemId);
	Collection<WedoService> listItemServices(Byte workshopId, Long itemId);
	Optional<WedoService> getItemService(Byte workshopId, Long itemId,Long serviceId);
	
	
	/**
	 * ************************************
	 * 	Order Item Image Related Methods
	 * ************************************
	 */
	
	void addItemImages(Byte workshopId, Long itemId, List<Image> images);
	void updateItemImage(Byte workshopId, Long itemId, Image image);
	void removeItemImage(Byte workshopId, Long imageId);
	void removeItemImagesByItem(Byte workshopId, Long itemId);
	Optional<Image> getItemImage(Byte workshopId, Long imageId);
	Collection<Image> listItemImages(Byte workshopId, Long itemId);
	Collection<Image> listItemImages(Byte workshopId, Long itemId, ImageType imageType);
	
	
	/**
	 * ************************************
	 * 	Order Payment Detail Related Methods
	 * ************************************
	 */
	
	Long addAdvancedPayment(Byte workshopId, Long userId, Long orderId,Payment payment);
	Long addItemPayment(Byte workshopId, Long userId, Long orderId,Long itemId, Payment payment);
	Collection<Payment> listOrderPayments(Byte workshopId, Long userId, Long orderId);
	String checkPaymentStatus(Payment payment,Long userId);
	void addItemPaymentAdjustment(Byte workshopId, Long userId, Long orderId, Long itemId,PaymentAdjustment paymentAdjustment);
	
	/**
	 * ************************************
	 * 	Item Discount Related Methods
	 * ************************************
	 */
	void addDiscountOnOrderItem(Byte workshopId, Long orderId, Long itemId, List<Discount> discounts);
	Collection<Discount> listDiscountsOnItem(Byte workshopId, Long itemId);

	/**
	 * ************************************
	 * 	Invoice Related Methods
	 * ************************************
	 */
	void saveInvoice(Byte workshopId, Long orderId, Long itemId, Byte itemState);
	
	Collection<Invoice> getInvoices(Byte workshopId, Long orderId, Long itemId,
			Long date, Integer duration);
	
	/**
	 * ************************************
	 * 	Item Defects Related Methods
	 * ************************************
	 */
	void addItemDefects(Byte workshopId, Long itemId, List<Byte> defects);
	Collection<Byte> listItemDefects(Byte workshopId, Long itemId);
	
	/**
	 * ************************************
	 * 	Order Feedback Related Methods
	 * ************************************
	 */
	Long addFeedback(Long userId, Feedback feedback);
	
	Optional<Feedback> getFeedback(Long userId, Byte workshopId, Long orderId, Long itemId);

	Optional<LastDeliveredItem> getLastDeliveredItem(Long userId, Byte workshopId);
	
	Integer getUserOrderCounts(Long userId);
	Collection<PaymentTransaction> listTransactions(final Byte workshopId,Long orderId,Long itemId,Long userId, Long startDate, Long endDate, Integer offset, Integer limit);
	Collection<Shipment> getShipmentOrderAssignee(Byte workshopId,
			Long orderId, Long itemId);
	void updateItemRating(Byte workshopId, Long orderId, Long itemId,
			Short rating);
	Collection<ShipmentPoint> getShipmentRoutes(Byte workshopId,
			Long userId, Long shipmentDate, Double latitude, Double longitude, Byte currentTimeSlot);
	
	/**
	 * ************************************
	 * 	Orders Report Related Methods
	 * ************************************
	 */
	Collection<Order> getTaxReports(Byte workshopId, String state, Long fromDate, Long toDate);
	
	Collection<Order> getTaxReportsByDateRange(Byte workshopId, Long fromDate, Long toDate);
	
}