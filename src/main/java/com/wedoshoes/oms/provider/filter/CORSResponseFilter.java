package com.wedoshoes.oms.provider.filter;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;

import org.springframework.stereotype.Service;

@Provider
@Service
public class CORSResponseFilter implements ContainerResponseFilter {

	@Override
	public void filter(final ContainerRequestContext requestContext,
			final ContainerResponseContext responseContext) throws IOException {
		
		MultivaluedMap<String, Object> headers = responseContext.getHeaders();

		headers.add("Access-Control-Allow-Origin", "*");
		headers.add("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, PATCH");			
		headers.add("Access-Control-Allow-Headers", 
				"User-Agent, Authorization, X-UserID, X-ServerID, X-Secret, Device-Token,"
				+ " Device-Type, Contact-Mode-Id, device-token, device-type, Content-Type, Accept, Origin, X-Requested-With");		
		headers.add("Access-Control-Expose-Headers", "Authorization, X-Custom-header");
		headers.add("Access-Control-Allow-Credentials", true);
	}

}
