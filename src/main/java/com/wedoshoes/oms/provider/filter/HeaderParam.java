package com.wedoshoes.oms.provider.filter;

public interface HeaderParam {

	public static final String USER_AGENT = "User-Agent";
	public static final String AUTHORIZATION = "Authorization";
	public static final String USER_ID = "X-UserID";
	public static final String SERVER_ID = "X-ServerID";
	public static final String SECRET = "X-Secret";
}
