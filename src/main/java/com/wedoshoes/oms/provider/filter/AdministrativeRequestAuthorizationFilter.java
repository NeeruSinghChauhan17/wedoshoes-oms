package com.wedoshoes.oms.provider.filter;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.wedoshoes.oms.http.impl.UnAuthorizedException;
import com.wedoshoes.oms.resource.impl.Administrative;
import com.wedoshoes.oms.security.OAuthService;
import com.wedoshoes.oms.security.Token;



/**
 * This filter used for Administrative Action.
 * First authenticate user by validating OauthToken, 
 * then authorize user by checking api permission for user Type.
 * @author Navrattan Yadav
 *
 */
@Provider
@Service
@Administrative(action = "")
public class AdministrativeRequestAuthorizationFilter implements ContainerRequestFilter {

	private static final Logger LOGGER = LoggerFactory.getLogger(SecuredRequestAuthorizationFilter.class);
	
	@Context
	private ResourceInfo resourceInfo;
	
	@Autowired
	private OAuthService oAuthService;
	
	@Override
	public void filter(ContainerRequestContext requestContext)
			throws IOException {
		final String token = requestContext.getHeaderString(HeaderParam.AUTHORIZATION);
		Long userId = null;
	    try {
	    	userId = Long.parseLong(requestContext.getHeaderString(HeaderParam.USER_ID));
	    } catch (NumberFormatException e) {
	    	throw new UnAuthorizedException();
	    }
		final String userAgent = requestContext.getHeaderString(HeaderParam.USER_AGENT);
		
		if(StringUtils.isEmpty(userAgent) || StringUtils.isEmpty(token)) {
			LOGGER.error("Authentication failed. Missing userId or token in request.UserID = {} and Token = {}",
					userId, token);
			throw new UnAuthorizedException();
		}
		
		/* Get action requested by user */
		final String action = resourceInfo.getResourceMethod().
				getAnnotation(Administrative.class).action();
		
			/*
			 * Authenticate and Authorize User from OAuth Server
			 */
		oAuthService.authorize(userId, action, new Token(userId, userAgent, token, null));
		requestContext.getHeaders().add("user_id", String.valueOf(userId));
		
	}
}
