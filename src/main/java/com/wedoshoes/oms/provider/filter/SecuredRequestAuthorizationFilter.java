package com.wedoshoes.oms.provider.filter;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.wedoshoes.oms.http.impl.UnAuthorizedException;
import com.wedoshoes.oms.resource.impl.Secured;
import com.wedoshoes.oms.security.OAuthService;
import com.wedoshoes.oms.security.Token;


/**
 * This filter authenticate user by validating OAuth Token.   
 * @author Navrattan Yadav
 */
@Secured
@Provider
@Service
public class SecuredRequestAuthorizationFilter implements ContainerRequestFilter {

	private static final Logger LOGGER = LoggerFactory.getLogger(SecuredRequestAuthorizationFilter.class);
	
	@Context
	UriInfo uriInfo;
	
	@Autowired
	private OAuthService oAuthService;
	
	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		
		final String token = requestContext.getHeaderString(HeaderParam.AUTHORIZATION);
		Long userId = null;
	    try {
	    	userId = Long.parseLong(uriInfo.getPathParameters().getFirst("user_id"));
	    } catch (NumberFormatException e) {
	    	throw new UnAuthorizedException();
	    }
	    
		final String userAgent = requestContext.getHeaderString(HeaderParam.USER_AGENT);
		if(StringUtils.isEmpty(userAgent) || StringUtils.isEmpty(token)) {
			LOGGER.error("Authentication failed. Missing userId or token. UserID = {} and Token = {}",
					userId, token);
			throw new UnAuthorizedException();
		}
		// Authenticate User From OAuth Server
		oAuthService.authenticate(userId, new Token(userId, userAgent, token, null));
	}
}
