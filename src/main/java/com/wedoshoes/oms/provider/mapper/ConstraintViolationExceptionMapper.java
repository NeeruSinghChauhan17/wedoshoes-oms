package com.wedoshoes.oms.provider.mapper;

import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.springframework.stereotype.Service;

/**
 * Jersey return ConstraintViolationException for invalid JSON with HTTP status 412. 
 * This class change HTTP status 400 from 412.
 * @author Navrattan Yadav
 *
 */
@Provider
@Service
public class ConstraintViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {

    @Override
    public Response toResponse(ConstraintViolationException exception) {
        return Response.status(Status.BAD_REQUEST).entity(exception.getMessage()).build();
    }
}
