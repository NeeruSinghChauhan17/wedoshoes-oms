package com.wedoshoes.oms.provider.mapper;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.springframework.stereotype.Service;

/**
 * 
 * @author Navrattan Yadav
 *
 */
@Provider
@Service
public class IllegalArgumentExceptionMapper implements ExceptionMapper<IllegalArgumentException> {

	@Override
	public Response toResponse(final IllegalArgumentException exception) {
		return Response.status(Status.BAD_REQUEST).type(MediaType.APPLICATION_JSON).
				entity(exception.getMessage()).build();
	}
}
