package com.wedoshoes.oms.application;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.dropwizard.Configuration;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

/**
 * 
 * @author Navrattan Yadav
 *
 */
public class CoreServiceConfiguration extends Configuration {
	 @JsonProperty("swagger")
	    public SwaggerBundleConfiguration swaggerBundleConfiguration;
}
