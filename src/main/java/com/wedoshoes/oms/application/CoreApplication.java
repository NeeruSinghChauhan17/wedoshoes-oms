package com.wedoshoes.oms.application;

import javax.validation.ConstraintViolationException;
import javax.ws.rs.ext.ExceptionMapper;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.google.common.collect.Lists;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.wedoshoes.oms.healthcheck.CoreHealthCheck;
import com.wedoshoes.oms.provider.filter.AdministrativeRequestAuthorizationFilter;
import com.wedoshoes.oms.provider.filter.CORSResponseFilter;
import com.wedoshoes.oms.provider.filter.SecuredRequestAuthorizationFilter;
import com.wedoshoes.oms.provider.mapper.ConstraintViolationExceptionMapper;
import com.wedoshoes.oms.provider.mapper.CustomExceptionMapper;
import com.wedoshoes.oms.provider.mapper.IllegalArgumentExceptionMapper;
import com.wedoshoes.oms.provider.mapper.IllegalStateExceptionMapper;
import com.wedoshoes.oms.resource.AdminResource;
import com.wedoshoes.oms.resource.CartResource;
import com.wedoshoes.oms.resource.CouponResource;
import com.wedoshoes.oms.resource.FeedbackResource;
import com.wedoshoes.oms.resource.InvoiceResource;
import com.wedoshoes.oms.resource.OrderResource;
import com.wedoshoes.oms.resource.ShipmentResource;
import com.wedoshoes.oms.resource.TestResource;
import com.wedoshoes.oms.resource.impl.AdminResourceImpl;
import com.wedoshoes.oms.resource.impl.CartResourceImpl;
import com.wedoshoes.oms.resource.impl.CouponResourceImpl;
import com.wedoshoes.oms.resource.impl.FeedbackResourceImpl;
import com.wedoshoes.oms.resource.impl.InvoiceResourceImpl;
import com.wedoshoes.oms.resource.impl.OrderResourceImpl;
import com.wedoshoes.oms.resource.impl.ShipmentResourceImpl;
import com.wedoshoes.oms.resource.impl.TestResourceImpl;

import io.dropwizard.Application;
import io.dropwizard.lifecycle.Managed;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

/**
 * 
 * @author Navrattan Yadav
 *
 */
public class CoreApplication extends Application<CoreServiceConfiguration> implements Managed {

	private ClassPathXmlApplicationContext classPathXmlApplicationContext;

	public static void main(final String[] args) throws Exception {
		new CoreApplication().run(Lists.newArrayList("server", "wedoshoes-oms.yaml").toArray(new String[2]));
	}

	@Override
	public void initialize(final Bootstrap<CoreServiceConfiguration> bootstrap) {
		 bootstrap.addBundle(new SwaggerBundle<CoreServiceConfiguration>() {
	            @Override
	            protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(CoreServiceConfiguration sampleConfiguration) {
	                // this would be the preferred way to set up swagger, you can also construct the object here programtically if you want
	            sampleConfiguration.swaggerBundleConfiguration.setSchemes(new String[] { "https" });
	            	return sampleConfiguration.swaggerBundleConfiguration;
	           
	            }
	        });
	}
	
	@Override
	public void run(final CoreServiceConfiguration configuration, final Environment environment) throws Exception {
		classPathXmlApplicationContext = new ClassPathXmlApplicationContext("spring/application-config.xml");
		
		final CoreHealthCheck coreHealthCheck = classPathXmlApplicationContext.getBean(CoreHealthCheck.class);
		
		final AdminResource adminResource = classPathXmlApplicationContext.getBean(AdminResourceImpl.class);
		
		final CartResource cartResource = classPathXmlApplicationContext.getBean(CartResourceImpl.class);
		
		final CouponResource couponResource = classPathXmlApplicationContext.
				getBean(CouponResourceImpl.class);
		final OrderResource orderResource = classPathXmlApplicationContext.getBean(OrderResourceImpl.class);
		
		final ShipmentResource shipmentResource = classPathXmlApplicationContext.getBean(ShipmentResourceImpl.class);
		
		final TestResource testResource = classPathXmlApplicationContext.getBean(TestResourceImpl.class);
		
		final InvoiceResource invoiceResource = classPathXmlApplicationContext.getBean(InvoiceResourceImpl.class);

		final FeedbackResource feedbackResource = classPathXmlApplicationContext.getBean(FeedbackResourceImpl.class);
		
		/** =============================================================== **/
		/** ======================  Filters  ============================== **/
		/** =============================================================== **/
		
		final SecuredRequestAuthorizationFilter sarf = classPathXmlApplicationContext.
				getBean(SecuredRequestAuthorizationFilter.class);

		final AdministrativeRequestAuthorizationFilter araf = classPathXmlApplicationContext.
				getBean(AdministrativeRequestAuthorizationFilter.class);
		
		final CORSResponseFilter corsResponseFilter = classPathXmlApplicationContext
				.getBean(CORSResponseFilter.class);

		
		/** =============================================================== **/
		/** ===================== Exception Mapper ======================== **/
		/** =============================================================== **/
		
		final ExceptionMapper<IllegalStateException> illegalStateExceptionMapper = classPathXmlApplicationContext
				.getBean(IllegalStateExceptionMapper.class);
		final ExceptionMapper<IllegalArgumentException> illegalArgumentExceptionMapper = classPathXmlApplicationContext
				.getBean(IllegalArgumentExceptionMapper.class);
		final ExceptionMapper<ConstraintViolationException> coExceptionMapper = classPathXmlApplicationContext
				.getBean(ConstraintViolationExceptionMapper.class);
		final CustomExceptionMapper cem = classPathXmlApplicationContext.
				getBean(CustomExceptionMapper.class);
		
		environment.jersey().register(illegalArgumentExceptionMapper);
		environment.jersey().register(illegalStateExceptionMapper);
		environment.jersey().register(coExceptionMapper);
		environment.jersey().register(cem);
		environment.jersey().register(orderResource);
		environment.jersey().register(cartResource);
		environment.jersey().register(adminResource);
		environment.jersey().register(couponResource);
		environment.jersey().register(testResource);
		environment.jersey().register(shipmentResource);
		environment.jersey().register(corsResponseFilter);
		environment.jersey().register(sarf);
		environment.jersey().register(araf);
		environment.jersey().register(invoiceResource);
		environment.jersey().register(feedbackResource);
		
		environment.healthChecks().register("Wedoshoes-OMS-Health-Check", coreHealthCheck);
		
		environment.lifecycle().manage(this);
	}

	@Override
	public String getName() {
		return "wedoshoes-oms";
	}



	@Override
	public void start() throws Exception {
		// Do nothing
	}

	@Override
	public void stop() throws Exception {
		final ComboPooledDataSource comboPooledDataSource = classPathXmlApplicationContext
				.getBean(ComboPooledDataSource.class);
		comboPooledDataSource.close();
		classPathXmlApplicationContext.close();
	}
}
