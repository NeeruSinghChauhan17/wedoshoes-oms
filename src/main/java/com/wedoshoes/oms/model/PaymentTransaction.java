package com.wedoshoes.oms.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wedoshoes.oms.model.order.Payment;
import com.wedoshoes.oms.model.order.PaymentMode;
import com.wedoshoes.oms.model.order.TransactionType;

@JsonInclude(Include.NON_NULL)
public class PaymentTransaction extends Payment {

    private final Long orderId;
    private final Long itemId;

    @SuppressWarnings("unused")
    private PaymentTransaction() {
        this(null, null, null, null, null, null, null, null, null, null);
    }

    public PaymentTransaction(final Long id, final Long userId,
            final String txnId, final PaymentMode mode,
            final BigDecimal amount, final Long date,
            final TransactionType transactionType, final String description,
            final Long orderId, final Long itemId) {
        super(id, userId, txnId, mode, amount, date, transactionType,
                description,null,null,null);
        this.orderId = orderId;
        this.itemId = itemId;
    }

    @JsonProperty("order_id")
    public Long getOrderId() {
        return orderId;
    }

    @JsonProperty("item_id")
    public Long getItemId() {
        return itemId;
    }

}
