package com.wedoshoes.oms.model;

import java.math.BigDecimal;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wedoshoes.oms.model.order.PriceValueType;

@JsonInclude(Include.NON_NULL)
public class Coupon {

	private final Integer id;
	private final String code;
	private final String description;
	private final BigDecimal value;
	private final PriceValueType valueType;
	private final Long startDate;
	private final Long endDate;
	private final Integer maxUse;
	private final BigDecimal minOrderAmount;
	private final CouponSource couponSource;
	private final BigDecimal maxDiscount;
	//
	private final Set<Integer> products;
	private final Set<Integer> services;
	private final Set<Integer> userTypes;
	private final Set<Integer> categories;
	private final Integer usedCount;

	@SuppressWarnings("unused")
	private Coupon() {
		this(null, null, null, null, null, null, null, null, null, null, null,
				null, null, null, null, null);
	}

	public Coupon(final Integer id, final String code,
			final String description, final BigDecimal value,
			final PriceValueType valueType, final Long startDate,
			final Long endDate, final Integer maxUse,
			final BigDecimal minOrderAmount, final CouponSource couponSource,
			final BigDecimal maxDiscount, final Set<Integer> products,
			final Set<Integer> services, final Set<Integer> userTypes,
			final Set<Integer> categories, final Integer usedCount) {
		this.id = id;
		this.code = code;
		this.description = description;
		this.value = value;
		this.valueType = valueType;
		this.startDate = startDate;
		this.endDate = endDate;
		this.maxUse = maxUse;
		this.minOrderAmount = minOrderAmount;
		this.couponSource = couponSource;
		this.maxDiscount = maxDiscount;
		this.products = products;
		this.services = services;
		this.userTypes = userTypes;
		this.categories = categories;
		this.usedCount=usedCount;
	}

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("code")
	public String getCode() {
		return code;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("value")
	public BigDecimal getValue() {
		return value;
	}

	@JsonProperty("value_type")
	public PriceValueType getValueType() {
		return valueType;
	}

	@JsonProperty("start_date")
	public Long getStartDate() {
		return startDate;
	}

	@JsonProperty("end_date")
	public Long getEndDate() {
		return endDate;
	}

	@JsonProperty("max_use")
	public Integer getMaxUse() {
		return maxUse;
	}

	@JsonProperty("min_order_amount")
	public BigDecimal getMinOrderAmount() {
		return minOrderAmount;
	}

	@JsonProperty("coupon_source")
	public CouponSource getCouponSource() {
		return couponSource;
	}

	@JsonProperty("max_discount")
	public BigDecimal getMaxDiscount() {
		return maxDiscount;
	}

	@JsonProperty("products")
	public Set<Integer> getProducts() {
		return products;
	}

	@JsonProperty("services")
	public Set<Integer> getServices() {
		return services;
	}

	@JsonProperty("user_types")
	public Set<Integer> getUserTypes() {
		return userTypes;
	}

	@JsonProperty("categories")
	public Set<Integer> getCategories() {
		return categories;
	}
	
	@JsonProperty("used_count")
	public Integer getUsedCount() {
		return usedCount;
	}

	public static Coupon createFromId(final Integer couponId) {
		return new Coupon(couponId, null, null, null, null, null, null, null,
				null, null, null, null, null, null, null, null);
	}

	public static Coupon createWithId(final Integer couponId,
			final Coupon coupon) {

		return new Coupon(couponId, coupon.getCode(), coupon.getDescription(),
				coupon.getValue(), coupon.getValueType(),
				coupon.getStartDate(), coupon.getEndDate(), coupon.getMaxUse(),
				coupon.getMinOrderAmount(), coupon.getCouponSource(),
				coupon.getMaxDiscount(), coupon.getProducts(),
				coupon.getServices(), coupon.getUserTypes(),
				coupon.getCategories(), coupon.getUsedCount());
	}

	public static Coupon createWithUserTypesServicesProductsAndCategories(
			final Set<Integer> userTypes, final Set<Integer> services,
			final Set<Integer> products, final Set<Integer> categories,
			final Coupon coupon) {

		return new Coupon(coupon.getId(), coupon.getCode(),
				coupon.getDescription(), coupon.getValue(),
				coupon.getValueType(), coupon.getStartDate(),
				coupon.getEndDate(), coupon.getMaxUse(),
				coupon.getMinOrderAmount(), coupon.getCouponSource(),
				coupon.getMaxDiscount(), products, services, userTypes,
				categories, coupon.getUsedCount());
	}

	public static Coupon createWithValueType(final PriceValueType valueType,
			final Coupon coupon) {

		return new Coupon(coupon.getId(), coupon.getCode(),
				coupon.getDescription(), coupon.getValue(), valueType,
				coupon.getStartDate(), coupon.getEndDate(), coupon.getMaxUse(),
				coupon.getMinOrderAmount(), coupon.getCouponSource(),
				coupon.getMaxDiscount(), coupon.getProducts(),
				coupon.getServices(), coupon.getUserTypes(),
				coupon.getCategories(), coupon.getUsedCount());
	}

	public static Coupon createWithUserType(
			final Set<Integer> userType,
			final Coupon coupon) {

		return new Coupon(coupon.getId(), coupon.getCode(), coupon.getDescription(),
				coupon.getValue(), coupon.getValueType(),
				coupon.getStartDate(), coupon.getEndDate(), coupon.getMaxUse(),
				coupon.getMinOrderAmount(), coupon.getCouponSource(),
				coupon.getMaxDiscount(), coupon.getProducts(),
				coupon.getServices(), userType, coupon.getCategories(), coupon.getUsedCount());
	}
}
