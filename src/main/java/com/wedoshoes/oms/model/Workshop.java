package com.wedoshoes.oms.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Workshop {
		  
	private final Byte id;
	private final String name;
	private final Long phone;
	private final String emailId;
	private final String streetAddress;
	private final String city;
	private final Short district;
	private final Byte state;
	private final String pin;
	private final String landmark;
	private final Long createDate;
	
	
	@SuppressWarnings("unused")
	private Workshop() {
		this(null, null, null, null, null, null, null, null, null, null, null);
	}
	
	

	public Workshop(final Byte id, final String name, final Long phone, final String emailId,
			final String streetAddress, final String city, final Short district, final Byte state,
			final String pin, final String landmark, final Long createDate) {
		this.id = id;
		this.name = name;
		this.phone = phone;
		this.emailId = emailId;
		this.streetAddress = streetAddress;
		this.city = city;
		this.district = district;
		this.state = state;
		this.pin = pin;
		this.landmark = landmark;
		this.createDate = createDate;
	}



	@JsonProperty("id")
	public Byte getId() {
		return id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("phone")
	public Long getPhone() {
		return phone;
	}

	@JsonProperty("email_id")
	public String getEmailId() {
		return emailId;
	}

	@JsonProperty("street_address")
	public String getStreetAddress() {
		return streetAddress;
	}

	@JsonProperty("city")
	public String getCity() {
		return city;
	}

	@JsonProperty("state")
	public Byte getState() {
		return state;
	}

	@JsonProperty("pin")
	public String getPin() {
		return pin;
	}

	@JsonProperty("landmark")
	public String getLandmark() {
		return landmark;
	}

	@JsonProperty("create_date")
	public Long getCreateDate() {
		return createDate;
	}

	@JsonProperty("district")
	public Short getDistrict() {
		return district;
	}

	
	
	
	public static Workshop createWithId(final Byte id, final Workshop workshop ) {
		return new Workshop(id, workshop.getName(), workshop.getPhone(), 
				workshop.getEmailId(), workshop.getStreetAddress(), workshop.getCity(), 
				workshop.getDistrict(), workshop.getState(), workshop.getPin(), workshop.getLandmark(), 
				workshop.getCreateDate());
	}
	
	public static Workshop createfromId(final Byte id) {
		return new Workshop(id, null, null, null, null, null, null, null, null, null, null);
	}
}
