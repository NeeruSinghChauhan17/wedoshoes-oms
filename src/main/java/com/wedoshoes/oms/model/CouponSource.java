package com.wedoshoes.oms.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class CouponSource {
	
	private final Integer id;
	private final String name;
	
	@SuppressWarnings("unused")
	private CouponSource() {
		this(null, null);
	}
	
	public CouponSource(final Integer id, final String name) {
		this.id = id;
		this.name = name;
	}
	
	@JsonProperty("id")
	public Integer getId() {
		return id;
	}
	
	@JsonProperty("name")
	public String getName() {
		return name;
	}

	
	public static CouponSource createWithId(final Integer id){
		return new CouponSource(id, null);
	}
	
	public static CouponSource createWithId(final Integer sourceId, final CouponSource couponSource) {
		return new CouponSource(sourceId, couponSource.getName());
	}

	public static CouponSource createFromId(Integer id) {
		return new CouponSource(id, null);
	}

}
