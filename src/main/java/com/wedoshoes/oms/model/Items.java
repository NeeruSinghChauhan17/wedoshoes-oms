package com.wedoshoes.oms.model;



import java.util.List;
import java.util.stream.Collectors;

import com.wedoshoes.oms.model.cost.EstimatedCost;
import com.wedoshoes.oms.model.cost.ItemSummary;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.WedoService;

public class Items {
	
	private Items() {
	}

	public static ItemSummary asItemSummary(final Item item) {
		final List<WedoService> services = item.getServices();
		final List<Integer>  serviceList = services != null ? services.stream().parallel().
				map(service -> service.getServiceId()).collect(Collectors.toList()) : null;
		return new ItemSummary(item.getParentServiceId(), item.getProductId(),
				serviceList , item.isExpressProcessing(), item.getSize(), item.isPacking());
	}
	
	public static EstimatedCost getEstimatedCost(final Item item) {
		
		return new EstimatedCost(item.getConvenienceCharge(), item.getExpressProcessingCharge(),
				null, item.getPackingCharge(), null, item.getServiceCost());
	}
	
	
}
