package com.wedoshoes.oms.model;

/**
 * 
 * @author Navrattan Yadav
 *
 */

public enum TransactionStatus {
	SUCCESS, FAILED, REFUND, RETURNED, CANCELLED, IN_PROGRESS;
}