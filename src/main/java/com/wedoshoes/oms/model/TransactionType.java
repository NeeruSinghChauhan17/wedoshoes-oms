package com.wedoshoes.oms.model;

/**
 * 
 * @author Navrattan Yadav
 *
 */
public enum TransactionType {

	DEBIT, CREDIT;
}
