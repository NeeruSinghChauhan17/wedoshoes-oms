package com.wedoshoes.oms.model.state;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(value=Include.NON_NULL)
public class UsersLocation {
	
	private final Long userId;
	private final Double latitude;
	private final Double longitude;

	@SuppressWarnings("unused")
	private UsersLocation(){
		this(null,null,null);
	}
	
	public UsersLocation(final Long userId, 
			final Double latitude, final Double longitude){
		this.userId=userId;
		this.latitude=latitude;
		this.longitude=longitude;
	}

	@JsonProperty("user_id")
	public Long getUserId() {
		return userId;
	}


	@JsonProperty("lat")
	public Double getLatitude() {
		return latitude;
	}

	@JsonProperty("lng")
	public Double getLongitude() {
		return longitude;
	}
	
	
}
