package com.wedoshoes.oms.model.state;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wedoshoes.oms.model.shipment.Shipment;

@JsonInclude(Include.NON_NULL)
public class StateTransition {

	private final Long id;
	private final Long userId;
	private final Long date;
	private final Byte state;
	private final Byte colPointId;
	private final Byte workshopId;
	private final String comment;
	private final Shipment shipment;
	private final UsersLocation usersLocation;
	
	public StateTransition() {
		this(null, null, null, null, null, null, null, null, null);
	}
	
	public StateTransition(final Long id, final Long userId, final Long date, final Byte state,
			final String comment, final Byte colPointId, final Byte workshopId, final Shipment shipment,
			final UsersLocation usersLocation) {
		this.id = id;
		this.userId = userId;
		this.date = date;
		this.state = state;
		this.comment = comment;
		this.colPointId = colPointId;
		this.workshopId = workshopId;
		this.shipment = shipment;
		this.usersLocation=usersLocation;
	}

	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	@JsonProperty("user_id")
	public Long getUserId() {
		return userId;
	}

	@JsonProperty("date")
	public Long getDate() {
		return date;
	}

	@JsonProperty("state")
	public Byte getState() {
		return state;
	}

	@JsonProperty("comment")
	public String getComment() {
		return comment;
	}
	
	@JsonProperty("col_point_id")
	public Byte getColPointId() {
		return colPointId;
	}
	
	@JsonProperty("workshop_id")
	public Byte getWorkshopId() {
		return workshopId;
	}
	
	
	@JsonProperty("shipment")
	public Shipment getShipment() {
		return shipment;
	}
	
	@JsonProperty("users_location")
	public UsersLocation getUsersLocation() {
		return usersLocation;
	}

	public static StateTransition createWithUserIdAndState( 
			final Long userId, final Byte state, final String comment) {
		return new StateTransition(null, userId, null, 
				state, comment, null, null, null, null);
	}
	
	public static StateTransition createWithUserId( 
			final Long userId, final StateTransition transition) {
		return new StateTransition(transition.getId(), userId, transition.getDate(), 
				transition.getState(), transition.getComment(), transition.getColPointId(),
				transition.getWorkshopId(), transition.getShipment(), transition.getUsersLocation());
	}
}
