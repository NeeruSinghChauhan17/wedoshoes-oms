package com.wedoshoes.oms.model.order;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Image {
	
	private final Long id;
	private final String hresUrl;
	private final String lresUrl;
	private final ImageType type;
	private final String description;
	
	
	@SuppressWarnings("unused")
	private Image() {
		this(null, null, null, null, null);
	}
	
	public Image(final Long id, final String hresUrl,final String lresUrl, 
			final ImageType type, final String description) {
		this.id = id;
		this.hresUrl = hresUrl;
		this.lresUrl = lresUrl;
		this.type = type;
		this.description = description;
	}

	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	@JsonProperty("hres_url")
	public String getHresUrl() {
		return hresUrl;
	}

	@JsonProperty("lres_url")
	public String getLresUrl() {
		return lresUrl;
	}

	@JsonProperty("type")
	public ImageType getType() {
		return type;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}
	
	public static Image createWithId(final Long id, final Image image) {
		return new Image(id, image.getHresUrl(), image.getLresUrl(), image.getType(), image.getDescription());
	}

}
