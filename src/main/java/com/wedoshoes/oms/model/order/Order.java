package com.wedoshoes.oms.model.order;

import static com.wedoshoes.oms.model.order.ItemState.CANCELLED;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Order {

	private final Long id;
	private final String customerName;
	private final String lastName;
	private final String cc;
	private final Long phone;
	private final Long alternatePhone;
	private final Long userId;
	private final Byte pickUpTimeSlotId;
	private final Long pickUpDate;
	private final Long createDate;
	private final Integer createTime;
	private final Long createrUserId;
	private final List<Payment> payments;
	private final List<Item> items;
	private final Address pickUpAddress;
	private final Address deliveryAddress;
	private final BigDecimal advancePayment;
	private final Long referredByUserId;
	private final Byte workshopId;
	

	@SuppressWarnings("unused")
	private Order() {
		this(null, null, null, null, null, null ,null,null, null, null, null, null,
				null, null, null, null, null, null, null);
	}
	
	public Order(final Long id, final Long userId,final String customerName, final String lastName, final String cc,
			final Long phone,final Long alternatePhone, final Byte pickUpTimeSlotId,
			final Long pickUpDate, final List<Payment> payments,  final Long createDate,
			final Integer createTime,
			final Long createrUserId, final List<Item> items,
			final Address pickUpAddress, final Address deliveryAddress,
			final BigDecimal advancePayment, final Long referredByUserId,
			final Byte workshopId) {
		this.id = id;
		this.userId = userId;
		this.customerName = customerName;
		this.cc = cc;
		this.phone = phone;
		this.alternatePhone = alternatePhone;
		this.pickUpTimeSlotId = pickUpTimeSlotId;
		this.pickUpDate = pickUpDate;
		this.payments = payments;
		this.createDate = createDate;
		this.createTime = createTime;
		this.createrUserId = createrUserId;
		this.items = items;
		this.deliveryAddress = deliveryAddress;
		this.pickUpAddress = pickUpAddress;
		this.advancePayment = advancePayment;
		this.referredByUserId = referredByUserId;
		this.workshopId = workshopId;
		this.lastName=lastName;
	}

	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	@JsonProperty("pickup_timeslot_id")
	public Byte getPickUpTimeSlotId() {
		return pickUpTimeSlotId;
	}

	@JsonProperty("pickup_date")
	public Long getPickUpDate() {
		return pickUpDate;
	}

	@JsonProperty("payments")
	public List<Payment> getPayments() {
		return payments;
	}

	@JsonProperty("create_date")
	public Long getCreateDate() {
		return createDate;
	}

	@JsonProperty("crater_user_id")
	public Long getCreaterUserId() {
		return createrUserId;
	}

	@JsonProperty("user_id")
	public Long getUserId() {
		return userId;
	}

	@JsonProperty("items")
	public List<Item> getItems() {
		return items;
	}
	
	@JsonProperty("pick_up_address")
	public Address getPickUpAddress() {
		return pickUpAddress;
	}

	@JsonProperty("delivery_address")
	public Address getDeliveryAddress() {
		return deliveryAddress;
	}
	
	@JsonProperty("customer_name")
	public String getCustomerName() {
		return customerName;
	}

	@JsonProperty("phone")
	public Long getPhone() {
		return phone;
	}

	@JsonProperty("last_name")
	public String getLastName() {
		return lastName;
	}

	@JsonProperty("cc")
	public String getCc() {
		return cc;
	}

	@JsonProperty("alternate_phone")
	public Long getAlternatePhone() {
		return alternatePhone;
	}
	
	@JsonProperty("advance_payment")
	public BigDecimal getAdvancePayment() {
		return advancePayment;
	}
	
	@JsonProperty("referred_by_user_id")
	public Long getReferredByUserId() {
		return referredByUserId;
	}
	
	@JsonProperty("create_time")
	public Integer getCreateTime() {
		return createTime;
	}
	
	@JsonProperty("workshop_id")
	public Byte getWorkshopId() {
		return workshopId;
	}

	@JsonProperty("total_cost")
	public BigDecimal calculateTotalCost() {
		if(items != null) {
			BigDecimal cost = BigDecimal.ZERO;
			for(Item item : items) {
				if(item.getState() != CANCELLED)  {
					final BigDecimal itemCost = item.calculateTotalCost();
					if(itemCost != null) {
						cost = cost.add(itemCost);
					}
				}
			}
			if(cost != BigDecimal.ZERO) {
				return cost.setScale(2, RoundingMode.UP);
			}
		}
		return null;
	}
	
	@JsonProperty("total_cost_credited")
	public BigDecimal calculateTotalCostCredited() {
		BigDecimal totalCostCredited = BigDecimal.ZERO;
		
		if(advancePayment != null) {
			totalCostCredited = totalCostCredited.add(advancePayment);
		}
		
		if(items != null) {
			for(Item item : items) {
				final BigDecimal itemCostCredited = item.getCostCreadited();
				if(itemCostCredited != null) {
					totalCostCredited = totalCostCredited.add(itemCostCredited);
				}
			}
			if(totalCostCredited != BigDecimal.ZERO) {
				return totalCostCredited;
			}
		}
		return null;
	}
	
	public static Order createFromId(final Long orderId) {
		return new Order(orderId, null, null, null, null, null,null, null, null,
				null,null, null, null, null, null, null, null,null, null);
	}
	
	public static Order createWithId(final Long orderId, final Order order) {
		return new Order(orderId, order.getUserId(),order.getCustomerName(), order.getLastName(),order.getCc(), order.getPhone(),
				order.getAlternatePhone(),order.getPickUpTimeSlotId(),
				order.getPickUpDate(), order.getPayments(), order.getCreateDate(), order.getCreateTime(),
				order.getCreaterUserId(), order.getItems(), order.getPickUpAddress(),
				order.getDeliveryAddress(), order.getAdvancePayment(),
				order.getReferredByUserId(), order.getWorkshopId());
	}
	
	public static Order createWithUserId(final Long userId, final Order order) {
		return new Order(order.getId(), userId,order.getCustomerName(), order.getLastName(),order.getCc(), order.getPhone(),
				order.getAlternatePhone(),order.getPickUpTimeSlotId(),
				order.getPickUpDate(), order.getPayments(), order.getCreateDate(), order.getCreateTime(),
				order.getCreaterUserId(), order.getItems(), order.getPickUpAddress(),
				order.getDeliveryAddress(), order.getAdvancePayment(), order.getReferredByUserId(),
				order.getWorkshopId());
	}

	
	public static Order createWithItems(final Order order, final List<Item> items) {
		return new Order(order.getId(), order.getUserId(),order.getCustomerName(), order.getLastName(),order.getCc(), order.getPhone(),
				order.getAlternatePhone(), order.getPickUpTimeSlotId(),
				order.getPickUpDate(), order.getPayments(), order.getCreateDate(), order.getCreateTime(),
				order.getCreaterUserId(), items,order.getPickUpAddress(), order.getDeliveryAddress(),
				order.getAdvancePayment(), order.getReferredByUserId(), order.getWorkshopId());
	}
	
	public static Order createWithAdvancePayment(final Long orderId, final BigDecimal advancePayment ) {
		return new Order(orderId, null, null, null, null,null, null, null, null, null, null,
				null, null, null, null, null, advancePayment, null, null);
	}
	
	public static Order createFromIdAndItems(final Long id, final List<Item> items) {
		return new Order(id, null,null, null, null,null, null, null, null, null, 
				null, null, null, items, null, null, null, null, null);
	}

	public static Order createWithDeliveryAndPickupAddress(final Address pickupAddress, final Address deliveryAddress, 
			final Order order) {
		return new Order(order.getId(), order.getUserId(),order.getCustomerName(), order.getLastName(),order.getCc(), order.getPhone(),
				order.getAlternatePhone(), order.getPickUpTimeSlotId(),
				order.getPickUpDate(), order.getPayments(), order.getCreateDate(), order.getCreateTime(),
				order.getCreaterUserId(), order.getItems(), pickupAddress, deliveryAddress,
				order.getAdvancePayment(), order.getReferredByUserId(), order.getWorkshopId());
	}

}
