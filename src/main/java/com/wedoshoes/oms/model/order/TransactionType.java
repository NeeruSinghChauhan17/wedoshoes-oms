package com.wedoshoes.oms.model.order;

import java.util.HashMap;
import java.util.Map;

public enum TransactionType {

	CREDIT(1), DEBIT(2);
	
	
	private static final Map<Integer, TransactionType> CODE_TO_TYPE =  new HashMap<>();
	
	static {
		for(TransactionType paymentMode : TransactionType.values()) {
			CODE_TO_TYPE.put(paymentMode.getCode(), paymentMode);
		}
	}
	
	private final Integer code;

	private TransactionType(Integer code) {
		this.code = code;
	}
	
	public Integer getCode() {
		return code;
	}
	
	public static TransactionType codeToType(final Integer code) {
		return CODE_TO_TYPE.get(code);
	}

}
