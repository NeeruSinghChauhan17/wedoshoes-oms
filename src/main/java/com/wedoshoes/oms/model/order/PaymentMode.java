package com.wedoshoes.oms.model.order;

import java.util.HashMap;
import java.util.Map;

public enum PaymentMode {

	COD(1), PAYU(2), PAYTM(3), WEDO_CREDIT(4), PAID_IN_URBAN_CLAP(5),CHEQUE(6),BANK(7);
	
	
	private static final Map<Integer, PaymentMode> CODE_TO_TYPE =  new HashMap<>();
	
	static {
		for(PaymentMode paymentMode : PaymentMode.values()) {
			CODE_TO_TYPE.put(paymentMode.getCode(), paymentMode);
		}
	}
	
	private final Integer code;

	private PaymentMode(Integer code) {
		this.code = code;
	}
	
	public Integer getCode() {
		return code;
	}


	public static PaymentMode codeToType(final Integer code) {
		return CODE_TO_TYPE.get(code);
	}
	
	public static PaymentMode defaultMode() {
		return COD;
	}
}
