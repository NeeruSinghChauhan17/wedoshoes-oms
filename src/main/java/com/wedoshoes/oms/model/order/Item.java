package com.wedoshoes.oms.model.order;

import static com.wedoshoes.oms.dao.impl.DefaultUtils.defaultBigDecimalIfNull;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wedoshoes.oms.model.Size;
import com.wedoshoes.oms.model.cost.EstimatedCost;
import com.wedoshoes.oms.model.payment.PaymentAdjustmentType;
import com.wedoshoes.oms.model.state.StateTransition;
import com.wedoshoes.oms.util.DateUtil;

@JsonInclude(Include.NON_NULL)
public class Item {

	private final Long id;
	private final Long orderId;
	private final Byte state;
	//private final BigDecimal taxPercentage;
	private final GST gst;
	private final Long createDate;
	private final Integer createTime;
	private final Long deliveryDate;
	private final Integer deliveryTime;
	private final Long estDeliveryDate;
	private final Integer estDeliveryTime;
	private final Byte deliveryTimeSlotId;
	private final BigDecimal convenienceCharge;
	private final BigDecimal expressProcessingCharge;
	private final BigDecimal packingCharge;
	private final Integer parentServiceId;
	private final Integer categoryId;
	private final Integer productId;
	private final Integer size;
	private final Integer brandId;
	private final Size sizeType;
	private final List<WedoService> services;
	private final List<Discount> discounts;
	private final List<Image> images;
	private final Boolean expressProcessing;
	private final Boolean packing;
	private final BigDecimal costCreadited;
	private final BigDecimal badDebts;
	private final BigDecimal adjustment;
	private final PaymentAdjustmentType adjustmentType;
	private final List<StateTransition> stateTransitions;
	private final Byte rating;
	private final Byte priority;
	
	@SuppressWarnings("unused")
	private Item() {
		this(null, null, null, null, null, null, null, null, null, null,
				null, null, null, null, null, null, null, null, null,null,
				null, null, null,null, null, null, null, null, null, null, null, null);
	}
	
	public Item(final Long id, final Long orderId, final Byte state,final GST gst
			/*final BigDecimal taxPercentage*/, final Long createDate, final Integer createTime,
			final Long deliveryDate,final Integer deliveryTime, final Long estDeliveryDate,
			final Integer estDeliveryTime,final Byte deliveryTimeSlotId,
			final BigDecimal convenienceCharge,final BigDecimal expressProcessingCharge,
			final Integer parentServiceId,final Integer categoryId, final Integer productId,
			final Integer size,	final Integer brandId, final Size sizeType,
			final List<WedoService> services, final List<Discount> discounts,
			final List<Image> images, final Boolean expressProcessing,
			final Boolean packing, final BigDecimal packingCharge,
			final BigDecimal costCreadited,	final BigDecimal badDebts,
			final BigDecimal adjustment, final PaymentAdjustmentType adjustmentType,
			final List<StateTransition> stateTransitions, final Byte rating, final Byte priority) {
		this.id = id;
		this.orderId = orderId;
		this.state = state;
		this.deliveryTimeSlotId = deliveryTimeSlotId;
		this.gst=gst;
//		this.taxPercentage = taxPercentage;
		this.createDate = createDate;
		this.createTime = createTime;
		this.deliveryDate = deliveryDate;
		this.deliveryTime = deliveryTime;
		this.estDeliveryDate = estDeliveryDate;
		this.estDeliveryTime = estDeliveryTime;
		this.convenienceCharge = convenienceCharge;
		this.expressProcessingCharge = expressProcessingCharge;
		this.parentServiceId = parentServiceId;
		this.categoryId = categoryId;
		this.productId = productId;
		this.size = size;
		this.brandId = brandId;
		this.sizeType = sizeType;
		this.services = services;
		this.discounts = discounts;
		this.images = images;
		this.expressProcessing = expressProcessing;
		this.packing = packing;
		this.packingCharge = packingCharge;
		this.costCreadited = costCreadited;
		this.badDebts = badDebts;
		this.adjustment = adjustment;
		this.adjustmentType = adjustmentType;
		this.stateTransitions = stateTransitions;
		this.rating = rating;
		this.priority = priority;
	}

	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	@JsonProperty("order_id")
	public Long getOrderId() {
		return orderId;
	}

	@JsonProperty("state")
	public Byte getState() {
		return state;
	}

//	@JsonProperty("tax_percentage")
//	public BigDecimal getTaxPercentage() {
//		return taxPercentage;
//	}

	@JsonProperty("gst")
	public GST getGST(){
		return gst;
	}
	
	@JsonProperty("create_date")
	public Long getCreateDate() {
		return createDate;
	}

	@JsonProperty("delivery_date")
	public Long getDeliveryDate() {
		return deliveryDate;
	}

	@JsonProperty("convenience_charge")
	public BigDecimal getConvenienceCharge() {
		return convenienceCharge;
	}

	@JsonProperty("express_processing_charge")
	public BigDecimal getExpressProcessingCharge() {
		return expressProcessingCharge;
	}

	@JsonProperty("parent_service_id")
	public Integer getParentServiceId() {
		return parentServiceId;
	}

	@JsonProperty("category_id")
	public Integer getCategoryId() {
		return categoryId;
	}

	@JsonProperty("product_id")
	public Integer getProductId() {
		return productId;
	}

	@JsonProperty("size")
	public Integer getSize() {
		return size;
	}

	@JsonProperty("brand_id")
	public Integer getBrandId() {
		return brandId;
	}

	@JsonProperty("size_type")
	public Size getSizeType() {
		return sizeType;
	}

	@JsonProperty("services")
	public List<WedoService> getServices() {
		return services;
	}

	@JsonProperty("discounts")
	public List<Discount> getDiscounts() {
		return discounts;
	}
	
	@JsonProperty("images") 
	public List<Image> getImages() {
		return images;
	}
	
	@JsonProperty("is_express_processing")
	public Boolean isExpressProcessing() {
		return expressProcessing;
	}

	@JsonProperty("packing_charge")
	public BigDecimal getPackingCharge() {
		return packingCharge;
	}

	@JsonProperty("is_packing")
	public Boolean isPacking() {
		return packing;
	}
	
	@JsonProperty("cost_creadited")
	public BigDecimal getCostCreadited() {
		return costCreadited;
	}

	@JsonProperty("bad_debts")
	public BigDecimal getBadDebts() {
		return badDebts;
	}
	
	@JsonProperty("adjustment")
	public BigDecimal getAdjustment() {
		return adjustment;
	}

	@JsonProperty("adjustment_type")
	public PaymentAdjustmentType getAdjustmentType() {
		return adjustmentType;
	}

	@JsonProperty("state_transitions")
	public List<StateTransition> getStateTransitions() {
		return stateTransitions;
	}
	
	@JsonProperty("rating")
	public Byte getRating() {
		return rating;
	}
	
	@JsonProperty("est_delivery_date")
	public Long getEstDeliveryDate() {
		return estDeliveryDate;
	}
	
	@JsonProperty("priority")
	public Byte getPriority() {
		return priority;
	}
	
	@JsonProperty("create_time")
	public Integer getCreateTime() {
		return createTime;
	}

	@JsonProperty("delivery_time")
	public Integer getDeliveryTime() {
		return deliveryTime;
	}
	
	@JsonProperty("delivery_timeslot_id")
	public Byte getDeliveryTimeSlotId() {
		return deliveryTimeSlotId;
	}

	@JsonProperty("est_delivery_time")
	public Integer getEstDeliveryTime() {
		return estDeliveryTime;
	}

	@JsonProperty("services_cost")
	public BigDecimal getServiceCost() {
		if(services != null) {
			final BigDecimal serviceCost= services.stream().
					map(service -> service.calculateTotalCost()).
					reduce(BigDecimal.ZERO, (x,y) -> x.add(y));
			
			if(serviceCost != BigDecimal.ZERO) {
				return serviceCost;
			}
		}
		return BigDecimal.ZERO;
	}
	
	@JsonProperty("total_discount")
	public BigDecimal getDiscount(final BigDecimal serviceCostWithoutDiscount ) {
		final BigDecimal totalCostWithoutDiscount=serviceCostWithoutDiscount.add(defaultBigDecimalIfNull(convenienceCharge))
		.add(defaultBigDecimalIfNull(expressProcessingCharge));
		if(discounts != null) {
			return discounts.stream().
					map(discount -> getValue(totalCostWithoutDiscount, discount)).
					collect(Collectors.toList()).stream().reduce(BigDecimal.ZERO, (x,y) -> x.add(y));
		}
		return BigDecimal.ZERO;
	}
	
	@JsonProperty("item_total_cost")
	public BigDecimal calculateTotalCost() {
		final BigDecimal serviceCost = getServiceCost();
		if(services != null && serviceCost != null) {			
			final BigDecimal discount = getDiscount(serviceCost); 
			final BigDecimal totalCost = serviceCost.
					subtract(defaultBigDecimalIfNull(discount)).
					add(defaultBigDecimalIfNull(convenienceCharge)).
					add(defaultBigDecimalIfNull(expressProcessingCharge)).
					add(defaultBigDecimalIfNull(packingCharge));
			//me-crt-prblm-nt-deflt-st
			//tax % is replaced by gst.11_July_2017
			final BigDecimal tax = totalCost.multiply(defaultBigDecimalIfNull(gst!=null ?gst.getGstValue():BigDecimal.ZERO)). divide(BigDecimal.valueOf(100));
			
			return totalCost.add(tax).setScale(2, RoundingMode.UP); 
		}
		return BigDecimal.ZERO;
	}
	
	@JsonProperty("tax_amount")
	public BigDecimal calculateTaxAmount() {
		final BigDecimal serviceCost = getServiceCost();
		if(services != null && serviceCost != null) {
			final BigDecimal discount = getDiscount(serviceCost);
			final BigDecimal totalCost = serviceCost.
					subtract(defaultBigDecimalIfNull(discount)).
					add(defaultBigDecimalIfNull(convenienceCharge)).
					add(defaultBigDecimalIfNull(expressProcessingCharge));
			//me-crt-prblm-nt-deflt-st
			final BigDecimal taxAmount = totalCost.multiply(defaultBigDecimalIfNull(gst!=null ?gst.getGstValue():BigDecimal.ZERO)).divide(BigDecimal.valueOf(100));
			
			return taxAmount.setScale(2, RoundingMode.UP);
 		}
		return BigDecimal.ZERO;
	}
	
	public static Item createWithCostCredited(final BigDecimal costCreadited, final Item item) {
		return new Item(item.getId(), item.getOrderId(), item.getState(),
				item.getGST(), item.getCreateDate(),item.getCreateTime(),
				item.getDeliveryDate(),item.getDeliveryTime(),
				item.getEstDeliveryDate(),item.getEstDeliveryTime(),item.getDeliveryTimeSlotId(),
				item.getConvenienceCharge(), item.getExpressProcessingCharge(),
				item.getParentServiceId(), item.getCategoryId(), item.getProductId(),
				item.getSize(), item.getBrandId(), item.getSizeType(), item.getServices(),
				item.getDiscounts(), item.getImages(), item.isExpressProcessing(),
				item.isPacking(), item.getPackingCharge(),
				costCreadited, item.getBadDebts(),
				item.getAdjustment(), item.getAdjustmentType(), item.getStateTransitions(),
				item.getRating(), item.getPriority());
	}
	
	public static Item createItemSummay(final Long id, final Long orderId, final Byte state, 
			final GST gst, final Long createDate, final Integer createTime,
			final Long deliveryDate,final Integer deliveryTime,
			final Long estDeliveryDate,final Integer estDeliveryTime,
			final Byte deliveryTimeslotId,
			final BigDecimal convenienceCharge,final BigDecimal expressProcessingCharge,
			final Integer parentServiceId,final Integer categoryId, final Integer productId,
			final Integer size,	final Integer brandId, final Size sizeType,
			final Boolean expressProcessing,final Boolean packing, final BigDecimal packingCharge,
			final BigDecimal costCreadited,	final BigDecimal badDebts,
			final BigDecimal adjustment, final PaymentAdjustmentType adjustmentType, final Byte rating,
			final Byte priority) {
		return new Item(id, orderId, state,  gst, createDate,createTime,
				deliveryDate,deliveryTime,estDeliveryDate,estDeliveryTime, deliveryTimeslotId,
				convenienceCharge, expressProcessingCharge, parentServiceId, categoryId,
				productId, size, brandId, sizeType, null, null, null, expressProcessing,
				packing, packingCharge, costCreadited, badDebts, adjustment, adjustmentType,
				null, rating, priority);
	}
	
	public static Item createWithItemId(final Long itemId, final Item item) {
		return new Item(itemId, item.getOrderId(), item.getState(), 
				item.getGST(),  item.getCreateDate(),item.getCreateTime(),
				item.getDeliveryDate(),item.getDeliveryTime(),
				item.getEstDeliveryDate(),item.getEstDeliveryTime(),item.getDeliveryTimeSlotId(),
				item.getConvenienceCharge(), item.getExpressProcessingCharge(),
				item.getParentServiceId(), item.getCategoryId(), item.getProductId(),
				item.getSize(), item.getBrandId(), item.getSizeType(), item.getServices(),
				item.getDiscounts(), item.getImages(), item.isExpressProcessing(),
				item.isPacking(), item.getPackingCharge(),
				item.getCostCreadited(), item.getBadDebts(),
				item.getAdjustment(), item.getAdjustmentType(), item.getStateTransitions(),
				item.getRating(),item.getPriority());
	}
	
	
	public static Item createWithItemIdAndOrderId(final Long itemId,final Long orderId, final Item item) {
		return new Item(itemId, orderId, item.getState(), 
				item.getGST(),  item.getCreateDate(),item.getCreateTime(),
				item.getDeliveryDate(),item.getDeliveryTime(),
				item.getEstDeliveryDate(),item.getEstDeliveryTime(),item.getDeliveryTimeSlotId(),
				item.getConvenienceCharge(), item.getExpressProcessingCharge(),
				item.getParentServiceId(), item.getCategoryId(), item.getProductId(),
				item.getSize(), item.getBrandId(), item.getSizeType(), item.getServices(),
				item.getDiscounts(), item.getImages(), item.isExpressProcessing(),
				item.isPacking(), item.getPackingCharge(),
				item.getCostCreadited(), item.getBadDebts(),
				item.getAdjustment(), item.getAdjustmentType(),
				item.getStateTransitions(), item.getRating(), item.getPriority());
	}
	
	
	public static Item createWithItemIdAndOrderIdAndProcessingCharges(final Long itemId,final Long orderId, final Item item, final BigDecimal processingCharge) {
		return new Item(itemId, orderId, item.getState(), 
				item.getGST(),  item.getCreateDate(),item.getCreateTime(),
				item.getDeliveryDate(),item.getDeliveryTime(),
				item.getEstDeliveryDate(),item.getEstDeliveryTime(),item.getDeliveryTimeSlotId(),
				item.getConvenienceCharge(), processingCharge,
				item.getParentServiceId(), item.getCategoryId(), item.getProductId(),
				item.getSize(), item.getBrandId(), item.getSizeType(), item.getServices(),
				item.getDiscounts(), item.getImages(), item.isExpressProcessing(),
				item.isPacking(), item.getPackingCharge(),
				item.getCostCreadited(), item.getBadDebts(),
				item.getAdjustment(), item.getAdjustmentType(),
				item.getStateTransitions(), item.getRating(), item.getPriority());
	}
	
	public static Item createFromItemId(final Long itemId) {
		return new Item(itemId, null, null, null, null, null, null, null, null,
				null, null, null, null, null, null, null, null, null, null, null,
				null, null, null, null, null, null,null, null, null, null, null, null);
	}
	
	
	public static Item createWithState(final Byte state, final Item item) {
		return new Item(item.getId(), item.getOrderId(), state, 
				item.getGST(),  item.getCreateDate(),item.getCreateTime(),
				item.getDeliveryDate(),item.getDeliveryTime(),
				item.getEstDeliveryDate(),item.getEstDeliveryTime(),item.getDeliveryTimeSlotId(),
				item.getConvenienceCharge(), item.getExpressProcessingCharge(),
				item.getParentServiceId(), item.getCategoryId(), item.getProductId(),
				item.getSize(), item.getBrandId(), item.getSizeType(), item.getServices(),
				item.getDiscounts(), item.getImages(), item.isExpressProcessing(),
				item.isPacking(), item.getPackingCharge(),
				item.getCostCreadited(), item.getBadDebts(),
				item.getAdjustment(), item.getAdjustmentType(),item.getStateTransitions(),
				item.getRating(), item.getPriority());
	}
	
	public static Item createWithDiscounts(final List<Discount> discounts, final Item item) {
		return new Item(item.getId(), item.getOrderId(), item.getState(), 
				item.getGST(),  item.getCreateDate(),item.getCreateTime(),
				item.getDeliveryDate(),item.getDeliveryTime(),
				item.getEstDeliveryDate(),item.getEstDeliveryTime(), item.getDeliveryTimeSlotId(),
				item.getConvenienceCharge(), item.getExpressProcessingCharge(),
				item.getParentServiceId(), item.getCategoryId(), item.getProductId(),
				item.getSize(), item.getBrandId(), item.getSizeType(), item.getServices(),
				discounts, item.getImages(), item.isExpressProcessing(),
				item.isPacking(), item.getPackingCharge(),
				item.getCostCreadited(), item.getBadDebts(),
				item.getAdjustment(), item.getAdjustmentType(),item.getStateTransitions(),
				item.getRating(), item.getPriority());
	}
	
	
	public static Item createWithServicesAndDiscountsAndImages(final Item item, 
			final Collection<WedoService> services, final Collection<Discount> discounts,
			final Collection<Image> images) {
		
		return new Item(item.getId(), item.getOrderId(), item.getState(), 
				item.getGST(),  item.getCreateDate(),item.getCreateTime(),
				item.getDeliveryDate(),item.getDeliveryTime(),
				item.getEstDeliveryDate(),item.getEstDeliveryTime(), item.getDeliveryTimeSlotId(),
				item.getConvenienceCharge(), item.getExpressProcessingCharge(),
				item.getParentServiceId(), item.getCategoryId(), item.getProductId(),
				item.getSize(), item.getBrandId(), item.getSizeType(),
				services.isEmpty() ? null : new ArrayList<WedoService>(services),
				discounts.isEmpty() ? null : new ArrayList<>(discounts),
						images.isEmpty() ? null : new ArrayList<>(images),
						item.isExpressProcessing(), item.isPacking(),
						item.getPackingCharge(),
						item.getCostCreadited(), item.getBadDebts(),
						item.getAdjustment(), item.getAdjustmentType(), 
						item.getStateTransitions(), item.getRating(), item.getPriority());
	}
	
	
	public static Item createWithServicesAndDiscounts(final Item item, 
			final Collection<WedoService> services, final Collection<Discount> discounts) {
		
		return new Item(item.getId(), item.getOrderId(), item.getState(), 
				item.getGST(),  item.getCreateDate(),item.getCreateTime(),
				item.getDeliveryDate(),item.getDeliveryTime(),
				item.getEstDeliveryDate(),item.getEstDeliveryTime(), item.getDeliveryTimeSlotId(),
				item.getConvenienceCharge(), item.getExpressProcessingCharge(),
				item.getParentServiceId(), item.getCategoryId(), item.getProductId(),
				item.getSize(), item.getBrandId(), item.getSizeType(),
				services.isEmpty() ? null : new ArrayList<WedoService>(services),
				discounts.isEmpty() ? null : new ArrayList<>(discounts),
						item.getImages(),item.isExpressProcessing(), item.isPacking(),
						item.getPackingCharge(),
						item.getCostCreadited(), item.getBadDebts(),
						item.getAdjustment(), item.getAdjustmentType(), 
						item.getStateTransitions(), item.getRating(), item.getPriority());
	}
	
	@JsonIgnore
	private BigDecimal getValue(final BigDecimal serviceCost, final Discount discount) {
		if(discount.getValueType() == PriceValueType.PERCENTAGE) {
			return serviceCost.multiply(discount.getValue()).divide(BigDecimal.valueOf(100));
		}
		return discount.getValue();
	}
	
	public static Item createWithEstimatedCostAndDeliveryDate(final Item item, final 
			EstimatedCost cost, final Long deliveryDate, final Integer deliveryTime,
			final Byte deliveryTimeslotId) {
		final Map<Integer, BigDecimal> servicesCost = cost.getServicesCost();
		return new Item(item.getId(), item.getOrderId(), item.getState(), 
				cost.getGST(), DateUtil.getEpochDate(),LocalTime.now().toSecondOfDay(),
				deliveryDate,deliveryTime, deliveryDate,deliveryTime, deliveryTimeslotId,
				cost.getConvenienceCharge(), cost.getExpressProcessingCharge(),
				item.getParentServiceId(), item.getCategoryId(), item.getProductId(),
				item.getSize(), item.getBrandId(), item.getSizeType(),
				item.getServices().stream().map(service -> 
				WedoService.createWithCost(service, 
						servicesCost.get(service.getServiceId()))).collect(Collectors.toList()),
				item.getDiscounts(), item.getImages(), item.isExpressProcessing(),
				item.isPacking(), cost.getPackingCharge(),
				item.getCostCreadited(), item.getBadDebts(),
				item.getAdjustment(), item.getAdjustmentType(), item.getStateTransitions(),
				item.getRating(), item.getPriority());
		
	}
	
	public static Item createWithCostingDetail(final Long id, final BigDecimal adjustment,
			final PaymentAdjustmentType adjustmentType, final BigDecimal costCreadited, final BigDecimal badDebts,
			final GST gst, final BigDecimal convenienceCharge, 
			final BigDecimal expressProcessingCharge, final BigDecimal packingCharge,final Byte state ) {
		return new Item(id, null, state, gst, null, null, null, null, null, null, null,
				convenienceCharge, expressProcessingCharge,
				null, null, null, null, null, null, null, null, null, null, null, 
				packingCharge, costCreadited, badDebts, adjustment, adjustmentType, null, null, null);
	}
	
	public static Item createWithStateTransition(final Item item, 
			final List<StateTransition> transitions) {
		return new Item(item.getId(), item.getOrderId(), item.getState(),
				item.getGST(),  item.getCreateDate(),item.getCreateTime(),
				item.getDeliveryDate(),item.getDeliveryTime(),
				item.getEstDeliveryDate(),item.getEstDeliveryTime(),item.getDeliveryTimeSlotId(),
				item.getConvenienceCharge(), item.getExpressProcessingCharge(),
				item.getParentServiceId(), item.getCategoryId(), item.getProductId(),
				item.getSize(), item.getBrandId(), item.getSizeType(), item.getServices(),
				item.getDiscounts(), item.getImages(), item.isExpressProcessing(),
				item.isPacking(), item.getPackingCharge(),
				item.getCostCreadited(), item.getBadDebts(),
				item.getAdjustment(), item.getAdjustmentType(),transitions,
				item.getRating(), item.getPriority());
	}
	
	public static Item createFromIdAndState(final Long id, final Byte state) {
		return new Item(id, null, state, null, null, null, null, null, null, null, 
				null, null, null, null, null, null, null, null, null, null, 
				null, null, null, null, null, null, null, null, null, null, null, null);
	}
	
	public static Item createWithDeliveryDate(final Item item,final Long deliveryDate){
        return new Item(item.getId(), item.getOrderId(), item.getState(),
                item.getGST(),  item.getCreateDate(),item.getCreateTime(),
                deliveryDate,item.getDeliveryTime(),
                item.getEstDeliveryDate(),item.getEstDeliveryTime(),item.getDeliveryTimeSlotId(),
                item.getConvenienceCharge(), item.getExpressProcessingCharge(),
                item.getParentServiceId(), item.getCategoryId(), item.getProductId(),
                item.getSize(), item.getBrandId(), item.getSizeType(), item.getServices(),
                item.getDiscounts(), item.getImages(), item.isExpressProcessing(),
                item.isPacking(), item.getPackingCharge(),
                item.getCostCreadited(), item.getBadDebts(),
                item.getAdjustment(), item.getAdjustmentType(),item.getStateTransitions(),
                item.getRating(), item.getPriority());
    }

	public static Item createWithEstimatedCost(final Item item,
			final EstimatedCost cost) {
		return new Item(item.getId(), item.getOrderId(), item.getState(), 
				cost.getGST(), item.getCreateDate(),item.getCreateTime(),
				item.getDeliveryDate(),item.getDeliveryTime(),
				item.getEstDeliveryDate(),item.getEstDeliveryTime(),item.getDeliveryTimeSlotId(),
				cost.getConvenienceCharge(), cost.getExpressProcessingCharge(),
				item.getParentServiceId(), item.getCategoryId(), item.getProductId(),
				item.getSize(), item.getBrandId(), item.getSizeType(), item.getServices(),
				item.getDiscounts(), item.getImages(), item.isExpressProcessing(),
				item.isPacking(), cost.getPackingCharge(),
				item.getCostCreadited(), item.getBadDebts(),
				item.getAdjustment(), item.getAdjustmentType(),
				item.getStateTransitions(), item.getRating(), item.getPriority());
	}

	public static Item createWithGST(final Item item,final GST gst){
        return new Item(item.getId(), item.getOrderId(), item.getState(),
                gst,  item.getCreateDate(),item.getCreateTime(),
                item.getDeliveryDate(),item.getDeliveryTime(),
                item.getEstDeliveryDate(),item.getEstDeliveryTime(),item.getDeliveryTimeSlotId(),
                item.getConvenienceCharge(), item.getExpressProcessingCharge(),
                item.getParentServiceId(), item.getCategoryId(), item.getProductId(),
                item.getSize(), item.getBrandId(), item.getSizeType(), item.getServices(),
                item.getDiscounts(), item.getImages(), item.isExpressProcessing(),
                item.isPacking(), item.getPackingCharge(),
                item.getCostCreadited(), item.getBadDebts(),
                item.getAdjustment(), item.getAdjustmentType(),item.getStateTransitions(),
                item.getRating(), item.getPriority());
    }

	

}