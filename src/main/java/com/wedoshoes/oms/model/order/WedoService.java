package com.wedoshoes.oms.model.order;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class WedoService {

	private final Integer serviceId;
	private final Short quantity;
	private final BigDecimal costPerService;
	
	@SuppressWarnings("unused")
	private WedoService() {
		this(null, null, null);
	}
	
	public WedoService(final Integer serviceId, final Short quantity,
			final BigDecimal costPerService) {
		this.serviceId = serviceId;
		this.quantity = quantity;
		this.costPerService = costPerService;
	}
	
	@JsonProperty("service_id")
	public Integer getServiceId() {
		return serviceId;
	}

	@JsonProperty("quantity")
	public Short getQuantity() {
		return quantity;
	}

	@JsonProperty("cost_per_service")
	public BigDecimal getCostPerService() {
		return costPerService;
	}
	
	@JsonIgnore
	public BigDecimal calculateTotalCost() {
		if(costPerService != null) {
			return costPerService.multiply(BigDecimal.valueOf(quantity ));
		}
		return BigDecimal.ZERO;
	}
	
	public static WedoService createWithCost(final WedoService service , final BigDecimal costPerService) {
		return new WedoService(service.getServiceId(), service.getQuantity(), costPerService);
	}
}
