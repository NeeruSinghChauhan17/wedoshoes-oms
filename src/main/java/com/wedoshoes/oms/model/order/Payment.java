
package com.wedoshoes.oms.model.order;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wedoshoes.oms.model.payment.PaymentAdjustment;

@JsonInclude(Include.NON_NULL)
public class Payment {

	private final Long id;
	private final Long userId;
	private final String txnId;
	private final PaymentMode mode;
	private final BigDecimal amount;
	private final Long date;
	private final TransactionType transactionType;
	private final String description;
	private final String checksum;
	private final String status;
	private final PaymentAdjustment adjustment;
	
	@SuppressWarnings("unused")
	private Payment() {
		this(null, null, null, null, null, null, null, null, null,null,null);
	}

	public Payment( final Long id,final Long userId, final String txnId, final PaymentMode mode, final BigDecimal amount,
			final Long date,final TransactionType transactionType, final String description,final String checksum,final String status,final PaymentAdjustment adjustment) {
		this.id = id;
		this.userId = userId;
		this.txnId = txnId;
		this.mode = mode;
		this.amount = amount;
		this.date = date;
		this.transactionType = transactionType;
		this.description = description;
		this.checksum=checksum;
		this.status=status;
		this.adjustment=adjustment;
	}

	@JsonProperty("adjustment")
	public PaymentAdjustment getAdjustment() {
		return adjustment;
	}

	@JsonProperty("amount")
	public BigDecimal getAmount() {
		return amount;
	}

	@JsonProperty("date")
	public Long getDate() {
		return date;
	}
	
	@JsonProperty("txn_id")
	public String getTxnId() {
		return txnId;
	}

	@JsonProperty("mode")
	public PaymentMode getMode() {
		return mode;
	}

	@JsonProperty("transaction_type")
	public TransactionType getTransactionType() {
		return transactionType;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	
	@JsonProperty("user_id")
	public Long getUserId() {
		return userId;
	}
	
	@JsonProperty("checksum")
	public String getChecksum() {
		return checksum;
	}
	
	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	public static Payment createWithTransactionTypeAndUserId(final TransactionType transationType,
			final Long userId, final Payment payment) {
		return new Payment(payment.getId(), userId, payment.getTxnId(), payment.getMode(), payment.getAmount(),
				payment.getDate(), transationType, payment.getDescription(),payment.getChecksum(),payment.getStatus(),payment.getAdjustment());
	}
	
	public static Payment createWithId(final Long id,final Payment payment) {
		return new Payment(id, payment.getUserId(), payment.getTxnId(), payment.getMode(), payment.getAmount(),
				payment.getDate(), payment.getTransactionType(), payment.getDescription(),payment.getChecksum(),payment.getStatus(),payment.getAdjustment());
	}
	
	public static Payment createWithStatus(final Payment payment, String status) {
		return new Payment(payment.getId(), payment.getUserId(), payment.getTxnId(), payment.getMode(), payment.getAmount(),
				payment.getDate(), payment.getTransactionType(), payment.getDescription(),payment.getChecksum(),status,payment.getAdjustment());
	}

	public static Payment createWithTransactionId(final Payment payment,final String txnId) {
		return new Payment(payment.getId(), payment.getUserId(), txnId, payment.getMode(), payment.getAmount(),
				payment.getDate(), payment.getTransactionType(), payment.getDescription(),payment.getChecksum(),payment.getStatus(),payment.getAdjustment());
	}
	
	@Override
	public String toString() {
		return "Payment [id=" + id + ", userId=" + userId + ", txnId=" + txnId
				+ ", mode=" + mode + ", amount=" + amount + ", date=" + date
				+ ", transactionType=" + transactionType + ", description="
				+ description + "]";
	}
	
}
