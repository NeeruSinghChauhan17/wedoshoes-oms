package com.wedoshoes.oms.model.order;

import java.util.HashMap;
import java.util.Map;

public enum ImageType {

	BEFORE_SERVICING(1),
	AFTER_SERVICING(2);
	
	private static final Map<Integer, ImageType> CODE_TO_TYPE =  new HashMap<>();
	
	static {
		for(ImageType imageType : ImageType.values()) {
			CODE_TO_TYPE.put(imageType.getCode(), imageType);
		}
	}
	
	private final Integer code;

	private ImageType(Integer code) {
		this.code = code;
	}
	
	public Integer getCode() {
		return code;
	}


	public static ImageType codeToType(final Integer code) {
		return CODE_TO_TYPE.get(code);
	}
	
	public static ImageType defaultType() {
		return BEFORE_SERVICING;
	}
}
