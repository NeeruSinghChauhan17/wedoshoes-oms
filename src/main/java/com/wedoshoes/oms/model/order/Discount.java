package com.wedoshoes.oms.model.order;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Discount {
	
	private final Integer id;
	private final BigDecimal value;
	private final PriceValueType valueType;
	
	@SuppressWarnings("unused")
	private Discount() {
		this(null, null, null);
	}
	
	public Discount(final Integer id, final BigDecimal value,
			final PriceValueType valueType) {
		this.id = id;
		this.value = value;
		this.valueType = valueType;
	}

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("value")
	public BigDecimal getValue() {
		return value;
	}

	@JsonProperty("value_type")
	public PriceValueType getValueType() {
		return valueType;
	}
}
