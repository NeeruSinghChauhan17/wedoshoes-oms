package com.wedoshoes.oms.model.order;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Greesh Kumar
 *
 */
@JsonInclude(Include.NON_NULL)
public class ItemDefect {

	private final Byte defectId;
	private final String name;
	
	@SuppressWarnings("unused")
	private ItemDefect() {
		this(null, null);
	}
	public ItemDefect(final Byte defectId, final String name) {
		this.name = name;
		this.defectId = defectId;
	}
	
	@JsonProperty("name")
	public String getName() {
		return name;
	}
	@JsonProperty("defect_id")
	public Byte getDefectId() {
		return defectId;
	}
	
}
