package com.wedoshoes.oms.model.order;

import java.util.HashMap;
import java.util.Map;

public enum CartState {

	ACTIVE(1),
	INACTIVE(0);
	
	private static final Map<Integer, CartState> CODE_TO_TYPE =  new HashMap<>();
	
	static {
		for(CartState cartState : CartState.values()) {
			CODE_TO_TYPE.put(cartState.getCode(), cartState);
		}
	}
	
	private final Integer code; 
	
	private CartState(final Integer code){
		this.code = code;
	}
	
	public Integer getCode() {
		return code;
	}
	
	public static CartState codeToType(final Integer code) {
		return CODE_TO_TYPE.get(code);
	}
	
//	public static void main(String[] args) {
//		System.out.println(CartState.codeToType(1));
//	}
}
