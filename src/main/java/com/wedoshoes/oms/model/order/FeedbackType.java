package com.wedoshoes.oms.model.order;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.google.common.collect.Maps;

public enum FeedbackType {

	SERVICE_QUALITY(1, "Service Quality"), 
	
	PRICES_COMMUNICATED_CHARGED(2, "Prices Communicated/Charged"), 
	
	CUSTOMER_SUPPORT(3, "Customer Support"), 
	
	PICKUP_DELIVERY(4, "Pickup & Delivery"), 
	
	PROFESSIONALISM(5, "Professionalism"), 
	
	OTHERS(6, "Others");

	private static final Map<Integer, FeedbackType> CODE_TO_CATEGORY = Maps.newHashMap();
	
	private static final Map<String, FeedbackType> DISPLAY_NAME_TO_CATEGORY = Maps.newHashMap();

	static {
		for (final FeedbackType category : FeedbackType.values()) {
			CODE_TO_CATEGORY.put(category.getCode(), category);
		}
		for (FeedbackType category : FeedbackType.values()) {
			DISPLAY_NAME_TO_CATEGORY.put(category.getDisplayName(), category);
		}
	}

	private final Integer code;

	private final String displayName;

	private FeedbackType(final Integer code, final String displayName) {
		this.code = code;
		this.displayName = displayName;
	}

	public Integer getCode() {
		return code;
	}

	public String getDisplayName() {
		return displayName;
	}

	@JsonValue
	public String toValue() {
		return getDisplayName();
	}

	@JsonCreator
	public static FeedbackType nameToType(final String displayName) {
		return DISPLAY_NAME_TO_CATEGORY.get(displayName);
	}

	public static FeedbackType codeToCategory(final Integer code) {
		return CODE_TO_CATEGORY.get(code);
	}

}
