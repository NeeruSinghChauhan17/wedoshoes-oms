package com.wedoshoes.oms.model.order;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Address {

	private final String streetAddress;
	private final String city;
	private final String state;
	private final String pin;
	private final Integer locality;
	private final String landmark;
	private final Double latitude;
	private final Double longitude;
	
	@SuppressWarnings("unused")
	private Address() {
		this(null, null, null, null, null, null, null, null);
	}

	public Address(final String streetAddress, final String city, final String state,
			final String pin, final String landmark, final Integer locality, 
			final Double latitude, final Double longitude) {
		this.streetAddress = streetAddress;
		this.city = city;
		this.state = state;
		this.pin = pin;
		this.landmark = landmark;
		this.locality = locality;
		this.latitude=latitude;
		this.longitude=longitude;
	}

	@JsonProperty("state")
	public String getState() {
		return state;
	}

	@JsonProperty("street_address")
	public String getStreetAddress() {
		return streetAddress;
	}

	@JsonProperty("city")
	public String getCity() {
		return city;
	}

	@JsonProperty("pin")
	public String getPin() {
		return pin;
	}

	@JsonProperty("landmark")
	public String getLandmark() {
		return landmark;
	}

	@JsonProperty("locality")
	public Integer getLocality() {
		return locality;
	}
	
	@JsonProperty("lat")
	public Double getLatitude() {
		return latitude;
	}

	@JsonProperty("lng")
	public Double getLongitude() {
		return longitude;
	}

	public static Address createWithLocality(Integer localityId, Address address){
		return new Address(address.getStreetAddress(), address.getCity(), address.getState(), 
				address.getPin(), address.getLandmark(), localityId, address.getLatitude(), address.getLongitude());
	}
}
