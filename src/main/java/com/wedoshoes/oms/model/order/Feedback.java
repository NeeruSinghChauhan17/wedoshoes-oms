package com.wedoshoes.oms.model.order;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Feedback {
	
	private final Long id;
	private final Long userId;
	private final Long orderId;
	private final Long itemId;
	private final String name;
	private final String email;
	private final String cc;
	private final String phone;
	private final String text;
	private final Long feedbackDate;
	private final Short rating;
	private final List<FeedbackType> types;
	
	
	@SuppressWarnings("unused")
	private Feedback() {
		this(null, null, null, null, null, null, null, null, null, null,null, Arrays.asList(FeedbackType.OTHERS));
	}
	
	
	public Feedback(final Long id, final Long userId, final Long orderId,final Long itemId, final String name,
			final String email,final String cc, final String phone, final String text, final Long feedbackDate,
			final Short rating, final List<FeedbackType> types) {
		this.id = id;
		this.userId = userId;
		this.orderId = orderId;
		this.itemId=itemId;
		this.name = name;
		this.email = email;
		this.cc = cc;
		this.phone = phone;
		this.text = text;
		this.feedbackDate = feedbackDate;
		this.rating = rating;
		this.types = types;
	}

	/*public static Feedback createWithTypes(final List<FeedbackType> types, final ){
		return new Feedback(final Long id, final Long userId, final Long orderId,final Long itemId, final String name,
				final String email, final String phone, final String text, final Long feedbackDate,
				final Short rating, final List<FeedbackType> types);
	}*/
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	@JsonProperty("user_id")
	public Long getUserId() {
		return userId;
	}

	@JsonProperty("order_id")
	public Long getOrderId() {
		return orderId;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("email")
	public String getEmail() {
		return email;
	}

	@JsonProperty("phone")
	public String getPhone() {
		return phone;
	}
	@JsonProperty("cc")
	public String getCc() {
		return cc;
	}

	@JsonProperty("text")
	public String getText() {
		return text;
	}

	@JsonProperty("feedback_date")
	public Long getFeedbackDate() {
		return feedbackDate;
	}

	@JsonProperty("rating")
	public Short getRating() {
		return rating;
	}

	
	@JsonProperty("types")
	public List<FeedbackType> getTypes() {
		return types;
	}

	@JsonProperty("item_id")
	public Long getItemId() {
		return itemId;
	}


	public static Feedback createWithOrderIdAndItemId(final Long orderId, final Long itemId, final Feedback feedback){
		return new Feedback(feedback.getId(), feedback.getUserId(), orderId,itemId, feedback.getName(), feedback.getEmail(), 
				feedback.getCc(),feedback.getPhone(), feedback.getText(), feedback.getFeedbackDate(), feedback.getRating(), 
				feedback.getTypes());
 	}
	
	public static Feedback createFromId(final Long id){
		return new Feedback(id, null, null, null, null, null, null, null,null, null, null, null); 
	}
	
}