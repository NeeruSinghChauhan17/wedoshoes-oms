package com.wedoshoes.oms.model.order;

/**
 * 
 * @author Navrattan Yadav
 *
 */
public interface ItemState {

	/**
	 *   EXTERNAL STATES
	 */
	public static final Byte NEW = 1;
	public static final Byte PICKED = 2; 
	public static final Byte ANALYSED = 3;
	public static final Byte PROCESSED = 4;
	public static final Byte DELIVERED = 5;
	public static final Byte CANCELLED = 6;
	public static final Byte RE_SCHEDULE_FOR_PICKUP = 7;
	public static final Byte RE_SCHEDULE_FOR_DELIVERY = 8;
	public static final Byte RE_SERVICE = 9;
	//UPDATED
	public static final Byte QA_REJECTED = 23;
	public static final Byte MISSED = 24;
	
	/**
	 *  INTERNAL 
	 */

	public static final Byte SCHEDULE_FOR_PICKUP = 10;
	public static final Byte RECEIVED_AT_CP = 11;
	public static final Byte SEND_TO_WORKSHOP = 12;
	public static final Byte RECEIVED_AT_WORKSHOP = 13;
	public static final Byte ANALYSIS = 14;
	public static final Byte WAITING_FOR_APPORVAL = 15;
	public static final Byte IN_PROGRESS = 16;
	public static final Byte MARK_FOR_QUALITY_CHECK = 17;
	public static final Byte MARK_FOR_PACKING = 18;
	public static final Byte READY_FOR_DELIVERY = 19;
	public static final Byte SCHEDULE_FOR_DELIVERY = 20;
	public static final Byte INVOICE_GENERATED = 21;
	public static final Byte RE_WORK = 22;

}
