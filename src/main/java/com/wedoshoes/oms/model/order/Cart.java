package com.wedoshoes.oms.model.order;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Cart {
	
	private final Long id;
	private final Long userId;
	private final Long createDate;
	private final List<Item> items;
	private final CartState state;
	private final Integer size;
	
	@SuppressWarnings("unused")
	private Cart() {
		this(null, null, null, null, null,null);
	}
	
	public Cart(final Long id, final Long userId, final Long createDate, final List<Item> items,
			final CartState state, final Integer size) {
		this.id = id;
		this.userId = userId;
		this.createDate = createDate;
		this.items = items;
		this.state = state;
		this.size=size;
	}

	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	@JsonProperty("user_id")
	public Long getUserId() {
		return userId;
	}

	@JsonProperty("create_date")
	public Long getCreateDate() {
		return createDate;
	}

	@JsonProperty("items")
	public List<Item> getItems() {
		return items;
	}
	
	@JsonProperty("state")
	public CartState getState() {
		return state;
	}

	@JsonProperty("size")
	public Integer getSize() {
		return size;
	}
  
	public static Cart createWithId(final Long id){
		return new Cart(id, null, null, null, null, null);
	}
	
	public static Cart createWithItems(final Cart cart, final List<Item> items){
		return new Cart(cart.getId(), cart.getUserId(), cart.getCreateDate(), items, cart.getState(), cart.getSize());
	}
}