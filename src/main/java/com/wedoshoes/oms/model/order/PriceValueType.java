package com.wedoshoes.oms.model.order;

public enum PriceValueType {

	FIXED,
	PERCENTAGE;
}
