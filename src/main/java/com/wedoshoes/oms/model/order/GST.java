package com.wedoshoes.oms.model.order;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(value=Include.NON_NULL)
public class GST {

	private final BigDecimal igst ;
	private final BigDecimal cgst ;
	private final BigDecimal sgst ;
	private final Boolean isIntrastate;
	
	@SuppressWarnings("unused")
	private GST(){
		this(BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, null);
	}
	
	public GST(final BigDecimal igst, final BigDecimal cgst, final BigDecimal sgst, 
			final Boolean isIntrastate){
		this.igst=igst;
		this.cgst=cgst;
		this.sgst=sgst;
		this.isIntrastate=isIntrastate;
	}

	@JsonProperty("igst")
	public BigDecimal getIgst() {
		return igst;
	}

	@JsonProperty("cgst")
	public BigDecimal getCgst() {
		return cgst;
	}

	@JsonProperty("sgst")
	public BigDecimal getSgst() {
		return sgst;
	}
	
	@JsonIgnore
	public Boolean isIntrastate() {
		return isIntrastate;
	}

	@JsonIgnore
	public BigDecimal getGstWithIntrastate(){
		return cgst.add(sgst);
	}
	
	@JsonIgnore
	public BigDecimal getGstWithInterstate(){
		return igst;
	}
	
	@JsonIgnore
	public BigDecimal getGstValue(){
		return igst.add(cgst).add(sgst); 
	}
}


