package com.wedoshoes.oms.model.payment;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PaymentStatus {
	
	private final String transactionId;
	private final String checksum;
	
	public PaymentStatus() {
		
		this(null, null);
	}
	
	public PaymentStatus(final String transactionId, final String checksum) {
		
		this.transactionId=transactionId;
		this.checksum=checksum;
		
	}

	@JsonProperty("transaction_id")
	public String getTransactionId() {
		return transactionId;
	}

	@JsonProperty("checksum")
	public String getChecksum() {
		return checksum;
	}
	
}
