package com.wedoshoes.oms.model.payment;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class PayuUserStatus {


	@JsonProperty("merchantTransactionId")
	private String merchantTransactionId;
	
	@JsonProperty("paymentId")
	private Long paymentId;
	
	@JsonProperty("status")
	private String status;
	
	@JsonProperty("amount")
	private Float amount;

	public String getMerchantTransactionId() {
		return merchantTransactionId;
	}

	public Long getPaymentId() {
		return paymentId;
	}

	public String getStatus() {
		return status;
	}

	public Float getAmount() {
		return amount;
	}
	
}
