package com.wedoshoes.oms.model.payment;

import java.util.HashMap;
import java.util.Map;

public enum PaymentAdjustmentType {
    
	LEAVE_AS_BALANCE(1),BAD_DEBTS(2),COMPENSATION(3),PROMOTIONAL_OFFER(4),ROUND_OFF_DISCOUNT(5),RETURNED_UNPROCESSED(6),
	DISCOUNT_COUPON(7),CASH_DISCOUNT(8);
	
private static final Map<Integer, PaymentAdjustmentType> CODE_TO_TYPE =  new HashMap<>();
	
	static {
		for(PaymentAdjustmentType paymentAdjustmentType : PaymentAdjustmentType.values()) {
			CODE_TO_TYPE.put(paymentAdjustmentType.getCode(), paymentAdjustmentType);
		}
	}
	
	private final Integer code;

	private PaymentAdjustmentType(Integer code) {
		this.code = code;
	}
	
	public Integer getCode() {
		return code;
	}


	public static PaymentAdjustmentType codeToType(final Integer code) {
		return CODE_TO_TYPE.get(code);
	}
	
	
}