package com.wedoshoes.oms.model.payment;
public enum PaymentState {
	SUCCESS, PENDING;
}