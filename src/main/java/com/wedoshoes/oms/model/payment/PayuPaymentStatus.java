package com.wedoshoes.oms.model.payment;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PayuPaymentStatus {

	@JsonProperty("status")
	private Integer status;
	
	@JsonProperty("message")
	private String message;
	
	
	@JsonProperty("result")
	private List<PayuUserStatus> result;
	
	@JsonProperty("errorCode")
	private Integer errorCode;
	
	@JsonProperty("responseCode")
	private Integer responseCode;

	public Integer getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}

	public List<PayuUserStatus> getResult() {
		return result;
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public Integer getResponseCode() {
		return responseCode;
	}

	
}
