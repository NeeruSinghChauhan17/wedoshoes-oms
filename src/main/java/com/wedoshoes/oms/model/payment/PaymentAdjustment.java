package com.wedoshoes.oms.model.payment;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class PaymentAdjustment {

	private final BigDecimal amount;
	private final PaymentAdjustmentType adjustmentType;
	
	
	@SuppressWarnings("unused")
	private PaymentAdjustment(){
		this(null,null);
	}

	public PaymentAdjustment(final BigDecimal amount,
			final  PaymentAdjustmentType adjustmentType) {
		super();
		this.amount = amount;
		this.adjustmentType = adjustmentType;
	}

	@JsonProperty("amount")
	public BigDecimal getAmount() {
		return amount;
	}

	@JsonProperty("adjustment_type")
	public PaymentAdjustmentType getAdjustmentType() {
		return adjustmentType;
	}
	
}
