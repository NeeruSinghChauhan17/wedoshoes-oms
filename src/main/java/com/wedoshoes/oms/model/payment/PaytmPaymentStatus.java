package com.wedoshoes.oms.model.payment;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PaytmPaymentStatus {

	@JsonProperty("TXNID")
	private String TXNID;
	
	@JsonProperty("BANKTXNID")
	private String BANKTXNID;
	
	@JsonProperty("ORDERID")
	private String ORDERID;
	
	@JsonProperty("TXNAMOUNT")
	private String TXNAMOUNT;
	
	@JsonProperty("STATUS")
	private String STATUS;
	
	@JsonProperty("TXNTYPE")
	private String TXNTYPE;
	
	@JsonProperty("GATEWAYNAME")
	private String GATEWAYNAME;
	
	@JsonProperty("RESPCODE")
	private String RESPCODE;
	
	@JsonProperty("RESPMSG")
	private String RESPMSG;
	
	@JsonProperty("BANKNAME")
	private String BANKNAME;
	
	@JsonProperty("MID")
	private String MID;
	
	@JsonProperty("PAYMENTMODE")
	private String PAYMENTMODE;
	
	@JsonProperty("REFUNDAMT")
	private String REFUNDAMT;
	
	@JsonProperty("TXNDATE")
	private String TXNDATE;
	
	@JsonProperty("MERC_UNQ_REF")
	private String MERC_UNQ_REF;

	public String getTXNID() {
		return TXNID;
	}

	public String getBANKTXNID() {
		return BANKTXNID;
	}

	public String getORDERID() {
		return ORDERID;
	}

	public String getTXNAMOUNT() {
		return TXNAMOUNT;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public String getTXNTYPE() {
		return TXNTYPE;
	}

	public String getGATEWAYNAME() {
		return GATEWAYNAME;
	}

	public String getRESPCODE() {
		return RESPCODE;
	}

	public String getRESPMSG() {
		return RESPMSG;
	}

	public String getBANKNAME() {
		return BANKNAME;
	}

	public String getMID() {
		return MID;
	}

	public String getPAYMENTMODE() {
		return PAYMENTMODE;
	}

	public String getREFUNDAMT() {
		return REFUNDAMT;
	}

	public String getTXNDATE() {
		return TXNDATE;
	}

	public String getMERC_UNQ_REF() {
		return MERC_UNQ_REF;
	}
	
	
}
