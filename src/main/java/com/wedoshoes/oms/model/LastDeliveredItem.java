package com.wedoshoes.oms.model;

import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wedoshoes.oms.model.order.Image;

@JsonInclude(value=Include.NON_NULL)
public class LastDeliveredItem {

	private final Long orderId;
	private final Long itemId;
	private final Long deliveryDate;
	private final Collection<Image> images;
	
	@SuppressWarnings("unused")
	private LastDeliveredItem(){
		this(null, null ,null ,null);
	}
	
	public LastDeliveredItem( final Long orderId, final Long itemId, 
			final Long deliveryDate, final Collection<Image> images) {
		this.orderId=orderId;
		this.itemId=itemId;
		this.deliveryDate=deliveryDate;
		this.images=images;
	}

	@JsonProperty("order_id")
	public Long getOrderId() {
		return orderId;
	}

	@JsonProperty("item_id")
	public Long getItemId() {
		return itemId;
	}

	@JsonProperty("delivery_date")
	public Long getDeliveryDate() {
		return deliveryDate;
	}

	@JsonProperty("images")
	public Collection<Image> getImages() {
		return images;
	}

	public static LastDeliveredItem createWithImages(final Collection<Image> images, 
			final LastDeliveredItem lastDeliveredItem){
		return new LastDeliveredItem(lastDeliveredItem.getOrderId(), lastDeliveredItem.getItemId(), 
				lastDeliveredItem.getDeliveryDate(), images);
	}
}
