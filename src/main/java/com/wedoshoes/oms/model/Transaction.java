package com.wedoshoes.oms.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Navrattan Yadav
 *
 */

@JsonInclude(value = Include.NON_NULL)
public class Transaction {

	private final Long id;
	private final Long userId;
	private final Long walletId;
	private final BigDecimal amount;
	private final TransactionType type;
	private final TransactionStatus status;
	private final String description;
	private final Long date;

	@SuppressWarnings("unused")
	private Transaction() {
		this(null, null, null, null, null, null, null, null);
	}

	public Transaction(final Long transactionId, final Long userId,
			final Long walletId, final BigDecimal amount,
			final TransactionType type, final TransactionStatus status,
			final String description, final Long date) {
		this.id = transactionId;
		this.userId = userId;
		this.walletId = walletId;
		this.amount = amount;
		this.type = type;
		this.status = status;
		this.description = description;
		this.date = date;
	}

	@JsonProperty("transaction_id")
	public Long getTransactionId() {
		return id;
	}

	@JsonProperty("user_id")
	public Long getUserId() {
		return userId;
	}

	@JsonProperty("amount")
	public BigDecimal getAmount() {
		return amount;
	}

	@JsonProperty("type")
	public TransactionType getType() {
		return type;
	}

	@JsonProperty("status")
	public TransactionStatus getStatus() {
		return status;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("date")
	public Long getDate() {
		return date;
	}

	@JsonProperty("wallet_id")
	public Long getWalletId() {
		return walletId;
	}

	public static Transaction createWithId(final Transaction walletTransaction,
			final Long transactionId) {
		return new Transaction(transactionId, walletTransaction.getUserId(),
				walletTransaction.getWalletId(), walletTransaction.getAmount(),
				walletTransaction.getType(), walletTransaction.getStatus(),
				walletTransaction.getDescription(), walletTransaction.getDate());
	}

	public static Transaction createWithUserId(
			final Transaction walletTransaction, final Long userId) {
		return new Transaction(walletTransaction.getTransactionId(), userId,
				walletTransaction.getWalletId(), walletTransaction.getAmount(),
				walletTransaction.getType(), walletTransaction.getStatus(),
				walletTransaction.getDescription(), walletTransaction.getDate());
	}
	
	public static Transaction createFromBalanceAndType(final BigDecimal balance, 
			final TransactionType type) {
		return new Transaction(null, null,null, balance,type,null,null, null);
	}

}
