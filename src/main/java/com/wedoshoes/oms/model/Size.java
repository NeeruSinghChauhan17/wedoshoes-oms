package com.wedoshoes.oms.model;

import java.util.HashMap;
import java.util.Map;

public enum Size {

	NO_SIZE(0),
	SMALL(1),
	MEDIUM(2),
	LARGE(3),
	XL(4),
	XXL(5);
	
	private static final Map<Integer, Size> CODE_TO_TYPE =  new HashMap<>();
	
	static {
		for(Size size : Size.values()) {
			CODE_TO_TYPE.put(size.getCode(), size);
		}
	}
	private Integer code;

	private Size(final Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}
	
	public static Size codeToType(final Integer code) {
		return CODE_TO_TYPE.get(code);
	}
	
}
