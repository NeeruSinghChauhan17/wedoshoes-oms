package com.wedoshoes.oms.model.notification;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Email {
	
	private final String[] to;
	private final String[] bcc;
	private final String[] cc;
	private final String from;
	private final String body;
	private final String subject;
	private final List<Attachment> attachments;
	
	@SuppressWarnings("unused")
	private Email(){
		this(null, null, null, null, null, null, null);
	}
	
	public Email(final String body, final String subject, final String[] to, final String[] bcc,
			final String[] cc, final String from, final List<Attachment> attachments) {
		this.body = body;
		this.subject = subject;
		this.to = to;
		this.bcc =bcc;
		this.cc = cc;
		this.from = from;
		this.attachments = attachments;
	}

	@JsonProperty("body")
	public String getBody() {
		return body;
	}

	@JsonProperty("subject")
	public String getSubject() {
		return subject;
	}

	@JsonProperty("to")
	public String[] getTo() {
		return to;
	}
	
	@JsonProperty("bcc")
	public String[] getBcc() {
		return bcc;
	}
	
	@JsonProperty("cc")
	public String[] getCc() {
		return cc;
	}

	@JsonProperty("from")
	public String getFrom() {
		return from;
	}

	@JsonIgnore
	public Boolean hasAttachment() {
		return (attachments != null && !attachments.isEmpty());
	}

	@JsonProperty("attachments")
	public List<Attachment> getAttachments() {
		return attachments;
	}
	
	public static Email createWithTo(final String[] to, final Email email) {
		return new Email(email.getBody(), email.getSubject(), to, null,null,
				email.getFrom(), email.getAttachments());
	}
}
