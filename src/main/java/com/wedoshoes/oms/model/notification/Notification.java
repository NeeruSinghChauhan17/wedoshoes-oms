package com.wedoshoes.oms.model.notification;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wedoshoes.oms.http.impl.model.core.CommSettings;

@JsonInclude(Include.NON_NULL)
public class Notification {
	
	private final Email email;
	private final SMS sms;
	private final Push push;
	private final CommSettings commSett;
	private final Boolean saveSettings;
	
	@SuppressWarnings("unused")
	private Notification() {
		this(null, null, null, null, null);
	}

	public Notification(final Email email, final SMS sms, final Push push, final CommSettings commSett,
			final Boolean saveSettings) {
		this.email = email;
		this.sms = sms;
		this.push = push;
		this.commSett = commSett;
		this.saveSettings = saveSettings;
	}

	@JsonProperty("comm_settings")
	public CommSettings getCommSett() {
		return commSett;
	}
	
	@JsonProperty("save_settings")
	public Boolean canSaveSettings() {
		return saveSettings;
	}

	@JsonProperty("email")
	public Email getEmail() {
		return email;
	}

	@JsonProperty("sms")
	public SMS getSms() {
		return sms;
	}

	@JsonProperty("push")
	public Push getPush() {
		return push;
	}
	
	@JsonIgnore
	public Boolean canSendEmail() {
	  return email != null;
	}
	
	@JsonIgnore
	public Boolean canSendSMS() {
	  return sms != null;
	}
	
	@JsonIgnore
	public Boolean canSendPush() {
	  return push != null;
	}
	
	public static class NotificationBuilder {
		
		private Email email;
		private SMS sms;
		private Push push;
		private CommSettings commSett;
		private Boolean saveSettings;
		
		public NotificationBuilder push(final Push push) {
			this.push = push;
			return this;
		}

		public NotificationBuilder email(final Email email) {
			this.email = email;
			return this;
		}
		
		public NotificationBuilder sms(final SMS sms) {
			this.sms = sms;
			return this;
		}
		public NotificationBuilder commSett(final CommSettings commSett) {
			this.commSett = commSett;
			return this;
		}
		public NotificationBuilder saveSettings(final Boolean saveSettings) {
			this.saveSettings = saveSettings;
			return this;
		}
		
		public Notification build() {
			return new Notification(email, sms, push, commSett,saveSettings);
		}
		
		public Boolean isEmpty() {
			return email == null && push == null && sms == null;
		}
	}
	
}
