package com.wedoshoes.oms.model.notification;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Attachment {
	
	private final String name;
	private final String mimeType;
	private final String base64EncodedBody;
	
	@SuppressWarnings("unused")
	private Attachment() {
		this(null, null, null);
	}

	public Attachment(final String name, final String mimeType, final String base64EncodedBody) {
		this.name = name;
		this.mimeType = mimeType;
		this.base64EncodedBody = base64EncodedBody;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}
	
	@JsonProperty("mime_type")
	public String getMimeType() {
		return mimeType;
	}

	@JsonProperty("body")
	public String getBase64EncodedBody() {
		return base64EncodedBody;
	}
	
}
