package com.wedoshoes.oms.model.notification;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Navrattan Yadav
 *
 */
@JsonInclude(Include.NON_NULL)
public class PushPayload {

	private final String title;
	private final String body;
	private final Boolean customPayload;
	private final Map<String, String> customData;
	
	public PushPayload() {
		this(null, null, null, null);
	}
	
	
	public PushPayload(final String title, final String body, final Boolean customPayload,
			final Map<String, String> customData) {
		this.title = title;
		this.body = body;
		this.customData = customData;
		this.customPayload = customPayload;
	}

	public static PushPayload createWithCustomData(final Map<String, String> data){
		return new PushPayload(null, null, Boolean.TRUE, data);
	}
	
	public static PushPayload createWithNotification(final String title, final String body){
		return new PushPayload(title, body, Boolean.FALSE, null);
	}
	
	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	@JsonProperty("body")
	public String getBody() {
		return body;
	}

	@JsonProperty("is_custom_payload")
	public Boolean isCustomPayload() {
		return customPayload;
	}

	@JsonProperty("custom_data")
	public Map<String, String> getCustomData() {
		
		
		return customData;
	}
	
}

