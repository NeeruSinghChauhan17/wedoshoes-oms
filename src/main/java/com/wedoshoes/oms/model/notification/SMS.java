package com.wedoshoes.oms.model.notification;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class SMS {

	private final List<Long> phones;
	private final String text;
	
	@SuppressWarnings("unused")
	private SMS() {
		this(null, null);
	}
	
	public SMS(final List<Long> phones, final String text) {
		this.phones = phones;
		this.text = text;
	}

	@JsonProperty("phones")
	public List<Long> getPhones() {
		return phones;
	}
	@JsonProperty("text")
	public String getText() {
		return text;
	}

}
