package com.wedoshoes.oms.model.notification;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Push {

	private final PushPayload payload;
	
	@SuppressWarnings("unused")
	private Push(){
		this(null);
	}
	
	public Push(final PushPayload payload) {
		this.payload = payload;
	}

	@JsonProperty("payload")
	public PushPayload getPayload() {
		return payload;
	}

}
