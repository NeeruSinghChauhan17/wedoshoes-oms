package com.wedoshoes.oms.model.shipment;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Shipment {

	private final Long id;
	private final Long userId;
	private final Long orderId;
	private final Long itemId;
	private final Byte currentState;
	private final Byte initialState;
	private final Long shipmentDate;
	private final Long lastUpdatedDate;
	private final String customerSignatureURL;
	
	@SuppressWarnings("unused")
	private Shipment() {
		this(null, null, null, null, null, null, null, null, null);
	}

	public Shipment(final Long id, final Long userId, final Long orderId, final Long itemId,final Byte initialState,
			final Byte currentState, final Long shipmentDate, final Long lastUpdatedDate, final String customerSignatureURL) {
		this.id = id;
		this.userId = userId;
		this.orderId = orderId;
		this.itemId = itemId;
		this.initialState = initialState;
		this.currentState = currentState;
		this.shipmentDate = shipmentDate;
		this.lastUpdatedDate = lastUpdatedDate;
		this.customerSignatureURL=customerSignatureURL;
	}

	
	@JsonProperty("current_state")
	public Byte getCurrentState() {
		return currentState;
	}

	@JsonIgnore
	public Byte getInitialState() {
		return initialState;
	}

	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	
	@JsonProperty("user_id")
	public Long getUserId() {
		return userId;
	}

	@JsonProperty("order_id")
	public Long getOrderId() {
		return orderId;
	}

	@JsonProperty("item_id")
	public Long getItemId() {
		return itemId;
	}

	@JsonProperty("shipment_date")
	public Long getShipmentDate() {
		return shipmentDate;
	}

	@JsonProperty("last_updated_date")
	public Long getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	
	@JsonProperty("customer_signature_url")
	public String getCustomerSignatureURL() {
		return customerSignatureURL;
	}


	public static Shipment createFromId(final Long id) {
		return new Shipment(id, null, null,null, null, null, null, null,null);
	}
	
	public static Shipment createWithOrderIdAndItemIdAndState(final Long orderId, final Long itemId,
			final Byte initialState,final Byte currentState, final Shipment shipment) {
		return new Shipment(shipment.getId(), shipment.getUserId(), orderId, itemId, 
				initialState,currentState, shipment.getShipmentDate(), shipment.getLastUpdatedDate(),
				shipment.getCustomerSignatureURL());
	}
	
	public static Shipment createWithState(final Byte currentState, final Shipment shipment) {
		return new Shipment(shipment.getId(), shipment.getUserId(), shipment.getOrderId(), shipment.getItemId(), 
				null,currentState, shipment.getShipmentDate(), shipment.getLastUpdatedDate(), shipment.getCustomerSignatureURL());
	}
}
