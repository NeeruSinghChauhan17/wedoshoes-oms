package com.wedoshoes.oms.model.shipment;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class ShipmentItem {

	private final Long id;
	private final Long shipmentId;
	private final Byte state;
	
	
	
	@SuppressWarnings("unused")
	private ShipmentItem() {
		this(null, null, null);
	}
	
	public ShipmentItem(final Long id, final Long shipmentId, final Byte state) {
		this.id = id;
		this.shipmentId = shipmentId;
		this.state = state;
	}
	
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	
	@JsonProperty("shipment_id")
	public Long getShipmentId() {
		return shipmentId;
	}
	
	@JsonProperty("state")
	public Byte getState() {
		return state;
	}
}
