package com.wedoshoes.oms.model.shipment;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ShipmentOrder {
	private final Long id;
	private final List<ShipmentItem> items;
	
	@SuppressWarnings("unused")
	private ShipmentOrder() {
		this(null, null);
	}
	
	public ShipmentOrder(final Long id, final List<ShipmentItem> items) {
		this.id = id;
		this.items = items;
	}

	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	@JsonProperty("items")
	public List<ShipmentItem> getItems() {
		return items;
	}
}
