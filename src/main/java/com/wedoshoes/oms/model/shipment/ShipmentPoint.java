package com.wedoshoes.oms.model.shipment;

import java.util.Optional;

import static com.wedoshoes.oms.dao.impl.DefaultUtils.defaultByteIfNull;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wedoshoes.oms.model.order.Item;
import com.wedoshoes.oms.model.order.ItemState;
import com.wedoshoes.oms.model.order.Order;

@JsonInclude(Include.NON_NULL)
public class ShipmentPoint {
	
		private final  Double latitude;
		private final Double longitude;
		private final Byte priority;
		private final Byte timeSlot;
		
		@SuppressWarnings("unused")
		private ShipmentPoint() {
			this(null,null,null,null);
		}
		
		public ShipmentPoint(final  Double latitude,final Double longitude,final Byte priority,final Byte timeSlot){
			this.latitude=latitude;
			this.longitude=longitude;
			this.priority=priority;
			this.timeSlot=timeSlot;
		}

		@JsonProperty("latitude")
		public Double getLatitude() {
			return latitude;
		}

		@JsonProperty("longitude")
		public Double getLongitude() {
			return longitude;
		}

		@JsonProperty("priority")
		public Byte getPriority() {
			return priority;
		}
		
		@JsonProperty("time_slot")
		public Byte getTimeSlot() {
			return timeSlot;
		}

		public static ShipmentPoint createWithOrder(final ShipmentOrder shipment,final Optional<Order> order, final Optional<Item> item){
			if(order.isPresent()){
			 for(ShipmentItem shipmentItem:shipment.getItems()){
				if(shipmentItem.getState()==ItemState.SCHEDULE_FOR_PICKUP ){
					return new ShipmentPoint(order.get().getPickUpAddress().getLatitude(),order.get().getPickUpAddress().getLongitude(),defaultByteIfNull(item.get().getPriority()),order.get().getPickUpTimeSlotId());
				}else if(shipmentItem.getState()==ItemState.SCHEDULE_FOR_DELIVERY){
					return new ShipmentPoint(order.get().getDeliveryAddress().getLatitude(),order.get().getDeliveryAddress().getLongitude(),defaultByteIfNull(item.get().getPriority()),item.get().getDeliveryTimeSlotId());
				}
			 }
			}
			return null;
		}

		/**
		 * Return DeliveryTimeSlot if shipmentItemId matches with itemId present in the  Order 
		 */
		/*private static Byte getItemTimeSlot(ShipmentItem shipmentItem, Order order) {
			for(Item item:order.getItems()){
				if(item.getId().equals(shipmentItem.getId())){
					return item.getDeliveryTimeSlotId();
				}
			}
			return null;
		}*/

		/**
		 * Return Priority if shipmentItemId matches with itemId present in the  Order 
		 */
		/*private static Byte getPriority(final ShipmentItem shipmentItem, final Order order) {
			for(Item item:order.getItems()){
				if(item !=null && item.getId().equals(shipmentItem.getId())){
					return item.getPriority();
				}
			}
			return null;
		}*/
		
}
