package com.wedoshoes.oms.model.shipment;

import java.math.BigDecimal;
import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * 
 * @author Alone.gk
 *
 */
@JsonInclude(Include.NON_NULL)
public class ShipmentSummary {

	private final Long id;
	private final Long userId;
	private final Collection<Long> orders;
	private final Integer pickupItems;
	private final Integer pickedItems;
	private final Integer deliveryItems;
	private final Integer deliveredItems;
	private final Integer canceledItems;
	private final Integer reServiceItems;
	private final Integer reScheduleForPickup;
	private final Integer reScheduleForDelivery;
	private BigDecimal totalAmount;
	private BigDecimal collectedAmount;
	private final Long createDate;
	
	@SuppressWarnings("unused")
	private ShipmentSummary(){
		this(null);
	}
	
	public ShipmentSummary(final Builder b){
		this.id=b.id;
		this.userId=b.userId;
		this.pickupItems=b.pickupItems;
		this.pickedItems= b.pickedItems;
		this.deliveryItems=b.deliveryItems;
		this.deliveredItems=b.deliveredItems;
		this.canceledItems=b.canceledItems;
		this.reServiceItems=b.reServiceItems;
		this.reScheduleForPickup=b.reScheduleForPickup;
		this.reScheduleForDelivery=b.reScheduleForDelivery;
		this.totalAmount=BigDecimal.ZERO;
		this.collectedAmount=BigDecimal.ZERO;
		this.createDate=b.createDate;
		this.orders = b.orders;
	}

	public void addTotalAmount(final BigDecimal amount){
		this.totalAmount= this.totalAmount.add(amount);
	}
	public void addCollectedAmount(final BigDecimal amount){
		this.collectedAmount= this.collectedAmount.add(amount);
	}

	@JsonIgnore
	public Collection<Long> getOrders() {
		return orders;
	}

	@JsonIgnore
	public Boolean isEmpty(){
		return (pickupItems + deliveryItems) == 0;
	}
	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	@JsonProperty("user_id")
	public Long getUserId() {
		return userId;
	}

	@JsonProperty("total_items")
	public Integer getTotalItems() {
		return pickupItems+deliveryItems;
	}

	@JsonProperty("processed_items")
	public Integer getProcessedItems() {
		return pickedItems+deliveredItems+canceledItems+
				reServiceItems+reScheduleForPickup+reScheduleForDelivery;
	}

	@JsonProperty("total_orders")
	public Integer getTotalOrders() {
		return orders.size();
	}

	@JsonProperty("total_locations")
	public Integer getTotalLocations() {
		return orders.size();
	}

	@JsonProperty("pickup_items")
	public Integer getPickupItems() {
		return pickupItems;
	}

	@JsonProperty("picked_items")
	public Integer getPickedItems() {
		return pickedItems;
	}

	@JsonProperty("delivery_items")
	public Integer getDeliveryItems() {
		return deliveryItems;
	}

	@JsonProperty("delivered_items")
	public Integer getDeliveredItems() {
		return deliveredItems;
	}

	@JsonProperty("canceled_items")
	public Integer getCanceledItems() {
		return canceledItems;
	}

	@JsonProperty("re_service_items")
	public Integer getReServiceItems() {
		return reServiceItems;
	}

	@JsonProperty("re_schedule_for_pick_up")
	public Integer getReScheduleForPickup() {
		return reScheduleForPickup;
	}

	@JsonProperty("re_schedule_for_delivery")
	public Integer getReScheduleForDelivery() {
		return reScheduleForDelivery;
	}

	@JsonIgnore
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	@JsonProperty("collected_amount")
	public BigDecimal getCollectedAmount() {
		return collectedAmount;
	}

	@JsonProperty("create_date")
	public Long getCreateDate() {
		return createDate;
	}
	

	public static class Builder{
		Long id;
		Long userId;
		Integer pickupItems;
		Integer pickedItems;
		Integer deliveryItems;
		Integer deliveredItems;
		Integer canceledItems;
		Integer reServiceItems;
		Integer reScheduleForPickup;
		Integer reScheduleForDelivery;
		BigDecimal totalAmount;
		BigDecimal collectedAmount;
		Long createDate;
		Collection<Long> orders;
		
		public Builder id(Long id){
			this.id = id;
			return this;
		}
		
		public Builder userId(Long userId){
			this.userId = userId;
			return this;
		}
		
		public Builder pickupItems(Integer pickupItems){
			this.pickupItems = pickupItems;
			return this;
		}
		public Builder pickedItem(Integer pickedItems){
			this.pickedItems = pickedItems;
			return this;
		}
		public Builder deliveryItems(Integer deliveryItems){
			this.deliveryItems = deliveryItems;
			return this;
		}
		public Builder deliveredItems(Integer deliveredItems){
			this.deliveredItems = deliveredItems;
			return this;
		}
		public Builder canceledItems(Integer canceledItems){
			this.canceledItems = canceledItems;
			return this;
		}
		public Builder reServiceItems(Integer reServiceItems){
			this.reServiceItems = reServiceItems;
			return this;
		}
		public Builder reScheduleForPickup(Integer reScheduleForPickup){
			this.reScheduleForPickup = reScheduleForPickup;
			return this;
		}
		public Builder reScheduleForDelivery(Integer reScheduleForDelivery){
			this.reScheduleForDelivery = reScheduleForDelivery;
			return this;
		}
		
		public Builder createDate(Long createDate){
			this.createDate = createDate;
			return this;
		}
		
		public Builder totalAmount(BigDecimal totalAmount){
			this.totalAmount = totalAmount;
			return this;
		}
		public Builder collectedAmount(BigDecimal collectedAmount){
			this.collectedAmount = collectedAmount;
			return this;
		}
		public Builder orders(Collection<Long> orders){
			this.orders = orders;
			return this;
		}
		
		public ShipmentSummary build(){
			return new ShipmentSummary(this);
		}
	}

}
