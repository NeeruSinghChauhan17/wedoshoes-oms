package com.wedoshoes.oms.model.cost;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class ItemSummary {

	private final Integer parentServiceId;
	private final Integer productId;
	private final List<Integer> services;
	private final Boolean expressDelivery;
	private final Integer productSizeId;
	private final Boolean packing;
	
	@SuppressWarnings("unused")
	private ItemSummary() {
		this(null, null, null, Boolean.FALSE, null, Boolean.FALSE);
	}
	
	public ItemSummary(final Integer parentServiceId, final Integer productId,
			final List<Integer> services, final Boolean expressDelivery,
			final Integer productSizeId, final Boolean packing) {
		this.parentServiceId = parentServiceId;
		this.productId = productId;
		this.services = services;
		this.expressDelivery = expressDelivery;
		this.productSizeId = productSizeId;
		this.packing = packing;
	}

	@JsonProperty("parent_service_id")
	public Integer getParentServiceId() {
		return parentServiceId;
	}

	@JsonProperty("product_id")
	public Integer getProductId() {
		return productId;
	}

	@JsonProperty("services")
	public List<Integer> getServices() {
		return services;
	}

	@JsonProperty("is_express_delivery")
	public Boolean isExpressDelivery() {
		return expressDelivery;
	}

	@JsonProperty("product_size_id")
	public Integer getProductSize() {
		return productSizeId;
	}

	@JsonProperty("is_packing")
	public Boolean getPacking() {
		return packing;
	}	
}
