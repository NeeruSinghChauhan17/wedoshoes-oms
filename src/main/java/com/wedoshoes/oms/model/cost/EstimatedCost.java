package com.wedoshoes.oms.model.cost;

import java.math.BigDecimal;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.wedoshoes.oms.model.order.GST;

/**
 *
 * @author Navrattan Yadav
 *
 */
@JsonInclude(Include.NON_NULL)
public class EstimatedCost {

	private final BigDecimal convenienceCharge;
	private final BigDecimal expressProcessingCharge;
	private final GST gst;
	private final BigDecimal packingCharge;
	private final Map<Integer, BigDecimal> servicesCost;
	private final BigDecimal totalServicesCost;
	
	@SuppressWarnings("unused")
	private EstimatedCost() {
		this(null, null, null, null, null, null);
	}
	
	public EstimatedCost( final BigDecimal convenienceCharge,
			final BigDecimal expressProcessingCharge, final GST gst,
			final BigDecimal packingCharge, final Map<Integer, BigDecimal> servicesCost,
			final BigDecimal totalServicesCost) {
		this.convenienceCharge = convenienceCharge;
		this.expressProcessingCharge = expressProcessingCharge;
		this.gst=gst;
		this.packingCharge = packingCharge;
		this.servicesCost = servicesCost;
		this.totalServicesCost = totalServicesCost;
	}

	@JsonProperty("convenience_charge")
	public BigDecimal getConvenienceCharge() {
		return convenienceCharge;
	}

	@JsonProperty("gst")
	public GST getGST() {
		return gst;
	}

	@JsonProperty("express_processing_charges")
	public BigDecimal getExpressProcessingCharge() {
		return expressProcessingCharge;
	}
	
	@JsonProperty("packing_charge")
	public BigDecimal getPackingCharge() {
		return packingCharge;
	}

	@JsonProperty("total_services_cost")
	public BigDecimal getTotalServicesCost() {
		return totalServicesCost;
	}

	@JsonProperty("services_cost")
	public Map<Integer, BigDecimal> getServicesCost() {
		return servicesCost;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((convenienceCharge == null) ? 0 : convenienceCharge
						.hashCode());
		result = prime
				* result
				+ ((expressProcessingCharge == null) ? 0
						: expressProcessingCharge.hashCode());
		result = prime * result
				+ ((packingCharge == null) ? 0 : packingCharge.hashCode());
		result = prime * result
				+ ((servicesCost == null) ? 0 : servicesCost.hashCode());
		result = prime * result
				+ ((gst == null) ? 0 : gst.hashCode());
		result = prime
				* result
				+ ((totalServicesCost == null) ? 0 : totalServicesCost
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (obj == null){
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		final EstimatedCost other = (EstimatedCost) obj;
		
		// Verify Convenience Charge
		if (convenienceCharge == null) {
			if (other.convenienceCharge != null){
				return false;
			}
		}else if (convenienceCharge.compareTo(other.convenienceCharge) != 0){
			return false;
		}
		
		// Verify Tax Percentage
//		if (taxPercentage == null) {
//			if (other.taxPercentage != null){
//				return false;
//			}
//		}else if (taxPercentage.compareTo(other.taxPercentage) != 0){
//			return false;
//		}
		
		// Verify Total Service Cost
		if (totalServicesCost == null) {
			if (other.totalServicesCost != null){
				return false;
			}
		}else if(totalServicesCost.compareTo(other.totalServicesCost) != 0){
			return false;
		}
		
		// Verify Service Cost
		if (servicesCost == null) {
			if (other.servicesCost != null){
				return false;
			}
		}else if(servicesCost.values().stream().mapToDouble(BigDecimal :: doubleValue).sum() != 
				other.servicesCost.values().stream().mapToDouble(BigDecimal :: doubleValue).sum()){
			return false;
		}
		
		/*// Verify Express Processing Charge
		if (expressProcessingCharge == null){
			if (other.expressProcessingCharge != null){
				return false;
			}
		}else if (expressProcessingCharge.compareTo(other.expressProcessingCharge) != 0){
			return false;
		}*/
		
		/*// Verify Packing Charge
		if(packingCharge == null){
			if (other.packingCharge != null){
				return false;
			}
		}else if (packingCharge.compareTo(other.packingCharge) != 0){
			return false;
		}*/
		
		return true;
	}
}
