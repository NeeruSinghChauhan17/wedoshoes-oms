package com.wedoshoes.oms.model;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Pin {

	private final Short id;
	private final String code;
	
	@SuppressWarnings("unused")
	private Pin() {
		this(null, null);
	}
	
	public Pin(final Short id, final String code) {
		this.id = id;
		this.code = code;
	}
	
	@JsonProperty("id")
	public Short getId() {
		return id;
	}
	
	@JsonProperty("code")
	public String getCode() {
		return code;
	}

	public static Pin createFromCode(final String code) {
		return new Pin(null,code);
	}
	
}
