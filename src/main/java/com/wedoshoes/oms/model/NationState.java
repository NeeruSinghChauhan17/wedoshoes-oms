package com.wedoshoes.oms.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class NationState {

	private final Byte id;
	private final String name;
	
	@SuppressWarnings("unused")
	private NationState() {
		this(null, null);
	}
	
	public NationState(final Byte id, final String name) {
		this.id = id;
		this.name = name;
	}

	@JsonProperty("id")
	public Byte getId() {
		return id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}
	
	public static NationState createFromId(final Byte id) {
		return new NationState(id, null);
	}
	
}
