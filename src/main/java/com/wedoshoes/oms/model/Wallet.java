package com.wedoshoes.oms.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Navrattan Yadav
 *
 */

@JsonInclude(value = Include.NON_NULL)
public class Wallet {

	private final Long id;
	private final Long userId;
	private final BigDecimal balance;
	private final Long createDate;
	private final Boolean active;

	@SuppressWarnings("unused")
	private Wallet() {
		this(null, null, null, null, null);
	}

	public Wallet(final Long userId, final Long id, final BigDecimal balance, 
			final Long createDate, final Boolean active) {
		this.userId = userId;
		this.id = id;
		this.balance = balance;
		this.createDate = createDate;
		this.active= active;
	}

	@JsonProperty("user_id")
	public Long getUserId() {
		return userId;
	}

	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	@JsonProperty("balance")
	public BigDecimal getBalance() {
		return balance;
	}

	@JsonProperty("create_date")
	public Long getCreateDate() {
		return createDate;
	}

	public Boolean isActive() {
		return active;
	}

	public static Wallet createWithUserId(final Long userId, final Wallet wallet) {
		return new Wallet(userId, wallet.getId(), wallet.getBalance(), 
				wallet.getCreateDate(), wallet.isActive());
	}
	
	public static Wallet createFromBalance(final BigDecimal balance) {
		return new Wallet(null, null, balance, null, null);
	}
}
