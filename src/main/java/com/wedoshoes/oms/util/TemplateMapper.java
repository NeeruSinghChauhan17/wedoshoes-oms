package com.wedoshoes.oms.util;

import java.util.Map;

import org.springframework.util.StringUtils;

public class TemplateMapper {

	public static String map(final String template, final Map<String, Object> params ) {
		if(StringUtils.isEmpty(template)) {
			return template;
		}
		String trimedTemplate = template.trim();
		if(params != null ) {
			for (Map.Entry<String, Object> e : params.entrySet()) {
				trimedTemplate = trimedTemplate.replace("{"+e.getKey()+"}", e.getValue().toString());
			}
		}
		return trimedTemplate;
	}
	
		
	/*private static Boolean isParamMissing(final String query) {
		if(query.matches("(.*)[{](.*.)[}](.*)")) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}*/
}
