package com.wedoshoes.oms.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.TimeZone;

/**
 * 
 * @author Navrattan Yadav
 *
 */
public class DateUtil {

	private static final long SEC_IN_A_DAY = 60 * 60 * 24;

	/**
	 * Return Epoch Date in Seconds without time. For Example : For date
	 * 28-12-2016 1:30:34 it return 28-12-2016 : 00:00:00 in epoch seconds.
	 * 
	 * @return
	 */
	public static Long getEpochDate() {
		return (Instant.now().getEpochSecond() / SEC_IN_A_DAY) * SEC_IN_A_DAY;
	}

	/*public static void main(String[] args) {
		System.out.println(convertToDBDateString(getEpochDate()));
	}*/

	public static String convertToDBDateString(final Long seconds) {

		String dateString = null;
		SimpleDateFormat sdfr = new SimpleDateFormat("yyyy-MM-dd");
		try {
			sdfr.setTimeZone(TimeZone.getTimeZone("IST"));
			dateString = sdfr.format(seconds * 1000);
		} catch (Exception ex) {
			System.out.println(ex);
		}
		return dateString;
	}
	
	public static String convertToDateString(final Long seconds) {

		String dateString = null;
		SimpleDateFormat sdfr = new SimpleDateFormat("dd/MMM/yyyy");
		try {
			sdfr.setTimeZone(TimeZone.getTimeZone("IST"));
			dateString = sdfr.format(seconds * 1000);
		} catch (Exception ex) {
			System.out.println(ex);
		}
		return dateString;
	}

	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}
}
