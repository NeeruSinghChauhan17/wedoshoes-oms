package com.wedoshoes.oms.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.google.common.collect.Lists;
import com.wedoshoes.oms.model.order.FeedbackType;

public class FeedbackCategories {

	public static Map<Integer, ArrayList<FeedbackType>> getFeedbackCategories() {
        Map<Integer, ArrayList<FeedbackType>> feedbackCategories = new HashMap<>();

        feedbackCategories.put(1, Lists.newArrayList(
                FeedbackType.SERVICE_QUALITY,
                FeedbackType.PRICES_COMMUNICATED_CHARGED,
                FeedbackType.CUSTOMER_SUPPORT, FeedbackType.PICKUP_DELIVERY,
                FeedbackType.PROFESSIONALISM, FeedbackType.OTHERS));

        feedbackCategories.put(2, Lists.newArrayList(
                FeedbackType.PRICES_COMMUNICATED_CHARGED,
                FeedbackType.CUSTOMER_SUPPORT, FeedbackType.SERVICE_QUALITY,
                FeedbackType.PICKUP_DELIVERY, FeedbackType.PROFESSIONALISM,
                FeedbackType.OTHERS));

        feedbackCategories.put(3, Lists.newArrayList(
                FeedbackType.SERVICE_QUALITY, FeedbackType.PROFESSIONALISM,
                FeedbackType.PRICES_COMMUNICATED_CHARGED,
                FeedbackType.PICKUP_DELIVERY, FeedbackType.CUSTOMER_SUPPORT,
                FeedbackType.OTHERS));

        feedbackCategories.put(4, Lists.newArrayList(
                FeedbackType.PROFESSIONALISM,
                FeedbackType.PRICES_COMMUNICATED_CHARGED,
                FeedbackType.SERVICE_QUALITY, FeedbackType.CUSTOMER_SUPPORT,
                FeedbackType.PICKUP_DELIVERY, FeedbackType.OTHERS));

        feedbackCategories.put(5, Lists.newArrayList(
                FeedbackType.SERVICE_QUALITY, FeedbackType.PICKUP_DELIVERY,
                FeedbackType.PRICES_COMMUNICATED_CHARGED,
                FeedbackType.CUSTOMER_SUPPORT, FeedbackType.PROFESSIONALISM,
                FeedbackType.OTHERS));
        return feedbackCategories;
    }

}
