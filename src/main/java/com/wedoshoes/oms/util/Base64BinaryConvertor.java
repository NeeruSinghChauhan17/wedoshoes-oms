package com.wedoshoes.oms.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.codec.binary.Base64;

public class Base64BinaryConvertor {

	
//	public static void main(String[] args) throws IOException {
//		
//		/*File file = new File("/home/craterzone/Desktop/sms.pdf");
//		//FileInputStream inputStream = new FileInputStream();
//		
//		//System.out.println(inputStream.toString());
//		FileInputStream fis = new FileInputStream(file);*/
//		
//	  //  String str = encodeFileToBase64Binary("/home/craterzone/Desktop/sms.pdf");
//	   // System.out.print(str);
//	      
//	    
//	    Long phone = 93192912830391141l;
//	    String cc = "91";
//	    
//	   Pattern p = Pattern.compile("^"+cc);
//	   
//	   
//	    System.out.println(p.matcher(String.valueOf(phone)).find());
//
//	}
//	
	
	public static String encodeFileToBase64Binary(File file)
			throws IOException {

		byte[] bytes = loadFile(file);
		byte[] encoded = Base64.encodeBase64(bytes);
		String encodedString = new String(encoded);
		return encodedString;
	}
	
	@SuppressWarnings("resource")
	private static byte[] loadFile(File file) throws IOException {
	    InputStream is = new FileInputStream(file);

	    long length = file.length();
	   
	    byte[] bytes = new byte[(int)length];
	    
	    int offset = 0;
	    int numRead = 0;
	    while (offset < bytes.length && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
	        offset += numRead;
	    }
	    if (offset < bytes.length) {
	        throw new IOException("Could not completely read file "+file.getName());
	    }
	    is.close();
	    return bytes;
	}
}
