package com.wedoshoes.oms.invoice;

public class Invoice {
	private final String invoiceId;
	private final Byte workshopId;
	private final Long orderId;
	private final Long itemId;
	private final Long createdDate;
	private final Long createdTime;

	@SuppressWarnings("unused")
	private Invoice() {
		this(null, null, null, null, null, null);
	}
	
	public Invoice(final String invoiceId, final Byte workshopId, final Long orderId, final Long itemId, final Long createdDate, final Long createdTime){
		this.invoiceId=invoiceId;
		this.workshopId=workshopId;
		this.orderId=orderId;
		this.itemId=itemId;
		this.createdDate=createdDate;
		this.createdTime=createdTime;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public Byte getWorkshopId() {
		return workshopId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public Long getItemId() {
		return itemId;
	}

	public Long getCreatedDate() {
		return createdDate;
	}

	public Long getCreatedTime() {
		return createdTime;
	}
}
