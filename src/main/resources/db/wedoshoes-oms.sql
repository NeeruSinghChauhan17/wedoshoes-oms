--  create the database 
-- drop database if exists wedoshoes_oms;
create database if not exists wedoshoes_oms;

--  use the database
use wedoshoes_oms;


--  create user and grant priviliges to wedoshoes user from locahost 
grant all privileges on wedoshoes_oms.* to 'wedoshoes'@'localhost' identified by 'wedoshoes';
 
--  create user and grant priviliges to wedoshoes user from remote host
grant all privileges on wedoshoes_oms.* to 'wedoshoes'@'%' identified by 'wedoshoes';
 
flush privileges;

--  --------------------------------------------------------------------------------------
--  ------------------------------- Order Tables -----------------------------------------
--  --------------------------------------------------------------------------------------

--  the table represents a user Order
CREATE TABLE IF NOT EXISTS `orders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `pick_up_date` INT UNSIGNED NOT NULL,
  `pick_up_timeslot` varchar(100) NOT NULL,
  `create_date` INT UNSIGNED NOT NULL,
  `create_time` MEDIUMINT UNSIGNED NOT NULL,
  `creater_user_id` bigint(20) NOT NULL,
  `referred_by_user_id` bigint(20) NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `phone` bigint(20) NOT NULL,
  `alternate_phone` bigint(20),
  `pick_up_street_address` varchar(500) NOT NULL,
  `pick_up_city` varchar(100) NOT NULL,
  `pick_up_state` varchar(100) NOT NULL,
  `pick_up_pin` char(11) NOT NULL,
  `pick_up_locality` int NOT NULL,
  `pick_up_landmark` varchar(200) NOT NULL,
  `delivery_street_address` varchar(500) NOT NULL,
  `delivery_city` varchar(100) NOT NULL,
  `delivery_state` varchar(100) NOT NULL,
  `delivery_pin` char(10) NOT NULL,
  `delivery_locality` int NOT NULL,
  `delivery_landmark` varchar(200) NOT NULL,
  `workshop_id` TINYINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_user_id_orders` (`user_id`),
  INDEX `idx_create_date_orders` (`create_date`),
  INDEX `idx_creater_user_id_orders` (`creater_user_id`),
  INDEX `idx_pick_up_date_orders` (`pick_up_date`),
  INDEX `idx_phone_orders` (`phone`),
  INDEX `idx_customer_name_orders` (`customer_name`),
  INDEX `idx_referred_by_user_id_orders` (`referred_by_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--  --------------------------------------------------------------------------------------
--  ------------------------------- Order Items Tables -----------------------------------
--  --------------------------------------------------------------------------------------

--  the table represents a Order
CREATE TABLE IF NOT EXISTS `orders_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) NOT NULL,
  `state` TINYINT UNSIGNED NOT NULL,
  `priority` TINYINT UNSIGNED NOT NULL,
  `tax_percentage` decimal(10,2) UNSIGNED NOT NULL,
  `create_date` INT UNSIGNED NOT NULL,
  `create_time` MEDIUMINT UNSIGNED NOT NULL,
  `delivery_date` INT UNSIGNED NOT NULL,
  `delivery_time` MEDIUMINT UNSIGNED NOT NULL,
  `est_delivery_date` INT UNSIGNED NOT NULL,
  `est_delivery_time` MEDIUMINT UNSIGNED NOT NULL,
  `delivery_timeslot` varchar(100) NOT NULL DEFAULT 1,
  `convenience_charge` decimal(10,2) UNSIGNED NOT NULL,
  `express_processing_charge` decimal(10,2) UNSIGNED NOT NULL,
  `packing_charge` decimal(10,2) UNSIGNED NOT NULL,
  `parent_service_id` SMALLINT UNSIGNED NOT NULL,
  `category_id` SMALLINT UNSIGNED NOT NULL,
  `product_id` INT UNSIGNED NOT NULL,
  `size` TINYINT UNSIGNED NOT NULL,
  `brand_id` SMALLINT UNSIGNED NOT NULL,
  `item_size_type` TINYINT UNSIGNED NOT NULL,
  `is_express_processing` TINYINT NOT NULL,
  `packing` TINYINT NOT NULL,
  `cost_credited` decimal(10,2) UNSIGNED NOT NULL,
  `bad_debts` decimal(10,2) UNSIGNED NOT NULL,
  `adjustment` decimal(10,2) NOT NULL,
  `adjustment_type` enum('FIXED','PERCENTAGE'),
  `rating` TINYINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_state_orders_items` (`state`),
  INDEX `idx_delivery_date_orders_items` (`delivery_date`),
  INDEX `idx_priority_orders_items` (`priority`),
  INDEX `idx_order_id_orders_items` (`order_id`),
  FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--  the table represents a Item Topup Service mapping
CREATE TABLE IF NOT EXISTS `order_item_services` (
  `item_id` bigint(20) NOT NULL,
  `service_id` SMALLINT UNSIGNED NOT NULL,
  `quantity` TINYINT UNSIGNED NOT NULL DEFAULT 1,
  `cost` decimal(10,2) UNSIGNED NOT NULL,
  PRIMARY KEY (`item_id`,`service_id`),
  FOREIGN KEY (`item_id`) REFERENCES `orders_items` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- the table represents Items Defects Services
CREATE TABLE IF NOT EXISTS `order_item_defects` (
  `item_id` bigint(20) NOT NULL,
  `defect_id` TINYINT UNSIGNED NOT NULL,
  PRIMARY KEY (`item_id`,`defect_id`),
  FOREIGN KEY (`item_id`) REFERENCES `orders_items` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--  the table represents a item state transition
CREATE TABLE IF NOT EXISTS `items_state_transition` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `create_date` INT UNSIGNED NOT NULL,
  `state` TINYINT UNSIGNED NOT NULL,
  `col_point_id` TINYINT UNSIGNED NOT NULL,
  `workshop_id` TINYINT UNSIGNED NOT NULL,
  `comment` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_item_id_items_state_transition` (`item_id`),
  INDEX `idx_col_point_id_items_state_transition` (`col_point_id`),
  INDEX `idx_workshop_id_items_state_transition` (`workshop_id`),
  FOREIGN KEY (`item_id`) REFERENCES `orders_items` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--  --------------------------------------------------------------------------------------
--  ------------------------------- Cart Tables ------------------------------------------
--  --------------------------------------------------------------------------------------

--  the table represents a Cart
CREATE TABLE IF NOT EXISTS `cart` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `create_date` INT UNSIGNED NOT NULL,
  `active` TINYINT UNSIGNED NOT NULL DEFAULT '1',
  `size` TINYINT UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `idx_user_id_cart` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--  the table represents a Item
CREATE TABLE IF NOT EXISTS `cart_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cart_id` bigint(20) NOT NULL,
  `is_express_processing` TINYINT NOT NULL,
  `packing` TINYINT NOT NULL,
  `parent_service_id` SMALLINT UNSIGNED NOT NULL,
  `category_id` SMALLINT UNSIGNED NOT NULL,
  `product_id` INT UNSIGNED NOT NULL,
  `size` TINYINT UNSIGNED NOT NULL,
  `brand_id` SMALLINT UNSIGNED NOT NULL,
  `item_size_type` TINYINT UNSIGNED NOT NULL,
  `create_date` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_cart_id_cart_items` (`cart_id`),
  FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--  the table represents a Item Topup Service mapping
CREATE TABLE IF NOT EXISTS `cart_item_services` (
  `item_id` bigint(20) NOT NULL,
  `service_id` SMALLINT UNSIGNED NOT NULL,
  `quantity` TINYINT UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`item_id`,`service_id`),
  FOREIGN KEY (`item_id`) REFERENCES `cart_items` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--  the table represents a Feedback on a Order
CREATE TABLE `feedbacks` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) NOT NULL DEFAULT 6,
  `email` varchar(255) NOT NULL,
  `name` varchar(100) NOT NULL,
  `phone` bigint(20) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `feedback` varchar(500) DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `create_date` bigint(20) NOT NULL,
  `rating` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_item_id_feedbacks` (`item_id`),
  KEY `idx_order_id_feedbacks` (`order_id`),
  KEY `idx_user_id_feedbacks` (`user_id`),
  CONSTRAINT `feedbacks_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `orders_items` (`id`),
  CONSTRAINT `feedbacks_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;

--  the table represents a images of an item
CREATE TABLE IF NOT EXISTS `images` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) NOT NULL,
  `hres_url` varchar(2083) NOT NULL,
  `lres_url` varchar(2083) NOT NULL,
  `type` TINYINT NOT NULL,
  `description` varchar(150) NOT NULL,
   PRIMARY KEY (`id`),
  INDEX `idx_item_id_images` (`item_id`),
  FOREIGN KEY (`item_id`) REFERENCES `orders_items` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--  --------------------------------------------------------------------------------------
--  ------------------------------- Order Payments Tables ------------------------------------------
--  --------------------------------------------------------------------------------------

-- ths table represent user advanced Payment
CREATE TABLE IF NOT EXISTS `orders_advance_payment` (
  `order_id` bigint(20) NOT NULL,
  `amount` decimal(10,2) UNSIGNED NOT NULL,
  PRIMARY KEY (`order_id`),
  FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ths table represent User Advanced Payment Transation
CREATE TABLE IF NOT EXISTS `orders_payment_txn` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `txn_id` varchar(100) NOT NULL,
  `mode` TINYINT UNSIGNED NOT NULL,
  `amount` decimal(10,2) UNSIGNED NOT NULL,
  `date` INT UNSIGNED NOT NULL,
  `type` TINYINT UNSIGNED NOT NULL,
  `description` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_type_orders_payment_txn` (`type`),
  INDEX `idx_order_id_orders_payment_txn` (`order_id`),
  INDEX `idx_item_id_orders_payment_txn` (`item_id`),
  FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--  the table represents a Order
CREATE TABLE IF NOT EXISTS `discounts` (
  `id` INT UNSIGNED NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `value` decimal(10,2) NOT NULL,
  `value_type` enum('FIXED','PERCENTAGE') NOT NULL DEFAULT 'PERCENTAGE',
  PRIMARY KEY (`item_id`,`id`),
  FOREIGN KEY (`item_id`) REFERENCES `orders_items` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--  --------------------------------------------------------------------------------------
--  ------------------------------- Coupons Tables ------------------------------------------
--  --------------------------------------------------------------------------------------

-- the table represents Coupon Source
CREATE TABLE IF NOT EXISTS `coupon_source` (
  `id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- the table represents a Coupon
CREATE TABLE IF NOT EXISTS `coupon` (
  `id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` char(12) NOT NULL,
  `description` varchar(100) NOT NULL,
  `value` decimal(10,2) NOT NULL,
  `value_type` enum('FIXED','PERCENTAGE') NOT NULL,
  `start_date` INT UNSIGNED NOT NULL,
  `end_date` INT UNSIGNED NOT NULL,
  `max_use` tinyint NOT NULL default 0,
  `min_order_amount` decimal(10,2) NOT NULL,
  `source_id` TINYINT UNSIGNED NOT NULL,
  `max_discount` decimal(10,2) NOT NULL,
  `used_count` tinyint NOT NULL default 0,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`source_id`) REFERENCES `coupon_source` (`id`),
  UNIQUE INDEX `unidx_code_coupon_source` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- the table represents Coupons_products
CREATE TABLE IF NOT EXISTS `coupons_products` (
  `coupon_id` SMALLINT UNSIGNED NOT NULL,
  `product_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`coupon_id`,`product_id`),
  FOREIGN KEY (`coupon_id`) REFERENCES `coupon` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- the table represents Coupons Services
CREATE TABLE IF NOT EXISTS `coupons_services` (
  `coupon_id` SMALLINT UNSIGNED NOT NULL,
  `service_id` SMALLINT UNSIGNED NOT NULL,
  PRIMARY KEY (`coupon_id`,`service_id`),
 FOREIGN KEY (`coupon_id`) REFERENCES `coupon` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- the table represents Coupons user type
CREATE TABLE IF NOT EXISTS `coupons_user_types` (
  `coupon_id` SMALLINT UNSIGNED NOT NULL,
  `user_type_id` TINYINT UNSIGNED NOT NULL,
  PRIMARY KEY (`coupon_id`,`user_type_id`),
  FOREIGN KEY (`coupon_id`) REFERENCES `coupon` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- the table represents Coupons categoies
CREATE TABLE IF NOT EXISTS `coupons_categories` (
  `coupon_id` SMALLINT UNSIGNED NOT NULL,
  `category_id` SMALLINT UNSIGNED NOT NULL,
  PRIMARY KEY (`coupon_id`,`category_id`),
  FOREIGN KEY (`coupon_id`) REFERENCES `coupon` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- the table represents invoices
CREATE TABLE IF NOT EXISTS `invoice` (
  `id` varchar(50) NOT NULL,
  `workshop_id` tinyint(10) NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `created_date` int(10) unsigned NOT NULL,
  `created_time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `unidx_workshop_id_order_id_item_id_invoice` (`workshop_id`,`order_id`,`item_id`),
  FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  FOREIGN KEY (`item_id`) REFERENCES `orders_items` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- the table represents shipments
CREATE TABLE IF NOT EXISTS `shipments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `shipment_date` int(10) unsigned NOT NULL,
  `last_updated_date` int(10) unsigned NOT NULL,
  `state` TINYINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_user_id_shipments` (`user_id`),
  INDEX `idx_shipment_date_shipments` (`shipment_date`),
  FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  FOREIGN KEY (`item_id`) REFERENCES `orders_items` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- the table repersents customers signature
 CREATE TABLE IF NOT EXISTS `customers_signature` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `shipment_id` BIGINT(20) NOT NULL,
  `user_id` BIGINT(20) NOT NULL,
  `item_id` BIGINT(20) NULL,
  `order_id` BIGINT(20) NOT NULL,
  `transition_state` TINYINT(4) NOT NULL,
  `customers_signature_url` VARCHAR(2083) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  FOREIGN KEY (`item_id`) REFERENCES `orders_items` (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

  
  -- 
  CREATE TABLE IF NOT EXISTS `shipments_summary` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `user_id` BIGINT(20) NOT NULL,
  `total_orders` SMALLINT NOT NULL DEFAULT 0,
  `completed_orders` SMALLINT NOT NULL DEFAULT 0,
  `total_locations` SMALLINT NOT NULL DEFAULT 0,
  `attempted_locations` SMALLINT NOT NULL DEFAULT 0,
  `total_items` SMALLINT NOT NULL DEFAULT 0,
  `completed_items` SMALLINT NOT NULL DEFAULT 0,
  `pickup_items` SMALLINT NOT NULL DEFAULT 0,
  `picked_items` SMALLINT NOT NULL DEFAULT 0,
  `delivery_items` SMALLINT NOT NULL DEFAULT 0,
  `delivered_items` SMALLINT NOT NULL DEFAULT 0,
  `express_delivery_items` SMALLINT NOT NULL DEFAULT 0,
  `completed_express_delivery_items` SMALLINT NOT NULL DEFAULT 0,
  `canceled` SMALLINT NOT NULL DEFAULT 0,
  `re_service` SMALLINT NOT NULL DEFAULT 0,
  `re_schedule_for_delivery` SMALLINT NOT NULL DEFAULT 0,
  `re_schedule_for_pickup` SMALLINT NOT NULL DEFAULT 0,
  `total_amount` SMALLINT NOT NULL DEFAULT 0,
  `collected_amount` SMALLINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
  )ENGINE=InnoDB DEFAULT CHARSET=utf8; 
  